:: For the model/status.model.yaml#BoxStatus.isInbound field which is a non-required boolean value,
:: it is necessary to generate the spring server without the property "nullable" and the C# client
:: with the property "nullable". The first part of this script takes care of copying the model and
:: api definitions and modifying the isInbound property to match the desired behavior.


del /s /q .\swagger
mkdir .\swagger

set process_management_openapi=C:\Users\hess\Documents\C1_Work\C10_Efeu\process-management-dynamic\openapi


xcopy %process_management_openapi%\swagger.yaml swagger\ /Y
xcopy %process_management_openapi%\model swagger\model\ /S /Y
xcopy %process_management_openapi%\api swagger\api\ /S /Y

@echo off
for /f "delims=" %%a in (swagger\model\status.model.yaml) do (
    echo %%a >> status.tmp
    if "%%a" == "    isInbound:" echo.      nullable: true >> status.tmp
)
@echo on
move /y status.tmp swagger\model\status.model.yaml


java ^
-jar ../../openapi-generator.jar generate ^
-g csharp-netcore ^
-o ./out ^
-i swagger/swagger.yaml ^
-c ./config.json

xcopy ".\out\src\EfeuPortal.Clients.ProcessManagement\Api" "..\..\..\EfeuPortal\Clients\ProcessManagement\Api\" /K /D /H /Y
xcopy ".\out\src\EfeuPortal.Clients.ProcessManagement\Client" "..\..\..\EfeuPortal\Clients\ProcessManagement\Client\" /K /D /H /Y
xcopy ".\out\src\EfeuPortal.Clients.ProcessManagement\Model" "..\..\..\EfeuPortal\Clients\ProcessManagement\Model\" /K /D /H /Y

del /s /q .\out\
del /s /q .\swagger\
rmdir /s /q .\out\
