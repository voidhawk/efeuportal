java ^
-jar C:/Users/hess/Documents/C4_Programmdateien/dev_swagger/openapi-generator.jar generate ^
-g csharp-netcore ^
-o ./out ^
-i C:\Users\hess\Documents\C1_Work\C10_Efeu\logging-service\openapi\swagger.yaml ^
-c ./config.json

xcopy ".\out\src\EfeuPortal.Clients.LoggingService\Api" "..\..\..\EfeuPortal\Clients\LoggingService\Api\" /K /D /H /Y
xcopy ".\out\src\EfeuPortal.Clients.LoggingService\Client" "..\..\..\EfeuPortal\Clients\LoggingService\Client\" /K /D /H /Y
xcopy ".\out\src\EfeuPortal.Clients.LoggingService\Model" "..\..\..\EfeuPortal\Clients\LoggingService\Model\" /K /D /H /Y

del /s /q .\out\
rmdir /s /q .\out\