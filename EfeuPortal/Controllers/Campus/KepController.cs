﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Kep;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    /// <summary>
    /// Die LogisticServiceProvider
    /// </summary>
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class KepController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(VehicleController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly KepService _kepService;
        private readonly SystemService _systemService;

        public KepController(IHttpContextAccessor httpContextAccessor, SystemService systemService, KepService kepService)
        {
            _kepService = kepService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
        }

        [HttpPost("data/kep")]
        public ActionResult<EfCaKepProviderResp> AddLogisticServiceProvider(EfCaModelCollector kepProviders)
        {
            EfCaKepProviderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"AddLogisticServiceProvider(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }

            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"AddLogisticServiceProvider(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Kep", "AddLogisticServiceProvider", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _kepService.AddLogisticServiceProviders(_internalUserInfos, kepProviders.ServiceProviders);

                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddLogisticServiceProvider({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"AddLogisticServiceProvider(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident => equal
        /// Name => Regex
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/kep/find")]
        public ActionResult<EfCaKepProviderResp> FindLogisticServiceProvidersByFinder([FromBody]EfCaServiceProvider finder)
        {
            EfCaKepProviderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindLogisticServiceProvidersByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindLogisticServiceProvidersByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Kep", "FindLogisticServiceProvidersByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _kepService.FindLogisticServiceProvidersByFinder(_internalUserInfos, finder);

                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindLogisticServiceProvidersByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindLogisticServiceProvidersByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        //[HttpGet("data/kep")]
        //public ActionResult<EfCaKepProviderResp> GetAllLogisticServiceProviders()
        //{
        //    EfCaKepProviderResp response = null;
        //    if (_internalUserInfos == null)
        //    {
        //        string errorMsg = $"GetAllLogisticServiceProviders(..), JWT valid but TenantId is missing";
        //        log.Error(errorMsg);

        //        response = new EfCaKepProviderResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    Stopwatch stopWatch = null;
        //    if (log.IsDebugEnabled)
        //    {
        //        stopWatch = new Stopwatch();
        //        stopWatch.Start();
        //        log.Debug($"GetAllKepProviders(..) called.");
        //    }
        //    string roleContainsFunctionMsg;
        //    if (_systemService.RoleContainsFunction(_internalUserInfos, "Kep", "GetAllLogisticServiceProviders", out roleContainsFunctionMsg) == false)
        //    {
        //        log.Error(roleContainsFunctionMsg);

        //        response = new EfCaKepProviderResp();
        //        response.Error = true;
        //        response.ErrorMsg = roleContainsFunctionMsg;
        //        return response;
        //    }

        //    try
        //    {
        //        response = _kepService.GetAllLogisticServiceProviders(_internalUserInfos);
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMsg = $"GetAllLogisticServiceProviders({_internalUserInfos.TenantId}), Exception: {ex.Message}";
        //        log.Error(errorMsg);
        //        response = new EfCaKepProviderResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    finally
        //    {
        //        if (log.IsDebugEnabled)
        //        {
        //            stopWatch.Stop();
        //            log.Debug($"GetAllLogisticServiceProviders(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
        //        }
        //    }
        //}

        [HttpPut("data/kep")]
        public ActionResult<EfCaKepProviderResp> PutLogisticServiceProvider(EfCaServiceProvider kepProvider)
        {
            EfCaKepProviderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutLogisticServiceProvider(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutLogisticServiceProvider(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Kep", "PutLogisticServiceProvider", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _kepService.PutLogisticServiceProvider(_internalUserInfos, kepProvider);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutLogisticServiceProvider({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutLogisticServiceProvider(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/kep/{ident}")]
        public ActionResult<EfCaKepProviderResp> DeleteLogisticServiceProviders(string ident)
        {
            EfCaKepProviderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteLogisticServiceProviders(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteLogisticServiceProviders(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Kep", "DeleteLogisticServiceProviders", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _kepService.DeleteLogisticServiceProviders(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteLogisticServiceProviders({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaKepProviderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteLogisticServiceProviders(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}