﻿using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(VehicleController));
        
        private readonly InternalUserInfos _internalUserInfos;
        private readonly VehicleService _vehicleService;
        private readonly SystemService _systemService;

        public VehicleController(IHttpContextAccessor httpContextAccessor, SystemService systemService, VehicleService vehicleService)
        {
            _vehicleService = vehicleService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicles"></param>
        /// <returns></returns>
        [HttpPost("data/vehicle")]
        public ActionResult<EfCaVehicleResp> PostAddVehicles([FromBody]EfCaModelCollector vehicles)
        {
            EfCaVehicleResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddVehicles(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddVehicles(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Vehicle", "PostAddVehicles", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _vehicleService.AddVehicles(_internalUserInfos, vehicles.Vehicles);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddVehicles(...), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddVehicles(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
            return response;
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident, Type => equal
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/vehicle/find")]
        public ActionResult<EfCaVehicleResp> FindVehiclesByFinder([FromBody]EfCaVehicle finder)
        {
            EfCaVehicleResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindVehiclesByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindVehiclesByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Vehicle", "FindVehiclesByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _vehicleService.FindVehiclesByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindVehiclesByFinder(...), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindVehiclesByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
            return response;
        }

        //[HttpGet("data/vehicle")]
        //public ActionResult<EfCaVehicleResp> GetAllVehicles()
        //{
        //    EfCaVehicleResp response = null;
        //    if (_internalUserInfos == null)
        //    {
        //        string errorMsg = $"GetAllVehicles(..), JWT valid but TenantId is missing";
        //        log.Error(errorMsg);

        //        response = new EfCaVehicleResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    Stopwatch stopWatch = null;
        //    if (log.IsDebugEnabled)
        //    {
        //        stopWatch = new Stopwatch();
        //        stopWatch.Start();
        //        log.Debug($"GetAllVehicles(..) called.");
        //    }
        //    string roleContainsFunctionMsg;
        //    if (_systemService.RoleContainsFunction(_internalUserInfos, "Vehicle", "GetAllVehicles", out roleContainsFunctionMsg) == false)
        //    {
        //        log.Error(roleContainsFunctionMsg);

        //        response = new EfCaVehicleResp();
        //        response.Error = true;
        //        response.ErrorMsg = roleContainsFunctionMsg;
        //        return response;
        //    }

        //    try
        //    {
        //        response = _vehicleService.GetAllVehicle(_internalUserInfos);
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMsg = $"GetAllVehicles({_internalUserInfos.TenantId}), Exception: {ex.Message}";
        //        log.Error(errorMsg);
        //        response = new EfCaVehicleResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    finally
        //    {
        //        if (log.IsDebugEnabled)
        //        {
        //            stopWatch.Stop();
        //            log.Debug($"GetAllVehicles(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
        //        }
        //    }
        //}

        [HttpPut("data/vehicle")]
        public ActionResult<EfCaVehicleResp> PutVehicle(EfCaVehicle vehicle)
        {
            EfCaVehicleResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutVehicle(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutVehicle(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Vehicle", "PutVehicle", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _vehicleService.PutVehicle(_internalUserInfos, vehicle);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutVehicle({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutVehicle(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/vehicle/{ident}")]
        public ActionResult<EfCaVehicleResp> DeleteVehicle(string ident)
        {
            EfCaVehicleResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteVehicle(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteVehicle(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Vehicle", "DeleteVehicle", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _vehicleService.DeleteVehicle(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteVehicle({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteVehicle(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}