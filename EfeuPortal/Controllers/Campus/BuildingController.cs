﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class BuildingController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(BuildingController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly BuildingService _buildingService;
        private readonly SystemService _systemService;

        public BuildingController(IHttpContextAccessor httpContextAccessor, SystemService systemService, BuildingService buildingService)
        {
            _buildingService = buildingService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
        }

        [HttpPost("data/place/building")]
        public ActionResult<EfCaBuildingResp> PostAddBuildings([FromBody] EfCaModelCollector buildings)
        {
            EfCaBuildingResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddBuildings(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddBuildings(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Building", "PostAddBuildings", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _buildingService.AddBuildings(_internalUserInfos, buildings.Buildings);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddBuildings({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddBuildings(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident, Type, AddressId => equal
        /// 
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/place/building/find")]
        public ActionResult<EfCaBuildingResp> FindBuildingsByFinder([FromBody]EfCaBuilding finder)
        {
            EfCaBuildingResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindBuildingsByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindBuildingsByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Building", "FindBuildingsByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _buildingService.FindBuildingsByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindBuildingsByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindBuildingsByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpPut("data/place/building")]
        public ActionResult<EfCaBuildingResp> PutBuilding(EfCaBuilding building)
        {
            EfCaBuildingResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutBuilding(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutBuilding(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Building", "PutBuilding", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _buildingService.PutBuilding(_internalUserInfos, building);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutBuilding({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutBuilding(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpPatch("data/place/building")]
        public ActionResult<EfCaBuildingResp> PatchBuilding(EfCaBuilding building)
        {
            EfCaBuildingResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PatchBuilding(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PatchBuilding(..) called.");
            }
            string roleContainsFunctionMsg;
            //JSt: ToDo: Die Rolle stimmt nicht!!!
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Building", "PutBuilding", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _buildingService.PatchBuilding(_internalUserInfos, building);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PatchBuilding({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PatchBuilding(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/place/building/{ident}")]
        public ActionResult<EfCaBuildingResp> DeleteBuilding(string ident)
        {
            EfCaBuildingResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteBuilding(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteBuilding(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Building", "DeleteBuilding", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _buildingService.DeleteBuilding(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteBuilding({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteBuilding(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}