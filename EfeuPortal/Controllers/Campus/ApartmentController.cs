﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Apartment;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class ApartmentController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ApartmentController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly ApartmentService _apartmentService;
        private readonly SystemService _systemService;

        public ApartmentController(IHttpContextAccessor httpContextAccessor, SystemService systemService, ApartmentService apartmentService)
        {
            _apartmentService = apartmentService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
        }

        /// <summary>
        /// Add a new apartment.
        /// 
        /// </summary>
        /// 
        /// <remarks>
        /// Sample request:
        ///
        ///     POST 
        ///     [
        ///       {
        ///         "ident": "JSt",
        ///       }
        ///     ]
        ///
        /// </remarks>
        /// <param name="apartments"></param>
        /// <returns></returns>
        [HttpPost("data/place/apartment")]
        public ActionResult<EfCaApartmentResp> PostAddApartments(EfCaModelCollector apartments)
        {
            EfCaApartmentResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddApartments(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddApartments(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Apartment", "PostAddApartments", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _apartmentService.AddApartments(_internalUserInfos, apartments.Apartments);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddApartments({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddApartments(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident, AddressId => equal
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/place/apartment/find")]
        public ActionResult<EfCaApartmentResp> FindApartmentsByFinder([FromBody]EfCaApartment finder)
        {
            EfCaApartmentResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindApartmentsByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindApartmentsByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Apartment", "FindApartmentsByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _apartmentService.FindApartmentsByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindApartmentsByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindApartmentsByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        //[HttpGet("data/place/apartment")]
        //public ActionResult<EfCaApartmentResp> GetAllApartments()
        //{
        //    EfCaApartmentResp response = null;
        //    if (_internalUserInfos == null)
        //    {
        //        string errorMsg = $"GetAllApartments(..), JWT valid but TenantId is missing";
        //        log.Error(errorMsg);

        //        response = new EfCaApartmentResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    Stopwatch stopWatch = null;
        //    if (log.IsDebugEnabled)
        //    {
        //        stopWatch = new Stopwatch();
        //        stopWatch.Start();
        //        log.Debug($"GetAllApartments(..) called.");
        //    }
        //    string roleContainsFunctionMsg;
        //    if (_systemService.RoleContainsFunction(_internalUserInfos, "Apartment", "GetAllApartments", out roleContainsFunctionMsg) == false)
        //    {
        //        log.Error(roleContainsFunctionMsg);

        //        response = new EfCaApartmentResp();
        //        response.Error = true;
        //        response.ErrorMsg = roleContainsFunctionMsg;
        //        return response;
        //    }

        //    try
        //    {
        //        response = _apartmentService.GetAllApartments(_internalUserInfos);
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMsg = $"GetAllApartments({_internalUserInfos.TenantId}), Exception: {ex.Message}";
        //        log.Error(errorMsg);
        //        response = new EfCaApartmentResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    finally
        //    {
        //        if (log.IsDebugEnabled)
        //        {
        //            stopWatch.Stop();
        //            log.Debug($"GetAllApartments(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
        //        }
        //    }
        //}

        [HttpPut("data/place/apartment")]
        public ActionResult<EfCaApartmentResp> PutApartment(EfCaApartment apartment)
        {
            EfCaApartmentResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutApartment(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutApartment(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Apartment", "PutApartment", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _apartmentService.PutApartment(_internalUserInfos, apartment);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutApartment({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutApartment(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Deletes an Address and removes all dependencies in relating objects
        /// 
        /// ToDo´s
        /// Remove all relations from Building(s)
        /// 
        /// </summary>
        /// <param name="ident"></param>
        /// <returns></returns>
        [HttpDelete("data/place/apartment/{ident}")]
        public ActionResult<EfCaApartmentResp> DeleteApartment(string ident)
        {
            EfCaApartmentResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteApartment(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteApartment(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Apartment", "DeleteApartment", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _apartmentService.DeleteApartment(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteApartment({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaApartmentResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteApartment(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}