﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class BoxMountingDeviceController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(VehicleController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly BoxMountingDeviceService _mountService;
        private readonly SystemService _systemService;
        private readonly BuildingService _buildingService;

        public BoxMountingDeviceController(IHttpContextAccessor httpContextAccessor, SystemService systemService, BoxMountingDeviceService mountService, BuildingService buildingService)
        {
            _mountService = mountService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _buildingService = buildingService;
        }

        #region BoxMountingDevice
        /// <summary>
        /// Add a new "Bock(s)"
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     [
        ///     {
        ///       "ident": "Test-BoxMount-001",
        ///        "mountCapacity": 1,
        ///       "type": "private",
        ///       "x": 49.4,
        ///       "y": 8.5
        ///     }
        ///     ]
        ///
        /// </remarks>
        /// <param name="boxMountingDevices"></param>
        /// <returns></returns>
        [HttpPost("data/mount")]
        public ActionResult<EfCaBoxMountingDeviceResp> PostAddBoxMountingDevices([FromBody] EfCaModelCollector boxMountingDevices)
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddBoxMountingDevices(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddBoxMountingDevices(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "PostAddBoxMountingDevices", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _mountService.AddBoxMountingDevices(_internalUserInfos, boxMountingDevices.BoxMountingDevices);
                /**
                 * Spezial Funktionalität um über die Stammdaten UI ein Dock anlegen und dem Building zuzuordnen .
                 * Voraussetzungen jeweils GENAU 1 BoxMountingDevice und 1 Building und das Anlegen der BoxMountingDevice war erfolgreich
                 */
                if(response.Error == false && response.BoxMountingDevices.Count == 1 && boxMountingDevices.Buildings != null && boxMountingDevices.Buildings.Count == 1)
                {
                    EfCaBuildingResp efCaBuildingResp = _buildingService.PutModifyBuildingReferences(_internalUserInfos, "ADD", "BoxMountingDevice", response.BoxMountingDevices[0].Ident, boxMountingDevices.Buildings[0]);
                    if(efCaBuildingResp.Error == true)
                    {
                        response.DetailInfos.Add(new DetailInfo() { Ident = response.BoxMountingDevices[0].Ident , Detail = "Dock not assigned to a building"});
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddBoxMountingDevices({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddBoxMountingDevices(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident, Type, MountCapacity => equal
        /// 
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/mount/find")]
        public ActionResult<EfCaBoxMountingDeviceResp> FindBoxMountingDevicesByFinder([FromBody]EfCaBoxMountingDevice finder)
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindBoxMountingDevicesByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindBoxMountingDevicesByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "FindBoxMountingDevicesByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _mountService.FindBoxMountingDevicesByFinder(_internalUserInfos, finder);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindBoxMountingDevicesByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindBoxMountingDevicesByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpGet("data/mount/{ident}")]
        public ActionResult<EfCaBoxMountingDeviceResp> GetBoxMountingDeviceDetails(string ident)
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"GetBoxMountingDeviceDetails(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"GetBoxMountingDeviceDetails(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "GetBoxMountingDeviceDetails", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _mountService.GetBoxMountingDeviceDetails(_internalUserInfos, ident);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"GetBoxMountingDeviceDetails({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"GetBoxMountingDeviceDetails(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpPut("data/mount")]
        public ActionResult<EfCaBoxMountingDeviceResp> PutBoxMountingDevice(EfCaBoxMountingDevice boxMountingDevice)
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutBoxMountingDevice(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutBoxMountingDevice(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "PutBoxMountingDevice", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _mountService.PutBoxMountingDevice(_internalUserInfos, boxMountingDevice);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutBoxMountingDevice({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutBoxMountingDevice(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Deletes a BoxMountingDevice (Übergabebock)
        /// 
        /// ToDo´s
        /// Remove all relations from Building(s)
        /// Remove all relations from Apartment(s)
        /// Remove all relations from Contact(s)
        /// 
        /// </summary>
        /// <param name="ident"></param>
        /// <returns></returns>
        [HttpDelete("data/mount/{ident}")]
        public ActionResult<EfCaBoxMountingDeviceResp> DeleteBoxMountingDevice(string ident)
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteBoxMountingDevice(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteBoxMountingDevice(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "DeleteBoxMountingDevice", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _mountService.DeleteBoxMountingDevice(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteBoxMountingDevice({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteBoxMountingDevice(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        #endregion

        #region Slot management
        /// <summary>
        /// Creates reservations for BoxMountingDevices, depending on free timeslots.
        /// <para>The list  "SlotReservationRequests" contains the slot reservations.</para>
        /// Stored in DB
        /// <para>Right = "BoxMountingDevice", function = "PostBoxMountingDeviceSlotReservations"</para>
        /// </summary>
        /// <param name="slotReservationRequests"></param>
        /// <returns>A specified Bock and the reserved timeslots</returns>
        [HttpPost("data/mount/slot")]
        public ActionResult<EfCaBoxMountingDeviceResp> PostBoxMountingDeviceSlotReservations([FromBody] EfCaModelCollector slotReservationRequests)
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostBoxMountingDeviceSlotReservations(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostBoxMountingDeviceSlotReservations(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "PostBoxMountingDeviceSlotReservations", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _mountService.PostBoxMountingDeviceSlotReservations(_internalUserInfos, slotReservationRequests.SlotReservationRequests);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostBoxMountingDeviceSlotReservations({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostBoxMountingDeviceSlotReservations(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Returns all reservations for specified BoxMountingDevices in a calendar week.
        /// 
        /// In EfCaModelCollector only the parameter "slotReservations" is used.
        /// <para>Role = "BoxMountingDevice", function = "FindBoxMountingDeviceSlotReservations"</para>
        /// </summary>
        /// <param name="date"></param>
        /// <param name="slotReservations"></param>
        /// <returns>Complete calendar week</returns>
        [HttpPost("data/mount/slotreservations")]
        public ActionResult<EfCaBoxMountingDeviceResp> FindBoxMountingDeviceSlotReservations(DateTimeOffset date, [FromBody]EfCaModelCollector slotReservations)
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindBoxMountingDeviceSlotReservations(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindBoxMountingDeviceSlotReservations(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "FindBoxMountingDeviceSlotReservations", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            if (slotReservations == null || slotReservations.SlotReservations == null || slotReservations.SlotReservations.Count <= 0)
            {
                response.Error = true;
                response.ErrorMsg = "FindBoxMountingDeviceSlotReservations(..), parameter error SlotReservations not valid";
                return response;
            }

            try
            {
                //Implementierung aus UseCase(3)
                response = _mountService.GetBoxMountingDeviceReservations(_internalUserInfos, slotReservations.SlotReservations, date);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindBoxMountingDeviceSlotReservations({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindBoxMountingDeviceSlotReservations(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
            return response;
        }

        /// <summary>
        /// Query all exisiting slots. The scheduled entries aren´t included.
        /// </summary>
        /// <returns>The basic data of the existing slots</returns>
        [HttpGet("data/mount/slot")]
        public ActionResult<EfCaBoxMountingDeviceResp> FindBoxMountingDeviceSlots()
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindBoxMountingDeviceSlots(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindBoxMountingDeviceSlots(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "FindBoxMountingDeviceSlots", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _mountService.ProcessBoxMountingDeviceSlotRequests(_internalUserInfos, null);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindBoxMountingDeviceSlots({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindBoxMountingDeviceSlots(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
            return response;
        }

        /// <summary>
        /// Deletes all specified bock reservations 
        /// </summary>
        /// <returns></returns>
        [HttpPost("data/mount/slots/delete")]
        public ActionResult<EfCaBoxMountingDeviceResp> DeleteBoxMountingDeviceReservations([FromBody] EfCaModelCollector scheduledBoxMountingSlots)
        {
            EfCaBoxMountingDeviceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteBoxMountingDeviceReservations(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            if(scheduledBoxMountingSlots == null || scheduledBoxMountingSlots.ScheduledBoxMountSlots == null || scheduledBoxMountingSlots.ScheduledBoxMountSlots.Count == 0)
            {
                string errorMsg = $"DeleteBoxMountingDeviceReservations(..), parameter error";
                log.Error(errorMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteBoxMountingDeviceReservations(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "BoxMountingDevice", "DeleteBoxMountingDeviceReservations", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _mountService.DeleteBoxMountingDeviceSlotRequests(_internalUserInfos, scheduledBoxMountingSlots);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteBoxMountingDeviceReservations({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaBoxMountingDeviceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteBoxMountingDeviceReservations(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        #endregion
    }
}