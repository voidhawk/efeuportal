﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using static EfeuPortal.Clients.ProcessManagement.Model.OrderStatus;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(OrderController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly OrderService _orderService;
        private readonly SystemService _systemService;

        public OrderController(IHttpContextAccessor httpContextAccessor,  SystemService systemService, OrderService orderService)
        {
            _orderService = orderService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
        }

        #region Order
        [HttpPost("data/order")]
        public ActionResult<EfCaOrderResp> PostAddOrders(EfCaModelCollector orders)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddOrders(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddOrders(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "PostAddOrders", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                if(orders.Orders != null)
                {
                    response = _orderService.AddOrders(_internalUserInfos, orders.Orders);
                }
                else if (orders.Idents != null)
                {
                    response = _orderService.PostCreateDependentOrders(_internalUserInfos, orders.Idents);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddOrders({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddOrders(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpPost("data/order/find")]
        public ActionResult<EfCaOrderResp> FindOrdersByFinder([FromBody]EfCaOrder finder)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindOrdersByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindOrdersByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "FindOrdersByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _orderService.FindOrdersByFinder(_internalUserInfos, finder, null);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindOrdersByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindOrdersByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpPut("data/order")]
        public ActionResult<EfCaOrderResp> PutOrder(EfCaOrder address)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutOrder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutOrder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "PutOrder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _orderService.PutOrder(_internalUserInfos, address);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutOrder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutOrder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/order")]
        public ActionResult<EfCaOrderResp> DeleteOrders(string ident)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteOrders(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteOrders(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "DeleteOrders", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> idents = new List<string>();
                idents.Add(ident);
                response = _orderService.DeleteOrders(_internalUserInfos, idents);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteOrders({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteOrders(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        #endregion

        #region Order Bitmaps
        [HttpPost("data/order/bmp")]
        public ActionResult<EfCaOrderResp> PostAddOrderImages([FromBody] EfCaModelCollector images)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddOrderImages(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }

            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddOrderImages(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "PostAddOrderImages", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _orderService.AddOrderImages(_internalUserInfos, images.ImageDocumentation);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddOrderImages({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddOrderImages(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Uses the "Idents" and the "ImageDocumentation" object as finder(s) but not all parameters are supported. Refer to the more detailled description below.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Supported finders
        ///     "Idents" as array of "EfCaOrder.Ident"
        ///     
        ///     "EfCaImageDocumentation" {
        ///        "Ident" => "equal",
        ///        "Order" => "equal",
        ///        "Description" => "equal"
        ///     }
        ///
        /// </remarks>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/order/bmp/find")]
        public ActionResult<EfCaOrderResp> GetOrderImages([FromBody] EfCaModelCollector finder)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"GetOrderImages(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"GetOrderImages(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "GetOrderImages", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _orderService.FindOrderImageDocumentations(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"GetOrderImages({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"GetOrderImages(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// <para>Wenn die Liste der EfCaImageDocumentation gesetzt ist, werden diese in der Collection gelöscht und
        /// in den entsprechenden EfCaOrder(s) die Referenzen entfernt.</para>
        /// </summary>
        /// <param name="deleteBmpsFromOrders"></param>
        /// <returns></returns>
        [HttpDelete("data/order/bmp")]
        public ActionResult<EfCaOrderResp> DeleteOrderImages([FromBody] EfCaModelCollector deleteBmpsFromOrders)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteOrderImages(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteOrderImages(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "DeleteOrderImages", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _orderService.DeleteOrderImages(_internalUserInfos, deleteBmpsFromOrders);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteOrderImages({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteOrderImages(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        #endregion

        #region Plannig system
        [HttpPost("data/ro/order")]
        public ActionResult<EfCaOrderResp> PostAddWebApiOrder(EfCaModelCollector orders)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddWebApiOrder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            //order.Version = null;
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddWebApiOrder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "PostAddWebApiOrder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _orderService.ProcessPlanningOrders(_internalUserInfos, orders.Idents, ImportActionType.NEW);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddWebApiOrder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddOrder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpPost("data/ro/delorder")]
        public ActionResult<EfCaOrderResp> DeleteWebApiOrders(EfCaModelCollector orders)
        {
            EfCaOrderResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteWebApiOrders(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            //order.Version = null;
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteWebApiOrders(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Order", "DeleteWebApiOrders", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _orderService.DeletePlanningOrders(_internalUserInfos, orders.Idents);
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteWebApiOrders({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteWebApiOrders(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }
        #endregion
    }
}