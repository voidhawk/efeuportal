﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.SyncMeetingPoint;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class SyncMeetingPointController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SyncMeetingPointController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly SyncMeetingPointService _syncMeetingPointService;
        private readonly SystemService _systemService;
        private readonly BuildingService _buildingService;

        public SyncMeetingPointController(IHttpContextAccessor httpContextAccessor, SystemService systemService, SyncMeetingPointService syncMeetingPointService,
            BuildingService buildingService)
        {
            _syncMeetingPointService = syncMeetingPointService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _buildingService = buildingService;
        }

        [HttpPost("data/syncpoint")]
        public ActionResult<EfCaSyncMeetingPointResp> PostAddSyncMeetingPoints([FromBody]EfCaModelCollector syncMeetingPoints)
        {
            EfCaSyncMeetingPointResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddSyncMeetingPoints(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddSyncMeetingPoints(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "SyncMeetingPoint", "PostAddSyncMeetingPoints", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _syncMeetingPointService.AddSyncMeetingPoints(_internalUserInfos, syncMeetingPoints.SyncMeetingPoints);
                if (response.Error == false && response.SyncMeetingPoints.Count == 1 && syncMeetingPoints.Buildings != null && syncMeetingPoints.Buildings.Count == 1)
                {
                    EfCaBuildingResp efCaBuildingResp = _buildingService.PutModifyBuildingReferences(_internalUserInfos, "ADD", "SyncMeetingPoint", response.SyncMeetingPoints[0].Ident, syncMeetingPoints.Buildings[0]);
                    if (efCaBuildingResp.Error == true)
                    {
                        response.DetailInfos.Add(new DetailInfo() { Ident = response.SyncMeetingPoints[0].Ident, Detail = "SyncMeetingPoint not assigned to a building" });
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddSyncMeetingPoints({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddSyncMeetingPoints(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident => equal
        /// Info => Regex
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/syncpoint/find")]
        public ActionResult<EfCaSyncMeetingPointResp> FindSyncMeetingPointsByFinder([FromBody]EfCaSyncMeetingPoint finder)
        {
            EfCaSyncMeetingPointResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindSyncMeetingPointsByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindSyncMeetingPointsByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "SyncMeetingPoint", "FindSyncMeetingPointsByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _syncMeetingPointService.FindSyncMeetingPointsByFinder(_internalUserInfos, finder);

                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindSyncMeetingPointsByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindSyncMeetingPointsByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpPut("data/syncpoint")]
        public ActionResult<EfCaSyncMeetingPointResp> PutSyncMeetingPoint(EfCaSyncMeetingPoint syncMeetingPoint)
        {
            EfCaSyncMeetingPointResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutSyncMeetingPoint(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutSyncMeetingPoint(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "SyncMeetingPoint", "PutSyncMeetingPoint", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _syncMeetingPointService.PutSyncMeetingPoint(_internalUserInfos, syncMeetingPoint);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutSyncMeetingPoint({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutSyncMeetingPoint(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/syncpoint/{ident}")]
        public ActionResult<EfCaSyncMeetingPointResp> DeleteSyncMeetingPoint(string ident)
        {
            EfCaSyncMeetingPointResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteSyncMeetingPoint(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteSyncMeetingPoint(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "SyncMeetingPoint", "DeleteSyncMeetingPoint", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _syncMeetingPointService.DeleteSyncMeetingPoints(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteSyncMeetingPoint({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaSyncMeetingPointResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteSyncMeetingPoint(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}