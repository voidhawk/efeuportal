﻿using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Warehouse;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class WarehouseController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(WarehouseController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly WarehouseService _warehouseService;
        private readonly SystemService _systemService;

        public WarehouseController(IHttpContextAccessor httpContextAccessor, SystemService systemService, WarehouseService warehouseService)
        {
            _systemService = systemService;
            _warehouseService = warehouseService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
        }

        [HttpPost("data/warehouse/places")]
        public ActionResult<EfCaWarehousePlaceResp> PostAddWarehousePlaces([FromBody] EfCaModelCollector warehousePlaces)
        {
            EfCaWarehousePlaceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddWarehousePlaces(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddWarehousePlaces(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Warehouse", "PostAddWarehousePlaces", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _warehouseService.AddWarehousePlaces(_internalUserInfos, warehousePlaces.WarehousePlaces);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddWarehousePlaces({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddWarehousePlaces(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Uses the default EfCaWarehousePlace object as finder but not all parameters are supported. Refer to the more detailled description below.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Supported finders
        ///     {
        ///        "Ident" => "equal"
        ///     }
        ///
        /// </remarks>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/warehouse/places/find")]
        public ActionResult<EfCaWarehousePlaceResp> FindWarehousePlacesByFinder([FromBody] EfCaWarehousePlace finder)
        {
            EfCaWarehousePlaceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindWarehousePlacesByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindWarehousePlacesByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Warehouse", "FindWarehousePlacesByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _warehouseService.FindWarehousePlacesByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindWarehousePlacesByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindWarehousePlacesByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpPut("data/warehouse/places")]
        public ActionResult<EfCaWarehousePlaceResp> PutWarehousePlace(EfCaWarehousePlace warehousePlace)
        {
            EfCaWarehousePlaceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutWarehousePlace(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutWarehousePlace(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Warehouse", "PutWarehousePlace", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                //response = _addressService.PutAddress(_internalUserInfos, warehousePlace);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutWarehousePlace({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutWarehousePlace(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/warehouse/places/{ident}")]
        public ActionResult<EfCaWarehousePlaceResp> DeleteWarehousePlace(string ident)
        {
            EfCaWarehousePlaceResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteWarehousePlace(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteWarehousePlace(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Warehouse", "DeleteWarehousePlace", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _warehouseService.DeleteWarehousePlace(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteWarehousePlace({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaWarehousePlaceResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteWarehousePlace(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}
