﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus;
using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class TransportBoxController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(TransportBoxController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly TransportBoxService _transportBoxService;
        private readonly SystemService _systemService;

        public TransportBoxController(IHttpContextAccessor httpContextAccessor, SystemService systemService, TransportBoxService transportBoxService)
        {
            _transportBoxService = transportBoxService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
        }

        #region TransportBox Stammdaten
        /// <summary>
        /// Add a new "TransportBox"
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///       "ident": "TransportBox_xx",
        ///       "lenght": 10,
        ///       "width": 20,
        ///       "height": 30,
        ///       "weight": 40
        ///     }
        ///
        /// </remarks>
        /// <param name="transportBoxes"></param>
        /// <returns></returns>
        [HttpPost("data/box")]
        public ActionResult<EfCaTransportBoxResp> PostAddTransportBoxes([FromBody]EfCaModelCollector transportBoxes)
        {
            EfCaTransportBoxResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddTransportBoxes(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddTransportBoxes(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "TransportBox", "PostAddTransportBoxes", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _transportBoxService.AddTransportBoxes(_internalUserInfos, transportBoxes.TransportBoxes);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddTransportBoxes({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddTransportBoxes(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident, Type => equal
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/box/find")]
        public ActionResult<EfCaTransportBoxResp> FindTransportBoxesByFinder([FromBody]EfCaTransportBox finder)
        {
            EfCaTransportBoxResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindTransportBoxesByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindTransportBoxesByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "TransportBox", "FindTransportBoxesByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _transportBoxService.FindTransportBoxesByFinder(_internalUserInfos, finder);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindTransportBoxesByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindTransportBoxesByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        //[HttpGet("data/box")]
        //public ActionResult<EfCaTransportBoxResp> GetAllTransportBoxes()
        //{
        //    EfCaTransportBoxResp response = null;
        //    if (_internalUserInfos == null)
        //    {
        //        string errorMsg = $"GetAllTransportBoxes(..), JWT valid but TenantId is missing";
        //        log.Error(errorMsg);

        //        response = new EfCaTransportBoxResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    Stopwatch stopWatch = null;
        //    if (log.IsDebugEnabled)
        //    {
        //        stopWatch = new Stopwatch();
        //        stopWatch.Start();
        //        log.Debug($"GetAllTransportBoxes(..) called.");
        //    }
        //    string roleContainsFunctionMsg;
        //    if (_systemService.RoleContainsFunction(_internalUserInfos, "TransportBox", "GetAllTransportBoxes", out roleContainsFunctionMsg) == false)
        //    {
        //        log.Error(roleContainsFunctionMsg);

        //        response = new EfCaTransportBoxResp();
        //        response.Error = true;
        //        response.ErrorMsg = roleContainsFunctionMsg;
        //        return response;
        //    }

        //    try
        //    {
        //        response = _transportBoxService.GetAllTransportBoxes(_internalUserInfos);
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMsg = $"GetAllTransportBoxes({_internalUserInfos.TenantId}), Exception: {ex.Message}";
        //        log.Error(errorMsg);
        //        response = new EfCaTransportBoxResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    finally
        //    {
        //        if (log.IsDebugEnabled)
        //        {
        //            stopWatch.Stop();
        //            log.Debug($"GetAllTransportBoxes(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
        //        }
        //    }
        //}

        [HttpPut("data/box")]
        public ActionResult<EfCaTransportBoxResp> PutTransportBox(EfCaTransportBox transportBox)
        {
            EfCaTransportBoxResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutTransportBox(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutTransportBox(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "TransportBox", "PutTransportBox", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _transportBoxService.PutTransportBox(_internalUserInfos, transportBox);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutTransportBox({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutTransportBox(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/box/{ident}")]
        public ActionResult<EfCaTransportBoxResp> DeleteTransportBox(string ident)
        {
            EfCaTransportBoxResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteTransportBox(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteTransportBox(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "TransportBox", "DeleteTransportBox", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _transportBoxService.DeleteTransportBoxes(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteTransportBox({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteTransportBox(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        #endregion

        #region TransportBoxType Stammdaten
        /// <summary>
        /// Add a new "TransportBoxType"
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///       "transportBoxTypes" : [
        ///       {
        ///           "Type": "SMALL_PACKAGE_BOX",
        ///           "Description": "Small default box",
        ///           "lenght": 550,
        ///           "width": 400,
        ///           "height": 350,
        ///           "weight": 40000
        ///         }
        ///       ]
        ///     }
        ///
        /// </remarks>
        /// <param name="transportBoxTypes"></param>
        /// <returns></returns>
        [HttpPost("data/boxtype")]
        public ActionResult<EfCaTransportBoxResp> PostAddTransportBoxTypes([FromBody] EfCaModelCollector transportBoxTypes)
        {
            EfCaTransportBoxResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddTransportBoxTypes(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddTransportBoxTypes(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "TransportBox", "PostAddTransportBoxTypes", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }
            if(transportBoxTypes == null || transportBoxTypes.TransportBoxTypes == null || transportBoxTypes.TransportBoxTypes.Count <= 0)
            {
                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = "The parameter TransportBoxTypes is not valid";

                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddTransportBoxTypes(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
                return response;
            }

            try
            {
                response = _transportBoxService.AddTransportBoxTypes(_internalUserInfos, transportBoxTypes.TransportBoxTypes);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddTransportBoxes({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddTransportBoxTypes(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpPost("data/boxtype/find")]
        public ActionResult<EfCaTransportBoxResp> FindTransportBoxTypesByFinder([FromBody] EfCaTransportBoxType finder)
        {
            EfCaTransportBoxResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindTransportBoxTypesByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindTransportBoxTypesByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "TransportBox", "FindTransportBoxTypesByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _transportBoxService.FindTransportBoxTypesByFinder(_internalUserInfos, finder);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindTransportBoxTypesByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindTransportBoxTypesByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/boxtype/{ident}")]
        public ActionResult<EfCaTransportBoxResp> DeleteTransportBoxType(string ident)
        {
            EfCaTransportBoxResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteTransportBoxType(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteTransportBoxType(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "TransportBox", "DeleteTransportBoxType", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                    return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _transportBoxService.DeleteTransportBoxTypes(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteTransportBoxType({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTransportBoxResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteTransportBoxType(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        #endregion

        #region TransportBox schedules https://lsogit.fzi.de/efeu/efeuportal/-/issues/44
        // Auf Wunsch vom FZI entfernt
        #endregion
    }
}