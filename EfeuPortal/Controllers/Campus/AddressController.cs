﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AddressController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly AddressService _addressService;
        private readonly SystemService _systemService;

        public AddressController(IHttpContextAccessor httpContextAccessor, SystemService systemService, AddressService addressService)
        {
            _systemService = systemService;
            _addressService = addressService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addresses"></param>
        /// <returns></returns>
        [HttpPost("data/place/address")]
        public ActionResult<EfCaAddressResp> PostAddAddresses([FromBody] EfCaModelCollector addresses)
        {
            EfCaAddressResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddAddresses(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddAddresses(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Address", "PostAddAddresses", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                if (addresses.Addresses.Count == 1 && addresses.Buildings != null && addresses.Buildings.Count > 0)
                {
                    response = _addressService.AddAddressData(_internalUserInfos, addresses);
                }
                else
                {
                    response = _addressService.AddAddresses(_internalUserInfos, addresses.Addresses);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddAddresses({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddAddresses(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Uses the default EfCaAddress object as finder but not all parameters are supported. Refer to the more detailled description below.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Supported finders
        ///     {
        ///        "Ident" => "equal",
        ///        "ZipCode" => "equal",
        ///        "Label" => "regex",
        ///        "City" => "regex",
        ///        "Street" => "regex"
        ///     }
        ///
        /// </remarks>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/place/address/find")]
        public ActionResult<EfCaAddressResp> FindAddressesByFinder([FromBody]EfCaAddress finder)
        {
            EfCaAddressResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindAddressesByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindAddressesByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Address", "FindAddressesByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _addressService.FindAddressesByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindAddressesByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindAddressesByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpPut("data/place/address")]
        public ActionResult<EfCaAddressResp> PutAddress(EfCaAddress address)
        {
            EfCaAddressResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutAddress(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutAddress(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Address", "PutAddress", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _addressService.PutAddress(_internalUserInfos, address);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutAddress({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutAddress(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/place/address/{ident}")]
        public ActionResult<EfCaAddressResp> DeleteAddress(string ident)
        {
            EfCaAddressResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteAddress(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteAddress(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Address", "DeleteAddress", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _addressService.DeleteAddresses(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteAddress({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteAddress(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}