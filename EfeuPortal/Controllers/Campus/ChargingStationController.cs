﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.ChargingStation;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    /// <summary>
    /// Controller fuer die Ladestationen der Roboter Fahrzeuge
    /// </summary>
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class ChargingStationController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(VehicleController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly ChargingStationService _chargingStationService;
        private readonly SystemService _systemService;

        public ChargingStationController(IHttpContextAccessor httpContextAccessor, SystemService systemService, ChargingStationService chargingStationService)
        {
            _chargingStationService = chargingStationService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
        }

        [HttpPost("data/charging")]
        public ActionResult<EfCaChargingStationResp> PostAddChargingStations([FromBody] EfCaModelCollector chargingStations)
        {
            EfCaChargingStationResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddChargingStations(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddChargingStations(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "ChargingStation", "PostAddChargingStations", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _chargingStationService.AddChargingStations(_internalUserInfos, chargingStations.ChargingStations);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddChargingStations({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddChargingStations(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident, Type => equal
        /// Info => Regex
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/charging/find")]
        public ActionResult<EfCaChargingStationResp> FindChargingStationsByFinder([FromBody]EfCaChargingStation finder)
        {
            EfCaChargingStationResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindChargingStationsByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindChargingStationsByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "ChargingStation", "FindChargingStationsByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _chargingStationService.FindChargingStationsByFinder(_internalUserInfos, finder);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindChargingStationsByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindChargingStationsByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        //[HttpGet("data/charging")]
        //public ActionResult<EfCaChargingStationResp> GetAllChargingStations()
        //{
        //    EfCaChargingStationResp response = null;
        //    if (_internalUserInfos == null)
        //    {
        //        string errorMsg = $"GetAllChargingStations(..), JWT valid but TenantId is missing";
        //        log.Error(errorMsg);

        //        response = new EfCaChargingStationResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    Stopwatch stopWatch = null;
        //    if (log.IsDebugEnabled)
        //    {
        //        stopWatch = new Stopwatch();
        //        stopWatch.Start();
        //        log.Debug($"GetAllDockingStations(..) called.");
        //    }
        //    string roleContainsFunctionMsg;
        //    if (_systemService.RoleContainsFunction(_internalUserInfos, "ChargingStation", "GetAllChargingStations", out roleContainsFunctionMsg) == false)
        //    {
        //        log.Error(roleContainsFunctionMsg);

        //        response = new EfCaChargingStationResp();
        //        response.Error = true;
        //        response.ErrorMsg = roleContainsFunctionMsg;
        //        return response;
        //    }

        //    try
        //    {
        //        response = _chargingStationService.GetAllChargingStations(_internalUserInfos);
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMsg = $"GetAllChargingStations({_internalUserInfos.TenantId}), Exception: {ex.Message}";
        //        log.Error(errorMsg);
        //        response = new EfCaChargingStationResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    finally
        //    {
        //        if (log.IsDebugEnabled)
        //        {
        //            stopWatch.Stop();
        //            log.Debug($"GetAllChargingStations(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
        //        }
        //    }
        //}

        [HttpPut("data/charging")]
        public ActionResult<EfCaChargingStationResp> PutChargingStation(EfCaChargingStation dockingStation)
        {
            EfCaChargingStationResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutChargingStation(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutChargingStation(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "ChargingStation", "PutChargingStation", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _chargingStationService.PutChargingStation(_internalUserInfos, dockingStation);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutChargingStation({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutChargingStation(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/charging/{ident}")]
        public ActionResult<EfCaChargingStationResp> DeleteChargingStation(string ident)
        {
            EfCaChargingStationResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteChargingStation(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteChargingStation(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "ChargingStation", "DeleteChargingStation", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _chargingStationService.DeleteChargingStation(_internalUserInfos, ids);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteChargingStation({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaChargingStationResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteChargingStation(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}