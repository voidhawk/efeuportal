﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ContactController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly ContactService _contactService;
        private readonly SystemService _systemService;

        public ContactController(IHttpContextAccessor httpContextAccessor, SystemService systemService, ContactService contactService)
        {
            _contactService = contactService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
        }

        [HttpPost("data/place/contact")]
        public ActionResult<EfCaContactResp> PostAddContacts([FromBody] EfCaModelCollector contacts)
        {
            EfCaContactResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PostAddContacts(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostAddContacts(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Contact", "PostAddContacts", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _contactService.AddContacts(_internalUserInfos, contacts.Contacts);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostAddContacts({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostAddContact(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Finder: Valid finder parameters
        /// 
        /// Ident, email => equal
        /// Firstname, Lastname => Regex
        /// </summary>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("data/place/contact/find")]
        public ActionResult<EfCaContactResp> FindContactsByFinder([FromBody]EfCaContact finder)
        {
            EfCaContactResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindContactsByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindContactsByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Contact", "FindContactsByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _contactService.FindContactsByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindContactsByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindContactsByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        //[HttpGet("data/place/contact")]
        //public ActionResult<EfCaContactResp> GetAllContacts()
        //{
        //    EfCaContactResp response = null;
        //    if (_internalUserInfos == null)
        //    {
        //        string errorMsg = $"GetAllContacts(..), JWT valid but TenantId is missing";
        //        log.Error(errorMsg);

        //        response = new EfCaContactResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    Stopwatch stopWatch = null;
        //    if (log.IsDebugEnabled)
        //    {
        //        stopWatch = new Stopwatch();
        //        stopWatch.Start();
        //        log.Debug($"GetAllContacts(..) called.");
        //    }
        //    string roleContainsFunctionMsg;
        //    if (_systemService.RoleContainsFunction(_internalUserInfos, "Contact", "GetAllContacts", out roleContainsFunctionMsg) == false)
        //    {
        //        log.Error(roleContainsFunctionMsg);

        //        response = new EfCaContactResp();
        //        response.Error = true;
        //        response.ErrorMsg = roleContainsFunctionMsg;
        //        return response;
        //    }

        //    try
        //    {
        //        response = _contactService.GetAllContacts(_internalUserInfos);
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMsg = $"GetAllContacts({_internalUserInfos.TenantId}), Exception: {ex.Message}";
        //        log.Error(errorMsg);
        //        response = new EfCaContactResp();
        //        response.Error = true;
        //        response.ErrorMsg = errorMsg;
        //        return response;
        //    }
        //    finally
        //    {
        //        if (log.IsDebugEnabled)
        //        {
        //            stopWatch.Stop();
        //            log.Debug($"GetAllContacts(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
        //        }
        //    }
        //}

        [HttpPut("data/place/contact")]
        public ActionResult<EfCaContactResp> PutContact(EfCaContact contact)
        {
            EfCaContactResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutContact(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutContact(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Contact", "PutContact", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _contactService.PutContact(_internalUserInfos, contact);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutContact({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutContact(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("data/place/contact/{ident}")]
        public ActionResult<EfCaContactResp> DeleteContact(string ident)
        {
            EfCaContactResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteContact(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteContact(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Contact", "DeleteContact", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                List<string> ids = new List<string>();
                ids.Add(ident);

                response = _contactService.DeleteContacts(_internalUserInfos, ids);
                //foreach (DetailInfo item in response.DetailInfos)
                //{
                //    if (item.Detail.Equals("SUCCESS"))
                //    {
                //        EfCaUser finder = new EfCaUser() { Email = item.Ident };
                //        EfCaUserResp efCaUserResp = _systemService.FindUsersByFinder(_internalUserInfos, finder);
                //        if (efCaUserResp.Error == false && efCaUserResp.Users.Count == 1)
                //        {
                //            _systemService.DeleteUser(_internalUserInfos, efCaUserResp.Users[0].Ident);
                //        }
                //    }
                //}

                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteContact({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaContactResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteContact(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}