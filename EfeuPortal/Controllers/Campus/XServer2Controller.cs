﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Campus.XServer2;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EfeuPortal.Controllers.Campus
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class XServer2Controller : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(XServer2Controller));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly XServer2Service _xServer2Service;

        public XServer2Controller(IHttpContextAccessor httpContextAccessor, SystemService systemService, XServer2Service xServer2Service)
        {
            _systemService = systemService;
            _xServer2Service = xServer2Service;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("xserver/route/calculateroute")]
        public async Task<EfCaXServerResp> CalculateRoute([FromBody] EfCaRouteCalculation routeCalculation)
        {
            EfCaXServerResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"CalculateRoute(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"CalculateRoute(..) called.");
            }
            //string roleContainsFunctionMsg;
            //if (_systemService.RoleContainsFunction(_internalUserInfos, "XServer", "CalculateRoute", out roleContainsFunctionMsg) == false)
            //{
            //    log.Error(roleContainsFunctionMsg);

            //    response = new EfCaXServerResp();
            //    response.Error = true;
            //    response.ErrorMsg = roleContainsFunctionMsg;
            //    return response;
            //}

            try
            {
                response = await _xServer2Service.CalculateRoute(_internalUserInfos, routeCalculation);
            }
            catch (Exception ex)
            {
                string errorMsg = $"CalculateRoute({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"CalculateRoute(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("xserver/route/calculateRouteStartEnd")]
        public async Task<EfCaXServerResp> CalculateRouteStartEnd([FromBody] EfCaRouteCalculationStartEnd routeCalculation)
        {
            EfCaXServerResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"CalculateRoute(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"CalculateRoute(..) called.");
            }
            //string roleContainsFunctionMsg;
            //if (_systemService.RoleContainsFunction(_internalUserInfos, "XServer", "CalculateRoute", out roleContainsFunctionMsg) == false)
            //{
            //    log.Error(roleContainsFunctionMsg);

            //    response = new EfCaXServerResp();
            //    response.Error = true;
            //    response.ErrorMsg = roleContainsFunctionMsg;
            //    return response;
            //}

            try
            {
                response = await _xServer2Service.CalculateRouteStartEnd(_internalUserInfos, routeCalculation);
            }
            catch (Exception ex)
            {
                string errorMsg = $"CalculateRoute({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"CalculateRoute(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        #region location
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Supported values zipCode, city, street, houseNumber, country, state
        /// 
        ///     POST /Todo
        ///     {
        ///           "zipCode": "76131",
        ///           "city": "Karslruhe",
        ///           "street": "Haid und Neu",
        ///           "houseNumber": "15",
        ///     }        
        /// </remarks>
        /// <param name="searchByAddress"></param>
        /// <returns></returns>
        [HttpPost("xserver/location/searchbyaddress")]
        public async Task<EfCaXServerResp> SearchByAddress(EfCaAddress searchByAddress)
        {
            EfCaXServerResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"SearchByAddress(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }

            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"SearchByAddress(..) called.");
            }
            try
            {
                response = await _xServer2Service.SearchByAddressRequestAsync(_internalUserInfos, searchByAddress);

                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"SearchByAddress({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"SearchByAddress(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///         "text": "Illingen"
        ///     }
        /// </remarks>
        /// <param name="searchByText"></param>
        /// <returns></returns>
        [HttpPost("xserver/location/searchbytext/{searchByText}")]
        public async Task<EfCaXServerResp> SearchByText(string searchByText)
        {
            EfCaXServerResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"SearchByText(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }

            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"SearchByText(..) called.");
            }
            try
            {
                response = await _xServer2Service.SearchByTextRequestAsync(_internalUserInfos, searchByText);

                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"SearchByText({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"SearchByText(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// Search  using coordinates
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///         "longitude": 6.130806,
        ///         "latitude": 49.61055
        ///     }
        /// </remarks>
        /// <param name="searchByPosition"></param>
        /// <returns></returns>
        [HttpPost("xserver/location/searchbyposition")]
        public async Task<EfCaXServerResp> SearchByPosition(EfCaCoordinate searchByPosition)
        {
            EfCaXServerResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"SearchByPosition(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            //string roleContainsFunctionMsg;
            //if (_systemService.RoleContainsFunction(_internalUserInfos, "XServer", "CalculateRoute", out roleContainsFunctionMsg) == false)
            //{
            //    log.Error(roleContainsFunctionMsg);

            //    response = new EfCaXServerResp();
            //    response.Error = true;
            //    response.ErrorMsg = roleContainsFunctionMsg;
            //    return response;
            //}

            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"SearchByPosition(..) called.");
            }
            try
            {
                response = await _xServer2Service.SearchByPositionRequestAsync(_internalUserInfos, searchByPosition);

                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"SearchByPosition({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"SearchByPosition(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        #endregion

        #region load
        [HttpPost("xserver/load/packbins")]
        public async Task<EfCaXServerResp> PackBins([FromBody] EfCaModelCollector packBins)
        {
            EfCaXServerResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PackBins(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PackBins(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "XServer2", "PackBins", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = await _xServer2Service.PackBins(_internalUserInfos, packBins);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PackBins({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PackBins(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }
        #endregion
    }
}
