﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EfeuPortal.Controllers.System
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class TenantController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(TenantController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;

        public TenantController(IHttpContextAccessor httpContextAccessor, SystemService systemService)
        {
            _systemService = systemService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost("system/tenant")]
        [ProducesResponseType(typeof(EfCaTenantResp), StatusCodes.Status200OK)]
        public ActionResult<EfCaTenantResp> AddTenant([FromBody] EfCaTenant item)
        {
            EfCaTenantResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"AddTenant(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"AddTenant(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Tenant", "AddTenant", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.AddTenant(_internalUserInfos, item);
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddTenant({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"AddTenant(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Uses the EfCaTenant object as finder but not all parameters are supported. Refer to the more detailled description below.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Supported finders
        ///     {
        ///        "" => "equal",
        ///        "" => "regex"
        ///     }
        ///
        /// </remarks>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("system/tenant/find")]
        public ActionResult<EfCaTenantResp> FindTenantsByFinder([FromBody] EfCaTenant finder)
        {
            EfCaTenantResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindTenantsByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindTenantsByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Tenant", "FindTenantsByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.FindTenantsByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindTenantsByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindTenantsByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpDelete("system/tenant/{ident}")]
        public ActionResult<EfCaTenantResp> DeleteTenant(string ident)
        {
            EfCaTenantResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteTenant(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteTenant(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Tenant", "DeleteTenant", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.DeleteTenant(_internalUserInfos, ident);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteTenant({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteVehicle(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        /// 
        /// <para>https://lsogit.fzi.de/efeu/efeuportal/-/issues/44</para>
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("system/tenant")]
        public ActionResult<EfCaTenantResp> PutTenant(EfCaTenant item)
        {
            EfCaTenantResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutTenant(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutTenant(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Role", "PutTenant", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.PutTenant(_internalUserInfos, item);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutTenant({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaTenantResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutTenant(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}
