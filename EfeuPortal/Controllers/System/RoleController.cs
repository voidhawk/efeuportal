﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.System
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(RoleController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;

        public RoleController(IHttpContextAccessor httpContextAccessor, SystemService systemService)
        {
            _systemService = systemService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost("system/role")]
        public ActionResult<EfCaRoleResp> AddRole([FromBody]EfCaRole item)
        {
            EfCaRoleResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"AddRole(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"AddRole(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Role", "AddRole", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.AddRole(_internalUserInfos, item);
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddRole({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"AddRole(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Uses the EfCaRole object as finder but not all parameters are supported. Refer to the more detailled description below.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Supported finders
        ///     {
        ///        "" => "equal",
        ///        "" => "regex"
        ///     }
        ///
        /// </remarks>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("system/role/find")]
        public ActionResult<EfCaRoleResp> FindRolesByFinder([FromBody]EfCaRole finder)
        {
            EfCaRoleResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindRolesByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindRolesByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Role", "FindRolesByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.FindRolesByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindRolesByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindRolesByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpPut("system/role")]
        public ActionResult<EfCaRoleResp> PutRole(EfCaRole item)
        {
            EfCaRoleResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutRole(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutRole(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "Role", "PutRole", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.PutRole(_internalUserInfos, item);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutRole({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaRoleResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutRole(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}