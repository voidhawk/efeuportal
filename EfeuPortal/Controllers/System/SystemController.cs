﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EfeuPortal.Controllers.System
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class SystemController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(TenantController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly BoxMountingDeviceService _mountService;
        private readonly AddressService _addressService;

        public SystemController(IHttpContextAccessor httpContextAccessor, SystemService systemService, BoxMountingDeviceService mountService, AddressService addressService)
        {
            _systemService = systemService;
            _mountService = mountService;
            _addressService = addressService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
        }

        [HttpPost("system")]
        public ActionResult<EfCaSystemResp> SystemWorkInProgess(int useCase, [FromBody] EfCaModelCollector workInProgess, bool nonsense)
        {
            EfCaSystemResp response = new EfCaSystemResp();
            if (_internalUserInfos == null)
            {
                string errorMsg = $"SystemWorkInProgess(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                if (useCase == 42)
                {
                    foreach (EfCaUser user in workInProgess.Users)
                    {
                        EfCaUserResp userResp = _systemService.AddUser(_internalUserInfos, user);
                        if (userResp.Error == false)
                        {
                            response.AddDetailInfos(new DetailInfo() { Ident = user.Email, Detail = userResp.FirstLoginPassword });
                        }
                    }
                }
                else if (useCase == 43)
                {
                    /*
                     * Initiale Daten Depot
                     */

                    _addressService.AddAddressData_43(_internalUserInfos, workInProgess);
                }
                else if (useCase == 44)
                {
                    /*
                     * Initiale Daten Customer
                     */

                    EfCaAddressResp addressResp =  _addressService.AddAddressData_44(_internalUserInfos, workInProgess);
                    response.AddDetailInfos(addressResp.DetailInfos);
                    response.Error = addressResp.Error;
                    response.ErrorMsg = addressResp.ErrorMsg;
                }
                else
                {
                    response.Error = true;
                    response.ErrorMsg = "NOT IMPLEMENTED";
                }

                //response = _mountService.WorkInProgess(_internalUserInfos, useCase, workInProgess);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"SystemWorkInProgess({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaSystemResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
        }
    }
}
