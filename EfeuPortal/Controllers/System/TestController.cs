﻿using EfeuPortal.Models;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Controllers.System
{
    [AllowAnonymous]
    [Produces("application/json")]
    [ApiController]
    public class TestController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(TestController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly AddressService _addressService;
        private readonly SystemService _systemService;
        private readonly TestService _testService;

        public TestController(SystemService systemService, TestService testService, AddressService addressService)
        {
            _systemService = systemService;
            _testService = testService;
            _addressService = addressService;
            _internalUserInfos = new InternalUserInfos() { Email = "juergen.stolz@ptv.de", TenantId = "cca37826-5162-4d6e-ac4f-20fa7eea2fda", Role = "" };
        }

        /// <summary>
        /// Test CRUD of the masterdata
        /// </summary>
        /// <returns></returns>
        [HttpPost("efeuportal/test/masterdata")]
        public ActionResult<SystemTestResponse> PostExecuteMasterDataTests()
        {
            SystemTestResponse response = null;
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostExecuteMasterDataTests(..) called.");
            }
            //string roleContainsFunctionMsg;
            //if (_systemService.RoleContainsFunction(_internalUserInfos, "Address", "PostAddAddresses", out roleContainsFunctionMsg) == false)
            //{
            //    log.Error(roleContainsFunctionMsg);

            //    response = new SystemTestResponse();
            //    response.Error = true;
            //    response.ErrorMsg = roleContainsFunctionMsg;
            //    return response;
            //}

            try
            {
                response = _testService.ExecTests(_internalUserInfos);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostExecuteMasterDataTests({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new SystemTestResponse();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostExecuteMasterDataTests(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Example orders
        /// </summary>
        /// <returns></returns>
        [HttpPost("efeuportal/test/order")]
        public ActionResult<SystemTestResponse> PostExecuteOrderDataTests()
        {
            SystemTestResponse response = null;
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostExecuteOrderDataTests(..) called.");
            }
            //string roleContainsFunctionMsg;
            //if (_systemService.RoleContainsFunction(_internalUserInfos, "Address", "PostAddAddresses", out roleContainsFunctionMsg) == false)
            //{
            //    log.Error(roleContainsFunctionMsg);

            //    response = new SystemTestResponse();
            //    response.Error = true;
            //    response.ErrorMsg = roleContainsFunctionMsg;
            //    return response;
            //}

            try
            {
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostExecuteOrderDataTests({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new SystemTestResponse();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostExecuteOrderDataTests(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Map tests
        /// </summary>
        /// <returns></returns>
        [HttpPost("efeuportal/test/Map")]
        public ActionResult<SystemTestResponse> PostExecuteMapDataTests()
        {
            SystemTestResponse response = null;
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostExecuteMapDataTests(..) called.");
            }
            //string roleContainsFunctionMsg;
            //if (_systemService.RoleContainsFunction(_internalUserInfos, "Address", "PostAddAddresses", out roleContainsFunctionMsg) == false)
            //{
            //    log.Error(roleContainsFunctionMsg);

            //    response = new SystemTestResponse();
            //    response.Error = true;
            //    response.ErrorMsg = roleContainsFunctionMsg;
            //    return response;
            //}

            try
            {
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostExecuteMapDataTests({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new SystemTestResponse();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostExecuteMapDataTests(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Planning tests (order export, planning, tour, route)
        /// </summary>
        /// <returns></returns>
        [HttpPost("efeuportal/test/planning")]
        public ActionResult<SystemTestResponse> PostExecutePlanningDataTests()
        {
            SystemTestResponse response = null;
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PostExecutePlanningDataTests(..) called.");
            }
            //string roleContainsFunctionMsg;
            //if (_systemService.RoleContainsFunction(_internalUserInfos, "Address", "PostAddAddresses", out roleContainsFunctionMsg) == false)
            //{
            //    log.Error(roleContainsFunctionMsg);

            //    response = new SystemTestResponse();
            //    response.Error = true;
            //    response.ErrorMsg = roleContainsFunctionMsg;
            //    return response;
            //}

            try
            {
                response = new SystemTestResponse() { Error = true, ErrorMsg = "NOT IMPLEMENTED" };
                DetailInfo addressDetails = new DetailInfo();
                addressDetails.Ident = "PostExecutePlanningDataTests";
                addressDetails.Detail = "Check failed, not implemented";
                response.AddDetailInfos(addressDetails);
            }
            catch (Exception ex)
            {
                string errorMsg = $"PostExecutePlanningDataTests({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new SystemTestResponse();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PostExecutePlanningDataTests(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }
    }
}
