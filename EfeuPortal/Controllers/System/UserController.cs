﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Controllers.System
{
    [Authorize]
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(UserController));

        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;

        public UserController(IHttpContextAccessor httpContextAccessor, SystemService systemService)
        {
            _systemService = systemService;
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost("system/user")]
        public ActionResult<EfCaUserResp> AddUser([FromBody]EfCaUser item)
        {
            EfCaUserResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"AddUser(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"AddUser(..) called.");
            }
            string roleContainsFunctionMsg;
            if(_systemService.RoleContainsFunction(_internalUserInfos, "User", "AddUser", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.AddUser(_internalUserInfos, item);
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddUser({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"AddUser(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// Uses the EfCaUser object as finder but not all parameters are supported. Refer to the more detailled description below.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Supported finders
        ///     {
        ///        "" => "equal",
        ///        "" => "regex"
        ///     }
        ///
        /// </remarks>
        /// <param name="finder"></param>
        /// <returns></returns>
        [HttpPost("system/user/find")]
        public ActionResult<EfCaUserResp> FindUsersByFinder([FromBody]EfCaUser finder)
        {
            EfCaUserResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FindUsersByFinder(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FindUsersByFinder(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "User", "FindUsersByFinder", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.FindUsersByFinder(_internalUserInfos, finder);
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindUsersByFinder({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FindUsersByFinder(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        [HttpPut("system/user")]
        public ActionResult<EfCaUserResp> PutUser(EfCaUser item)
        {
            EfCaUserResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"PutUser(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"PutUser(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "User", "PutUser", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.PutUser(_internalUserInfos, item);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"PutUser({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"PutUser(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpDelete("system/user/{ident}")]
        public ActionResult<EfCaUserResp> DeleteUser(string ident)
        {
            EfCaUserResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"DeleteUser(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"DeleteUser(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "User", "DeleteUser", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.DeleteUser(_internalUserInfos, ident);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteUser({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"DeleteUser(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpPut("system/user/changepwd")]
        public ActionResult<EfCaUserResp> ChangeUserPassword(EfCaChangePasswordRequest item)
        {
            EfCaUserResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"ChangeUserPassword(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"ChangeUserPassword(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "User", "ChangePassword", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.ChangePassword(_internalUserInfos, item);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ChangeUserPassword({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"ChangeUserPassword(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        [HttpPost("system/user/register")]
        public ActionResult<EfCaRegisterResp> RegisterUser([FromBody] EfCaRegisterUser register)
        {
            EfCaRegisterResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"RegisterUser(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaRegisterResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"RegisterUser(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "User", "RegisterUser", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaRegisterResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.Register(_internalUserInfos, register);
            }
            catch (Exception ex)
            {
                string errorMsg = $"RegisterUser({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaRegisterResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"RegisterUser(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }

        /// <summary>
        /// You need the Operator role for resetting a lost password.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("system/user/resetpwd")]
        public ActionResult<EfCaUserResp> ResetUserPassword(EfCaLogin item)
        {
            EfCaUserResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"ResetUserPassword(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"ResetUserPassword(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, "User", "ResetUserPwd", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _systemService.ResetPassword(_internalUserInfos, item);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ResetUserPassword({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaUserResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"ResetUserPassword(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
    }
}