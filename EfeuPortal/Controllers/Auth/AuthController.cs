﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using EfeuPortal.Helpers;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Models;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace EfeuPortal.Controllers.Auth
{
    [Route("api")]
    [Produces("application/json")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AuthController));

        private readonly AppSettings _appSettings;
        private SystemService _systemService;

        public AuthController(IHttpContextAccessor httpContextAccessor, IOptions<AppSettings> appSettings, IConfiguration config, SystemService systemService)
        {
            _appSettings = appSettings.Value;
            _systemService = systemService;
        }

        [HttpPost("auth/login")]
        public ActionResult<EfCaLoginResp> Login([FromBody] EfCaLogin login)
        {
            EfCaLoginResp response = new EfCaLoginResp();
            response = _systemService.Login(login);

            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("auth/requestpwdreset")]
        public void RequestPasswordReset([FromBody] EfCaResetPasswordRequest email)
        {
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"RequestPasswordReset(..) called.");
            }


            try
            {
                EfCaResetPasswordRequestResp response = _systemService.RequestPasswordReset(email);
                if (!response.Error == response.Token.Length > 0) {
                    // send email here
                    Console.WriteLine("http://localhost:3000/passwordreset?token=" + response.Token);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"RequestPasswordReset() Exception: {ex.Message}";
                log.Error(errorMsg);

                return;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"RequestPasswordReset(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rq"></param>
        /// <returns></returns>
        [HttpPost("auth/changepwdwithtoken")]
        public ActionResult<Response> ChangePasswordWithToken([FromBody] EfCaNewPasswordRequest rq)
        {
            Response response = new Response();
            response.Error = false;


            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"ChangePasswordWithToken(..) called.");
            }


            try
            {
                response = _systemService.ResetPasswordWithToken(rq);
            }
            catch (Exception ex)
            {
                string errorMsg = "Could not decode token";
                log.Error(errorMsg);
                response.ErrorMsg = errorMsg;
                response.Error = true;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"ChangePasswordWithToken(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }

            return response;
        }
    }
}