using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EfeuPortal.Services.System;
using EfeuPortal.Services.FZI;
using EfeuPortal.Models.Shared;
using EfeuPortal.Helpers;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models;
using System.Collections.Generic;
using System.Net;

namespace EfeuPortal.Controllers.FZI
{
    [Authorize]
    [Route("fzi")]
    [Produces("application/json")]
    [ApiController]
    public class AdminAppController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AdminAppController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly FZIAdminAppService _fziService;
        private readonly FZIAuthorizationService _fziAuthorizationService;
        private readonly string _roleSection = "AdminApp";
        public AdminAppController(IHttpContextAccessor httpContextAccessor, SystemService systemService, FZIAdminAppService fziService, 
        FZIAuthorizationService fziAuthorizationService)
        {
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _fziService = fziService;
            _fziAuthorizationService = fziAuthorizationService;
        }
        
        [HttpPost("app/admin/modifyOrder")]
        public ActionResult<Response> PostAdminModifyOrder([FromBody]AdminModifyOrderRequest request)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostAdminModifyOrder", response)) {
                return response;
            }
            response.Error = _fziService.ModifyOrder(request, _internalUserInfos);
            return response;
        }

        [HttpPost("app/admin/deleteOrder")]
        public ActionResult<Response> PostDeleteOrder([FromBody]AdminDeleteOrderRequest request)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostDeleteOrder", response)) {
                return response;
            }
            response.Error = _fziService.DeleteOrder(request, _internalUserInfos);
            return response;
        }

        [HttpPost("app/admin/returnBox")]
        public ActionResult<Response> PostReturnBox([FromBody]AdminReturnBoxRequest request)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostReturnBox", response)) {
                return response;
            }
            response.Error = _fziService.ReturnBox(request, _internalUserInfos);
            return response;
        }


        [HttpPost("app/admin/lockVehicle")]
        public ActionResult<VehicleLock> PostLockVehicle([FromBody]LockVehicleRequest request)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostLockVehicle", null)) {
                return Forbid();
            }
            return _fziService.LockVehicle(request, _internalUserInfos);
        }

        [HttpPost("app/admin/unlockVehicle")]
        public ActionResult<Response> PostUnlockVehicle([FromBody]UnlockVehicleRequest request)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostUnlockVehicle", response)) {
                return response;
            }
            response.Error = _fziService.UnlockVehicle(request, _internalUserInfos);
            return response;
        }

        [HttpGet("app/admin/getVehicleLocks/{vehicleId}")]
        public ActionResult<List<VehicleLock>> GetVehicleLocks(string vehicleId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetVehicleLocks", null)) {
                return Forbid();
            }
            return _fziService.GetVehicleLocks(vehicleId, _internalUserInfos);
        }

        [HttpPost("app/admin/createBoxRequest/{contactId}")]
        public ActionResult<Response> PostCreateBoxRequestAdmin(string contactId, [FromBody]BoxRequest request)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCreateBoxRequestAdmin", response)) {
                return response;
            }
            response.Error = _fziService.CreateBoxRequestAdmin(contactId, request, _internalUserInfos);
            return response;
        }

        [HttpGet("app/admin/vehicleInformations")]
        public ActionResult<List<VehicleInformation>> GetVehicleInformations()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetVehicleInformations", null)) {
                return Forbid();
            }
            return _fziService.GetVehicleInformations(_internalUserInfos);
        }

        [HttpGet("app/admin/tourOverview")]
        public ActionResult<List<TourInformation>> GetTourOverview()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetTourOverview", null)) {
                return Forbid();
            }
            return _fziService.GetTourOverview(_internalUserInfos);
        }

        [HttpGet("app/admin/packageInformations")]
        public ActionResult<List<PackageInformationAdmin>> GetPackageInformations()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetPackageInformations", null)) {
                return Forbid();
            }
            return _fziService.GetPackageInformations(_internalUserInfos);
        }

        [HttpGet("app/admin/getFinishedPackages")]
        public ActionResult<List<PackageInformationAdmin>> GetFinishedPackages()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetFinishedPackages", null)) {
                return Forbid();
            }
            return _fziService.GetFinishedPackages(_internalUserInfos);
        }

        [HttpGet("app/admin/getPackage/{orderId}")]
        public ActionResult<PackageInformationAdmin> GetPackage(string orderId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetPackage", null)) {
                return Forbid();
            }
            return _fziService.GetPackage(orderId, _internalUserInfos);
        }

        [HttpGet("app/admin/boxOrderInformations")]
        public ActionResult<List<BoxOrderInformationAdmin>> GetBoxOrderInformations()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetBoxOrderInformations", null)) {
                return Forbid();
            }
            return _fziService.GetBoxOrderInformations(_internalUserInfos);
        }

        [HttpGet("app/admin/getFinishedBoxOrders")]
        public ActionResult<List<BoxOrderInformationAdmin>> GetFinishedBoxOrders()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetFinishedBoxOrders", null)) {
                return Forbid();
            }
            return _fziService.GetFinishedBoxOrders(_internalUserInfos);
        }

        [HttpGet("app/admin/getBoxOrder/{orderId}")]
        public ActionResult<BoxOrderInformationAdmin> GetBoxOrder(string orderId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetBoxOrder", null)) {
                return Forbid();
            }
            return _fziService.GetBoxOrder(orderId, _internalUserInfos);
        }

        [HttpGet("app/admin/getRechargingOrders")]
        public ActionResult<List<RechargeOrderInfoAdmin>> GetRechargingOrders()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetRechargingOrders", null)) {
                return Forbid();
            }
            return _fziService.GetRechargingOrders(_internalUserInfos);
        }

        [HttpPost("app/admin/removePackagesFromOrder")]
        public ActionResult<Response> PostRemovePackagesFromOrder([FromBody]AdminRemovePackagesFromOrderRequest request)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRemovePackagesFromOrder", response)) {
                return response;
            }
            response.Error = _fziService.RemovePackagesFromOrder(request, _internalUserInfos);
            return response;
        }

        [HttpPost("app/admin/deleteToursForVehicle/{vehicleId}")]
        public ActionResult<TourDeletionResult> PostDeleteToursForVehicle(string vehicleId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostDeleteToursForVehicle", null)) {
                return Forbid();
            }
            return _fziService.DeleteToursForVehicle(_internalUserInfos, vehicleId);
        }

        [HttpGet("app/admin/getCustomerList")]
        public ActionResult<List<CustomerInfoAdmin>> GetCustomerList()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetCustomerList", null)) {
                return Forbid();
            }
            return _fziService.GetCustomerList(_internalUserInfos);
        }

        [HttpGet("app/admin/getPlanningStatusLogs")]
        public ActionResult<List<PlanningStatus>> GetPlanningStatusLogs()
        {
             if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetPlanningStatusLogs", null)) {
                return Forbid();
            }
            return _fziService.GetPlanningStatusLogs(_internalUserInfos);
        }
    }
}