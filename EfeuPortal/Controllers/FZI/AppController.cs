using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EfeuPortal.Services.System;
using EfeuPortal.Services.FZI;
using EfeuPortal.Models.Shared;
using EfeuPortal.Helpers;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models;
using System.Collections.Generic;
using System;
using System.IO;

namespace EfeuPortal.Controllers.FZI
{
    [Authorize]
    [Route("fzi")]
    [Produces("application/json")]
    [ApiController]
    public class AppController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(IntraLogAppController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly FZIAppService _fziService;
        private readonly FZIAuthorizationService _fziAuthorizationService;
        private readonly string _roleSection = "App";
        public AppController(IHttpContextAccessor httpContextAccessor, SystemService systemService, FZIAppService fziService,
            FZIAuthorizationService fziAuthorizationService)
        {
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _fziService = fziService;
            _fziAuthorizationService = fziAuthorizationService;
        }

        [HttpGet("app/packageImage/{packageOrderIdent}")]
        public ActionResult<ImageDownload> GetPackageImage(string packageOrderIdent)
        {            
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetPackageImage", null)) {
                return Forbid();
            }
            return _fziService.GetPackageImages(packageOrderIdent, _internalUserInfos);
        }
    }
}