using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EfeuPortal.Services.System;
using EfeuPortal.Services.FZI;
using EfeuPortal.Models.Shared;
using EfeuPortal.Helpers;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models;
using System.Collections.Generic;
using System;

namespace EfeuPortal.Controllers.FZI
{
    [Authorize]
    [Route("fzi")]
    [Produces("application/json")]
    [ApiController]
    public class UserAppController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(UserAppController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly FZIUserAppService _fziService;
        private readonly FZIAuthorizationService _fziAuthorizationService;
        private readonly string _roleSection = "UserApp";
        public UserAppController(IHttpContextAccessor httpContextAccessor, SystemService systemService, FZIUserAppService fziService,
            FZIAuthorizationService fziAuthorizationService)
        {
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _fziService = fziService;
            _fziAuthorizationService = fziAuthorizationService;
        }

        [HttpPost("app/user/requestBox/{customerContactIdent}")]
        public ActionResult<BoxRequestResponse> PostRequestBox(string customerContactIdent, [FromBody]BoxRequest boxRequest)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRequestBox", null)) {
                return Forbid();
            }
            return _fziService.RequestBox(customerContactIdent, boxRequest, _internalUserInfos);
        }

        [HttpPost("app/user/modifyOrder")]
        public ActionResult<Response> PostModifyOrder([FromBody]ModifyOrderRequest modifyOrderRequest)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostModifyOrder", response)) {
                return response;
            }
            response.Error = _fziService.ModifyOrder(modifyOrderRequest, _internalUserInfos);
            return response;
        }

        [HttpPost("app/user/deleteBoxRequest")]
        public ActionResult<Response> PostDeleteBoxRequest([FromBody]DeleteBoxRequest deleteBoxRequest)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostDeleteBoxRequest", response)) {
                return response;
            }
            response.Error = _fziService.DeleteBoxRequest(deleteBoxRequest, _internalUserInfos);
            return response;
        }

        [HttpPost("app/user/cancelPickupOrder")]
        public ActionResult<Response> PostCancelPickupOrder([FromBody]CancelPickupOrderRequest cancelPickupOrderRequest)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCancelPickupOrder", response)) {
                return response;
            }
            response.Error = _fziService.CancelPickupOrder(cancelPickupOrderRequest, _internalUserInfos);
            return response;
        }

        [HttpPost("app/user/receivePackage/{customerContactIdent}")]
        public ActionResult<Response> PostReceivePackage(string customerContactIdent, [FromBody]CustomerReceivePackageEvent customerReceivePackageEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostReceivePackage", response)) {
                return response;
            }
            response.Error =_fziService.ReceivePackage(customerContactIdent, _internalUserInfos, customerReceivePackageEvent);
            return response;
        }

        [HttpPost("app/user/reuseBox")]
        public ActionResult<CustomerReuseBoxResponse> PostReuseBox([FromBody] CustomerReuseBoxEvent customerReuseBoxEvent)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostReuseBox", null)) {
                return Forbid();
            }
            return _fziService.ReuseBox(_internalUserInfos, customerReuseBoxEvent);
        }

        [HttpPost("app/user/sendPackage/{customerContactIdent}")]
        public ActionResult<Response> PostSendPackage(string customerContactIdent, [FromBody]CustomerSendPackageEvent customerSendPackageEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostSendPackage", response)) {
                return response;
            }
            response.Error =_fziService.SendPackage(customerContactIdent, _internalUserInfos, customerSendPackageEvent);
            return response;
        }

        [HttpPost("app/user/placeWasteInBox")]
        public ActionResult<Response> PostPlaceWasteInBox([FromBody]CustomerPlaceWasteEvent customerPlaceWasteEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostPlaceWasteInBox", response)) {
                return response;
            }
            response.Error =_fziService.PlaceWasteInBox(_internalUserInfos, customerPlaceWasteEvent);
            return response;
        }

        [HttpPost("app/user/openBox/{boxId}")]
        public ActionResult<Response> PostOpenBox(string boxId)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostOpenBox", response)) {
                return response;
            }
            response.Error = _fziService.PostOpenBox(_internalUserInfos, boxId);
            return response;
        }

        [HttpGet("app/user/currentOrdersOverview")]
        public ActionResult<List<OrderInformation>> GetCurrentOrdersOverview()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetCurrentOrdersOverview", null)) {
                return Forbid();
            }
            return _fziService.GetCurrentOrdersOverview(_internalUserInfos);
        }

        [HttpGet("app/user/finishedOrdersOverview")]
        public ActionResult<List<OrderInformation>> GetFinishedOrdersOverview()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetFinishedOrdersOverview", null)) {
                return Forbid();
            }
            return _fziService.GetFinishedOrdersOverview(_internalUserInfos);
        }

        [HttpPost("app/user/modifySettings")]
        public ActionResult<Response> PostModifySettings([FromBody] CustomerSettings request)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostModifySettings", response)) {
                return response;
            }
            response.Error = _fziService.ModifySettings(_internalUserInfos, request);
            return response;
        }

        [HttpGet("app/user/getSettings")]
        public ActionResult<CustomerSettings> GetSettings()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetSettings", null)) {
                return Forbid();
            }
            return _fziService.GetSettings(_internalUserInfos);
        }

        [HttpPost("app/user/resetUserDemo")]
        public ActionResult<Response> PostResetUserDemo()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostResetUserDemo", response)) {
                return response;
            }
            response.Error =_fziService.ResetDemo(_internalUserInfos);
            return response;
        }

        [HttpGet("app/user/syncMeetingPoints")]
        public ActionResult<List<SyncMeetingPointInformation>> GetUserSyncMeetingPoints()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetUserSyncMeetingPoints", null)) {
                return Forbid();
            }
            return _fziService.GetSyncMeetingPoints(_internalUserInfos);
        }

        [HttpGet("app/user/mounts")]
        public ActionResult<List<MountInformation>> GetUserMounts()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetUserMounts", null)) {
                return Forbid();
            }
            return _fziService.GetMounts(_internalUserInfos);
        }

        [HttpGet("app/user/getDepotAvailabilityTimeSlots")]
        public ActionResult<DepotAvailabilityTimeSlots> GetDepotAvailabilityTimeSlots()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetDepotAvailabilityTimeSlots", null)) {
                return Forbid();
            }
            return _fziService.GetDepotAvailabilityTimeSlots(_internalUserInfos);
        }

        [HttpGet("app/user/getDepotOpeningHours")]
        public ActionResult<DepotAvailabilityTimeSlots> GetDepotOpeningHours()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetDepotOpeningHours", null)) {
                return Forbid();
            }
            return _fziService.GetDepotOpeningHours(_internalUserInfos);
        }

        [HttpGet("app/user/getMinimumTimeWindowLengthMinutes")]
        public ActionResult<MinimumTimeWindowLength> GetMinimumTimeWindowLengthMinutes()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetMinimumTimeWindowLengthMinutes", null)) {
                return Forbid();
            }
            return _fziService.GetMinimumTimeWindowLengthMinutes(_internalUserInfos);
        }

        [HttpGet("app/user/getMinimumCommissioningTimeMinutes")]
        public ActionResult<MinimumCommissioningTimeMinutes> GetMinimumCommissioningTimeMinutes()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetMinimumCommissioningTimeMinutes", null)) {
                return Forbid();
            }
            return _fziService.GetMinimumCommissioningTimeMinutes(_internalUserInfos);
        }

        [HttpGet("app/user/getAvailableBoxes")]
        public ActionResult<List<AvailableBox>> GetAvailableBoxes()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetAvailableBoxes", null)) {
                return Forbid();
            }
            return _fziService.GetAvailableBoxes(_internalUserInfos);
        }

        [HttpGet("app/user/getPublicHolidaysBW2022To2028")]
        public ActionResult<List<DateTimeOffset>> GetPublicHolidaysBW2022To2028()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetPublicHolidaysBW2022To2028", null)) {
                return Forbid();
            }
            return _fziService.GetPublicHolidaysBW2022To2028(_internalUserInfos);
        }
    }
}