﻿using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.ChargingStation;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.ROPlanning;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Campus.TourData;

namespace EfeuPortal.Controllers.FZI
{
    [Authorize]
    [Route("fzi")]
    [Produces("application/json")]
    [ApiController]
    public class ProcessMgmtController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProcessMgmtController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly OrderService _orderService;
        private readonly FZIProcessService _fziService;
        private readonly FZIAuthorizationService _fziAuthorizationService;
        private readonly string _roleSection = "ProcessMgmt";

        public ProcessMgmtController(IHttpContextAccessor httpContextAccessor, SystemService systemService, 
            OrderService orderService, FZIProcessService fziService, FZIAuthorizationService fziAuthorizationService)
        {
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _orderService = orderService;
            _fziService = fziService;
            _fziAuthorizationService = fziAuthorizationService;
        }

        [HttpPost("process/plan")]
        public ActionResult<Response> PostStartPlanning()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostStartPlanning", response)) {
                return response;
            }
            response.Error =_fziService.Plan(_internalUserInfos);
            return response;
        }

        [HttpPost("process/trips/unfixAll")]
        public ActionResult<Response> PostUnfixAllTrips()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostUnfixAllTrips", response)) {
                return response;
            }
            response.Error = _fziService.UnfixAllTrips(_internalUserInfos);
            return response;
        }

        [HttpGet("process/trips/{vehicleId}")]
        public ActionResult<List<string>> GetAllToursForVehicle(string vehicleId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetAllToursForVehicle", null)) {
                return Forbid();
            }
            return _fziService.GetVehicleTours(vehicleId, _internalUserInfos);
        }

        [HttpPost("process/reservations/deleteAll")]
        public ActionResult<Response> PostDeleteAllReservations()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostDeleteAllReservations", response)) {
                return response;
            }
            response.Error = _fziService.DeleteAllReservations(_internalUserInfos);
            return response;
        }

        [HttpGet("process/reservations/{mountId}")]
        public ActionResult<List<MountReservation>> GetReservationsForMount(string mountId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetReservationsForMount", null)) {
                return Forbid();
            }
            return _fziService.GetReservationsForMount(mountId, _internalUserInfos);
        }

        [HttpDelete("process/reservations/delete/{extId}")]
        public ActionResult<Response> DeleteReservation(string extId)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "DeleteReservation", response)) {
                return response;
            }
            response.Error = _fziService.DeleteReservation(extId, _internalUserInfos);
            return response;
        }

        [HttpGet("process/reservations")]
        public ActionResult<List<MountReservation>> GetAllReservations()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetAllReservations", null)) {
                return Forbid();
            }
            return _fziService.GetAllReservations(_internalUserInfos);
        }

        [HttpPost("process/reservations")]
        public ActionResult<MountReservation> PostAddReservation([FromBody]MountReservation mountReservation)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostAddReservation", null)) {
                return Forbid();
            }
            return _fziService.AddReservation(mountReservation, _internalUserInfos);
        }      


        #region Planing functionality
        [HttpPost("planning/exec")]
        public ActionResult<EfCaPlanningResp> ExecutePlanning(string planningId)
        {
            EfCaPlanningResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"ExecutePlanning(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"ExecutePlanning(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, _roleSection, "ExecutePlanning", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _fziService.ExecutePlanning(_internalUserInfos, planningId);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ExecutePlanning({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"ExecutePlanning(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }

        [HttpPost("planning/exec/fixtours")]
        public ActionResult<EfCaPlanningResp> FixTours(EfCaModelCollector fixTrips)
        {
            EfCaPlanningResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"FixTours(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"FixTours(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, _roleSection, "FixTours", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _fziService.FixUnfixTours(_internalUserInfos, fixTrips.TourFixations);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"FixTours({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"FixTours(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        
        [HttpPost("planning/exec/releasetours")]
        public ActionResult<EfCaPlanningResp> ReleaseTours(EfCaModelCollector fixTrips)
        {
            EfCaPlanningResp response = null;
            if (_internalUserInfos == null)
            {
                string errorMsg = $"ReleaseTours(..), JWT valid but TenantId is missing";
                log.Error(errorMsg);

                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            Stopwatch stopWatch = null;
            if (log.IsDebugEnabled)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                log.Debug($"ReleaseTours(..) called.");
            }
            string roleContainsFunctionMsg;
            if (_systemService.RoleContainsFunction(_internalUserInfos, _roleSection, "ReleaseTours", out roleContainsFunctionMsg) == false)
            {
                log.Error(roleContainsFunctionMsg);

                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = roleContainsFunctionMsg;
                return response;
            }

            try
            {
                response = _fziService.FixUnfixTours(_internalUserInfos, fixTrips.TourFixations);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReleaseTours({_internalUserInfos.TenantId}), Exception: {ex.Message}";
                log.Error(errorMsg);
                response = new EfCaPlanningResp();
                response.Error = true;
                response.ErrorMsg = errorMsg;
                return response;
            }
            finally
            {
                if (log.IsDebugEnabled)
                {
                    stopWatch.Stop();
                    log.Debug($"ReleaseTours(..), ElapsedTime={stopWatch.ElapsedMilliseconds}");
                }
            }
        }
        [HttpPost("process/evaluation")]
        public ActionResult<SimulationInformation> PostSimulationConfig([FromBody]SimulationConfig simulationConfig)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostSimulationConfig", null)) {
                return Forbid();
            }
            return _fziService.PostSimulationConfig(_internalUserInfos, simulationConfig);
        }

        [HttpPost("process/warehouseLog")]
        public ActionResult<Response> PostWarehouseLog([FromBody]WarehouseLog warehouseLog)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostWarehouseLog", response)) {
                return response;
            }
            response.Error =_fziService.PostWarehouseLog(_internalUserInfos, warehouseLog);
            return response;
        }


        #endregion
    }
}