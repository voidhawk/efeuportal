using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EfeuPortal.Services.System;
using EfeuPortal.Services.FZI;
using EfeuPortal.Models.Shared;
using EfeuPortal.Helpers;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models;
using System.Collections.Generic;
using System;
using System.IO;

namespace EfeuPortal.Controllers.FZI
{
    [Authorize]
    [Route("fzi")]
    [Produces("application/json")]
    [ApiController]
    public class IntraLogAppController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(IntraLogAppController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly FZIIntraLogAppService _fziService;
        private readonly FZIAuthorizationService _fziAuthorizationService;
        private readonly string _roleSection = "IntraLogApp";
        public IntraLogAppController(IHttpContextAccessor httpContextAccessor, SystemService systemService, FZIIntraLogAppService fziService,
            FZIAuthorizationService fziAuthorizationService)
        {
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _fziService = fziService;
            _fziAuthorizationService = fziAuthorizationService;
        }
        
        [HttpGet("app/intralog/commissioning")]
        public ActionResult<List<Commission>> GetComissionings()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetComissionings", null)) {
                return Forbid();
            }
            return _fziService.GetCommissionings(_internalUserInfos);
        }

        [HttpGet("app/intralog/commissioning/{commissioningId}")]
        public ActionResult<Commission> GetComissioning(string commissioningId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetComissioning", null)) {
                return Forbid();
            }
            return _fziService.GetCommissioning(commissioningId, _internalUserInfos);
        }

        [HttpPost("app/intralog/deleteCreatedPackageOrder/{efeuPackageId}")]
        public ActionResult<Response> PostDeleteCreatedPackageOrder(string efeuPackageId)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostDeleteCreatedPackageOrder", response)) {
                return response;
            }
            response.Error =  _fziService.DeleteCreatedPackageOrder(efeuPackageId, _internalUserInfos);
            return response;
        }

        [HttpGet("app/intralog/transitionParcels")]
        public ActionResult<List<Parcel>> GetTransitionParcels()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetTransitionParcels", null)) {
                return Forbid();
            }
            return _fziService.GetTransitionParcels(_internalUserInfos);
        }

        [HttpGet("app/intralog/boxMountingDevices")]
        public ActionResult<List<MountingDevice>> GetBoxMountingDevices()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetBoxMountingDevices", null)) {
                return Forbid();
            }
            return _fziService.GetBoxMountingDevices(_internalUserInfos);
        }

        [HttpGet("app/intralog/appBarNumbers")]
        public ActionResult<AppBarNumbers> GetAppBarNumbers()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetAppBarNumbers", null)) {
                return Forbid();
            }
            return _fziService.GetAppBarNumbers(_internalUserInfos);
        }

        [HttpGet("app/intralog/parcels")]
        public ActionResult<List<Parcel>> GetParcels()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetParcels", null)) {
                return Forbid();
            }
            return _fziService.GetParcels(_internalUserInfos);
        }

        [HttpGet("app/intralog/warehousePlaces")]
        public ActionResult<List<WarehousePlace>> GetWarehousePlaces()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetWarehousePlaces", null)) {
                return Forbid();
            }
            return _fziService.GetWarehousePlaces(_internalUserInfos);
        }

        [HttpGet("app/intralog/incomingBoxes")]
        public ActionResult<List<IncomingBox>> GetIncomingBoxes()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetIncomingBoxes", null)) {
                return Forbid();
            }
            return _fziService.GetIncomingBoxes(_internalUserInfos);
        }

        [HttpGet("app/intralog/outboundParcels")]
        public ActionResult<List<Parcel>> GetOutboundParcels()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetOutboundParcels", null)) {
                return Forbid();
            }
            return _fziService.GetOutboundParcels(_internalUserInfos);
        }

        [HttpPost("app/intralog/cleanupIntralogData")]
        public ActionResult<Response> PostCleanupIntralogData()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCleanupIntralogData", response)) {
                return response;
            }
            response.Error =  _fziService.CleanupIntralogData(_internalUserInfos);
            return response;
        }

        [HttpGet("app/intralog/contacts")]
        public ActionResult<List<Contact>> GetContacts()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetContacts", null)) {
                return Forbid();
            }
            return _fziService.GetContacts(_internalUserInfos);
        }

        [HttpGet("app/intralog/selfPickupParcelsForContact/{contactId}")]
        public ActionResult<List<Parcel>> GetSelfPickupParcelsForContact(string contactId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetSelfPickupParcelsForContact", null)) {
                return Forbid();
            }
            return _fziService.GetSelfPickupParcelsForContact(contactId, _internalUserInfos);
        }

        [HttpGet("app/intralog/kepParcels")]
        public ActionResult<List<Parcel>> GetKepParcels()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetKepParcels", null)) {
                return Forbid();
            }
            return _fziService.GetKepParcels(_internalUserInfos);
        }

        [HttpGet("app/intralog/boxes")]
        public ActionResult<List<BoxInformation>> GetBoxInformations()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetBoxInformations", null)) {
                return Forbid();
            }
            return _fziService.GetBoxInformations(_internalUserInfos);
        }

        [HttpGet("app/intralog/allParcels")]
        public ActionResult<List<Parcel>> GetAllParcels()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetAllParcels", null)) {
                return Forbid();
            }
            return _fziService.GetAllParcels(_internalUserInfos);
        }

        [HttpPost("app/intralog/resetIntraDemo")]
        public ActionResult<Response> PostResetIntraDemo()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostResetIntraDemo", response)) {
                return response;
            }
            response.Error =_fziService.ResetDemo(_internalUserInfos);
            return response;
        }

        [HttpPost("app/intralog/openBox/{boxId}")]
        public ActionResult<Response> PostOpenBoxIntra(string boxId)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostOpenBoxIntra", response)) {
                return response;
            }
            response.Error = _fziService.PostOpenBox(_internalUserInfos, boxId);
            return response;
        }

        [HttpPost("app/intralog/test/resetData")]
        public ActionResult<Response> PostResetTestData()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostResetTestData", response)) {
                return response;
            }
            response.Error =_fziService.ResetTestData(_internalUserInfos);
            return response;
        }

        [HttpPost("app/intralog/test/completeCommissioning")]
        public ActionResult<Response> PostCompleteTestCommissioning()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCompleteTestCommissioning", response)) {
                return response;
            }
            response.Error =_fziService.CompleteTestCommissioning(_internalUserInfos);
            return response;
        }

        [HttpPost("app/intralog/test/completeCustomerStops")]
        public ActionResult<Response> PostCompleteTestCustomerStops()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCompleteTestCustomerStops", response)) {
                return response;
            }
            response.Error =_fziService.CompleteTestCustomerStops(_internalUserInfos);
            return response;
        }

        [HttpPost("app/intralog/test/registerPackages")]
        public ActionResult<Response> PostTestRegisterPackages()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostTestRegisterPackages", response)) {
                return response;
            }
            response.Error =_fziService.TestRegisterPackages(_internalUserInfos);
            return response;
        }
    }
}