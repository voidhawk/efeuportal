using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.ChargingStation;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.ROPlanning;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using EfeuPortal.Models.Campus.Box;

namespace EfeuPortal.Controllers.FZI
{
    [Authorize]
    [Route("fzi")]
    [Produces("application/json")]
    [ApiController]
    public class VehicleEventController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProcessMgmtController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly OrderService _orderService;
        private readonly FZIVehicleEventService _fziEventService;
        private readonly FZIAuthorizationService _fziAuthorizationService;
        private readonly string _roleSection = "VehicleEvent";

        public VehicleEventController(IHttpContextAccessor httpContextAccessor, SystemService systemService, 
            OrderService orderService, FZIVehicleEventService fziEventService, FZIAuthorizationService fziAuthorizationService)
        {
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _orderService = orderService;
            _fziEventService = fziEventService;
            _fziAuthorizationService = fziAuthorizationService;
        }

        [HttpPost("event/vehicle/deliverBoxToMount")]
        public ActionResult<Response> PostDeliverBoxToMount([FromBody]VehicleBoxDeliveryEvent vehicleBoxDeliveryEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostDeliverBoxToMount", response)) {
                return response;
            }
            response.Error =_fziEventService.DeliverBoxToMount(_internalUserInfos, vehicleBoxDeliveryEvent);
            return response;
        }

        [HttpPost("event/vehicle/pickupBoxFromMount")]
        public ActionResult<Response> PostPickupBoxFromMount([FromBody]VehicleBoxPickupEvent vehicleBoxPickupEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostPickupBoxFromMount", response)) {
                return response;
            }
            response.Error =_fziEventService.PickupBoxFromMount(_internalUserInfos, vehicleBoxPickupEvent);
            return response;
        }

        [HttpPost("event/vehicle/finishTour")]
        public ActionResult<Response> PostFinishTour([FromBody]FinishTourEvent finishTourEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostFinishTour", response)) {
                return response;
            }
            response.Error =_fziEventService.FinishTour(_internalUserInfos, finishTourEvent);
            return response;
        }

        [HttpPost("event/vehicle/requestMountForBoxDelivery")]
        public ActionResult<RequestMountForBoxDeliveryResponse> PostRequestMountForBoxDelivery([FromBody]RequestMountForBoxDeliveryEvent requestMountForBoxDeliveryEvent)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRequestMountForBoxDelivery", null)) {
                return Forbid();
            }
            return _fziEventService.RequestMountForBoxDelivery(_internalUserInfos, requestMountForBoxDeliveryEvent);
        }

        [HttpPost("event/vehicle/requestMountForWaiting")]
        public ActionResult<RequestMountForWaitingResponse> PostRequestMountForWaiting([FromBody] RequestMountForWaitingEvent requestMountForWaitingEvent)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRequestMountForWaiting", null)) {
                return Forbid();
            }
            return _fziEventService.RequestMountForWaiting(_internalUserInfos, requestMountForWaitingEvent);
        }

        [HttpPost("event/vehicle/departFromMountWaitingCharging")]
        public ActionResult<Response> PostDepartFromMountWaitingCharging([FromBody] DepartFromMountWaitingChargingEvent departFromMountWaitingChargingEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostDepartFromMountWaitingCharging", response)) {
                return response;
            }
            response.Error = _fziEventService.DepartFromMountWaitingCharging(_internalUserInfos, departFromMountWaitingChargingEvent);
            return response;
        }

        [HttpPost("event/vehicle/requestMountForCharging")]
        public ActionResult<RequestMountForChargingResponse> PostRequestMountForCharging([FromBody] RequestMountForChargingEvent requestMountForChargingEvent)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRequestMountForCharging", null)) {
                return Forbid();
            }
            return _fziEventService.RequestMountForCharging(_internalUserInfos, requestMountForChargingEvent);
        }

        [HttpPost("event/vehicle/requestSyncStopState")]
        public ActionResult<SyncStopStateResponse> PostRequestSyncStopState([FromBody]SyncStopStateRequest syncStopStateRequest)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRequestSyncStopState", null)) {
                return Forbid();
            }
            return _fziEventService.RequestSyncStopState(_internalUserInfos, syncStopStateRequest);
        }

        [HttpPost("event/vehicle/syncStopArrival")]
        public ActionResult<Response> PostSyncStopArrival([FromBody] SyncStopArrivalMessage syncStopArrivalMessage)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostSyncStopArrival", response)) {
                return response;
            }
            response.Error =_fziEventService.SyncStopArrival(_internalUserInfos, syncStopArrivalMessage);
            return response;
        }

        [HttpPost("event/vehicle/updateVehicle")]
        public ActionResult<Response> PostUpdateVehicle([FromBody]VehicleUpdate vehicleUpdate)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostUpdateVehicle", response)) {
                return response;
            }
            response.Error =_fziEventService.UpdateVehicle(_internalUserInfos, vehicleUpdate);
            return response;
        }

        [HttpPost("event/vehicle/startTour")]
        public ActionResult<Response> PostStartTour([FromBody]StartTourEvent startTourEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostStartTour", response)) {
                return response;
            }
            response.Error =_fziEventService.StartTour(_internalUserInfos, startTourEvent);
            return response;
        }

        [HttpPost("event/vehicle/requestMountForBoxPickup")]
        public ActionResult<RequestMountForBoxPickupResponse> PostRequestMountForBoxPickup([FromBody]RequestMountForBoxPickupEvent requestMountForBoxPickupEvent)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRequestMountForBoxPickup", null)) {
                return Forbid();
            }
            return _fziEventService.RequestMountForBoxPickup(_internalUserInfos, requestMountForBoxPickupEvent);
        }
    }
}