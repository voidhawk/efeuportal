using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EfeuPortal.Services.System;
using EfeuPortal.Services.FZI;
using EfeuPortal.Models.Shared;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Clients.ProcessManagement.Model;
using System.Collections.Generic;

namespace EfeuPortal.Controllers.FZI
{
    [Authorize]
    [Route("fzi")]
    [Produces("application/json")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(UserAppController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly FZIStatusService _fziStatusService;
        private readonly FZIAuthorizationService _fziAuthorizationService;
        private readonly string _roleSection = "Status";
        public StatusController(IHttpContextAccessor httpContextAccessor, SystemService systemService, FZIStatusService fziStatusService, 
            FZIAuthorizationService fziAuthorizationService)
        {
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _fziStatusService = fziStatusService;
            _fziAuthorizationService = fziAuthorizationService;
        }

        [HttpPut("status/mountStatus")]
        public ActionResult<Response> PutMountStatus([FromBody]MountStatus mountStatus)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PutMountStatus", response)) {
                return response;
            }
            response.Error =_fziStatusService.UpdateMountStatus(_internalUserInfos, mountStatus);
            return response;
        }

        [HttpGet("status/mountStatus")]
        public ActionResult<List<MountStatus>> GetMountStatusList()
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetMountStatusList", response)) {
                return Forbid();
            }
            return _fziStatusService.GetMountStatusList(_internalUserInfos);
        }

        [HttpGet("status/mountStatus/{mountId}")]
        public ActionResult<MountStatus> GetMountStatus(string mountId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetMountStatus", null)) {
                return Forbid();
            }
            return _fziStatusService.GetMountStatus(mountId, _internalUserInfos);
        }

        [HttpPut("status/vehicleStatus")]
        public ActionResult<Response> PutVehicleStatus([FromBody]VehicleStatus vehicleStatus)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PutVehicleStatus", response)) {
                return response;
            }
            response.Error =_fziStatusService.UpdateVehicleStatus(_internalUserInfos, vehicleStatus);
            return response;
        }

        [HttpGet("status/vehicleStatus")]
        public ActionResult<List<VehicleStatus>> GetVehicleStatusList()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetVehicleStatusList", null)) {
                return Forbid();
            }
            return _fziStatusService.GetVehicleStatusList(_internalUserInfos);
        }

        [HttpGet("status/vehicleStatus/{vehicleId}")]
        public ActionResult<VehicleStatus> GetVehicleStatus(string vehicleId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetVehicleStatus", null)) {
                return Forbid();
            }
            return _fziStatusService.GetVehicleStatus(_internalUserInfos, vehicleId);
        }

        [HttpPut("status/boxStatus")]
        public ActionResult<Response> PutBoxStatus([FromBody]BoxStatus boxStatus)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PutBoxStatus", response)) {
                return response;
            }
            response.Error =_fziStatusService.UpdateBoxStatus(_internalUserInfos, boxStatus);
            return response;
        }

        [HttpGet("status/boxStatus")]
        public ActionResult<List<BoxStatus>> GetBoxStatusList()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetBoxStatusList", null)) {
                return Forbid();
            }
            return _fziStatusService.GetBoxStatusList(_internalUserInfos);
        }

        [HttpGet("status/boxStatus/{boxId}")]
        public ActionResult<BoxStatus> GetBoxStatus(string boxId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetBoxStatus", null)) {
                return Forbid();
            }
            return _fziStatusService.GetBoxStatus(_internalUserInfos, boxId);
        }

        [HttpGet("status/tourStatus")]
        public ActionResult<List<TourStatus>> GetTourStatusList()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetTourStatusList", null)) {
                return Forbid();
            }
            return _fziStatusService.GetTourStatusList(_internalUserInfos);
        }   

        [HttpPost("status/tourStatus")]
        public ActionResult<TourStatus> PostCreateTourStatus([FromBody]TourStatus tourStatus)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCreateTourStatus", null)) {
                return Forbid();
            }
            return _fziStatusService.CreateTourStatus(_internalUserInfos, tourStatus);
        }

        [HttpGet("status/boxStatus/isInDepot/{lockerId}/{lockId}")]
        public ActionResult<BoxInDepotResponse> GetIsBoxInDepot(string lockerId, string lockId)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetIsBoxInDepot", null)) {
                return Forbid();
            }
            return _fziStatusService.IsBoxInDepot(_internalUserInfos, lockerId, lockId);
        } 
    }
}