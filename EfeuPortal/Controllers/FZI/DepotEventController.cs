using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.ChargingStation;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.ROPlanning;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using EfeuPortal.Models.Campus.Box;

namespace EfeuPortal.Controllers.FZI
{
    [Authorize]
    [Route("fzi")]
    [Produces("application/json")]
    [ApiController]
    public class DepotEventController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProcessMgmtController));
        private readonly InternalUserInfos _internalUserInfos;
        private readonly SystemService _systemService;
        private readonly OrderService _orderService;
        private readonly FZIDepotEventService _fziEventService;
        private readonly FZIAuthorizationService _fziAuthorizationService;
        private readonly string _roleSection = "DepotEvent";

        public DepotEventController(IHttpContextAccessor httpContextAccessor, SystemService systemService, 
            OrderService orderService, FZIDepotEventService fziEventService, FZIAuthorizationService fziAuthorizationService)
        {
            _internalUserInfos = httpContextAccessor.CurrentUserInfos();
            _systemService = systemService;
            _orderService = orderService;
            _fziEventService = fziEventService;
            _fziAuthorizationService = fziAuthorizationService;
        }

        [HttpPost("event/depot/checkCustomer")]
        public ActionResult<CheckCustomerResponse> PostCheckCustomer([FromBody]CheckCustomerRequest checkCustomerRequest)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCheckCustomer", null)) {
                return Forbid();
            }
            return _fziEventService.CheckCustomer(checkCustomerRequest, _internalUserInfos);
        }

        [HttpPost("event/depot/registerPackage")]
        public ActionResult<RegisterPackageResponse> PostRegisterPackage([FromBody]PackageInformation packageInformation)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRegisterPackage", null)) {
                return Forbid();
            }
            return _fziEventService.PostRegisterPackage(packageInformation, _internalUserInfos);
        }

        [HttpPost("event/depot/addImageToPackage/{packageOrderIdent}")]
        public ActionResult<Response> PostAddImageToPackage([FromRoute]string packageOrderIdent, [FromBody]ImageUpload body)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostAddImageToPackage", response)) {
                return response;
            }
            response.Error = _fziEventService.AddImageToPackage(packageOrderIdent, body, _internalUserInfos);
            return response;
        }

        [HttpPost("event/depot/assignBoxToOrder")]
        public ActionResult<Response> PostAssignBoxToOrder([FromBody]AssignBoxToOrderEvent assignBoxToOrderEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostAssignBoxToOrder", response)) {
                return response;
            }
            response.Error =_fziEventService.AssignBoxToOrder(_internalUserInfos, assignBoxToOrderEvent);
            return response;
        }
        
        [HttpPost("event/depot/placeBoxOnMount")]
        public ActionResult<Response> PostPlaceBoxOnMount([FromBody]PlaceBoxOnMountEvent placeBoxOnMountEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostPlaceBoxOnMount", response)) {
                return response;
            }
            response.Error =_fziEventService.PlaceBoxOnMount(_internalUserInfos, placeBoxOnMountEvent);
            return response;
        }

        [HttpPost("event/depot/placePackageInBox")]
        public ActionResult<Response> PostPlacePackageInBox([FromBody]DepotSendPackageEvent depotSendPackageEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostPlacePackageInBox", response)) {
                return response;
            }
            response.Error =_fziEventService.PlacePackageInBoxDepot(_internalUserInfos, depotSendPackageEvent);
            return response;
        }

        [HttpPost("event/depot/receivePackageFromBox")]
        public ActionResult<Response> PostReceivePackageFromBox([FromBody]DepotReceivePackageEvent depotReceivePackageEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostReceivePackageFromBox", response)) {
                return response;
            }
            response.Error =_fziEventService.ReceivePackageFromBox(_internalUserInfos, depotReceivePackageEvent);
            return response;
        }
        
        [HttpPost("event/depot/removeWasteFromBox")]
        public ActionResult<Response> PostRemoveWasteFromBox([FromBody]DepotRemoveWasteEvent depotRemoveWasteEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostRemoveWasteFromBox", response)) {
                return response;
            }
            response.Error =_fziEventService.RemoveWasteFromBox(_internalUserInfos, depotRemoveWasteEvent);
            return response;
        }

        [HttpPost("event/depot/completeCommissioning")]
        public ActionResult<Response> PostCompleteCommissioning([FromBody]CompleteCommissioningEvent completeCommissioningEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCompleteCommissioning", response)) {
                return response;
            }
            response.Error =_fziEventService.CompleteCommissioning(_internalUserInfos, completeCommissioningEvent);
            return response;
        }

        [HttpPost("event/depot/labelParcel")]
        public ActionResult<Response> PostLabelParcel([FromBody]LabelParcelEvent labelParcelEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostLabelParcel", response)) {
                return response;
            }
            response.Error =_fziEventService.LabelParcel(_internalUserInfos, labelParcelEvent);
            return response;
        }

        [HttpPost("event/depot/labelOutboundParcel")]
        public ActionResult<RegisterPackageResponse> PostLabelOutboundParcel([FromBody]OutboundParcelInformation outboundParcelInformation)
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostLabelOutboundParcel", null)) {
                return Forbid();
            }
            return _fziEventService.LabelOutboundParcel(_internalUserInfos, outboundParcelInformation);
        }

        [HttpPost("event/depot/completeOutboundParcelStorage")]
        public ActionResult<Response> PostCompleteOutboundParcelStorage(CompleteOutboundParcelStorageEvent completeOutboundParcelStorageEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCompleteOutboundParcelStorage", response)) {
                return response;
            }
            response.Error = _fziEventService.CompleteOutboundParcelStorage(_internalUserInfos, completeOutboundParcelStorageEvent);
            return response;
        }

        [HttpPost("event/depot/completeParcelStorage")]
        public ActionResult<Response> PostCompleteParcelStorage(CompleteParcelStorageEvent completeParcelStorageEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostCompleteParcelStorage", response)) {
                return response;
            }
            response.Error = _fziEventService.CompleteParcelStorage(_internalUserInfos, completeParcelStorageEvent);
            return response;
        }

        [HttpPost("event/depot/confirmSelfPickup")]
        public ActionResult<Response> PostConfirmSelfPickup(ConfirmSelfPickupEvent confirmSelfPickupEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostConfirmSelfPickup", response)) {
                return response;
            }
            response.Error = _fziEventService.ConfirmSelfPickup(_internalUserInfos, confirmSelfPickupEvent);
            return response;
        }

        [HttpPost("event/depot/confirmKepPickup")]
        public ActionResult<Response> PostConfirmKepPickup(ConfirmKepPickupEvent confirmKepPickupEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostConfirmKepPickup", response)) {
                return response;
            }
            response.Error = _fziEventService.ConfirmKepPickup(_internalUserInfos, confirmKepPickupEvent);
            return response;
        }

        [HttpPost("event/depot/addInformationToOutboundPackage/{packageOrderIdent}")]
        public ActionResult<Response> PostAddInformationToOutboundPackage(string packageOrderIdent, [FromBody]PackageInformation packageInformation)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostAddInformationToOutboundPackage", response)) {
                return response;
            }
            response.Error = _fziEventService.AddInformationToOutboundPackage(_internalUserInfos, packageOrderIdent, packageInformation);
            return response;
        }

        [HttpPost("event/depot/deactivateBox")]
        public ActionResult<Response> PostDeactivateBox([FromBody]DeactivateBoxEvent deactivateBoxEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostDeactivateBox", response)) {
                return response;
            }
            response.Error =_fziEventService.DeactivateBox(_internalUserInfos, deactivateBoxEvent);
            return response;
        }

        [HttpPost("event/depot/storeBox")]
        public ActionResult<Response> PostStoreBox([FromBody]StoreBoxEvent storeBoxEvent)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostStoreBox", response)) {
                return response;
            }
            response.Error =_fziEventService.StoreBox(_internalUserInfos, storeBoxEvent);
            return response;
        }

        [HttpGet("event/depot/printJobs")]
        public ActionResult<List<PrintJob>> GetPrintJobs()
        {
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "GetPrintJobs", null)) {
                return Forbid();
            }
            return _fziEventService.GetPrintJobs(_internalUserInfos);
        }

        [HttpPost("event/depot/finishPrintJob/{extId}")]
        public ActionResult<Response> PostFinishPrintJob(string extId)
        {
            var response = new Response();
            if (!_fziAuthorizationService.isAuthorized(_internalUserInfos, _roleSection, "PostFinishPrintJob", response)) {
                return response;
            }
            response.Error =_fziEventService.FinishPrintJob(_internalUserInfos, extId);
            return response;
        }
    }
}