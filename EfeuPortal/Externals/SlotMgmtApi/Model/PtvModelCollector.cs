/*
 * PTV Group
 *
 * This is the (Swagger) API description to all of the slot management endpoints
 *
 * The version of the OpenAPI document: Version 1.0.1
 * Contact: juergen.stolz@ptvgroup.com
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = PTVGROUP.SlotMgmt.Client.OpenAPIDateConverter;

namespace PTVGROUP.SlotMgmt.Model
{
    /// <summary>
    /// PtvModelCollector
    /// </summary>
    [DataContract(Name = "PtvModelCollector")]
    public partial class PtvModelCollector : IEquatable<PtvModelCollector>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PtvModelCollector" /> class.
        /// </summary>
        /// <param name="idents">idents.</param>
        /// <param name="slotSchedulers">Kommentar.</param>
        /// <param name="slotReservationRequest">slotReservationRequest.</param>
        /// <param name="slotReservationResponse">slotReservationResponse.</param>
        /// <param name="userInfo">userInfo.</param>
        public PtvModelCollector(List<string> idents = default(List<string>), List<PtvSlotScheduler> slotSchedulers = default(List<PtvSlotScheduler>), PtvSlotReservationRequest slotReservationRequest = default(PtvSlotReservationRequest), List<PtvSlotReservationResponse> slotReservationResponse = default(List<PtvSlotReservationResponse>), PtvUserInfo userInfo = default(PtvUserInfo))
        {
            this.Idents = idents;
            this.SlotSchedulers = slotSchedulers;
            this.SlotReservationRequest = slotReservationRequest;
            this.SlotReservationResponse = slotReservationResponse;
            this.UserInfo = userInfo;
        }

        /// <summary>
        /// Gets or Sets Idents
        /// </summary>
        [DataMember(Name = "idents", EmitDefaultValue = true)]
        public List<string> Idents { get; set; }

        /// <summary>
        /// Kommentar
        /// </summary>
        /// <value>Kommentar</value>
        [DataMember(Name = "slotSchedulers", EmitDefaultValue = true)]
        public List<PtvSlotScheduler> SlotSchedulers { get; set; }

        /// <summary>
        /// Gets or Sets SlotReservationRequest
        /// </summary>
        [DataMember(Name = "slotReservationRequest", EmitDefaultValue = false)]
        public PtvSlotReservationRequest SlotReservationRequest { get; set; }

        /// <summary>
        /// Gets or Sets SlotReservationResponse
        /// </summary>
        [DataMember(Name = "slotReservationResponse", EmitDefaultValue = true)]
        public List<PtvSlotReservationResponse> SlotReservationResponse { get; set; }

        /// <summary>
        /// Gets or Sets UserInfo
        /// </summary>
        [DataMember(Name = "userInfo", EmitDefaultValue = false)]
        public PtvUserInfo UserInfo { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PtvModelCollector {\n");
            sb.Append("  Idents: ").Append(Idents).Append("\n");
            sb.Append("  SlotSchedulers: ").Append(SlotSchedulers).Append("\n");
            sb.Append("  SlotReservationRequest: ").Append(SlotReservationRequest).Append("\n");
            sb.Append("  SlotReservationResponse: ").Append(SlotReservationResponse).Append("\n");
            sb.Append("  UserInfo: ").Append(UserInfo).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as PtvModelCollector);
        }

        /// <summary>
        /// Returns true if PtvModelCollector instances are equal
        /// </summary>
        /// <param name="input">Instance of PtvModelCollector to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PtvModelCollector input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Idents == input.Idents ||
                    this.Idents != null &&
                    input.Idents != null &&
                    this.Idents.SequenceEqual(input.Idents)
                ) && 
                (
                    this.SlotSchedulers == input.SlotSchedulers ||
                    this.SlotSchedulers != null &&
                    input.SlotSchedulers != null &&
                    this.SlotSchedulers.SequenceEqual(input.SlotSchedulers)
                ) && 
                (
                    this.SlotReservationRequest == input.SlotReservationRequest ||
                    (this.SlotReservationRequest != null &&
                    this.SlotReservationRequest.Equals(input.SlotReservationRequest))
                ) && 
                (
                    this.SlotReservationResponse == input.SlotReservationResponse ||
                    this.SlotReservationResponse != null &&
                    input.SlotReservationResponse != null &&
                    this.SlotReservationResponse.SequenceEqual(input.SlotReservationResponse)
                ) && 
                (
                    this.UserInfo == input.UserInfo ||
                    (this.UserInfo != null &&
                    this.UserInfo.Equals(input.UserInfo))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Idents != null)
                    hashCode = hashCode * 59 + this.Idents.GetHashCode();
                if (this.SlotSchedulers != null)
                    hashCode = hashCode * 59 + this.SlotSchedulers.GetHashCode();
                if (this.SlotReservationRequest != null)
                    hashCode = hashCode * 59 + this.SlotReservationRequest.GetHashCode();
                if (this.SlotReservationResponse != null)
                    hashCode = hashCode * 59 + this.SlotReservationResponse.GetHashCode();
                if (this.UserInfo != null)
                    hashCode = hashCode * 59 + this.UserInfo.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
