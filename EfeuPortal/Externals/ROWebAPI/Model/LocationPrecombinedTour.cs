/*
 * Ptvag.Smartour.Business.ConnectManager.WebAPI
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = PTVGROUP.ROWebApi.Client.OpenAPIDateConverter;

namespace PTVGROUP.ROWebApi.Model
{
    /// <summary>
    /// LocationPrecombinedTour
    /// </summary>
    [DataContract(Name = "LocationPrecombinedTour")]
    public partial class LocationPrecombinedTour : IEquatable<LocationPrecombinedTour>, IValidatableObject
    {
        /// <summary>
        /// Defines FromWeekday
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum FromWeekdayEnum
        {
            /// <summary>
            /// Enum MON for value: MON
            /// </summary>
            [EnumMember(Value = "MON")]
            MON = 1,

            /// <summary>
            /// Enum TUE for value: TUE
            /// </summary>
            [EnumMember(Value = "TUE")]
            TUE = 2,

            /// <summary>
            /// Enum WED for value: WED
            /// </summary>
            [EnumMember(Value = "WED")]
            WED = 3,

            /// <summary>
            /// Enum THU for value: THU
            /// </summary>
            [EnumMember(Value = "THU")]
            THU = 4,

            /// <summary>
            /// Enum FRI for value: FRI
            /// </summary>
            [EnumMember(Value = "FRI")]
            FRI = 5,

            /// <summary>
            /// Enum SAT for value: SAT
            /// </summary>
            [EnumMember(Value = "SAT")]
            SAT = 6,

            /// <summary>
            /// Enum SUN for value: SUN
            /// </summary>
            [EnumMember(Value = "SUN")]
            SUN = 7

        }

        /// <summary>
        /// Gets or Sets FromWeekday
        /// </summary>
        [DataMember(Name = "FromWeekday", EmitDefaultValue = false)]
        public FromWeekdayEnum? FromWeekday { get; set; }
        /// <summary>
        /// Defines UntilWeekday
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum UntilWeekdayEnum
        {
            /// <summary>
            /// Enum MON for value: MON
            /// </summary>
            [EnumMember(Value = "MON")]
            MON = 1,

            /// <summary>
            /// Enum TUE for value: TUE
            /// </summary>
            [EnumMember(Value = "TUE")]
            TUE = 2,

            /// <summary>
            /// Enum WED for value: WED
            /// </summary>
            [EnumMember(Value = "WED")]
            WED = 3,

            /// <summary>
            /// Enum THU for value: THU
            /// </summary>
            [EnumMember(Value = "THU")]
            THU = 4,

            /// <summary>
            /// Enum FRI for value: FRI
            /// </summary>
            [EnumMember(Value = "FRI")]
            FRI = 5,

            /// <summary>
            /// Enum SAT for value: SAT
            /// </summary>
            [EnumMember(Value = "SAT")]
            SAT = 6,

            /// <summary>
            /// Enum SUN for value: SUN
            /// </summary>
            [EnumMember(Value = "SUN")]
            SUN = 7

        }

        /// <summary>
        /// Gets or Sets UntilWeekday
        /// </summary>
        [DataMember(Name = "UntilWeekday", EmitDefaultValue = false)]
        public UntilWeekdayEnum? UntilWeekday { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationPrecombinedTour" /> class.
        /// </summary>
        /// <param name="fromWeekday">fromWeekday.</param>
        /// <param name="untilWeekday">untilWeekday.</param>
        /// <param name="precombinedTour">precombinedTour.</param>
        /// <param name="precombinedTourSequence">precombinedTourSequence.</param>
        public LocationPrecombinedTour(FromWeekdayEnum? fromWeekday = default(FromWeekdayEnum?), UntilWeekdayEnum? untilWeekday = default(UntilWeekdayEnum?), string precombinedTour = default(string), int precombinedTourSequence = default(int))
        {
            this.FromWeekday = fromWeekday;
            this.UntilWeekday = untilWeekday;
            this.PrecombinedTour = precombinedTour;
            this.PrecombinedTourSequence = precombinedTourSequence;
        }

        /// <summary>
        /// Gets or Sets PrecombinedTour
        /// </summary>
        [DataMember(Name = "PrecombinedTour", EmitDefaultValue = false)]
        public string PrecombinedTour { get; set; }

        /// <summary>
        /// Gets or Sets PrecombinedTourSequence
        /// </summary>
        [DataMember(Name = "PrecombinedTourSequence", EmitDefaultValue = false)]
        public int PrecombinedTourSequence { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class LocationPrecombinedTour {\n");
            sb.Append("  FromWeekday: ").Append(FromWeekday).Append("\n");
            sb.Append("  UntilWeekday: ").Append(UntilWeekday).Append("\n");
            sb.Append("  PrecombinedTour: ").Append(PrecombinedTour).Append("\n");
            sb.Append("  PrecombinedTourSequence: ").Append(PrecombinedTourSequence).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as LocationPrecombinedTour);
        }

        /// <summary>
        /// Returns true if LocationPrecombinedTour instances are equal
        /// </summary>
        /// <param name="input">Instance of LocationPrecombinedTour to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(LocationPrecombinedTour input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.FromWeekday == input.FromWeekday ||
                    this.FromWeekday.Equals(input.FromWeekday)
                ) && 
                (
                    this.UntilWeekday == input.UntilWeekday ||
                    this.UntilWeekday.Equals(input.UntilWeekday)
                ) && 
                (
                    this.PrecombinedTour == input.PrecombinedTour ||
                    (this.PrecombinedTour != null &&
                    this.PrecombinedTour.Equals(input.PrecombinedTour))
                ) && 
                (
                    this.PrecombinedTourSequence == input.PrecombinedTourSequence ||
                    this.PrecombinedTourSequence.Equals(input.PrecombinedTourSequence)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                hashCode = hashCode * 59 + this.FromWeekday.GetHashCode();
                hashCode = hashCode * 59 + this.UntilWeekday.GetHashCode();
                if (this.PrecombinedTour != null)
                    hashCode = hashCode * 59 + this.PrecombinedTour.GetHashCode();
                hashCode = hashCode * 59 + this.PrecombinedTourSequence.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
