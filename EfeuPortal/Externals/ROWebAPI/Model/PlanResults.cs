/*
 * Ptvag.Smartour.Business.ConnectManager.WebAPI
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = PTVGROUP.ROWebApi.Client.OpenAPIDateConverter;

namespace PTVGROUP.ROWebApi.Model
{
    /// <summary>
    /// PlanResults
    /// </summary>
    [DataContract(Name = "PlanResults")]
    public partial class PlanResults : IEquatable<PlanResults>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlanResults" /> class.
        /// </summary>
        /// <param name="tours">tours.</param>
        /// <param name="deletedTours">deletedTours.</param>
        /// <param name="modifiedTours">modifiedTours.</param>
        /// <param name="unscheduledOrders">unscheduledOrders.</param>
        /// <param name="ignoredOrders">ignoredOrders.</param>
        /// <param name="allOrders">allOrders.</param>
        /// <param name="modifiedOrders">modifiedOrders.</param>
        public PlanResults(List<Tour> tours = default(List<Tour>), List<string> deletedTours = default(List<string>), List<string> modifiedTours = default(List<string>), List<string> unscheduledOrders = default(List<string>), List<string> ignoredOrders = default(List<string>), List<string> allOrders = default(List<string>), List<string> modifiedOrders = default(List<string>))
        {
            this.Tours = tours;
            this.DeletedTours = deletedTours;
            this.ModifiedTours = modifiedTours;
            this.UnscheduledOrders = unscheduledOrders;
            this.IgnoredOrders = ignoredOrders;
            this.AllOrders = allOrders;
            this.ModifiedOrders = modifiedOrders;
        }

        /// <summary>
        /// Gets or Sets Tours
        /// </summary>
        [DataMember(Name = "Tours", EmitDefaultValue = false)]
        public List<Tour> Tours { get; set; }

        /// <summary>
        /// Gets or Sets DeletedTours
        /// </summary>
        [DataMember(Name = "DeletedTours", EmitDefaultValue = false)]
        public List<string> DeletedTours { get; set; }

        /// <summary>
        /// Gets or Sets ModifiedTours
        /// </summary>
        [DataMember(Name = "ModifiedTours", EmitDefaultValue = false)]
        public List<string> ModifiedTours { get; set; }

        /// <summary>
        /// Gets or Sets UnscheduledOrders
        /// </summary>
        [DataMember(Name = "UnscheduledOrders", EmitDefaultValue = false)]
        public List<string> UnscheduledOrders { get; set; }

        /// <summary>
        /// Gets or Sets IgnoredOrders
        /// </summary>
        [DataMember(Name = "IgnoredOrders", EmitDefaultValue = false)]
        public List<string> IgnoredOrders { get; set; }

        /// <summary>
        /// Gets or Sets AllOrders
        /// </summary>
        [DataMember(Name = "AllOrders", EmitDefaultValue = false)]
        public List<string> AllOrders { get; set; }

        /// <summary>
        /// Gets or Sets ModifiedOrders
        /// </summary>
        [DataMember(Name = "ModifiedOrders", EmitDefaultValue = false)]
        public List<string> ModifiedOrders { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PlanResults {\n");
            sb.Append("  Tours: ").Append(Tours).Append("\n");
            sb.Append("  DeletedTours: ").Append(DeletedTours).Append("\n");
            sb.Append("  ModifiedTours: ").Append(ModifiedTours).Append("\n");
            sb.Append("  UnscheduledOrders: ").Append(UnscheduledOrders).Append("\n");
            sb.Append("  IgnoredOrders: ").Append(IgnoredOrders).Append("\n");
            sb.Append("  AllOrders: ").Append(AllOrders).Append("\n");
            sb.Append("  ModifiedOrders: ").Append(ModifiedOrders).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as PlanResults);
        }

        /// <summary>
        /// Returns true if PlanResults instances are equal
        /// </summary>
        /// <param name="input">Instance of PlanResults to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PlanResults input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Tours == input.Tours ||
                    this.Tours != null &&
                    input.Tours != null &&
                    this.Tours.SequenceEqual(input.Tours)
                ) && 
                (
                    this.DeletedTours == input.DeletedTours ||
                    this.DeletedTours != null &&
                    input.DeletedTours != null &&
                    this.DeletedTours.SequenceEqual(input.DeletedTours)
                ) && 
                (
                    this.ModifiedTours == input.ModifiedTours ||
                    this.ModifiedTours != null &&
                    input.ModifiedTours != null &&
                    this.ModifiedTours.SequenceEqual(input.ModifiedTours)
                ) && 
                (
                    this.UnscheduledOrders == input.UnscheduledOrders ||
                    this.UnscheduledOrders != null &&
                    input.UnscheduledOrders != null &&
                    this.UnscheduledOrders.SequenceEqual(input.UnscheduledOrders)
                ) && 
                (
                    this.IgnoredOrders == input.IgnoredOrders ||
                    this.IgnoredOrders != null &&
                    input.IgnoredOrders != null &&
                    this.IgnoredOrders.SequenceEqual(input.IgnoredOrders)
                ) && 
                (
                    this.AllOrders == input.AllOrders ||
                    this.AllOrders != null &&
                    input.AllOrders != null &&
                    this.AllOrders.SequenceEqual(input.AllOrders)
                ) && 
                (
                    this.ModifiedOrders == input.ModifiedOrders ||
                    this.ModifiedOrders != null &&
                    input.ModifiedOrders != null &&
                    this.ModifiedOrders.SequenceEqual(input.ModifiedOrders)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Tours != null)
                    hashCode = hashCode * 59 + this.Tours.GetHashCode();
                if (this.DeletedTours != null)
                    hashCode = hashCode * 59 + this.DeletedTours.GetHashCode();
                if (this.ModifiedTours != null)
                    hashCode = hashCode * 59 + this.ModifiedTours.GetHashCode();
                if (this.UnscheduledOrders != null)
                    hashCode = hashCode * 59 + this.UnscheduledOrders.GetHashCode();
                if (this.IgnoredOrders != null)
                    hashCode = hashCode * 59 + this.IgnoredOrders.GetHashCode();
                if (this.AllOrders != null)
                    hashCode = hashCode * 59 + this.AllOrders.GetHashCode();
                if (this.ModifiedOrders != null)
                    hashCode = hashCode * 59 + this.ModifiedOrders.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
