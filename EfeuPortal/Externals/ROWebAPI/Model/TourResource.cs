/*
 * Ptvag.Smartour.Business.ConnectManager.WebAPI
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = PTVGROUP.ROWebApi.Client.OpenAPIDateConverter;

namespace PTVGROUP.ROWebApi.Model
{
    /// <summary>
    /// TourResource
    /// </summary>
    [DataContract(Name = "TourResource")]
    public partial class TourResource : IEquatable<TourResource>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TourResource" /> class.
        /// </summary>
        /// <param name="type">type.</param>
        /// <param name="extId1">extId1.</param>
        /// <param name="name">name.</param>
        /// <param name="licensePlate">licensePlate.</param>
        public TourResource(string type = default(string), string extId1 = default(string), string name = default(string), string licensePlate = default(string))
        {
            this.Type = type;
            this.ExtId1 = extId1;
            this.Name = name;
            this.LicensePlate = licensePlate;
        }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "Type", EmitDefaultValue = false)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or Sets ExtId1
        /// </summary>
        [DataMember(Name = "ExtId1", EmitDefaultValue = false)]
        public string ExtId1 { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "Name", EmitDefaultValue = false)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets LicensePlate
        /// </summary>
        [DataMember(Name = "LicensePlate", EmitDefaultValue = false)]
        public string LicensePlate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class TourResource {\n");
            sb.Append("  Type: ").Append(Type).Append("\n");
            sb.Append("  ExtId1: ").Append(ExtId1).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  LicensePlate: ").Append(LicensePlate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as TourResource);
        }

        /// <summary>
        /// Returns true if TourResource instances are equal
        /// </summary>
        /// <param name="input">Instance of TourResource to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(TourResource input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Type == input.Type ||
                    (this.Type != null &&
                    this.Type.Equals(input.Type))
                ) && 
                (
                    this.ExtId1 == input.ExtId1 ||
                    (this.ExtId1 != null &&
                    this.ExtId1.Equals(input.ExtId1))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.LicensePlate == input.LicensePlate ||
                    (this.LicensePlate != null &&
                    this.LicensePlate.Equals(input.LicensePlate))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Type != null)
                    hashCode = hashCode * 59 + this.Type.GetHashCode();
                if (this.ExtId1 != null)
                    hashCode = hashCode * 59 + this.ExtId1.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.LicensePlate != null)
                    hashCode = hashCode * 59 + this.LicensePlate.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
