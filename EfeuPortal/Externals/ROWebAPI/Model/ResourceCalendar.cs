/*
 * Ptvag.Smartour.Business.ConnectManager.WebAPI
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = PTVGROUP.ROWebApi.Client.OpenAPIDateConverter;

namespace PTVGROUP.ROWebApi.Model
{
    /// <summary>
    /// ResourceCalendar
    /// </summary>
    [DataContract(Name = "ResourceCalendar")]
    public partial class ResourceCalendar : IEquatable<ResourceCalendar>, IValidatableObject
    {
        /// <summary>
        /// Defines ActionCode
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum ActionCodeEnum
        {
            /// <summary>
            /// Enum NEW for value: NEW
            /// </summary>
            [EnumMember(Value = "NEW")]
            NEW = 1,

            /// <summary>
            /// Enum UPDATE for value: UPDATE
            /// </summary>
            [EnumMember(Value = "UPDATE")]
            UPDATE = 2,

            /// <summary>
            /// Enum DELETE for value: DELETE
            /// </summary>
            [EnumMember(Value = "DELETE")]
            DELETE = 3

        }

        /// <summary>
        /// Gets or Sets ActionCode
        /// </summary>
        [DataMember(Name = "ActionCode", EmitDefaultValue = false)]
        public ActionCodeEnum? ActionCode { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceCalendar" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected ResourceCalendar() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceCalendar" /> class.
        /// </summary>
        /// <param name="resourceCalendarHeader">resourceCalendarHeader.</param>
        /// <param name="extId">extId (required).</param>
        /// <param name="creationTime">creationTime.</param>
        /// <param name="actionCode">actionCode.</param>
        /// <param name="context">context.</param>
        public ResourceCalendar(List<ResourceCalendarHeader> resourceCalendarHeader = default(List<ResourceCalendarHeader>), string extId = default(string), DateTime creationTime = default(DateTime), ActionCodeEnum? actionCode = default(ActionCodeEnum?), string context = default(string))
        {
            // to ensure "extId" is required (not null)
            this.ExtId = extId ?? throw new ArgumentNullException("extId is a required property for ResourceCalendar and cannot be null");
            this.ResourceCalendarHeader = resourceCalendarHeader;
            this.CreationTime = creationTime;
            this.ActionCode = actionCode;
            this.Context = context;
        }

        /// <summary>
        /// Gets or Sets ResourceCalendarHeader
        /// </summary>
        [DataMember(Name = "ResourceCalendarHeader", EmitDefaultValue = false)]
        public List<ResourceCalendarHeader> ResourceCalendarHeader { get; set; }

        /// <summary>
        /// Gets or Sets ExtId
        /// </summary>
        [DataMember(Name = "ExtId", IsRequired = true, EmitDefaultValue = false)]
        public string ExtId { get; set; }

        /// <summary>
        /// Gets or Sets CreationTime
        /// </summary>
        [DataMember(Name = "CreationTime", EmitDefaultValue = false)]
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// Gets or Sets Context
        /// </summary>
        [DataMember(Name = "Context", EmitDefaultValue = false)]
        public string Context { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ResourceCalendar {\n");
            sb.Append("  ResourceCalendarHeader: ").Append(ResourceCalendarHeader).Append("\n");
            sb.Append("  ExtId: ").Append(ExtId).Append("\n");
            sb.Append("  CreationTime: ").Append(CreationTime).Append("\n");
            sb.Append("  ActionCode: ").Append(ActionCode).Append("\n");
            sb.Append("  Context: ").Append(Context).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as ResourceCalendar);
        }

        /// <summary>
        /// Returns true if ResourceCalendar instances are equal
        /// </summary>
        /// <param name="input">Instance of ResourceCalendar to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ResourceCalendar input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.ResourceCalendarHeader == input.ResourceCalendarHeader ||
                    this.ResourceCalendarHeader != null &&
                    input.ResourceCalendarHeader != null &&
                    this.ResourceCalendarHeader.SequenceEqual(input.ResourceCalendarHeader)
                ) && 
                (
                    this.ExtId == input.ExtId ||
                    (this.ExtId != null &&
                    this.ExtId.Equals(input.ExtId))
                ) && 
                (
                    this.CreationTime == input.CreationTime ||
                    (this.CreationTime != null &&
                    this.CreationTime.Equals(input.CreationTime))
                ) && 
                (
                    this.ActionCode == input.ActionCode ||
                    this.ActionCode.Equals(input.ActionCode)
                ) && 
                (
                    this.Context == input.Context ||
                    (this.Context != null &&
                    this.Context.Equals(input.Context))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.ResourceCalendarHeader != null)
                    hashCode = hashCode * 59 + this.ResourceCalendarHeader.GetHashCode();
                if (this.ExtId != null)
                    hashCode = hashCode * 59 + this.ExtId.GetHashCode();
                if (this.CreationTime != null)
                    hashCode = hashCode * 59 + this.CreationTime.GetHashCode();
                hashCode = hashCode * 59 + this.ActionCode.GetHashCode();
                if (this.Context != null)
                    hashCode = hashCode * 59 + this.Context.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
