/*
 * Ptvag.Smartour.Business.ConnectManager.WebAPI
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mime;
using PTVGROUP.ROWebApi.Client;
using PTVGROUP.ROWebApi.Model;

namespace PTVGROUP.ROWebApi.Api
{

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface ITourPlanningApiSync : IApiAccessor
    {
        #region Synchronous Operations
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Object</returns>
        Object TourPlanningPlanAllOrders();

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of Object</returns>
        ApiResponse<Object> TourPlanningPlanAllOrdersWithHttpInfo();
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="planningAreaExtId"></param>
        /// <returns>WebApiResponse</returns>
        WebApiResponse TourPlanningPostAllOrdersAsync(string planningAreaExtId);

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="planningAreaExtId"></param>
        /// <returns>ApiResponse of WebApiResponse</returns>
        ApiResponse<WebApiResponse> TourPlanningPostAllOrdersAsyncWithHttpInfo(string planningAreaExtId);
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="fixationParams"></param>
        /// <returns>WebApiResponse</returns>
        WebApiResponse TourPlanningPutFixOrUnfixTours(List<FixationParam> fixationParams);

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="fixationParams"></param>
        /// <returns>ApiResponse of WebApiResponse</returns>
        ApiResponse<WebApiResponse> TourPlanningPutFixOrUnfixToursWithHttpInfo(List<FixationParam> fixationParams);
        #endregion Synchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface ITourPlanningApiAsync : IApiAccessor
    {
        #region Asynchronous Operations
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of Object</returns>
        System.Threading.Tasks.Task<Object> TourPlanningPlanAllOrdersAsync(System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (Object)</returns>
        System.Threading.Tasks.Task<ApiResponse<Object>> TourPlanningPlanAllOrdersWithHttpInfoAsync(System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="planningAreaExtId"></param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of WebApiResponse</returns>
        System.Threading.Tasks.Task<WebApiResponse> TourPlanningPostAllOrdersAsyncAsync(string planningAreaExtId, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="planningAreaExtId"></param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (WebApiResponse)</returns>
        System.Threading.Tasks.Task<ApiResponse<WebApiResponse>> TourPlanningPostAllOrdersAsyncWithHttpInfoAsync(string planningAreaExtId, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="fixationParams"></param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of WebApiResponse</returns>
        System.Threading.Tasks.Task<WebApiResponse> TourPlanningPutFixOrUnfixToursAsync(List<FixationParam> fixationParams, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="fixationParams"></param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (WebApiResponse)</returns>
        System.Threading.Tasks.Task<ApiResponse<WebApiResponse>> TourPlanningPutFixOrUnfixToursWithHttpInfoAsync(List<FixationParam> fixationParams, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));
        #endregion Asynchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface ITourPlanningApi : ITourPlanningApiSync, ITourPlanningApiAsync
    {

    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class TourPlanningApi : ITourPlanningApi
    {
        private PTVGROUP.ROWebApi.Client.ExceptionFactory _exceptionFactory = (name, response) => null;

        /// <summary>
        /// Initializes a new instance of the <see cref="TourPlanningApi"/> class.
        /// </summary>
        /// <returns></returns>
        public TourPlanningApi() : this((string)null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TourPlanningApi"/> class.
        /// </summary>
        /// <returns></returns>
        public TourPlanningApi(String basePath)
        {
            this.Configuration = PTVGROUP.ROWebApi.Client.Configuration.MergeConfigurations(
                PTVGROUP.ROWebApi.Client.GlobalConfiguration.Instance,
                new PTVGROUP.ROWebApi.Client.Configuration { BasePath = basePath }
            );
            this.Client = new PTVGROUP.ROWebApi.Client.ApiClient(this.Configuration.BasePath);
            this.AsynchronousClient = new PTVGROUP.ROWebApi.Client.ApiClient(this.Configuration.BasePath);
            this.ExceptionFactory = PTVGROUP.ROWebApi.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TourPlanningApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public TourPlanningApi(PTVGROUP.ROWebApi.Client.Configuration configuration)
        {
            if (configuration == null) throw new ArgumentNullException("configuration");

            this.Configuration = PTVGROUP.ROWebApi.Client.Configuration.MergeConfigurations(
                PTVGROUP.ROWebApi.Client.GlobalConfiguration.Instance,
                configuration
            );
            this.Client = new PTVGROUP.ROWebApi.Client.ApiClient(this.Configuration.BasePath);
            this.AsynchronousClient = new PTVGROUP.ROWebApi.Client.ApiClient(this.Configuration.BasePath);
            ExceptionFactory = PTVGROUP.ROWebApi.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TourPlanningApi"/> class
        /// using a Configuration object and client instance.
        /// </summary>
        /// <param name="client">The client interface for synchronous API access.</param>
        /// <param name="asyncClient">The client interface for asynchronous API access.</param>
        /// <param name="configuration">The configuration object.</param>
        public TourPlanningApi(PTVGROUP.ROWebApi.Client.ISynchronousClient client, PTVGROUP.ROWebApi.Client.IAsynchronousClient asyncClient, PTVGROUP.ROWebApi.Client.IReadableConfiguration configuration)
        {
            if (client == null) throw new ArgumentNullException("client");
            if (asyncClient == null) throw new ArgumentNullException("asyncClient");
            if (configuration == null) throw new ArgumentNullException("configuration");

            this.Client = client;
            this.AsynchronousClient = asyncClient;
            this.Configuration = configuration;
            this.ExceptionFactory = PTVGROUP.ROWebApi.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// The client for accessing this underlying API asynchronously.
        /// </summary>
        public PTVGROUP.ROWebApi.Client.IAsynchronousClient AsynchronousClient { get; set; }

        /// <summary>
        /// The client for accessing this underlying API synchronously.
        /// </summary>
        public PTVGROUP.ROWebApi.Client.ISynchronousClient Client { get; set; }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public String GetBasePath()
        {
            return this.Configuration.BasePath;
        }

        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public PTVGROUP.ROWebApi.Client.IReadableConfiguration Configuration { get; set; }

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public PTVGROUP.ROWebApi.Client.ExceptionFactory ExceptionFactory
        {
            get
            {
                if (_exceptionFactory != null && _exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return _exceptionFactory;
            }
            set { _exceptionFactory = value; }
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Object</returns>
        public Object TourPlanningPlanAllOrders()
        {
            PTVGROUP.ROWebApi.Client.ApiResponse<Object> localVarResponse = TourPlanningPlanAllOrdersWithHttpInfo();
            return localVarResponse.Data;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of Object</returns>
        public PTVGROUP.ROWebApi.Client.ApiResponse<Object> TourPlanningPlanAllOrdersWithHttpInfo()
        {
            PTVGROUP.ROWebApi.Client.RequestOptions localVarRequestOptions = new PTVGROUP.ROWebApi.Client.RequestOptions();

            String[] _contentTypes = new String[] {
            };

            // to determine the Accept header
            String[] _accepts = new String[] {
                "application/json",
                "text/json"
            };

            var localVarContentType = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null) localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);

            var localVarAccept = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null) localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);



            // make the HTTP request
            var localVarResponse = this.Client.Get<Object>("/api/planning", localVarRequestOptions, this.Configuration);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("TourPlanningPlanAllOrders", localVarResponse);
                if (_exception != null) throw _exception;
            }

            return localVarResponse;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of Object</returns>
        public async System.Threading.Tasks.Task<Object> TourPlanningPlanAllOrdersAsync(System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            PTVGROUP.ROWebApi.Client.ApiResponse<Object> localVarResponse = await TourPlanningPlanAllOrdersWithHttpInfoAsync(cancellationToken).ConfigureAwait(false);
            return localVarResponse.Data;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (Object)</returns>
        public async System.Threading.Tasks.Task<PTVGROUP.ROWebApi.Client.ApiResponse<Object>> TourPlanningPlanAllOrdersWithHttpInfoAsync(System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {

            PTVGROUP.ROWebApi.Client.RequestOptions localVarRequestOptions = new PTVGROUP.ROWebApi.Client.RequestOptions();

            String[] _contentTypes = new String[] {
            };

            // to determine the Accept header
            String[] _accepts = new String[] {
                "application/json",
                "text/json"
            };


            var localVarContentType = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null) localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);

            var localVarAccept = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null) localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);



            // make the HTTP request

            var localVarResponse = await this.AsynchronousClient.GetAsync<Object>("/api/planning", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("TourPlanningPlanAllOrders", localVarResponse);
                if (_exception != null) throw _exception;
            }

            return localVarResponse;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="planningAreaExtId"></param>
        /// <returns>WebApiResponse</returns>
        public WebApiResponse TourPlanningPostAllOrdersAsync(string planningAreaExtId)
        {
            PTVGROUP.ROWebApi.Client.ApiResponse<WebApiResponse> localVarResponse = TourPlanningPostAllOrdersAsyncWithHttpInfo(planningAreaExtId);
            return localVarResponse.Data;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="planningAreaExtId"></param>
        /// <returns>ApiResponse of WebApiResponse</returns>
        public PTVGROUP.ROWebApi.Client.ApiResponse<WebApiResponse> TourPlanningPostAllOrdersAsyncWithHttpInfo(string planningAreaExtId)
        {
            // verify the required parameter 'planningAreaExtId' is set
            if (planningAreaExtId == null)
                throw new PTVGROUP.ROWebApi.Client.ApiException(400, "Missing required parameter 'planningAreaExtId' when calling TourPlanningApi->TourPlanningPostAllOrdersAsync");

            PTVGROUP.ROWebApi.Client.RequestOptions localVarRequestOptions = new PTVGROUP.ROWebApi.Client.RequestOptions();

            String[] _contentTypes = new String[] {
            };

            // to determine the Accept header
            String[] _accepts = new String[] {
                "application/json",
                "text/json"
            };

            var localVarContentType = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null) localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);

            var localVarAccept = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null) localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);

            localVarRequestOptions.QueryParameters.Add(PTVGROUP.ROWebApi.Client.ClientUtils.ParameterToMultiMap("", "planningAreaExtId", planningAreaExtId));


            // make the HTTP request
            var localVarResponse = this.Client.Post<WebApiResponse>("/api/planning", localVarRequestOptions, this.Configuration);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("TourPlanningPostAllOrdersAsync", localVarResponse);
                if (_exception != null) throw _exception;
            }

            return localVarResponse;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="planningAreaExtId"></param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of WebApiResponse</returns>
        public async System.Threading.Tasks.Task<WebApiResponse> TourPlanningPostAllOrdersAsyncAsync(string planningAreaExtId, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            PTVGROUP.ROWebApi.Client.ApiResponse<WebApiResponse> localVarResponse = await TourPlanningPostAllOrdersAsyncWithHttpInfoAsync(planningAreaExtId, cancellationToken).ConfigureAwait(false);
            return localVarResponse.Data;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="planningAreaExtId"></param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (WebApiResponse)</returns>
        public async System.Threading.Tasks.Task<PTVGROUP.ROWebApi.Client.ApiResponse<WebApiResponse>> TourPlanningPostAllOrdersAsyncWithHttpInfoAsync(string planningAreaExtId, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            // verify the required parameter 'planningAreaExtId' is set
            if (planningAreaExtId == null)
                throw new PTVGROUP.ROWebApi.Client.ApiException(400, "Missing required parameter 'planningAreaExtId' when calling TourPlanningApi->TourPlanningPostAllOrdersAsync");


            PTVGROUP.ROWebApi.Client.RequestOptions localVarRequestOptions = new PTVGROUP.ROWebApi.Client.RequestOptions();

            String[] _contentTypes = new String[] {
            };

            // to determine the Accept header
            String[] _accepts = new String[] {
                "application/json",
                "text/json"
            };


            var localVarContentType = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null) localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);

            var localVarAccept = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null) localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);

            localVarRequestOptions.QueryParameters.Add(PTVGROUP.ROWebApi.Client.ClientUtils.ParameterToMultiMap("", "planningAreaExtId", planningAreaExtId));


            // make the HTTP request

            var localVarResponse = await this.AsynchronousClient.PostAsync<WebApiResponse>("/api/planning", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("TourPlanningPostAllOrdersAsync", localVarResponse);
                if (_exception != null) throw _exception;
            }

            return localVarResponse;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="fixationParams"></param>
        /// <returns>WebApiResponse</returns>
        public WebApiResponse TourPlanningPutFixOrUnfixTours(List<FixationParam> fixationParams)
        {
            PTVGROUP.ROWebApi.Client.ApiResponse<WebApiResponse> localVarResponse = TourPlanningPutFixOrUnfixToursWithHttpInfo(fixationParams);
            return localVarResponse.Data;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="fixationParams"></param>
        /// <returns>ApiResponse of WebApiResponse</returns>
        public PTVGROUP.ROWebApi.Client.ApiResponse<WebApiResponse> TourPlanningPutFixOrUnfixToursWithHttpInfo(List<FixationParam> fixationParams)
        {
            // verify the required parameter 'fixationParams' is set
            if (fixationParams == null)
                throw new PTVGROUP.ROWebApi.Client.ApiException(400, "Missing required parameter 'fixationParams' when calling TourPlanningApi->TourPlanningPutFixOrUnfixTours");

            PTVGROUP.ROWebApi.Client.RequestOptions localVarRequestOptions = new PTVGROUP.ROWebApi.Client.RequestOptions();

            String[] _contentTypes = new String[] {
                "application/json",
                "text/json",
                "application/x-www-form-urlencoded"
            };

            // to determine the Accept header
            String[] _accepts = new String[] {
                "application/json",
                "text/json"
            };

            var localVarContentType = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null) localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);

            var localVarAccept = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null) localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);

            localVarRequestOptions.Data = fixationParams;


            // make the HTTP request
            var localVarResponse = this.Client.Put<WebApiResponse>("/api/planning", localVarRequestOptions, this.Configuration);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("TourPlanningPutFixOrUnfixTours", localVarResponse);
                if (_exception != null) throw _exception;
            }

            return localVarResponse;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="fixationParams"></param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of WebApiResponse</returns>
        public async System.Threading.Tasks.Task<WebApiResponse> TourPlanningPutFixOrUnfixToursAsync(List<FixationParam> fixationParams, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            PTVGROUP.ROWebApi.Client.ApiResponse<WebApiResponse> localVarResponse = await TourPlanningPutFixOrUnfixToursWithHttpInfoAsync(fixationParams, cancellationToken).ConfigureAwait(false);
            return localVarResponse.Data;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="PTVGROUP.ROWebApi.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="fixationParams"></param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (WebApiResponse)</returns>
        public async System.Threading.Tasks.Task<PTVGROUP.ROWebApi.Client.ApiResponse<WebApiResponse>> TourPlanningPutFixOrUnfixToursWithHttpInfoAsync(List<FixationParam> fixationParams, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            // verify the required parameter 'fixationParams' is set
            if (fixationParams == null)
                throw new PTVGROUP.ROWebApi.Client.ApiException(400, "Missing required parameter 'fixationParams' when calling TourPlanningApi->TourPlanningPutFixOrUnfixTours");


            PTVGROUP.ROWebApi.Client.RequestOptions localVarRequestOptions = new PTVGROUP.ROWebApi.Client.RequestOptions();

            String[] _contentTypes = new String[] {
                "application/json", 
                "text/json", 
                "application/x-www-form-urlencoded"
            };

            // to determine the Accept header
            String[] _accepts = new String[] {
                "application/json",
                "text/json"
            };


            var localVarContentType = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null) localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);

            var localVarAccept = PTVGROUP.ROWebApi.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null) localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);

            localVarRequestOptions.Data = fixationParams;


            // make the HTTP request

            var localVarResponse = await this.AsynchronousClient.PutAsync<WebApiResponse>("/api/planning", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("TourPlanningPutFixOrUnfixTours", localVarResponse);
                if (_exception != null) throw _exception;
            }

            return localVarResponse;
        }

    }
}
