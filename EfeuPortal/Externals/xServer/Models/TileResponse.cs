// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("TileResponse")]
    public partial class TileResponse : ResponseBase
    {
        /// <summary>
        /// Initializes a new instance of the TileResponse class.
        /// </summary>
        public TileResponse()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the TileResponse class.
        /// </summary>
        /// <param name="image">The image as a byte array.</param>
        /// <param name="zoom">The zoom level of the tile image.</param>
        public TileResponse(byte[] image, double zoom, IList<ResultLimitation> resultLimitations = default(IList<ResultLimitation>), IList<MapFeature> features = default(IList<MapFeature>))
            : base(resultLimitations)
        {
            Image = image;
            Zoom = zoom;
            Features = features;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the image as a byte array.
        /// </summary>
        [JsonProperty(PropertyName = "image")]
        public byte[] Image { get; set; }

        /// <summary>
        /// Gets or sets the zoom level of the tile image.
        /// </summary>
        [JsonProperty(PropertyName = "zoom")]
        public double Zoom { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "features")]
        public IList<MapFeature> Features { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Image == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Image");
            }
            if (Features != null)
            {
                foreach (var element in Features)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
