// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents a visit week. If determineShortestVisitSequencePerDay is
    /// true, the sequence of visits is optimal. Otherwise, visits are sorted
    /// by visit day but the sequence of visits within a visit day has no
    /// special meaning.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xcluster.VisitWeek
    /// </summary>
    public partial class VisitWeek
    {
        /// <summary>
        /// Initializes a new instance of the VisitWeek class.
        /// </summary>
        public VisitWeek()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the VisitWeek class.
        /// </summary>
        /// <param name="travelTime">If determineShortestVisitSequencePerDay is
        /// true, this is set to the total travel time for this week.
        /// Otherwise, it is not set.</param>
        /// <param name="serviceTime">Total service time for that week.</param>
        public VisitWeek(IList<VisitDay> visitDays = default(IList<VisitDay>), IList<string> visitOrderIdsSkipped = default(IList<string>), double? travelTime = default(double?), double? serviceTime = default(double?))
        {
            VisitDays = visitDays;
            VisitOrderIdsSkipped = visitOrderIdsSkipped;
            TravelTime = travelTime;
            ServiceTime = serviceTime;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "visitDays")]
        public IList<VisitDay> VisitDays { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "visitOrderIdsSkipped")]
        public IList<string> VisitOrderIdsSkipped { get; set; }

        /// <summary>
        /// Gets or sets if determineShortestVisitSequencePerDay is true, this
        /// is set to the total travel time for this week. Otherwise, it is not
        /// set.
        /// </summary>
        [JsonProperty(PropertyName = "travelTime")]
        public double? TravelTime { get; set; }

        /// <summary>
        /// Gets or sets total service time for that week.
        /// </summary>
        [JsonProperty(PropertyName = "serviceTime")]
        public double? ServiceTime { get; set; }

    }
}
