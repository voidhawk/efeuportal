// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("UnspecifiedMapDataLimitation")]
    public partial class UnspecifiedMapDataLimitation : ResultLimitation
    {
        /// <summary>
        /// Initializes a new instance of the UnspecifiedMapDataLimitation
        /// class.
        /// </summary>
        public UnspecifiedMapDataLimitation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the UnspecifiedMapDataLimitation
        /// class.
        /// </summary>
        /// <param name="message">Description of the limitation.</param>
        /// <param name="index">Index of segment with wrong map data.</param>
        /// <param name="path">XPath-like identification of the field
        /// containing wrong data.</param>
        /// <param name="hint">Hint on how to resolve this issue.</param>
        public UnspecifiedMapDataLimitation(string message, int index, string path, string hint = default(string))
            : base(message, hint)
        {
            Index = index;
            Path = path;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets index of segment with wrong map data.
        /// </summary>
        [JsonProperty(PropertyName = "index")]
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets xPath-like identification of the field containing
        /// wrong data.
        /// </summary>
        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
            if (Path == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Path");
            }
        }
    }
}
