// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("RejectedRequestFault")]
    public partial class RejectedRequestFault : XServerFault
    {
        /// <summary>
        /// Initializes a new instance of the RejectedRequestFault class.
        /// </summary>
        public RejectedRequestFault()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the RejectedRequestFault class.
        /// </summary>
        /// <param name="hint">A free-text hint what to do to fix the
        /// problem.</param>
        public RejectedRequestFault(string hint = default(string))
            : base(hint)
        {
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

    }
}
