// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Represents a single Feature Layer theme reflecting the current state of
    /// the country or subdivision. Dynamic Feature Layer themes may be added,
    /// updated, or removed at any time. Theme IDs may occur more than once
    /// when the theme consists of data from more than one provider. Properties
    /// valid for the whole map can be found in FeatureLayerTheme.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xruntime.RegionFeatureLayerTheme
    /// </summary>
    public partial class RegionFeatureLayerTheme
    {
        /// <summary>
        /// Initializes a new instance of the RegionFeatureLayerTheme class.
        /// </summary>
        public RegionFeatureLayerTheme()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the RegionFeatureLayerTheme class.
        /// </summary>
        /// <param name="themeId">The identifier of the theme.</param>
        /// <param name="providerName">The name of the data provider of this
        /// Feature Layer theme if other than the reference provider name. It
        /// is not necessary equal to the name of the data provider of the
        /// related map data. Especially traffic incidents data may have other
        /// sources than the map data provider. If a theme consists of data
        /// from more than one provider, there will be multiple instances of
        /// this object.</param>
        /// <param name="referenceProviderName">The provider name of the map
        /// data this Feature Layer theme is based on. For best results the
        /// Feature Layer data should match the routing data of the underlying
        /// map, therefore this name usually equals the corresponding
        /// RegionDescription.providerName.</param>
        /// <param name="referenceProviderDataVersion">The data version of the
        /// map data this Feature Layer theme is based on. For best results the
        /// Feature Layer data should match the routing data of the underlying
        /// map, therefore this version number usually equals the corresponding
        /// RegionDescription.providerDataVersion.</param>
        public RegionFeatureLayerTheme(string themeId = default(string), string providerName = default(string), string referenceProviderName = default(string), string referenceProviderDataVersion = default(string))
        {
            ThemeId = themeId;
            ProviderName = providerName;
            ReferenceProviderName = referenceProviderName;
            ReferenceProviderDataVersion = referenceProviderDataVersion;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the identifier of the theme.
        /// </summary>
        [JsonProperty(PropertyName = "themeId")]
        public string ThemeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the data provider of this Feature Layer
        /// theme if other than the reference provider name. It is not
        /// necessary equal to the name of the data provider of the related map
        /// data. Especially traffic incidents data may have other sources than
        /// the map data provider. If a theme consists of data from more than
        /// one provider, there will be multiple instances of this object.
        /// </summary>
        [JsonProperty(PropertyName = "providerName")]
        public string ProviderName { get; set; }

        /// <summary>
        /// Gets or sets the provider name of the map data this Feature Layer
        /// theme is based on. For best results the Feature Layer data should
        /// match the routing data of the underlying map, therefore this name
        /// usually equals the corresponding RegionDescription.providerName.
        /// </summary>
        [JsonProperty(PropertyName = "referenceProviderName")]
        public string ReferenceProviderName { get; set; }

        /// <summary>
        /// Gets or sets the data version of the map data this Feature Layer
        /// theme is based on. For best results the Feature Layer data should
        /// match the routing data of the underlying map, therefore this
        /// version number usually equals the corresponding
        /// RegionDescription.providerDataVersion.
        /// </summary>
        [JsonProperty(PropertyName = "referenceProviderDataVersion")]
        public string ReferenceProviderDataVersion { get; set; }

    }
}
