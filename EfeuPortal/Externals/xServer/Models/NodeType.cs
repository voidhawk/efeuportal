// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Runtime;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines values for NodeType.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NodeType
    {
        [EnumMember(Value = "NETWORK_NODE")]
        NETWORKNODE,
        [EnumMember(Value = "LINK_NODE")]
        LINKNODE,
        [EnumMember(Value = "INPUT_NODE")]
        INPUTNODE
    }
    internal static class NodeTypeEnumExtension
    {
        internal static string ToSerializedValue(this NodeType? value)
        {
            return value == null ? null : ((NodeType)value).ToSerializedValue();
        }

        internal static string ToSerializedValue(this NodeType value)
        {
            switch( value )
            {
                case NodeType.NETWORKNODE:
                    return "NETWORK_NODE";
                case NodeType.LINKNODE:
                    return "LINK_NODE";
                case NodeType.INPUTNODE:
                    return "INPUT_NODE";
            }
            return null;
        }

        internal static NodeType? ParseNodeType(this string value)
        {
            switch( value )
            {
                case "NETWORK_NODE":
                    return NodeType.NETWORKNODE;
                case "LINK_NODE":
                    return NodeType.LINKNODE;
                case "INPUT_NODE":
                    return NodeType.INPUTNODE;
            }
            return null;
        }
    }
}
