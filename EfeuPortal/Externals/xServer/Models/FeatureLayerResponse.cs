// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("FeatureLayerResponse")]
    public partial class FeatureLayerResponse : ResponseBase
    {
        /// <summary>
        /// Initializes a new instance of the FeatureLayerResponse class.
        /// </summary>
        public FeatureLayerResponse()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the FeatureLayerResponse class.
        /// </summary>
        /// <param name="binaryFeatureLayer">The binary representation of the
        /// Feature Layer, returned as an encoded string.</param>
        /// <param name="featureLayerDescription">Meta information that
        /// describes the current Feature Layer content.</param>
        public FeatureLayerResponse(IList<ResultLimitation> resultLimitations = default(IList<ResultLimitation>), string binaryFeatureLayer = default(string), FeatureLayerDescription featureLayerDescription = default(FeatureLayerDescription))
            : base(resultLimitations)
        {
            BinaryFeatureLayer = binaryFeatureLayer;
            FeatureLayerDescription = featureLayerDescription;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the binary representation of the Feature Layer,
        /// returned as an encoded string.
        /// </summary>
        [JsonProperty(PropertyName = "binaryFeatureLayer")]
        public string BinaryFeatureLayer { get; set; }

        /// <summary>
        /// Gets or sets meta information that describes the current Feature
        /// Layer content.
        /// </summary>
        [JsonProperty(PropertyName = "featureLayerDescription")]
        public FeatureLayerDescription FeatureLayerDescription { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (FeatureLayerDescription != null)
            {
                FeatureLayerDescription.Validate();
            }
        }
    }
}
