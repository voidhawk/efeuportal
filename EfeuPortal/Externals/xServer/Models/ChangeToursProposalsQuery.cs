// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// The abstract base type for all specified actions to propose
    /// ChangeToursActions.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xtour.ChangeToursProposalsQuery
    /// </summary>
    [Newtonsoft.Json.JsonObject("ChangeToursProposalsQuery")]
    public partial class ChangeToursProposalsQuery
    {
        /// <summary>
        /// Initializes a new instance of the ChangeToursProposalsQuery class.
        /// </summary>
        public ChangeToursProposalsQuery()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ChangeToursProposalsQuery class.
        /// </summary>
        /// <param name="storedRequestId">ID of the stored PlanToursRequest
        /// that contains all information about the tours that could be changed
        /// as input plan. This request does not actually change the plan but
        /// just proposes changes. Thus, no new object is stored in the session
        /// storage.</param>
        public ChangeToursProposalsQuery(string storedRequestId)
        {
            StoredRequestId = storedRequestId;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets ID of the stored PlanToursRequest that contains all
        /// information about the tours that could be changed as input plan.
        /// This request does not actually change the plan but just proposes
        /// changes. Thus, no new object is stored in the session storage.
        /// </summary>
        [JsonProperty(PropertyName = "storedRequestId")]
        public string StoredRequestId { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (StoredRequestId == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "StoredRequestId");
            }
        }
    }
}
