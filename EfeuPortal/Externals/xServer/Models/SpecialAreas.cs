// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Defines the parameters concerning the special areas, such as urban
    /// segments, low emission zones, etc.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.routingprofile.SpecialAreas
    /// </summary>
    public partial class SpecialAreas
    {
        /// <summary>
        /// Initializes a new instance of the SpecialAreas class.
        /// </summary>
        public SpecialAreas()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the SpecialAreas class.
        /// </summary>
        /// <param name="residentsOnlyPenalty">The penalty for each segment
        /// within a residents only area. This setting affects segments which
        /// have set segments.RoadAttributes.residentsOnly.</param>
        /// <param name="urbanPenalty">The penalty for urban segments. A
        /// segment is treated as "urban" if the corresponding segment
        /// attribute is set and the network class is larger than or equal to
        /// minimumUrbanNetworkClass. This setting affects segments which have
        /// set segments.RoadAttributes.urban.</param>
        /// <param name="minimumUrbanNetworkClass">The minimum allowed urban
        /// network class</param>
        /// <param name="forbiddenLowEmissionZonePenalty">The penalty for
        /// segments that are forbidden for the current
        /// vehicleprofile.Engine.lowEmissionZoneTypes of the vehicle. This
        /// setting affects segments which do not match
        /// segments.RoadAttributes.lowEmissionZoneType.</param>
        /// <param name="deliveryOnlyPenalty">The penalty for delivery only
        /// segments that are allowed because
        /// vehicleprofile.LegalCondition.isDelivery is enabled. This setting
        /// affects segments which have set
        /// segments.RoadAttributes.deliveryOnly.</param>
        /// <param name="deliveryOnlyGateCost">The routing cost for gates that
        /// have are allowed to pass because
        /// vehicleprofile.LegalCondition.isDelivery is enabled. In contrast to
        /// a prohibited segment a gate is located at a specific point of the
        /// road network and passing through that point is prohibited by the
        /// gate.</param>
        public SpecialAreas(int? residentsOnlyPenalty = default(int?), int? urbanPenalty = default(int?), int? minimumUrbanNetworkClass = default(int?), int? forbiddenLowEmissionZonePenalty = default(int?), int? deliveryOnlyPenalty = default(int?), int? deliveryOnlyGateCost = default(int?))
        {
            ResidentsOnlyPenalty = residentsOnlyPenalty;
            UrbanPenalty = urbanPenalty;
            MinimumUrbanNetworkClass = minimumUrbanNetworkClass;
            ForbiddenLowEmissionZonePenalty = forbiddenLowEmissionZonePenalty;
            DeliveryOnlyPenalty = deliveryOnlyPenalty;
            DeliveryOnlyGateCost = deliveryOnlyGateCost;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the penalty for each segment within a residents only
        /// area. This setting affects segments which have set
        /// segments.RoadAttributes.residentsOnly.
        /// </summary>
        [JsonProperty(PropertyName = "residentsOnlyPenalty")]
        public int? ResidentsOnlyPenalty { get; set; }

        /// <summary>
        /// Gets or sets the penalty for urban segments. A segment is treated
        /// as "urban" if the corresponding segment attribute is set and the
        /// network class is larger than or equal to minimumUrbanNetworkClass.
        /// This setting affects segments which have set
        /// segments.RoadAttributes.urban.
        /// </summary>
        [JsonProperty(PropertyName = "urbanPenalty")]
        public int? UrbanPenalty { get; set; }

        /// <summary>
        /// Gets or sets the minimum allowed urban network class
        /// </summary>
        [JsonProperty(PropertyName = "minimumUrbanNetworkClass")]
        public int? MinimumUrbanNetworkClass { get; set; }

        /// <summary>
        /// Gets or sets the penalty for segments that are forbidden for the
        /// current vehicleprofile.Engine.lowEmissionZoneTypes of the vehicle.
        /// This setting affects segments which do not match
        /// segments.RoadAttributes.lowEmissionZoneType.
        /// </summary>
        [JsonProperty(PropertyName = "forbiddenLowEmissionZonePenalty")]
        public int? ForbiddenLowEmissionZonePenalty { get; set; }

        /// <summary>
        /// Gets or sets the penalty for delivery only segments that are
        /// allowed because vehicleprofile.LegalCondition.isDelivery is
        /// enabled. This setting affects segments which have set
        /// segments.RoadAttributes.deliveryOnly.
        /// </summary>
        [JsonProperty(PropertyName = "deliveryOnlyPenalty")]
        public int? DeliveryOnlyPenalty { get; set; }

        /// <summary>
        /// Gets or sets the routing cost for gates that have are allowed to
        /// pass because vehicleprofile.LegalCondition.isDelivery is enabled.
        /// In contrast to a prohibited segment a gate is located at a specific
        /// point of the road network and passing through that point is
        /// prohibited by the gate.
        /// </summary>
        [JsonProperty(PropertyName = "deliveryOnlyGateCost")]
        public int? DeliveryOnlyGateCost { get; set; }

    }
}
