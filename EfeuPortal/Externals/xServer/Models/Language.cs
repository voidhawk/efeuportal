// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Specifies a language by its code and its name.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.types.Language
    /// </summary>
    public partial class Language
    {
        /// <summary>
        /// Initializes a new instance of the Language class.
        /// </summary>
        public Language()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Language class.
        /// </summary>
        /// <param name="code">The language code.</param>
        /// <param name="name">The language name in the respective
        /// language.</param>
        public Language(string code = default(string), string name = default(string))
        {
            Code = code;
            Name = name;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the language code.
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the language name in the respective language.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

    }
}
