// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Defines the root type for request profiles. It contains all available
    /// profiles and holds global parameters like language settings.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.profile.RequestProfile
    /// </summary>
    public partial class RequestProfile
    {
        /// <summary>
        /// Initializes a new instance of the RequestProfile class.
        /// </summary>
        public RequestProfile()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the RequestProfile class.
        /// </summary>
        /// <param name="vehicleProfile">The vehicle profile.</param>
        /// <param name="featureLayerProfile">The Feature Layer
        /// profile.</param>
        /// <param name="routingProfile">The routing profile.</param>
        /// <param name="renderingProfile">The rendering profile.</param>
        /// <param name="clusterPlanningProfile">The cluster planning
        /// profile.</param>
        /// <param name="mapLanguage">The language used for geographic names
        /// that are part of the map. The default x-ptv-DFT means that names
        /// are given in the language spoken in that country or region.</param>
        /// <param name="userLanguage">The language of texts such as maneuver
        /// or traffic-incident descriptions. If the specified language is not
        /// supported, the following fallback languages are used: First, if
        /// subtags are present, the primary language is used. Second, English
        /// is used. As an example pt-BR → pt → en. The language of geographic
        /// names can be set by the field mapLanguage. As an example the
        /// description of a maneuver should be readable by the user but city
        /// names which can be found on local signs should be available in that
        /// language in order to be recognized.</param>
        public RequestProfile(VehicleProfile vehicleProfile = default(VehicleProfile), FeatureLayerProfile featureLayerProfile = default(FeatureLayerProfile), RoutingProfile routingProfile = default(RoutingProfile), RenderingProfile renderingProfile = default(RenderingProfile), ClusterPlanningProfile clusterPlanningProfile = default(ClusterPlanningProfile), string mapLanguage = default(string), string userLanguage = default(string))
        {
            VehicleProfile = vehicleProfile;
            FeatureLayerProfile = featureLayerProfile;
            RoutingProfile = routingProfile;
            RenderingProfile = renderingProfile;
            ClusterPlanningProfile = clusterPlanningProfile;
            MapLanguage = mapLanguage;
            UserLanguage = userLanguage;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the vehicle profile.
        /// </summary>
        [JsonProperty(PropertyName = "vehicleProfile")]
        public VehicleProfile VehicleProfile { get; set; }

        /// <summary>
        /// Gets or sets the Feature Layer profile.
        /// </summary>
        [JsonProperty(PropertyName = "featureLayerProfile")]
        public FeatureLayerProfile FeatureLayerProfile { get; set; }

        /// <summary>
        /// Gets or sets the routing profile.
        /// </summary>
        [JsonProperty(PropertyName = "routingProfile")]
        public RoutingProfile RoutingProfile { get; set; }

        /// <summary>
        /// Gets or sets the rendering profile.
        /// </summary>
        [JsonProperty(PropertyName = "renderingProfile")]
        public RenderingProfile RenderingProfile { get; set; }

        /// <summary>
        /// Gets or sets the cluster planning profile.
        /// </summary>
        [JsonProperty(PropertyName = "clusterPlanningProfile")]
        public ClusterPlanningProfile ClusterPlanningProfile { get; set; }

        /// <summary>
        /// Gets or sets the language used for geographic names that are part
        /// of the map. The default x-ptv-DFT means that names are given in the
        /// language spoken in that country or region.
        /// </summary>
        [JsonProperty(PropertyName = "mapLanguage")]
        public string MapLanguage { get; set; }

        /// <summary>
        /// Gets or sets the language of texts such as maneuver or
        /// traffic-incident descriptions. If the specified language is not
        /// supported, the following fallback languages are used: First, if
        /// subtags are present, the primary language is used. Second, English
        /// is used. As an example pt-BR → pt → en. The language of geographic
        /// names can be set by the field mapLanguage. As an example the
        /// description of a maneuver should be readable by the user but city
        /// names which can be found on local signs should be available in that
        /// language in order to be recognized.
        /// </summary>
        [JsonProperty(PropertyName = "userLanguage")]
        public string UserLanguage { get; set; }

    }
}
