// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("StartDurationInterval")]
    public partial class StartDurationInterval : Interval
    {
        /// <summary>
        /// Initializes a new instance of the StartDurationInterval class.
        /// </summary>
        public StartDurationInterval()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the StartDurationInterval class.
        /// </summary>
        /// <param name="start">The beginning of the time interval.</param>
        /// <param name="duration">The duration of the interval.</param>
        public StartDurationInterval(System.DateTime start, double duration)
        {
            Start = start;
            Duration = duration;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the beginning of the time interval.
        /// </summary>
        [JsonProperty(PropertyName = "start")]
        public System.DateTime Start { get; set; }

        /// <summary>
        /// Gets or sets the duration of the interval.
        /// </summary>
        [JsonProperty(PropertyName = "duration")]
        public double Duration { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            //Nothing to validate
        }
    }
}
