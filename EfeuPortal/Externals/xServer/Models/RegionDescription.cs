// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Description of a country or a subdivision.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xruntime.RegionDescription
    /// </summary>
    public partial class RegionDescription
    {
        /// <summary>
        /// Initializes a new instance of the RegionDescription class.
        /// </summary>
        public RegionDescription()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the RegionDescription class.
        /// </summary>
        /// <param name="providerName">The name of the data provider, e.g.
        /// TomTom or HERE.</param>
        /// <param name="providerDataVersion">The version number of the data
        /// given by the provider. It is not equal to the version number of the
        /// PTV map.</param>
        /// <param name="detailLevel">The detail level of the country or
        /// subdivision. According to this level, some data may not be
        /// available. Possible values include: 'DETAILED', 'EXTRACTED',
        /// 'TRANSIT', 'MIXED', 'UNSPECIFIED'</param>
        public RegionDescription(string providerName = default(string), string providerDataVersion = default(string), MapDetailLevel? detailLevel = default(MapDetailLevel?))
        {
            ProviderName = providerName;
            ProviderDataVersion = providerDataVersion;
            DetailLevel = detailLevel;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the name of the data provider, e.g. TomTom or HERE.
        /// </summary>
        [JsonProperty(PropertyName = "providerName")]
        public string ProviderName { get; set; }

        /// <summary>
        /// Gets or sets the version number of the data given by the provider.
        /// It is not equal to the version number of the PTV map.
        /// </summary>
        [JsonProperty(PropertyName = "providerDataVersion")]
        public string ProviderDataVersion { get; set; }

        /// <summary>
        /// Gets or sets the detail level of the country or subdivision.
        /// According to this level, some data may not be available. Possible
        /// values include: 'DETAILED', 'EXTRACTED', 'TRANSIT', 'MIXED',
        /// 'UNSPECIFIED'
        /// </summary>
        [JsonProperty(PropertyName = "detailLevel")]
        public MapDetailLevel? DetailLevel { get; set; }

    }
}
