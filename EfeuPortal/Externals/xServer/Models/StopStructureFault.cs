// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("StopStructureFault")]
    public partial class StopStructureFault : TripStructureFault
    {
        /// <summary>
        /// Initializes a new instance of the StopStructureFault class.
        /// </summary>
        public StopStructureFault()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the StopStructureFault class.
        /// </summary>
        /// <param name="vehicleId">ID of the vehicle serving the input tour
        /// with invalid structure in case of a PlanToursRequest. In case of a
        /// ChangeToursRequest this parameter contains the ID of the vehicle
        /// that would serve a tour with invalid structure.</param>
        /// <param name="tripId">ID of the input trip with invalid structure in
        /// case of a PlanToursRequest. In case of a ChangeToursRequest this
        /// parameter contains the ID of the trip that would have an invalid
        /// structure.</param>
        /// <param name="stopIndex">Index of the stop in stops of the trip
        /// containing the wrong structure.</param>
        /// <param name="hint">A free-text hint what to do to fix the
        /// problem.</param>
        public StopStructureFault(string vehicleId, string tripId, int stopIndex, string hint = default(string))
            : base(vehicleId, tripId, hint)
        {
            StopIndex = stopIndex;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets index of the stop in stops of the trip containing the
        /// wrong structure.
        /// </summary>
        [JsonProperty(PropertyName = "stopIndex")]
        public int StopIndex { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
        }
    }
}
