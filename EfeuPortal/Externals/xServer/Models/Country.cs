// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Base type for a country with or without subdivisions. Depending on how
    /// the data are built up a country can have subdivisions, e.g. a state in
    /// the US, or not.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xruntime.Country
    /// </summary>
    public partial class Country
    {
        /// <summary>
        /// Initializes a new instance of the Country class.
        /// </summary>
        public Country()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Country class.
        /// </summary>
        /// <param name="code">The country code.</param>
        /// <param name="description">Descriptive information about the
        /// country, only available if the country does not have
        /// subdivisions.</param>
        /// <param name="features">Lists the features which are available for
        /// this country, only available if the country does not have
        /// subdivisions.</param>
        public Country(string code = default(string), RegionDescription description = default(RegionDescription), RegionFeatures features = default(RegionFeatures), IList<Subdivision> subdivisions = default(IList<Subdivision>))
        {
            Code = code;
            Description = description;
            Features = features;
            Subdivisions = subdivisions;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets descriptive information about the country, only
        /// available if the country does not have subdivisions.
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public RegionDescription Description { get; set; }

        /// <summary>
        /// Gets or sets lists the features which are available for this
        /// country, only available if the country does not have subdivisions.
        /// </summary>
        [JsonProperty(PropertyName = "features")]
        public RegionFeatures Features { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "subdivisions")]
        public IList<Subdivision> Subdivisions { get; set; }

    }
}
