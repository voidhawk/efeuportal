// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Defines the state of a property that is relative to this country, i.e.
    /// allows a subsequent property to be enabled or disabled from its ID.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.featurelayerprofile.FeatureLayerProperty
    /// </summary>
    public partial class FeatureLayerProperty
    {
        /// <summary>
        /// Initializes a new instance of the FeatureLayerProperty class.
        /// </summary>
        public FeatureLayerProperty()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the FeatureLayerProperty class.
        /// </summary>
        /// <param name="id">The property ID. The special value "ALL" defines
        /// every property.</param>
        /// <param name="enabled">Enables or disables the property. If
        /// disabled, the rules that possess the property will be disabled as
        /// well.</param>
        public FeatureLayerProperty(string id, bool enabled)
        {
            Id = id;
            Enabled = enabled;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the property ID. The special value "ALL" defines every
        /// property.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets enables or disables the property. If disabled, the
        /// rules that possess the property will be disabled as well.
        /// </summary>
        [JsonProperty(PropertyName = "enabled")]
        public bool Enabled { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Id == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Id");
            }
        }
    }
}
