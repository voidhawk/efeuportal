// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Describes details on how and which segment a position was matched to.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xmatch.MatchedSegment
    /// </summary>
    public partial class MatchedSegment
    {
        /// <summary>
        /// Initializes a new instance of the MatchedSegment class.
        /// </summary>
        public MatchedSegment()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the MatchedSegment class.
        /// </summary>
        /// <param name="id">The Id of the matched segment.</param>
        /// <param name="polyline">The polyline of the matched segment</param>
        public MatchedSegment(string id, EncodedGeometry polyline)
        {
            Id = id;
            Polyline = polyline;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the Id of the matched segment.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the polyline of the matched segment
        /// </summary>
        [JsonProperty(PropertyName = "polyline")]
        public EncodedGeometry Polyline { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Id == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Id");
            }
            if (Polyline == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Polyline");
            }
            if (Polyline != null)
            {
                Polyline.Validate();
            }
        }
    }
}
