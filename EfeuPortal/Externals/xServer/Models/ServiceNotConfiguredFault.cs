// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("ServiceNotConfiguredFault")]
    public partial class ServiceNotConfiguredFault : RejectedRequestFault
    {
        /// <summary>
        /// Initializes a new instance of the ServiceNotConfiguredFault class.
        /// </summary>
        public ServiceNotConfiguredFault()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ServiceNotConfiguredFault class.
        /// </summary>
        /// <param name="service">The name of the service that is not
        /// configured.</param>
        /// <param name="hint">A free-text hint what to do to fix the
        /// problem.</param>
        public ServiceNotConfiguredFault(string service, string hint = default(string))
            : base(hint)
        {
            Service = service;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the name of the service that is not configured.
        /// </summary>
        [JsonProperty(PropertyName = "service")]
        public string Service { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Service == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Service");
            }
        }
    }
}
