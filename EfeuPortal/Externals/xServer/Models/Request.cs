// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents a single request that is currently executed or has been
    /// executed on this xserver.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xruntime.Request
    /// </summary>
    public partial class Request
    {
        /// <summary>
        /// Initializes a new instance of the Request class.
        /// </summary>
        public Request()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Request class.
        /// </summary>
        /// <param name="requestId">ID of this request.</param>
        /// <param name="serviceName">The used service.</param>
        /// <param name="methodName">The used method.</param>
        /// <param name="calculationTime">Calculation time of the
        /// request.</param>
        /// <param name="status">Status of this request. For example RUNNING or
        /// QUEUED. Possible values include: 'QUEUING', 'RUNNING', 'STOPPING',
        /// 'SUCCEEDED', 'FAILED', 'DELETED', 'UNKNOWN'</param>
        /// <param name="finished">Deprecated: inaccurate type assigned, use
        /// finishedAt instead.</param>
        /// <param name="finishedAt">Instant of time when the request
        /// ended.</param>
        public Request(string requestId, string serviceName, string methodName, long calculationTime, JobStatus? status = default(JobStatus?), IList<RequestInformation> requestInformation = default(IList<RequestInformation>), System.DateTime? finished = default(System.DateTime?), System.DateTime? finishedAt = default(System.DateTime?))
        {
            RequestId = requestId;
            ServiceName = serviceName;
            MethodName = methodName;
            Status = status;
            RequestInformation = requestInformation;
            Finished = finished;
            FinishedAt = finishedAt;
            CalculationTime = calculationTime;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets ID of this request.
        /// </summary>
        [JsonProperty(PropertyName = "requestId")]
        public string RequestId { get; set; }

        /// <summary>
        /// Gets or sets the used service.
        /// </summary>
        [JsonProperty(PropertyName = "serviceName")]
        public string ServiceName { get; set; }

        /// <summary>
        /// Gets or sets the used method.
        /// </summary>
        [JsonProperty(PropertyName = "methodName")]
        public string MethodName { get; set; }

        /// <summary>
        /// Gets or sets status of this request. For example RUNNING or QUEUED.
        /// Possible values include: 'QUEUING', 'RUNNING', 'STOPPING',
        /// 'SUCCEEDED', 'FAILED', 'DELETED', 'UNKNOWN'
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public JobStatus? Status { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "requestInformation")]
        public IList<RequestInformation> RequestInformation { get; set; }

        /// <summary>
        /// Gets or sets deprecated: inaccurate type assigned, use finishedAt
        /// instead.
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "finished")]
        public System.DateTime? Finished { get; set; }

        /// <summary>
        /// Gets or sets instant of time when the request ended.
        /// </summary>
        [JsonProperty(PropertyName = "finishedAt")]
        public System.DateTime? FinishedAt { get; set; }

        /// <summary>
        /// Gets or sets calculation time of the request.
        /// </summary>
        [JsonProperty(PropertyName = "calculationTime")]
        public long CalculationTime { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (RequestId == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "RequestId");
            }
            if (ServiceName == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "ServiceName");
            }
            if (MethodName == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "MethodName");
            }
            if (RequestInformation != null)
            {
                foreach (var element in RequestInformation)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
