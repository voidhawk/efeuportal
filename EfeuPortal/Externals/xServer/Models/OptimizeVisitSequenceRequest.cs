// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("OptimizeVisitSequenceRequest")]
    public partial class OptimizeVisitSequenceRequest : RequestBase
    {
        /// <summary>
        /// Initializes a new instance of the OptimizeVisitSequenceRequest
        /// class.
        /// </summary>
        public OptimizeVisitSequenceRequest()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the OptimizeVisitSequenceRequest
        /// class.
        /// </summary>
        /// <param name="distanceMode">Contains settings that specify how
        /// distances between locations are retrieved. When using direct
        /// distance the driven distances are estimated with a detour factor of
        /// 1.38 and for the duration a speed of 60 km/h is assumed. Only the
        /// following distance modes are supported: DirectDistance and
        /// ExistingDistanceMatrix.</param>
        /// <param name="scope">A user defined scope for persistent data like
        /// distance matrices.</param>
        /// <param name="storedProfile">The URL of the stored profile used by
        /// this request. The following types of URLs are supported:
        ///
        /// *  The name of the profile without its extension, e.g. `bicycle`.
        /// The corresponding file `bicycle.xml` must be available in the
        /// folder `conf/profiles`. Parent profiles must be located in the same
        /// folder. Profile names must not contain the special characters '-'
        /// and '+' and must not be called 'transport', 'labels', or
        /// 'background'.
        /// *  An http url, e.g. `http://localhost:8080/profiles/bicycle.xml`
        /// from which the profile must be available. In order to limit the
        /// access to outside servers, a whitelist can be configured in
        /// `xserver.conf` (see here). Parent profiles are not allowed for http
        /// urls.
        ///
        /// The URL is case-sensitive, although Microsoft Windows also accepts
        /// case-insensitive file names.</param>
        /// <param name="requestProfile">The profile used by this request
        /// (parameters override those from storedProfile).</param>
        /// <param name="coordinateFormat">The coordinate format of all
        /// geometries in request and response. The default of EPSG:4326
        /// represents WGS84. For more information see here.</param>
        /// <param name="geometryOptions">The target encodings of geometries,
        /// PLAIN if empty.</param>
        /// <param name="timeouts">timeout settings</param>
        /// <param name="startLocationId">If given, the location from
        /// OptimizeVisitSequenceRequest.locations with this ID is used as the
        /// start of the tour. It can be the same as
        /// OptimizeVisitSequenceRequest.endLocationId. If not given, the
        /// algorithm can freely choose the start of the tour from the given
        /// locations.</param>
        /// <param name="endLocationId">If given, the location from
        /// OptimizeVisitSequenceRequest.locations with this ID is used as the
        /// end of the tour. Can be the same as
        /// OptimizeVisitSequenceRequest.startLocationId. If not given, the
        /// algorithm can freely choose the end of the tour from the given
        /// locations.</param>
        /// <param name="optimizeVisitSequenceOptions">Additional parameters
        /// for visit sequence optimization.</param>
        public OptimizeVisitSequenceRequest(DistanceMode distanceMode, string scope = default(string), string storedProfile = default(string), RequestProfile requestProfile = default(RequestProfile), string coordinateFormat = default(string), GeometryOptions geometryOptions = default(GeometryOptions), Timeouts timeouts = default(Timeouts), IList<string> userLogs = default(IList<string>), IList<ClusterPlanningLocation> locations = default(IList<ClusterPlanningLocation>), string startLocationId = default(string), string endLocationId = default(string), OptimizeVisitSequenceOptions optimizeVisitSequenceOptions = default(OptimizeVisitSequenceOptions))
            : base(scope, storedProfile, requestProfile, coordinateFormat, geometryOptions, timeouts, userLogs)
        {
            Locations = locations;
            StartLocationId = startLocationId;
            EndLocationId = endLocationId;
            OptimizeVisitSequenceOptions = optimizeVisitSequenceOptions;
            DistanceMode = distanceMode;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "locations")]
        public IList<ClusterPlanningLocation> Locations { get; set; }

        /// <summary>
        /// Gets or sets if given, the location from
        /// OptimizeVisitSequenceRequest.locations with this ID is used as the
        /// start of the tour. It can be the same as
        /// OptimizeVisitSequenceRequest.endLocationId. If not given, the
        /// algorithm can freely choose the start of the tour from the given
        /// locations.
        /// </summary>
        [JsonProperty(PropertyName = "startLocationId")]
        public string StartLocationId { get; set; }

        /// <summary>
        /// Gets or sets if given, the location from
        /// OptimizeVisitSequenceRequest.locations with this ID is used as the
        /// end of the tour. Can be the same as
        /// OptimizeVisitSequenceRequest.startLocationId. If not given, the
        /// algorithm can freely choose the end of the tour from the given
        /// locations.
        /// </summary>
        [JsonProperty(PropertyName = "endLocationId")]
        public string EndLocationId { get; set; }

        /// <summary>
        /// Gets or sets additional parameters for visit sequence optimization.
        /// </summary>
        [JsonProperty(PropertyName = "optimizeVisitSequenceOptions")]
        public OptimizeVisitSequenceOptions OptimizeVisitSequenceOptions { get; set; }

        /// <summary>
        /// Gets or sets contains settings that specify how distances between
        /// locations are retrieved. When using direct distance the driven
        /// distances are estimated with a detour factor of 1.38 and for the
        /// duration a speed of 60 km/h is assumed. Only the following distance
        /// modes are supported: DirectDistance and ExistingDistanceMatrix.
        /// </summary>
        [JsonProperty(PropertyName = "distanceMode")]
        public DistanceMode DistanceMode { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (DistanceMode == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "DistanceMode");
            }
            if (Locations != null)
            {
                foreach (var element in Locations)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
