// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// A leg is defined by a start- and an end-waypoint and the segment (s)
    /// between them. It is guaranteed that a leg consists of at least one
    /// segment. It is also guaranteed that the leg of index *n* connects
    /// waypoints of indices *n* and *n+1*, i.e. the number of response
    /// waypoints equals the number of the list of legs plus 1.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xroute.Leg
    /// </summary>
    public partial class Leg
    {
        /// <summary>
        /// Initializes a new instance of the Leg class.
        /// </summary>
        public Leg()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Leg class.
        /// </summary>
        /// <param name="distance">The travel distance for the leg. It is the
        /// sum of distances of all segments in the leg and hence is a whole
        /// number without decimal places (see Segment.distance).</param>
        /// <param name="travelTime">The travel time for the leg. It is the sum
        /// of travel times of all segments in the leg and hence has up to
        /// three decimal places (see Segment.travelTime).</param>
        /// <param name="violated">If set to true, indicates that this leg
        /// contains a violation for the chosen vehicle.</param>
        /// <param name="startWaypointName">The name of the start waypoint of
        /// the leg. This user-defined name is available only if
        /// InputWaypoint.name for the corresponding input waypoint is
        /// set.</param>
        /// <param name="endWaypointName">The name of the end waypoint of the
        /// leg. This user-defined name is available only if InputWaypoint.name
        /// for the corresponding input waypoint is set.</param>
        /// <param name="startSegmentIndex">The index in the list of
        /// RouteResponse.segments of the start segment of the leg. It is
        /// available only if the list of segments is requested by
        /// ResultFields.segments.</param>
        /// <param name="endSegmentIndex">The index in the list of
        /// RouteResponse.segments of the end segment of the leg. It is
        /// available only if the list of segments is requested by
        /// ResultFields.segments.</param>
        /// <param name="startNodeIndex">The index in the list of
        /// RouteResponse.nodes of the start node of the leg. It is available
        /// only if the list of nodes is requested by
        /// ResultFields.nodes.</param>
        /// <param name="endNodeIndex">The index in the list of
        /// RouteResponse.nodes of the end node of the leg. It is available
        /// only if the list of nodes is requested by
        /// ResultFields.nodes.</param>
        /// <param name="startTollSectionIndex">The index in the list of
        /// Toll.sections of the first toll section of the leg. It is available
        /// only if the toll sections are requested in ResultFields.toll and if
        /// there is at least one toll section for the leg.</param>
        /// <param name="endTollSectionIndex">The index in the list of
        /// Toll.sections of the last toll section of the leg. It is available
        /// only if the toll sections are requested in ResultFields.toll and if
        /// there is at least one toll section for the leg.</param>
        /// <param name="tollSummary">The toll summary for the leg. The toll
        /// summary is only populated if requested by
        /// LegResultFields.tollSummary.</param>
        /// <param name="emissions">The emissions along the route.</param>
        /// <param name="polyline">The polyline of the leg. This polyline
        /// consists of all coordinates representing the leg and can be used to
        /// draw the leg onto a map. It is available only if requested by
        /// LegResultFields.polyline and it contains elevations only if
        /// requested by xroute.PolylineOptions.elevations. If the elevations
        /// data do not cover the complete polyline, an
        /// xroute.ElevationsNotAvailableLimitation will be available which
        /// states which parts of the polyline are not covered.</param>
        public Leg(double distance, double travelTime, bool violated, string startWaypointName = default(string), string endWaypointName = default(string), int? startSegmentIndex = default(int?), int? endSegmentIndex = default(int?), int? startNodeIndex = default(int?), int? endNodeIndex = default(int?), int? startTollSectionIndex = default(int?), int? endTollSectionIndex = default(int?), TollSummary tollSummary = default(TollSummary), Emissions emissions = default(Emissions), EncodedGeometry polyline = default(EncodedGeometry))
        {
            StartWaypointName = startWaypointName;
            EndWaypointName = endWaypointName;
            StartSegmentIndex = startSegmentIndex;
            EndSegmentIndex = endSegmentIndex;
            StartNodeIndex = startNodeIndex;
            EndNodeIndex = endNodeIndex;
            StartTollSectionIndex = startTollSectionIndex;
            EndTollSectionIndex = endTollSectionIndex;
            Distance = distance;
            TravelTime = travelTime;
            TollSummary = tollSummary;
            Emissions = emissions;
            Polyline = polyline;
            Violated = violated;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the name of the start waypoint of the leg. This
        /// user-defined name is available only if InputWaypoint.name for the
        /// corresponding input waypoint is set.
        /// </summary>
        [JsonProperty(PropertyName = "startWaypointName")]
        public string StartWaypointName { get; set; }

        /// <summary>
        /// Gets or sets the name of the end waypoint of the leg. This
        /// user-defined name is available only if InputWaypoint.name for the
        /// corresponding input waypoint is set.
        /// </summary>
        [JsonProperty(PropertyName = "endWaypointName")]
        public string EndWaypointName { get; set; }

        /// <summary>
        /// Gets or sets the index in the list of RouteResponse.segments of the
        /// start segment of the leg. It is available only if the list of
        /// segments is requested by ResultFields.segments.
        /// </summary>
        [JsonProperty(PropertyName = "startSegmentIndex")]
        public int? StartSegmentIndex { get; set; }

        /// <summary>
        /// Gets or sets the index in the list of RouteResponse.segments of the
        /// end segment of the leg. It is available only if the list of
        /// segments is requested by ResultFields.segments.
        /// </summary>
        [JsonProperty(PropertyName = "endSegmentIndex")]
        public int? EndSegmentIndex { get; set; }

        /// <summary>
        /// Gets or sets the index in the list of RouteResponse.nodes of the
        /// start node of the leg. It is available only if the list of nodes is
        /// requested by ResultFields.nodes.
        /// </summary>
        [JsonProperty(PropertyName = "startNodeIndex")]
        public int? StartNodeIndex { get; set; }

        /// <summary>
        /// Gets or sets the index in the list of RouteResponse.nodes of the
        /// end node of the leg. It is available only if the list of nodes is
        /// requested by ResultFields.nodes.
        /// </summary>
        [JsonProperty(PropertyName = "endNodeIndex")]
        public int? EndNodeIndex { get; set; }

        /// <summary>
        /// Gets or sets the index in the list of Toll.sections of the first
        /// toll section of the leg. It is available only if the toll sections
        /// are requested in ResultFields.toll and if there is at least one
        /// toll section for the leg.
        /// </summary>
        [JsonProperty(PropertyName = "startTollSectionIndex")]
        public int? StartTollSectionIndex { get; set; }

        /// <summary>
        /// Gets or sets the index in the list of Toll.sections of the last
        /// toll section of the leg. It is available only if the toll sections
        /// are requested in ResultFields.toll and if there is at least one
        /// toll section for the leg.
        /// </summary>
        [JsonProperty(PropertyName = "endTollSectionIndex")]
        public int? EndTollSectionIndex { get; set; }

        /// <summary>
        /// Gets or sets the travel distance for the leg. It is the sum of
        /// distances of all segments in the leg and hence is a whole number
        /// without decimal places (see Segment.distance).
        /// </summary>
        [JsonProperty(PropertyName = "distance")]
        public double Distance { get; set; }

        /// <summary>
        /// Gets or sets the travel time for the leg. It is the sum of travel
        /// times of all segments in the leg and hence has up to three decimal
        /// places (see Segment.travelTime).
        /// </summary>
        [JsonProperty(PropertyName = "travelTime")]
        public double TravelTime { get; set; }

        /// <summary>
        /// Gets or sets the toll summary for the leg. The toll summary is only
        /// populated if requested by LegResultFields.tollSummary.
        /// </summary>
        [JsonProperty(PropertyName = "tollSummary")]
        public TollSummary TollSummary { get; set; }

        /// <summary>
        /// Gets or sets the emissions along the route.
        /// </summary>
        [JsonProperty(PropertyName = "emissions")]
        public Emissions Emissions { get; set; }

        /// <summary>
        /// Gets or sets the polyline of the leg. This polyline consists of all
        /// coordinates representing the leg and can be used to draw the leg
        /// onto a map. It is available only if requested by
        /// LegResultFields.polyline and it contains elevations only if
        /// requested by xroute.PolylineOptions.elevations. If the elevations
        /// data do not cover the complete polyline, an
        /// xroute.ElevationsNotAvailableLimitation will be available which
        /// states which parts of the polyline are not covered.
        /// </summary>
        [JsonProperty(PropertyName = "polyline")]
        public EncodedGeometry Polyline { get; set; }

        /// <summary>
        /// Gets or sets if set to true, indicates that this leg contains a
        /// violation for the chosen vehicle.
        /// </summary>
        [JsonProperty(PropertyName = "violated")]
        public bool Violated { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Polyline != null)
            {
                Polyline.Validate();
            }
        }
    }
}
