// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Runtime;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines values for WeeklyOnDutyHoursLimitUSFMCSA3952013.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum WeeklyOnDutyHoursLimitUSFMCSA3952013
    {
        [EnumMember(Value = "SEVEN_DAYS_SIXTY_HOURS_ON_DUTY")]
        SEVENDAYSSIXTYHOURSONDUTY,
        [EnumMember(Value = "EIGHT_DAYS_SEVENTY_HOURS_ON_DUTY")]
        EIGHTDAYSSEVENTYHOURSONDUTY
    }
    internal static class WeeklyOnDutyHoursLimitUSFMCSA3952013EnumExtension
    {
        internal static string ToSerializedValue(this WeeklyOnDutyHoursLimitUSFMCSA3952013? value)
        {
            return value == null ? null : ((WeeklyOnDutyHoursLimitUSFMCSA3952013)value).ToSerializedValue();
        }

        internal static string ToSerializedValue(this WeeklyOnDutyHoursLimitUSFMCSA3952013 value)
        {
            switch( value )
            {
                case WeeklyOnDutyHoursLimitUSFMCSA3952013.SEVENDAYSSIXTYHOURSONDUTY:
                    return "SEVEN_DAYS_SIXTY_HOURS_ON_DUTY";
                case WeeklyOnDutyHoursLimitUSFMCSA3952013.EIGHTDAYSSEVENTYHOURSONDUTY:
                    return "EIGHT_DAYS_SEVENTY_HOURS_ON_DUTY";
            }
            return null;
        }

        internal static WeeklyOnDutyHoursLimitUSFMCSA3952013? ParseWeeklyOnDutyHoursLimitUSFMCSA3952013(this string value)
        {
            switch( value )
            {
                case "SEVEN_DAYS_SIXTY_HOURS_ON_DUTY":
                    return WeeklyOnDutyHoursLimitUSFMCSA3952013.SEVENDAYSSIXTYHOURSONDUTY;
                case "EIGHT_DAYS_SEVENTY_HOURS_ON_DUTY":
                    return WeeklyOnDutyHoursLimitUSFMCSA3952013.EIGHTDAYSSEVENTYHOURSONDUTY;
            }
            return null;
        }
    }
}
