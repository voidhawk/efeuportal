// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("OffRoadWaypoint")]
    public partial class OffRoadWaypoint : InputWaypoint
    {
        /// <summary>
        /// Initializes a new instance of the OffRoadWaypoint class.
        /// </summary>
        public OffRoadWaypoint()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the OffRoadWaypoint class.
        /// </summary>
        /// <param name="location">Specifies the route location for this
        /// waypoint. The given routing.OffRoadRouteLocation.offRoadCoordinate
        /// is part of the route polyline. A route actually reaches this
        /// coordinate and is connected to the road network by a segment of
        /// type segments.SegmentType.LINK\_SEGMENT. The
        /// routing.OffRoadRouteLocation.roadAccessCoordinate is usually not
        /// part of the route polyline, as it is only used to find the proper
        /// road to link to.</param>
        /// <param name="name">User-defined content to identify the waypoint in
        /// route response elements like Leg. Make sure that this name is
        /// unique even if a waypoint is visited more than once.</param>
        /// <param name="tourStopOptions">The logistic information on a
        /// waypoint.</param>
        public OffRoadWaypoint(OffRoadRouteLocation location, string name = default(string), TourStopOptions tourStopOptions = default(TourStopOptions))
            : base(name)
        {
            Location = location;
            TourStopOptions = tourStopOptions;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets specifies the route location for this waypoint. The
        /// given routing.OffRoadRouteLocation.offRoadCoordinate is part of the
        /// route polyline. A route actually reaches this coordinate and is
        /// connected to the road network by a segment of type
        /// segments.SegmentType.LINK\_SEGMENT. The
        /// routing.OffRoadRouteLocation.roadAccessCoordinate is usually not
        /// part of the route polyline, as it is only used to find the proper
        /// road to link to.
        /// </summary>
        [JsonProperty(PropertyName = "location")]
        public OffRoadRouteLocation Location { get; set; }

        /// <summary>
        /// Gets or sets the logistic information on a waypoint.
        /// </summary>
        [JsonProperty(PropertyName = "tourStopOptions")]
        public TourStopOptions TourStopOptions { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Location == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Location");
            }
            if (Location != null)
            {
                Location.Validate();
            }
        }
    }
}
