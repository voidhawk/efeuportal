// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("SubdivisionFallbackLimitation")]
    public partial class SubdivisionFallbackLimitation : ResultLimitation
    {
        /// <summary>
        /// Initializes a new instance of the SubdivisionFallbackLimitation
        /// class.
        /// </summary>
        public SubdivisionFallbackLimitation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the SubdivisionFallbackLimitation
        /// class.
        /// </summary>
        /// <param name="message">Description of the limitation.</param>
        /// <param name="requestedSubdivisionCode">The country subdivision code
        /// that was requested in allowedCountries, but cannot be used.</param>
        /// <param name="fallbackCountryCode">The enclosing country that is
        /// used as a fallback instead.</param>
        /// <param name="hint">Hint on how to resolve this issue.</param>
        public SubdivisionFallbackLimitation(string message, string requestedSubdivisionCode, string fallbackCountryCode, string hint = default(string))
            : base(message, hint)
        {
            RequestedSubdivisionCode = requestedSubdivisionCode;
            FallbackCountryCode = fallbackCountryCode;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the country subdivision code that was requested in
        /// allowedCountries, but cannot be used.
        /// </summary>
        [JsonProperty(PropertyName = "requestedSubdivisionCode")]
        public string RequestedSubdivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the enclosing country that is used as a fallback
        /// instead.
        /// </summary>
        [JsonProperty(PropertyName = "fallbackCountryCode")]
        public string FallbackCountryCode { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
            if (RequestedSubdivisionCode == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "RequestedSubdivisionCode");
            }
            if (FallbackCountryCode == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "FallbackCountryCode");
            }
        }
    }
}
