// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Represents a location that should be assigned to a cluster.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xcluster.PlanningLocation
    /// </summary>
    public partial class PlanningLocation
    {
        /// <summary>
        /// Initializes a new instance of the PlanningLocation class.
        /// </summary>
        public PlanningLocation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the PlanningLocation class.
        /// </summary>
        /// <param name="location">The geographic location that is represented
        /// by this planning location.</param>
        /// <param name="activity">Represents an absolute value of constant
        /// activity. Activity values of all locations in a cluster are
        /// accumulated to the totalActivity of the cluster. Arbitrary
        /// numerical values can be used as activity measure. Typical examples
        /// are sales figures, enterprise sizes, or population figures. If
        /// estimated tour durations should be balanced, activities can be
        /// considered as service times in seconds that have to be done at the
        /// corresponding location. In this case,
        /// considerActivitiesAsServiceTimes has to be enabled. The resulting
        /// tour duration of a cluster consists of the sum of given service
        /// times and estimated driving times.</param>
        /// <param name="groupId">Unique identifier of a group of locations
        /// this location belongs to. Locations that are in the same group will
        /// be assigned to the same cluster.</param>
        /// <param name="clusterCompatibility">Either compatible or
        /// incompatible clusters for this location. A compatibility list
        /// either contains only compatible clusters or only incompatible
        /// clusters for this location. Setting only one compatible cluster,
        /// fixes the location to this cluster.</param>
        public PlanningLocation(ClusterPlanningLocation location, double activity, string groupId = default(string), ClusterCompatibility clusterCompatibility = default(ClusterCompatibility))
        {
            Location = location;
            Activity = activity;
            GroupId = groupId;
            ClusterCompatibility = clusterCompatibility;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the geographic location that is represented by this
        /// planning location.
        /// </summary>
        [JsonProperty(PropertyName = "location")]
        public ClusterPlanningLocation Location { get; set; }

        /// <summary>
        /// Gets or sets represents an absolute value of constant activity.
        /// Activity values of all locations in a cluster are accumulated to
        /// the totalActivity of the cluster. Arbitrary numerical values can be
        /// used as activity measure. Typical examples are sales figures,
        /// enterprise sizes, or population figures. If estimated tour
        /// durations should be balanced, activities can be considered as
        /// service times in seconds that have to be done at the corresponding
        /// location. In this case, considerActivitiesAsServiceTimes has to be
        /// enabled. The resulting tour duration of a cluster consists of the
        /// sum of given service times and estimated driving times.
        /// </summary>
        [JsonProperty(PropertyName = "activity")]
        public double Activity { get; set; }

        /// <summary>
        /// Gets or sets unique identifier of a group of locations this
        /// location belongs to. Locations that are in the same group will be
        /// assigned to the same cluster.
        /// </summary>
        [JsonProperty(PropertyName = "groupId")]
        public string GroupId { get; set; }

        /// <summary>
        /// Gets or sets either compatible or incompatible clusters for this
        /// location. A compatibility list either contains only compatible
        /// clusters or only incompatible clusters for this location. Setting
        /// only one compatible cluster, fixes the location to this cluster.
        /// </summary>
        [JsonProperty(PropertyName = "clusterCompatibility")]
        public ClusterCompatibility ClusterCompatibility { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Location == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Location");
            }
            if (Location != null)
            {
                Location.Validate();
            }
            if (ClusterCompatibility != null)
            {
                ClusterCompatibility.Validate();
            }
        }
    }
}
