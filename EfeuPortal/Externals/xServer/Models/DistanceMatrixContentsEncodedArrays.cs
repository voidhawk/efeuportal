// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("DistanceMatrixContentsEncodedArrays")]
    public partial class DistanceMatrixContentsEncodedArrays : DistanceMatrixContents
    {
        /// <summary>
        /// Initializes a new instance of the
        /// DistanceMatrixContentsEncodedArrays class.
        /// </summary>
        public DistanceMatrixContentsEncodedArrays()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the
        /// DistanceMatrixContentsEncodedArrays class.
        /// </summary>
        /// <param name="distances">The distances of the requested matrix
        /// elements expressed in meters.</param>
        /// <param name="travelTimes">The travel times of the requested matrix
        /// elements expressed in milliseconds.</param>
        /// <param name="violated">The violated flags of the requested matrix
        /// elements.</param>
        /// <param name="estimatedByDirectDistance">The estimated by direct
        /// distance flags of the requested matrix elements.</param>
        /// <param name="tollCosts">The toll costs of the requested matrix
        /// elements in the currency that has been provided in the options of
        /// the corresponding xdima.CreateDistanceMatrixRequest.</param>
        public DistanceMatrixContentsEncodedArrays(byte[] distances = default(byte[]), byte[] travelTimes = default(byte[]), byte[] violated = default(byte[]), byte[] estimatedByDirectDistance = default(byte[]), byte[] tollCosts = default(byte[]))
        {
            Distances = distances;
            TravelTimes = travelTimes;
            Violated = violated;
            EstimatedByDirectDistance = estimatedByDirectDistance;
            TollCosts = tollCosts;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the distances of the requested matrix elements
        /// expressed in meters.
        /// </summary>
        [JsonProperty(PropertyName = "distances")]
        public byte[] Distances { get; set; }

        /// <summary>
        /// Gets or sets the travel times of the requested matrix elements
        /// expressed in milliseconds.
        /// </summary>
        [JsonProperty(PropertyName = "travelTimes")]
        public byte[] TravelTimes { get; set; }

        /// <summary>
        /// Gets or sets the violated flags of the requested matrix elements.
        /// </summary>
        [JsonProperty(PropertyName = "violated")]
        public byte[] Violated { get; set; }

        /// <summary>
        /// Gets or sets the estimated by direct distance flags of the
        /// requested matrix elements.
        /// </summary>
        [JsonProperty(PropertyName = "estimatedByDirectDistance")]
        public byte[] EstimatedByDirectDistance { get; set; }

        /// <summary>
        /// Gets or sets the toll costs of the requested matrix elements in the
        /// currency that has been provided in the options of the corresponding
        /// xdima.CreateDistanceMatrixRequest.
        /// </summary>
        [JsonProperty(PropertyName = "tollCosts")]
        public byte[] TollCosts { get; set; }

    }
}
