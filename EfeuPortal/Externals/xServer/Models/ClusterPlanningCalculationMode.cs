// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Runtime;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines values for ClusterPlanningCalculationMode.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ClusterPlanningCalculationMode
    {
        [EnumMember(Value = "PERFORMANCE")]
        PERFORMANCE,
        [EnumMember(Value = "QUALITY")]
        QUALITY
    }
    internal static class ClusterPlanningCalculationModeEnumExtension
    {
        internal static string ToSerializedValue(this ClusterPlanningCalculationMode? value)
        {
            return value == null ? null : ((ClusterPlanningCalculationMode)value).ToSerializedValue();
        }

        internal static string ToSerializedValue(this ClusterPlanningCalculationMode value)
        {
            switch( value )
            {
                case ClusterPlanningCalculationMode.PERFORMANCE:
                    return "PERFORMANCE";
                case ClusterPlanningCalculationMode.QUALITY:
                    return "QUALITY";
            }
            return null;
        }

        internal static ClusterPlanningCalculationMode? ParseClusterPlanningCalculationMode(this string value)
        {
            switch( value )
            {
                case "PERFORMANCE":
                    return ClusterPlanningCalculationMode.PERFORMANCE;
                case "QUALITY":
                    return ClusterPlanningCalculationMode.QUALITY;
            }
            return null;
        }
    }
}
