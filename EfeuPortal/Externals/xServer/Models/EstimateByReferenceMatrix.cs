// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("EstimateByReferenceMatrix")]
    public partial class EstimateByReferenceMatrix : DistanceMode
    {
        /// <summary>
        /// Initializes a new instance of the EstimateByReferenceMatrix class.
        /// </summary>
        public EstimateByReferenceMatrix()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the EstimateByReferenceMatrix class.
        /// </summary>
        /// <param name="id">The unique identifier of an existing distance
        /// matrix that contains every reference location of the reference
        /// location mappings. The ID is generated when the distance matrix is
        /// created. It is checked case sensitively.</param>
        public EstimateByReferenceMatrix(string id, IList<ReferenceLocationMapping> referenceLocationMappings = default(IList<ReferenceLocationMapping>))
        {
            Id = id;
            ReferenceLocationMappings = referenceLocationMappings;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the unique identifier of an existing distance matrix
        /// that contains every reference location of the reference location
        /// mappings. The ID is generated when the distance matrix is created.
        /// It is checked case sensitively.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "referenceLocationMappings")]
        public IList<ReferenceLocationMapping> ReferenceLocationMappings { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Id == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Id");
            }
            if (ReferenceLocationMappings != null)
            {
                foreach (var element in ReferenceLocationMappings)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
