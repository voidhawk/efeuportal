// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A scenario for the maximum quantities of goods of different kind.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xtour.MaximumQuantityScenario
    /// </summary>
    public partial class MaximumQuantityScenario
    {
        /// <summary>
        /// Initializes a new instance of the MaximumQuantityScenario class.
        /// </summary>
        public MaximumQuantityScenario()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the MaximumQuantityScenario class.
        /// </summary>
        public MaximumQuantityScenario(IList<double?> quantities = default(IList<double?>))
        {
            Quantities = quantities;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "quantities")]
        public IList<double?> Quantities { get; set; }

    }
}
