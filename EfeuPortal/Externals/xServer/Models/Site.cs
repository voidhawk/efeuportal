// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("Site")]
    public partial class Site : TourPlanningLocation
    {
        /// <summary>
        /// Initializes a new instance of the Site class.
        /// </summary>
        public Site()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Site class.
        /// </summary>
        /// <param name="id">A unique ID for this location. This ID can be used
        /// to reference the location from other elements, for example from
        /// orders or vehicles.</param>
        /// <param name="routeLocation">The exact map coordinate of this
        /// location.</param>
        /// <param name="serviceTimePerStop">Site dependent service time, for
        /// example to enter an area or to register at a customer. This service
        /// time will be considered for each stop that is performed at this
        /// site. Additionally there may be an order dependent or vehicle
        /// dependent service time of the stop. See Service time
        /// calculation.</param>
        /// <param
        /// name="ignoreVehicleDependentServiceTimeFactorForOrders">Indicates
        /// if vehicle dependent service time factors are relevant for this
        /// site, for example if the vehicle is unloaded by ramp staff. If this
        /// parameter is set to true, no vehicle dependent service times are
        /// taken into account for this site. See Service time
        /// calculation.</param>
        public Site(string id, RouteLocation routeLocation, IList<Interval> openingIntervals = default(IList<Interval>), double? serviceTimePerStop = default(double?), bool? ignoreVehicleDependentServiceTimeFactorForOrders = default(bool?))
            : base(id, routeLocation)
        {
            OpeningIntervals = openingIntervals;
            ServiceTimePerStop = serviceTimePerStop;
            IgnoreVehicleDependentServiceTimeFactorForOrders = ignoreVehicleDependentServiceTimeFactorForOrders;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "openingIntervals")]
        public IList<Interval> OpeningIntervals { get; set; }

        /// <summary>
        /// Gets or sets site dependent service time, for example to enter an
        /// area or to register at a customer. This service time will be
        /// considered for each stop that is performed at this site.
        /// Additionally there may be an order dependent or vehicle dependent
        /// service time of the stop. See Service time calculation.
        /// </summary>
        [JsonProperty(PropertyName = "serviceTimePerStop")]
        public double? ServiceTimePerStop { get; set; }

        /// <summary>
        /// Gets or sets indicates if vehicle dependent service time factors
        /// are relevant for this site, for example if the vehicle is unloaded
        /// by ramp staff. If this parameter is set to true, no vehicle
        /// dependent service times are taken into account for this site. See
        /// Service time calculation.
        /// </summary>
        [JsonProperty(PropertyName = "ignoreVehicleDependentServiceTimeFactorForOrders")]
        public bool? IgnoreVehicleDependentServiceTimeFactorForOrders { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
        }
    }
}
