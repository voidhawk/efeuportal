// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Specifies search options for segments.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xdata.SegmentsOptions
    /// </summary>
    public partial class SegmentsOptions
    {
        /// <summary>
        /// Initializes a new instance of the SegmentsOptions class.
        /// </summary>
        public SegmentsOptions()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the SegmentsOptions class.
        /// </summary>
        /// <param name="includedNetworkClasses">Specifies for which network
        /// classes the segments shall be included in the result. If not
        /// specified, the segments will be included whatever their network
        /// class.</param>
        public SegmentsOptions(IncludedNetworkClasses includedNetworkClasses = default(IncludedNetworkClasses), IList<SegmentType?> includedSegmentTypes = default(IList<SegmentType?>))
        {
            IncludedNetworkClasses = includedNetworkClasses;
            IncludedSegmentTypes = includedSegmentTypes;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets specifies for which network classes the segments shall
        /// be included in the result. If not specified, the segments will be
        /// included whatever their network class.
        /// </summary>
        [JsonProperty(PropertyName = "includedNetworkClasses")]
        public IncludedNetworkClasses IncludedNetworkClasses { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "includedSegmentTypes")]
        public IList<SegmentType?> IncludedSegmentTypes { get; set; }

    }
}
