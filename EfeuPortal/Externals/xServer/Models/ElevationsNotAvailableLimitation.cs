// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("ElevationsNotAvailableLimitation")]
    public partial class ElevationsNotAvailableLimitation : ResultLimitation
    {
        /// <summary>
        /// Initializes a new instance of the ElevationsNotAvailableLimitation
        /// class.
        /// </summary>
        public ElevationsNotAvailableLimitation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ElevationsNotAvailableLimitation
        /// class.
        /// </summary>
        /// <param name="message">Description of the limitation.</param>
        /// <param name="elevationsDataAvailable">Specifies whether elevation
        /// data are available at all. If true, the route is beyond the limits
        /// of the data, otherwise elevations could not be calculated, at
        /// all.</param>
        /// <param name="hint">Hint on how to resolve this issue.</param>
        public ElevationsNotAvailableLimitation(string message, bool elevationsDataAvailable, string hint = default(string), IList<IndexInterval> routePolylineIndices = default(IList<IndexInterval>), IList<IndexPairInterval> segmentPolylineIndices = default(IList<IndexPairInterval>), IList<IndexPairInterval> legPolylineIndices = default(IList<IndexPairInterval>))
            : base(message, hint)
        {
            RoutePolylineIndices = routePolylineIndices;
            SegmentPolylineIndices = segmentPolylineIndices;
            LegPolylineIndices = legPolylineIndices;
            ElevationsDataAvailable = elevationsDataAvailable;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "routePolylineIndices")]
        public IList<IndexInterval> RoutePolylineIndices { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "segmentPolylineIndices")]
        public IList<IndexPairInterval> SegmentPolylineIndices { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "legPolylineIndices")]
        public IList<IndexPairInterval> LegPolylineIndices { get; set; }

        /// <summary>
        /// Gets or sets specifies whether elevation data are available at all.
        /// If true, the route is beyond the limits of the data, otherwise
        /// elevations could not be calculated, at all.
        /// </summary>
        [JsonProperty(PropertyName = "elevationsDataAvailable")]
        public bool ElevationsDataAvailable { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
            if (RoutePolylineIndices != null)
            {
                foreach (var element in RoutePolylineIndices)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
            if (SegmentPolylineIndices != null)
            {
                foreach (var element1 in SegmentPolylineIndices)
                {
                    if (element1 != null)
                    {
                        element1.Validate();
                    }
                }
            }
            if (LegPolylineIndices != null)
            {
                foreach (var element2 in LegPolylineIndices)
                {
                    if (element2 != null)
                    {
                        element2.Validate();
                    }
                }
            }
        }
    }
}
