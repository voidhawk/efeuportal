// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("WaypointNotLinkedLimitation")]
    public partial class WaypointNotLinkedLimitation : ResultLimitation
    {
        /// <summary>
        /// Initializes a new instance of the WaypointNotLinkedLimitation
        /// class.
        /// </summary>
        public WaypointNotLinkedLimitation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the WaypointNotLinkedLimitation
        /// class.
        /// </summary>
        /// <param name="message">Description of the limitation.</param>
        /// <param name="hint">Hint on how to resolve this issue.</param>
        /// <param name="parameter">Parameter which influences the linking
        /// behaviour in this case.</param>
        public WaypointNotLinkedLimitation(string message, string hint = default(string), string parameter = default(string), IList<int?> waypointIndices = default(IList<int?>))
            : base(message, hint)
        {
            Parameter = parameter;
            WaypointIndices = waypointIndices;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets parameter which influences the linking behaviour in
        /// this case.
        /// </summary>
        [JsonProperty(PropertyName = "parameter")]
        public string Parameter { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "waypointIndices")]
        public IList<int?> WaypointIndices { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
        }
    }
}
