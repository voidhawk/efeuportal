// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("FeatureNotLicensedLimitation")]
    public partial class FeatureNotLicensedLimitation : ResultLimitation
    {
        /// <summary>
        /// Initializes a new instance of the FeatureNotLicensedLimitation
        /// class.
        /// </summary>
        public FeatureNotLicensedLimitation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the FeatureNotLicensedLimitation
        /// class.
        /// </summary>
        /// <param name="message">Description of the limitation.</param>
        /// <param name="key">The name of the license key</param>
        /// <param name="hint">Hint on how to resolve this issue.</param>
        public FeatureNotLicensedLimitation(string message, string key, string hint = default(string))
            : base(message, hint)
        {
            Key = key;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the name of the license key
        /// </summary>
        [JsonProperty(PropertyName = "key")]
        public string Key { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
            if (Key == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Key");
            }
        }
    }
}
