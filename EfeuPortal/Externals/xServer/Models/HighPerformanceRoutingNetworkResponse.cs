// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("HighPerformanceRoutingNetworkResponse")]
    public partial class HighPerformanceRoutingNetworkResponse : ResponseBase
    {
        /// <summary>
        /// Initializes a new instance of the
        /// HighPerformanceRoutingNetworkResponse class.
        /// </summary>
        public HighPerformanceRoutingNetworkResponse()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the
        /// HighPerformanceRoutingNetworkResponse class.
        /// </summary>
        /// <param name="id">The ID of the high-performance routing network
        /// which is generated when the network is created. Use this ID to
        /// reference the high-performance routing network for further use. The
        /// ID can be uninitialized. An uninitialized ID indicates that the
        /// requested high-performance routing network is already existing. In
        /// this case a HighPerformanceRoutingNetworkAlreadyAvailableLimitation
        /// is created. It may contain the ID of the already existing
        /// high-performance routing network. Otherwise an unitialized ID in
        /// the HighPerformanceRoutingNetworkAlreadyAvailableLimitation
        /// indicates that the user has requested a high-performance routing
        /// network which has been delivered with the map.</param>
        public HighPerformanceRoutingNetworkResponse(IList<ResultLimitation> resultLimitations = default(IList<ResultLimitation>), string id = default(string))
            : base(resultLimitations)
        {
            Id = id;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the ID of the high-performance routing network which
        /// is generated when the network is created. Use this ID to reference
        /// the high-performance routing network for further use. The ID can be
        /// uninitialized. An uninitialized ID indicates that the requested
        /// high-performance routing network is already existing. In this case
        /// a HighPerformanceRoutingNetworkAlreadyAvailableLimitation is
        /// created. It may contain the ID of the already existing
        /// high-performance routing network. Otherwise an unitialized ID in
        /// the HighPerformanceRoutingNetworkAlreadyAvailableLimitation
        /// indicates that the user has requested a high-performance routing
        /// network which has been delivered with the map.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

    }
}
