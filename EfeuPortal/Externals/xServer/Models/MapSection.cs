// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Abstract base type for the map section to be rendered. A map section
    /// represents the part of a map which is visible to the user. It can be
    /// defined in multiple ways by several derived types. Further details
    /// concerning the generation of a map image can be found in here.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xmap.MapSection
    /// </summary>
    [Newtonsoft.Json.JsonObject("MapSection")]
    public partial class MapSection
    {
        /// <summary>
        /// Initializes a new instance of the MapSection class.
        /// </summary>
        public MapSection()
        {
            CustomInit();
        }


        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

    }
}
