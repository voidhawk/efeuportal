// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Represents the summary of a cluster which was planned.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xcluster.ClusterReport
    /// </summary>
    public partial class ClusterReport
    {
        /// <summary>
        /// Initializes a new instance of the ClusterReport class.
        /// </summary>
        public ClusterReport()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ClusterReport class.
        /// </summary>
        /// <param name="clusterId">The ID of the cluster that is described by
        /// this report.</param>
        /// <param name="totalActivity">The total amount of activity of the
        /// locations assigned to this cluster. If
        /// considerActivitiesAsServiceTimes is enabled in the request, the
        /// value of totalActivity represents the estimated tour duration for
        /// the current cluster.</param>
        /// <param name="numberOfLocations">The number of locations that is
        /// assigned to this cluster. A detailed list of location IDs is
        /// specified in the clustered locations.</param>
        /// <param name="estimatedTravelTimeToCluster">The estimated travel
        /// time from the cluster center to the cluster. It is calculated by
        /// averaging the distance from the cluster center to its 5 nearest
        /// neighbors (locations). This value can be obtained only if clusters
        /// were specified in the request.</param>
        public ClusterReport(string clusterId, double totalActivity, int numberOfLocations, double? estimatedTravelTimeToCluster = default(double?))
        {
            ClusterId = clusterId;
            TotalActivity = totalActivity;
            NumberOfLocations = numberOfLocations;
            EstimatedTravelTimeToCluster = estimatedTravelTimeToCluster;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the ID of the cluster that is described by this
        /// report.
        /// </summary>
        [JsonProperty(PropertyName = "clusterId")]
        public string ClusterId { get; set; }

        /// <summary>
        /// Gets or sets the total amount of activity of the locations assigned
        /// to this cluster. If considerActivitiesAsServiceTimes is enabled in
        /// the request, the value of totalActivity represents the estimated
        /// tour duration for the current cluster.
        /// </summary>
        [JsonProperty(PropertyName = "totalActivity")]
        public double TotalActivity { get; set; }

        /// <summary>
        /// Gets or sets the number of locations that is assigned to this
        /// cluster. A detailed list of location IDs is specified in the
        /// clustered locations.
        /// </summary>
        [JsonProperty(PropertyName = "numberOfLocations")]
        public int NumberOfLocations { get; set; }

        /// <summary>
        /// Gets or sets the estimated travel time from the cluster center to
        /// the cluster. It is calculated by averaging the distance from the
        /// cluster center to its 5 nearest neighbors (locations). This value
        /// can be obtained only if clusters were specified in the request.
        /// </summary>
        [JsonProperty(PropertyName = "estimatedTravelTimeToCluster")]
        public double? EstimatedTravelTimeToCluster { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (ClusterId == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "ClusterId");
            }
        }
    }
}
