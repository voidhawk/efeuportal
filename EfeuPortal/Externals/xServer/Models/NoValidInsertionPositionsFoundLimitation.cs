// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("NoValidInsertionPositionsFoundLimitation")]
    public partial class NoValidInsertionPositionsFoundLimitation : ResultLimitation
    {
        /// <summary>
        /// Initializes a new instance of the
        /// NoValidInsertionPositionsFoundLimitation class.
        /// </summary>
        public NoValidInsertionPositionsFoundLimitation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the
        /// NoValidInsertionPositionsFoundLimitation class.
        /// </summary>
        /// <param name="message">Description of the limitation.</param>
        /// <param name="hint">Hint on how to resolve this issue.</param>
        public NoValidInsertionPositionsFoundLimitation(string message, string hint = default(string), IList<string> objectIds = default(IList<string>))
            : base(message, hint)
        {
            ObjectIds = objectIds;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "objectIds")]
        public IList<string> ObjectIds { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
        }
    }
}
