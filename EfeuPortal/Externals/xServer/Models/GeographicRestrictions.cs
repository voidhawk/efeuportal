// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines the geographic restrictions to be applied for route
    /// calculation. The effective routable area is determined by the
    /// intersection of the effective country list and the search space bounds.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.routing.GeographicRestrictions
    /// </summary>
    public partial class GeographicRestrictions
    {
        /// <summary>
        /// Initializes a new instance of the GeographicRestrictions class.
        /// </summary>
        public GeographicRestrictions()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the GeographicRestrictions class.
        /// </summary>
        /// <param name="searchSpaceBounds">The area to which the search space
        /// should be restricted.</param>
        public GeographicRestrictions(SearchSpaceBounds searchSpaceBounds = default(SearchSpaceBounds), IList<string> allowedCountries = default(IList<string>), IList<string> prohibitedCountries = default(IList<string>))
        {
            SearchSpaceBounds = searchSpaceBounds;
            AllowedCountries = allowedCountries;
            ProhibitedCountries = prohibitedCountries;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the area to which the search space should be
        /// restricted.
        /// </summary>
        [JsonProperty(PropertyName = "searchSpaceBounds")]
        public SearchSpaceBounds SearchSpaceBounds { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "allowedCountries")]
        public IList<string> AllowedCountries { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "prohibitedCountries")]
        public IList<string> ProhibitedCountries { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (SearchSpaceBounds != null)
            {
                SearchSpaceBounds.Validate();
            }
        }
    }
}
