// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Holds additional data for a subdivision.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xruntime.Subdivision
    /// </summary>
    public partial class Subdivision
    {
        /// <summary>
        /// Initializes a new instance of the Subdivision class.
        /// </summary>
        public Subdivision()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Subdivision class.
        /// </summary>
        /// <param name="code">The country code including the subdivision
        /// code.</param>
        /// <param name="description">Descriptive information about the
        /// subdivision.</param>
        /// <param name="features">Lists the features which are available for
        /// this subdivision.</param>
        public Subdivision(string code = default(string), RegionDescription description = default(RegionDescription), RegionFeatures features = default(RegionFeatures))
        {
            Code = code;
            Description = description;
            Features = features;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the country code including the subdivision code.
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets descriptive information about the subdivision.
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public RegionDescription Description { get; set; }

        /// <summary>
        /// Gets or sets lists the features which are available for this
        /// subdivision.
        /// </summary>
        [JsonProperty(PropertyName = "features")]
        public RegionFeatures Features { get; set; }

    }
}
