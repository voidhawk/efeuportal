// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("VisitOrder")]
    public partial class VisitOrder : Order
    {
        /// <summary>
        /// Initializes a new instance of the VisitOrder class.
        /// </summary>
        public VisitOrder()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the VisitOrder class.
        /// </summary>
        /// <param name="id">An unique ID used to identify the order.</param>
        /// <param name="locationId">ID of the location of the customer to be
        /// visited. This location has to be a customer site.</param>
        /// <param name="groupId">The unique identifier of an order group. The
        /// preferences that apply to orders with the same group ID are
        /// specified in the order group consideration. Order groups may be
        /// referenced in the order group IDs of a vehicle.</param>
        /// <param name="serviceTime">Overall service time at the site required
        /// for the visit. Additionally there may be a site dependent or
        /// vehicle dependent service time of the stop. See Service time
        /// calculation.</param>
        public VisitOrder(string id, string locationId, string groupId = default(string), IList<string> requiredVehicleEquipment = default(IList<string>), double? serviceTime = default(double?))
            : base(id, groupId, requiredVehicleEquipment)
        {
            LocationId = locationId;
            ServiceTime = serviceTime;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets ID of the location of the customer to be visited. This
        /// location has to be a customer site.
        /// </summary>
        [JsonProperty(PropertyName = "locationId")]
        public string LocationId { get; set; }

        /// <summary>
        /// Gets or sets overall service time at the site required for the
        /// visit. Additionally there may be a site dependent or vehicle
        /// dependent service time of the stop. See Service time calculation.
        /// </summary>
        [JsonProperty(PropertyName = "serviceTime")]
        public double? ServiceTime { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
            if (LocationId == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "LocationId");
            }
        }
    }
}
