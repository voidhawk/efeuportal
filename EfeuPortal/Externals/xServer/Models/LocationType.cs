// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Runtime;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines values for LocationType.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LocationType
    {
        [EnumMember(Value = "LOCALITY")]
        LOCALITY,
        [EnumMember(Value = "POSTAL_CODE")]
        POSTALCODE,
        [EnumMember(Value = "STREET")]
        STREET,
        [EnumMember(Value = "EXACT_ADDRESS")]
        EXACTADDRESS,
        [EnumMember(Value = "INTERPOLATED_ADDRESS")]
        INTERPOLATEDADDRESS,
        [EnumMember(Value = "UNSPECIFIED")]
        UNSPECIFIED
    }
    internal static class LocationTypeEnumExtension
    {
        internal static string ToSerializedValue(this LocationType? value)
        {
            return value == null ? null : ((LocationType)value).ToSerializedValue();
        }

        internal static string ToSerializedValue(this LocationType value)
        {
            switch( value )
            {
                case LocationType.LOCALITY:
                    return "LOCALITY";
                case LocationType.POSTALCODE:
                    return "POSTAL_CODE";
                case LocationType.STREET:
                    return "STREET";
                case LocationType.EXACTADDRESS:
                    return "EXACT_ADDRESS";
                case LocationType.INTERPOLATEDADDRESS:
                    return "INTERPOLATED_ADDRESS";
                case LocationType.UNSPECIFIED:
                    return "UNSPECIFIED";
            }
            return null;
        }

        internal static LocationType? ParseLocationType(this string value)
        {
            switch( value )
            {
                case "LOCALITY":
                    return LocationType.LOCALITY;
                case "POSTAL_CODE":
                    return LocationType.POSTALCODE;
                case "STREET":
                    return LocationType.STREET;
                case "EXACT_ADDRESS":
                    return LocationType.EXACTADDRESS;
                case "INTERPOLATED_ADDRESS":
                    return LocationType.INTERPOLATEDADDRESS;
                case "UNSPECIFIED":
                    return LocationType.UNSPECIFIED;
            }
            return null;
        }
    }
}
