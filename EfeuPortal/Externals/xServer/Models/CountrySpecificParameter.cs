// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines the country state type, that defines a country that can be
    /// enabled or disabled from its ID, and its subsequent rules, properties
    /// and action types can be enabled or disabled.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.featurelayerprofile.CountrySpecificParameter
    /// </summary>
    public partial class CountrySpecificParameter
    {
        /// <summary>
        /// Initializes a new instance of the CountrySpecificParameter class.
        /// </summary>
        public CountrySpecificParameter()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CountrySpecificParameter class.
        /// </summary>
        /// <param name="id">Defines the country ID type. The special value
        /// "\*" defines every country.</param>
        /// <param name="enabled">Enables or disables the country. If disabled,
        /// every subsequent rule, property and action type that is relative to
        /// this country will be disabled as well.</param>
        public CountrySpecificParameter(string id, IList<FeatureLayerRule> rules = default(IList<FeatureLayerRule>), IList<FeatureLayerProperty> properties = default(IList<FeatureLayerProperty>), IList<FeatureLayerAction> actions = default(IList<FeatureLayerAction>), bool? enabled = default(bool?))
        {
            Rules = rules;
            Properties = properties;
            Actions = actions;
            Id = id;
            Enabled = enabled;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "rules")]
        public IList<FeatureLayerRule> Rules { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "properties")]
        public IList<FeatureLayerProperty> Properties { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "actions")]
        public IList<FeatureLayerAction> Actions { get; set; }

        /// <summary>
        /// Gets or sets defines the country ID type. The special value "\*"
        /// defines every country.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets enables or disables the country. If disabled, every
        /// subsequent rule, property and action type that is relative to this
        /// country will be disabled as well.
        /// </summary>
        [JsonProperty(PropertyName = "enabled")]
        public bool? Enabled { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Id == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Id");
            }
            if (Rules != null)
            {
                foreach (var element in Rules)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
            if (Properties != null)
            {
                foreach (var element1 in Properties)
                {
                    if (element1 != null)
                    {
                        element1.Validate();
                    }
                }
            }
            if (Actions != null)
            {
                foreach (var element2 in Actions)
                {
                    if (element2 != null)
                    {
                        element2.Validate();
                    }
                }
            }
        }
    }
}
