// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Violation related information describing tours or trips. The violation
    /// report contains violation summaries of its parent object, which is
    /// either a tour report or a trip report.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xtour.ViolationReport
    /// </summary>
    [Newtonsoft.Json.JsonObject("ViolationReport")]
    public partial class ViolationReport
    {
        /// <summary>
        /// Initializes a new instance of the ViolationReport class.
        /// </summary>
        public ViolationReport()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ViolationReport class.
        /// </summary>
        /// <param name="maximumOpeningIntervalExceedance">The longest opening
        /// interval exceedance. Corresponds to tour events with violation type
        /// OPENING\_INTERVAL.</param>
        /// <param name="restPositionViolated">Indicates if any daily rest is
        /// scheduled at an unexpected position according to possible rest
        /// positions that can for example be defined by multi day drivers'
        /// working hours.</param>
        /// <param name="customerStopPositionInTripViolated">Indicates if any
        /// service is scheduled at an unexpected position according to the
        /// position in trip. Corresponds to tour events with violation type
        /// CUSTOMER\_STOP\_POSITION\_IN\_TRIP.</param>
        /// <param name="tripSectionViolated">Indicates if any service is
        /// scheduled at an unexpected position according to the trip section
        /// number. Corresponds to tour events with violation type
        /// TRIP\_SECTION.</param>
        public ViolationReport(double? maximumOpeningIntervalExceedance = default(double?), IList<double?> maximumQuantityScenarioExceedance = default(IList<double?>), IList<string> vehicleEquipmentViolations = default(IList<string>), bool? restPositionViolated = default(bool?), bool? customerStopPositionInTripViolated = default(bool?), bool? tripSectionViolated = default(bool?))
        {
            MaximumOpeningIntervalExceedance = maximumOpeningIntervalExceedance;
            MaximumQuantityScenarioExceedance = maximumQuantityScenarioExceedance;
            VehicleEquipmentViolations = vehicleEquipmentViolations;
            RestPositionViolated = restPositionViolated;
            CustomerStopPositionInTripViolated = customerStopPositionInTripViolated;
            TripSectionViolated = tripSectionViolated;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the longest opening interval exceedance. Corresponds
        /// to tour events with violation type OPENING\_INTERVAL.
        /// </summary>
        [JsonProperty(PropertyName = "maximumOpeningIntervalExceedance")]
        public double? MaximumOpeningIntervalExceedance { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "maximumQuantityScenarioExceedance")]
        public IList<double?> MaximumQuantityScenarioExceedance { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "vehicleEquipmentViolations")]
        public IList<string> VehicleEquipmentViolations { get; set; }

        /// <summary>
        /// Gets or sets indicates if any daily rest is scheduled at an
        /// unexpected position according to possible rest positions that can
        /// for example be defined by multi day drivers' working hours.
        /// </summary>
        [JsonProperty(PropertyName = "restPositionViolated")]
        public bool? RestPositionViolated { get; set; }

        /// <summary>
        /// Gets or sets indicates if any service is scheduled at an unexpected
        /// position according to the position in trip. Corresponds to tour
        /// events with violation type CUSTOMER\_STOP\_POSITION\_IN\_TRIP.
        /// </summary>
        [JsonProperty(PropertyName = "customerStopPositionInTripViolated")]
        public bool? CustomerStopPositionInTripViolated { get; set; }

        /// <summary>
        /// Gets or sets indicates if any service is scheduled at an unexpected
        /// position according to the trip section number. Corresponds to tour
        /// events with violation type TRIP\_SECTION.
        /// </summary>
        [JsonProperty(PropertyName = "tripSectionViolated")]
        public bool? TripSectionViolated { get; set; }

    }
}
