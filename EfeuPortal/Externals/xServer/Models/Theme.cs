// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines the parameters of a specific theme.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.featurelayerprofile.Theme
    /// </summary>
    public partial class Theme
    {
        /// <summary>
        /// Initializes a new instance of the Theme class.
        /// </summary>
        public Theme()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Theme class.
        /// </summary>
        /// <param name="id">Defines the theme ID.</param>
        /// <param name="enabled">Enables or disables the theme. Every
        /// subsequent country, rule, property and action type that is relative
        /// to this theme will be considered enabled or disabled for this
        /// theme.</param>
        /// <param name="priorityLevel">The priority level \[0-255\]. 100 is
        /// the default priority level, a 101 value defines a higher priority
        /// than 100.</param>
        public Theme(string id, IList<CountrySpecificParameter> countrySpecificParameters = default(IList<CountrySpecificParameter>), IList<FeatureResultProperty> featureResultProperties = default(IList<FeatureResultProperty>), bool? enabled = default(bool?), int? priorityLevel = default(int?), IList<string> featureScenarios = default(IList<string>))
        {
            CountrySpecificParameters = countrySpecificParameters;
            FeatureResultProperties = featureResultProperties;
            Id = id;
            Enabled = enabled;
            PriorityLevel = priorityLevel;
            FeatureScenarios = featureScenarios;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "countrySpecificParameters")]
        public IList<CountrySpecificParameter> CountrySpecificParameters { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "featureResultProperties")]
        public IList<FeatureResultProperty> FeatureResultProperties { get; set; }

        /// <summary>
        /// Gets or sets defines the theme ID.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets enables or disables the theme. Every subsequent
        /// country, rule, property and action type that is relative to this
        /// theme will be considered enabled or disabled for this theme.
        /// </summary>
        [JsonProperty(PropertyName = "enabled")]
        public bool? Enabled { get; set; }

        /// <summary>
        /// Gets or sets the priority level \[0-255\]. 100 is the default
        /// priority level, a 101 value defines a higher priority than 100.
        /// </summary>
        [JsonProperty(PropertyName = "priorityLevel")]
        public int? PriorityLevel { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "featureScenarios")]
        public IList<string> FeatureScenarios { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Id == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Id");
            }
            if (CountrySpecificParameters != null)
            {
                foreach (var element in CountrySpecificParameters)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
            if (FeatureResultProperties != null)
            {
                foreach (var element1 in FeatureResultProperties)
                {
                    if (element1 != null)
                    {
                        element1.Validate();
                    }
                }
            }
        }
    }
}
