// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Runtime;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines values for EmissionValueScenarioTypeAustralianNgaFactors2015.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EmissionValueScenarioTypeAustralianNgaFactors2015
    {
        [EnumMember(Value = "VEHICLE_SPECIFIC_AVERAGE_FUEL_CONSUMPTION")]
        VEHICLESPECIFICAVERAGEFUELCONSUMPTION,
        [EnumMember(Value = "FLEET_SPECIFIC_AVERAGE_FUEL_CONSUMPTION")]
        FLEETSPECIFICAVERAGEFUELCONSUMPTION,
        [EnumMember(Value = "ROUTE_SPECIFIC_AVERAGE_FUEL_CONSUMPTION")]
        ROUTESPECIFICAVERAGEFUELCONSUMPTION,
        [EnumMember(Value = "ACTUAL_FUEL_CONSUMPTION")]
        ACTUALFUELCONSUMPTION
    }
    internal static class EmissionValueScenarioTypeAustralianNgaFactors2015EnumExtension
    {
        internal static string ToSerializedValue(this EmissionValueScenarioTypeAustralianNgaFactors2015? value)
        {
            return value == null ? null : ((EmissionValueScenarioTypeAustralianNgaFactors2015)value).ToSerializedValue();
        }

        internal static string ToSerializedValue(this EmissionValueScenarioTypeAustralianNgaFactors2015 value)
        {
            switch( value )
            {
                case EmissionValueScenarioTypeAustralianNgaFactors2015.VEHICLESPECIFICAVERAGEFUELCONSUMPTION:
                    return "VEHICLE_SPECIFIC_AVERAGE_FUEL_CONSUMPTION";
                case EmissionValueScenarioTypeAustralianNgaFactors2015.FLEETSPECIFICAVERAGEFUELCONSUMPTION:
                    return "FLEET_SPECIFIC_AVERAGE_FUEL_CONSUMPTION";
                case EmissionValueScenarioTypeAustralianNgaFactors2015.ROUTESPECIFICAVERAGEFUELCONSUMPTION:
                    return "ROUTE_SPECIFIC_AVERAGE_FUEL_CONSUMPTION";
                case EmissionValueScenarioTypeAustralianNgaFactors2015.ACTUALFUELCONSUMPTION:
                    return "ACTUAL_FUEL_CONSUMPTION";
            }
            return null;
        }

        internal static EmissionValueScenarioTypeAustralianNgaFactors2015? ParseEmissionValueScenarioTypeAustralianNgaFactors2015(this string value)
        {
            switch( value )
            {
                case "VEHICLE_SPECIFIC_AVERAGE_FUEL_CONSUMPTION":
                    return EmissionValueScenarioTypeAustralianNgaFactors2015.VEHICLESPECIFICAVERAGEFUELCONSUMPTION;
                case "FLEET_SPECIFIC_AVERAGE_FUEL_CONSUMPTION":
                    return EmissionValueScenarioTypeAustralianNgaFactors2015.FLEETSPECIFICAVERAGEFUELCONSUMPTION;
                case "ROUTE_SPECIFIC_AVERAGE_FUEL_CONSUMPTION":
                    return EmissionValueScenarioTypeAustralianNgaFactors2015.ROUTESPECIFICAVERAGEFUELCONSUMPTION;
                case "ACTUAL_FUEL_CONSUMPTION":
                    return EmissionValueScenarioTypeAustralianNgaFactors2015.ACTUALFUELCONSUMPTION;
            }
            return null;
        }
    }
}
