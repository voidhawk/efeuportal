// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("TourStructureFault")]
    public partial class TourStructureFault : InvalidTourRequestFault
    {
        /// <summary>
        /// Initializes a new instance of the TourStructureFault class.
        /// </summary>
        public TourStructureFault()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the TourStructureFault class.
        /// </summary>
        /// <param name="hint">A free-text hint what to do to fix the
        /// problem.</param>
        public TourStructureFault(string hint = default(string))
            : base(hint)
        {
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

    }
}
