// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("LicenseFault")]
    public partial class LicenseFault : SetupFault
    {
        /// <summary>
        /// Initializes a new instance of the LicenseFault class.
        /// </summary>
        public LicenseFault()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the LicenseFault class.
        /// </summary>
        /// <param name="hint">A free-text hint what to do to fix the
        /// problem.</param>
        public LicenseFault(string hint = default(string), IList<string> unlicensedServices = default(IList<string>))
            : base(hint)
        {
            UnlicensedServices = unlicensedServices;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "unlicensedServices")]
        public IList<string> UnlicensedServices { get; set; }

    }
}
