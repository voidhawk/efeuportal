// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Contains basic information about an alternative route for the current
    /// route.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xroute.AlternativeRoute
    /// </summary>
    public partial class AlternativeRoute
    {
        /// <summary>
        /// Initializes a new instance of the AlternativeRoute class.
        /// </summary>
        public AlternativeRoute()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the AlternativeRoute class.
        /// </summary>
        /// <param name="distance">The distance of the complete route.</param>
        /// <param name="travelTime">The travel time for the complete
        /// route.</param>
        /// <param name="violated">If set to true, indicates that this route
        /// contains a violation for the chosen vehicle.</param>
        /// <param name="encodedPath">The encoded string describing the
        /// complete path of the calculated route. Use this string as an input
        /// of another route request to calculate the same route with different
        /// parameters.
        /// It is only returned if requested by
        /// ResultFields.encodedPath.</param>
        /// <param name="polyline">The polyline of the complete alternative
        /// route. This polyline consists of all coordinates representing the
        /// alternative route and can be used to draw the route into a map.
        /// It is only returned if requested by ResultFields.polyline.</param>
        public AlternativeRoute(double distance, double travelTime, bool violated, string encodedPath = default(string), EncodedGeometry polyline = default(EncodedGeometry))
        {
            Distance = distance;
            TravelTime = travelTime;
            Violated = violated;
            EncodedPath = encodedPath;
            Polyline = polyline;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the distance of the complete route.
        /// </summary>
        [JsonProperty(PropertyName = "distance")]
        public double Distance { get; set; }

        /// <summary>
        /// Gets or sets the travel time for the complete route.
        /// </summary>
        [JsonProperty(PropertyName = "travelTime")]
        public double TravelTime { get; set; }

        /// <summary>
        /// Gets or sets if set to true, indicates that this route contains a
        /// violation for the chosen vehicle.
        /// </summary>
        [JsonProperty(PropertyName = "violated")]
        public bool Violated { get; set; }

        /// <summary>
        /// Gets or sets the encoded string describing the complete path of the
        /// calculated route. Use this string as an input of another route
        /// request to calculate the same route with different parameters.
        /// It is only returned if requested by ResultFields.encodedPath.
        /// </summary>
        [JsonProperty(PropertyName = "encodedPath")]
        public string EncodedPath { get; set; }

        /// <summary>
        /// Gets or sets the polyline of the complete alternative route. This
        /// polyline consists of all coordinates representing the alternative
        /// route and can be used to draw the route into a map.
        /// It is only returned if requested by ResultFields.polyline.
        /// </summary>
        [JsonProperty(PropertyName = "polyline")]
        public EncodedGeometry Polyline { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Polyline != null)
            {
                Polyline.Validate();
            }
        }
    }
}
