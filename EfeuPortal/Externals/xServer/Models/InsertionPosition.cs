// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// The abstract base type to define the insertion position of a task, stop
    /// or trip.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xtour.InsertionPosition
    /// </summary>
    [Newtonsoft.Json.JsonObject("InsertionPosition")]
    public partial class InsertionPosition
    {
        /// <summary>
        /// Initializes a new instance of the InsertionPosition class.
        /// </summary>
        public InsertionPosition()
        {
            CustomInit();
        }


        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

    }
}
