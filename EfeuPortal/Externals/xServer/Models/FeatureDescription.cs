// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// This type represents settings for the description of a new feature.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xdata.FeatureDescription
    /// </summary>
    public partial class FeatureDescription
    {
        /// <summary>
        /// Initializes a new instance of the FeatureDescription class.
        /// </summary>
        public FeatureDescription()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the FeatureDescription class.
        /// </summary>
        /// <param name="timeDomain">The time domain.</param>
        public FeatureDescription(IList<KeyValuePair> attributes = default(IList<KeyValuePair>), string timeDomain = default(string))
        {
            Attributes = attributes;
            TimeDomain = timeDomain;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "attributes")]
        public IList<KeyValuePair> Attributes { get; set; }

        /// <summary>
        /// Gets or sets the time domain.
        /// </summary>
        [JsonProperty(PropertyName = "timeDomain")]
        public string TimeDomain { get; set; }

    }
}
