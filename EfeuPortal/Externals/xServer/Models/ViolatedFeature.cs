// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Contains the features of a Feature Layer theme valid for the segment in
    /// question.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xroute.ViolatedFeature
    /// </summary>
    public partial class ViolatedFeature
    {
        /// <summary>
        /// Initializes a new instance of the ViolatedFeature class.
        /// </summary>
        public ViolatedFeature()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ViolatedFeature class.
        /// </summary>
        /// <param name="themeId">The ID of the Feature Layer theme to which
        /// this feature belongs to.</param>
        public ViolatedFeature(string themeId, IList<KeyValuePair> attributes = default(IList<KeyValuePair>))
        {
            ThemeId = themeId;
            Attributes = attributes;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the ID of the Feature Layer theme to which this
        /// feature belongs to.
        /// </summary>
        [JsonProperty(PropertyName = "themeId")]
        public string ThemeId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "attributes")]
        public IList<KeyValuePair> Attributes { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (ThemeId == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "ThemeId");
            }
            if (Attributes != null)
            {
                foreach (var element in Attributes)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
