// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// A simplified version of a segment, returned by
    /// ReachableAreasResponse.segments.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xroute.ReachableAreasSegment
    /// </summary>
    public partial class ReachableAreasSegment
    {
        /// <summary>
        /// Initializes a new instance of the ReachableAreasSegment class.
        /// </summary>
        public ReachableAreasSegment()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ReachableAreasSegment class.
        /// </summary>
        /// <param name="distance">The accumulated distance of the segment
        /// during reachable areas calculation.</param>
        /// <param name="travelTime">The accumulated travel time of the segment
        /// during reachable areas calculation.</param>
        /// <param name="polyline">The polyline of the segment.</param>
        /// <param name="id">The id of the segment.</param>
        /// <param name="predecessorIndex">The index of the predecessor
        /// segment. Returns -1 if segment has no predecessor.</param>
        public ReachableAreasSegment(double distance, double travelTime, EncodedGeometry polyline = default(EncodedGeometry), string id = default(string), int? predecessorIndex = default(int?))
        {
            Distance = distance;
            TravelTime = travelTime;
            Polyline = polyline;
            Id = id;
            PredecessorIndex = predecessorIndex;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the accumulated distance of the segment during
        /// reachable areas calculation.
        /// </summary>
        [JsonProperty(PropertyName = "distance")]
        public double Distance { get; set; }

        /// <summary>
        /// Gets or sets the accumulated travel time of the segment during
        /// reachable areas calculation.
        /// </summary>
        [JsonProperty(PropertyName = "travelTime")]
        public double TravelTime { get; set; }

        /// <summary>
        /// Gets or sets the polyline of the segment.
        /// </summary>
        [JsonProperty(PropertyName = "polyline")]
        public EncodedGeometry Polyline { get; set; }

        /// <summary>
        /// Gets or sets the id of the segment.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the index of the predecessor segment. Returns -1 if
        /// segment has no predecessor.
        /// </summary>
        [JsonProperty(PropertyName = "predecessorIndex")]
        public int? PredecessorIndex { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Polyline != null)
            {
                Polyline.Validate();
            }
        }
    }
}
