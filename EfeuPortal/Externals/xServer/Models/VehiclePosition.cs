// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Position of the driver and his vehicle in a tour in execution.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xtour.VehiclePosition
    /// </summary>
    [Newtonsoft.Json.JsonObject("VehiclePosition")]
    public partial class VehiclePosition
    {
        /// <summary>
        /// Initializes a new instance of the VehiclePosition class.
        /// </summary>
        public VehiclePosition()
        {
            CustomInit();
        }


        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

    }
}
