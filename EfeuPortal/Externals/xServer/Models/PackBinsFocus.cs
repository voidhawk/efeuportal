// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Runtime;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines values for PackBinsFocus.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PackBinsFocus
    {
        [EnumMember(Value = "BUILD_LAYERS")]
        BUILDLAYERS,
        [EnumMember(Value = "REDUCE_LOADING_METERS")]
        REDUCELOADINGMETERS
    }
    internal static class PackBinsFocusEnumExtension
    {
        internal static string ToSerializedValue(this PackBinsFocus? value)
        {
            return value == null ? null : ((PackBinsFocus)value).ToSerializedValue();
        }

        internal static string ToSerializedValue(this PackBinsFocus value)
        {
            switch( value )
            {
                case PackBinsFocus.BUILDLAYERS:
                    return "BUILD_LAYERS";
                case PackBinsFocus.REDUCELOADINGMETERS:
                    return "REDUCE_LOADING_METERS";
            }
            return null;
        }

        internal static PackBinsFocus? ParsePackBinsFocus(this string value)
        {
            switch( value )
            {
                case "BUILD_LAYERS":
                    return PackBinsFocus.BUILDLAYERS;
                case "REDUCE_LOADING_METERS":
                    return PackBinsFocus.REDUCELOADINGMETERS;
            }
            return null;
        }
    }
}
