// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// This type represents meta data about a Feature Layer.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xdata.FeatureLayerDescription
    /// </summary>
    public partial class FeatureLayerDescription
    {
        /// <summary>
        /// Initializes a new instance of the FeatureLayerDescription class.
        /// </summary>
        public FeatureLayerDescription()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the FeatureLayerDescription class.
        /// </summary>
        /// <param name="themeId">The ID of the Feature Layer theme.</param>
        /// <param name="featureScenario">The optional scenario of the Feature
        /// Layer, if it was specified in the request. This scenario is used
        /// for displaying the Feature Layer, e.g. in the dashboard, and is
        /// also stored in the layer.</param>
        /// <param name="tenant">The tenant is returned only for
        /// administrators.</param>
        /// <param name="scope">The user defined scope.</param>
        /// <param name="createdAt">Indicates the date the Feature Layer was
        /// created.</param>
        /// <param name="lastUsedAt">Indicates the date the Feature Layer was
        /// used the last time.</param>
        /// <param name="size">Indicates the size of the Feature Layer on the
        /// hard drive in bytes.</param>
        /// <param name="providerInformation">Display information on the data
        /// providers of the map. This string consists of a comma-separated
        /// list of pairs of data provider name and provider version number,
        /// e.g. TomTom 2016.12. The string format can be changed at any time,
        /// it is for display purposes only.</param>
        public FeatureLayerDescription(string themeId, string featureScenario = default(string), string tenant = default(string), string scope = default(string), System.DateTime? createdAt = default(System.DateTime?), System.DateTime? lastUsedAt = default(System.DateTime?), long? size = default(long?), string providerInformation = default(string))
        {
            ThemeId = themeId;
            FeatureScenario = featureScenario;
            Tenant = tenant;
            Scope = scope;
            CreatedAt = createdAt;
            LastUsedAt = lastUsedAt;
            Size = size;
            ProviderInformation = providerInformation;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the ID of the Feature Layer theme.
        /// </summary>
        [JsonProperty(PropertyName = "themeId")]
        public string ThemeId { get; set; }

        /// <summary>
        /// Gets or sets the optional scenario of the Feature Layer, if it was
        /// specified in the request. This scenario is used for displaying the
        /// Feature Layer, e.g. in the dashboard, and is also stored in the
        /// layer.
        /// </summary>
        [JsonProperty(PropertyName = "featureScenario")]
        public string FeatureScenario { get; set; }

        /// <summary>
        /// Gets or sets the tenant is returned only for administrators.
        /// </summary>
        [JsonProperty(PropertyName = "tenant")]
        public string Tenant { get; set; }

        /// <summary>
        /// Gets or sets the user defined scope.
        /// </summary>
        [JsonProperty(PropertyName = "scope")]
        public string Scope { get; set; }

        /// <summary>
        /// Gets or sets indicates the date the Feature Layer was created.
        /// </summary>
        [JsonProperty(PropertyName = "createdAt")]
        public System.DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets indicates the date the Feature Layer was used the last
        /// time.
        /// </summary>
        [JsonProperty(PropertyName = "lastUsedAt")]
        public System.DateTime? LastUsedAt { get; set; }

        /// <summary>
        /// Gets or sets indicates the size of the Feature Layer on the hard
        /// drive in bytes.
        /// </summary>
        [JsonProperty(PropertyName = "size")]
        public long? Size { get; set; }

        /// <summary>
        /// Gets or sets display information on the data providers of the map.
        /// This string consists of a comma-separated list of pairs of data
        /// provider name and provider version number, e.g. TomTom 2016.12. The
        /// string format can be changed at any time, it is for display
        /// purposes only.
        /// </summary>
        [JsonProperty(PropertyName = "providerInformation")]
        public string ProviderInformation { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (ThemeId == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "ThemeId");
            }
        }
    }
}
