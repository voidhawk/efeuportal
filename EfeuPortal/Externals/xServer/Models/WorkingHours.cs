// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Defines which rules regarding drivers' working hours need to be
    /// complied with. See Working Hours.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.tourplanning.WorkingHours
    /// </summary>
    [Newtonsoft.Json.JsonObject("WorkingHours")]
    public partial class WorkingHours
    {
        /// <summary>
        /// Initializes a new instance of the WorkingHours class.
        /// </summary>
        public WorkingHours()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the WorkingHours class.
        /// </summary>
        /// <param name="drivingTimeRegulation">Defines which driving time
        /// regulation is applicable. A driving time regulation concerns
        /// drivers of heavy vehicles only. Possible values include: 'NONE',
        /// 'EU_EC_561_2006', 'US_FMCSA_395_2013'</param>
        /// <param name="workingTimeDirective">Defines which legislation
        /// regarding working time is applicable. Currently only supported for
        /// SingleDayWorkingHours. Must not be used together with
        /// drivingTimeRegulation. Possible values include: 'NONE',
        /// 'EU_2002_15_EC'</param>
        /// <param name="drivingTimeRegulationOptions">Defines options for the
        /// selected driving time regulation.</param>
        public WorkingHours(DrivingTimeRegulation? drivingTimeRegulation = default(DrivingTimeRegulation?), WorkingTimeDirective? workingTimeDirective = default(WorkingTimeDirective?), DrivingTimeRegulationOptions drivingTimeRegulationOptions = default(DrivingTimeRegulationOptions))
        {
            DrivingTimeRegulation = drivingTimeRegulation;
            WorkingTimeDirective = workingTimeDirective;
            DrivingTimeRegulationOptions = drivingTimeRegulationOptions;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets defines which driving time regulation is applicable. A
        /// driving time regulation concerns drivers of heavy vehicles only.
        /// Possible values include: 'NONE', 'EU_EC_561_2006',
        /// 'US_FMCSA_395_2013'
        /// </summary>
        [JsonProperty(PropertyName = "drivingTimeRegulation")]
        public DrivingTimeRegulation? DrivingTimeRegulation { get; set; }

        /// <summary>
        /// Gets or sets defines which legislation regarding working time is
        /// applicable. Currently only supported for SingleDayWorkingHours.
        /// Must not be used together with drivingTimeRegulation. Possible
        /// values include: 'NONE', 'EU_2002_15_EC'
        /// </summary>
        [JsonProperty(PropertyName = "workingTimeDirective")]
        public WorkingTimeDirective? WorkingTimeDirective { get; set; }

        /// <summary>
        /// Gets or sets defines options for the selected driving time
        /// regulation.
        /// </summary>
        [JsonProperty(PropertyName = "drivingTimeRegulationOptions")]
        public DrivingTimeRegulationOptions DrivingTimeRegulationOptions { get; set; }

    }
}
