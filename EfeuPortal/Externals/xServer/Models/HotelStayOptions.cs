// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    /// <summary>
    /// Options influencing use cases with overnight stays at hotels.
    /// Hint: Corresponding type in xServer API documentation -
    /// com.ptvgroup.xserver.xcluster.HotelStayOptions
    /// </summary>
    public partial class HotelStayOptions
    {
        /// <summary>
        /// Initializes a new instance of the HotelStayOptions class.
        /// </summary>
        public HotelStayOptions()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the HotelStayOptions class.
        /// </summary>
        /// <param name="maximumTravelDistanceToHotel">Approximate upper bound
        /// on the distance in meters the field worker is willing to travel to
        /// a hotel after completing all visits of a day.</param>
        /// <param name="maximumTravelDistanceFromHotel">Approximate upper
        /// bound on the distance in meters the field worker is willing to
        /// travel from a hotel to the first visit of a day.</param>
        public HotelStayOptions(double? maximumTravelDistanceToHotel = default(double?), double? maximumTravelDistanceFromHotel = default(double?))
        {
            MaximumTravelDistanceToHotel = maximumTravelDistanceToHotel;
            MaximumTravelDistanceFromHotel = maximumTravelDistanceFromHotel;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets approximate upper bound on the distance in meters the
        /// field worker is willing to travel to a hotel after completing all
        /// visits of a day.
        /// </summary>
        [JsonProperty(PropertyName = "maximumTravelDistanceToHotel")]
        public double? MaximumTravelDistanceToHotel { get; set; }

        /// <summary>
        /// Gets or sets approximate upper bound on the distance in meters the
        /// field worker is willing to travel from a hotel to the first visit
        /// of a day.
        /// </summary>
        [JsonProperty(PropertyName = "maximumTravelDistanceFromHotel")]
        public double? MaximumTravelDistanceFromHotel { get; set; }

    }
}
