// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace PTVGROUP.xServer2.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Newtonsoft.Json.JsonObject("RemoveToursAction")]
    public partial class RemoveToursAction : ChangeToursAction
    {
        /// <summary>
        /// Initializes a new instance of the RemoveToursAction class.
        /// </summary>
        public RemoveToursAction()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the RemoveToursAction class.
        /// </summary>
        /// <param name="storedRequestId">ID of the stored PlanToursRequest
        /// that contains all information about the tours to be changed as
        /// input plan. This request with changed input plan containing the
        /// tours information after the ChangeToursAction will be again stored
        /// in the session storage and the ID of the stored request will be
        /// returned as stored request.</param>
        public RemoveToursAction(string storedRequestId, IList<string> vehicleIds = default(IList<string>))
            : base(storedRequestId)
        {
            VehicleIds = vehicleIds;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "vehicleIds")]
        public IList<string> VehicleIds { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="Microsoft.Rest.ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public override void Validate()
        {
            base.Validate();
        }
    }
}
