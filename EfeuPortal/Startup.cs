using EfeuPortal.Helpers;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;

namespace EfeuPortal
{
    public class Startup
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(Startup));

        public Startup(IConfiguration configuration)
        {
            log.Info("***********************************************************************");
            log.Info($"Start efeuportal middleware {DateTimeOffset.Now}");
            log.Info("***********************************************************************");

            Configuration = configuration;
            BsonSerializer.RegisterSerializer<DateTimeOffset>(new DateTimeOffsetSupportingBsonDateTimeSerializer());


            //var conventionPack = new ConventionPack { new CamelCaseElementNameConvention() };
            //ConventionRegistry.Register("camelCase", conventionPack, t => true);

            ConventionPack camelCaseElementNameConvention = new ConventionPack();
            camelCaseElementNameConvention.Add(new CamelCaseElementNameConvention());
            ConventionRegistry.Register("camelCase", camelCaseElementNameConvention, t => true);

            //https://stackoverflow.com/questions/23448634/mongodb-c-sharp-driver-ignore-fields-on-binding
            //    [BsonIgnoreExtraElements] <= wird dann nicht mehr benötigt (kommt über die clazz)
            ConventionPack ignoreIfNullConvention = new ConventionPack();
            ignoreIfNullConvention.Add(new IgnoreIfNullConvention(true));
            ConventionRegistry.Register("ignoreIfNull", ignoreIfNullConvention, t => true);

            //conventions.Add(new EnumRepresentationConvention(BsonType.String));
            //ConventionRegistry.Register("PB_EnumRepresentationConvention", conventions, t => true);
            //BsonClassMap.RegisterClassMap<THaOrderDB>();
            //BsonClassMap.RegisterClassMap<THaOrder>();
            //BsonClassMap.RegisterClassMap<SBKOrder>();

            BsonClassMap.RegisterClassMap<EfCaContactDB>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<EfCaContact>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });

            //JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
            //{
            //    TypeNameHandling = TypeNameHandling.Auto,
            //    NullValueHandling = NullValueHandling.Ignore,
            //    MissingMemberHandling = MissingMemberHandling.Ignore
            //};

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug("PTVGROUP; Initializing OpenApi, ...");
            }
            services.AddControllers();

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.IgnoreNullValues = true;
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.Configure<AppSettings>(appSettingsSection);
            services.AddSingleton<MongoDBSvc>();
            services.AddSingleton<VehicleService>();
            services.AddSingleton<AddressService>();
            services.AddSingleton<ApartmentService>();
            services.AddSingleton<BuildingService>();
            services.AddSingleton<ChargingStationService>();
            services.AddSingleton<ContactService>();
            services.AddSingleton<KepService>();
            services.AddSingleton<BoxMountingDeviceService>();
            services.AddSingleton<SyncMeetingPointService>();
            services.AddSingleton<TransportBoxService>();
            services.AddSingleton<OrderService>();
            services.AddSingleton<FZIDepotEventService>();
            services.AddSingleton<FZIProcessService>();
            services.AddSingleton<FZIStatusService>();
            services.AddSingleton<FZIUserAppService>();
            services.AddSingleton<FZIAdminAppService>();
            services.AddSingleton<FZIVehicleEventService>();
            services.AddSingleton<FZILoggingService>();
            services.AddSingleton<FZIIntraLogAppService>();
            services.AddSingleton<FZIAuthorizationService>();
            services.AddSingleton<FZIAppService>();
            services.AddSingleton<FZIUserContactService>();
            services.AddSingleton<XServer2Service>();
            services.AddSingleton<TourService>();
            services.AddSingleton<SystemService>();
            services.AddSingleton<WarehouseService>();
            services.AddTransient<SystemService>();
            services.AddSingleton<TestService>();

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "efeuCampus GmbH",
                    Version = $"Version {appSettings.MiddlewareVersion}",                    
                    Description = "Partners FZI, IFL, KIT, PTV Group",
                    Contact = new OpenApiContact { Name = "FZI Support(efeu-contact@fzi.de), PTV Group Support(support.de@smartour.ptvgroup.com)", Email = "efeu-contact@fzi.de" }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Bearer",
                    Description = "Add a Bearer (jwt)",
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer", //This is were it was not working for me. I was using uppercase B
                    In = ParameterLocation.Header
                });

               c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer" }
                        }, new List<string>()
                    }
                });

                //c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                //{
                //    Name = "Bearer",
                //    Description = "Beschreibung",
                //    Type = SecuritySchemeType.Http,
                //    BearerFormat = "JWT",
                //    In = ParameterLocation.Header,
                //    Scheme = "bearer"
                //});
                //c.OperationFilter<AuthenticationRequirementsOperationFilter>();

                //Locate the XML file being generated by ASP.NET...
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                //... and tell Swagger to use those XML comments.
                c.IncludeXmlComments(xmlPath);
                c.CustomOperationIds(e => $"{e.ActionDescriptor.RouteValues["action"]}");
            });

            // Hier beginnt der Part mit JWT
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug("Configure(..) called");
            }

            app.ApplicationServices.GetRequiredService<SystemService>().Execute();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "EfeuCampus V1");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("MyPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            if (log.IsDebugEnabled)
            {
                log.Debug("Configure(..) finished");
            }
        }
    }
}
