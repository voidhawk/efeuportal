using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;

namespace EfeuPortal.Services.System
{
    public class SystemService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SystemService));

        private readonly AppSettings _appSettings;
        private readonly MongoDBSvc _mongoDBService;
        private readonly ContactService _contactService;

        private readonly byte[] _key;

        public SystemService(IConfiguration config, IOptions<AppSettings> appSettings,
            ContactService contactService)
        {
            string infoMsg = $"SystemService(...)";
            log.Info(infoMsg);
            _appSettings = appSettings.Value;
            _mongoDBService = new MongoDBSvc(config);
            _contactService = contactService;
            if (_key == null) {
                using (SHA256 mySHA256 = SHA256.Create())
                {
                    _key = mySHA256.ComputeHash(Encoding.ASCII.GetBytes(appSettings.Value.Secret));
                }
            }
        }

        #region auth
        /// <summary>
        /// If email and password are valid the user data and a token will be returned.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public EfCaLoginResp Login(EfCaLogin login)
        {
            EfCaLoginResp response = new EfCaLoginResp();

            LoginResp internalLoginResp = null;
            //if(login.Password.Length < 10)
            //Passwort sollte immer gehasht in DB stehen
                RandomPasswordGenerator randomPasswordGenerator = new RandomPasswordGenerator();
                EfCaLogin checkLogin = new EfCaLogin() { Email = login.Email, Password = randomPasswordGenerator.ComputeSha256Hash(login.Password) };
                internalLoginResp = _mongoDBService.UserLogin(checkLogin);
            /*else
            {
                internalLoginResp = _mongoDBService.UserLogin(login);
            }*/

            if (internalLoginResp.Error == false)
            {
                InternalUserInfos internalUserInfos = new InternalUserInfos() { Email = login.Email, TenantId = internalLoginResp.TenantId, Role = internalLoginResp.User.Role };
                string token = TokenProvider.CreateUserDataJWTToken(_appSettings.Secret, _appSettings.ExpireInMinutes, internalUserInfos);
                response.User = internalLoginResp.User;
                response.Token = token;
            }
            else
            {
                response.Error = true;
                response.ErrorMsg = internalLoginResp.ErrorMsg;
            }

            return response;
        }

        /// <summary>
        /// - Überprüfen ob die email schon existiert
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="register"></param>
        /// <returns></returns>
        public EfCaRegisterResp Register(InternalUserInfos internalUserInfos, EfCaRegisterUser register)
        {
            EfCaRegisterResp response = new EfCaRegisterResp();
            //RandomPasswordGenerator randomPasswordGenerator = new RandomPasswordGenerator();
            //EfCaLogin checkLogin = new EfCaLogin() { Email = register.Email, Password = randomPasswordGenerator.ComputeSha256Hash(register.PasswordHash) };

            EfCaUser finder = new EfCaUser() { Email = register.Email};
            EfCaUserResp userResp = _mongoDBService.FindUsersByFinder(internalUserInfos, finder);
            if (userResp.Error == true || userResp.Users.Count != 0)
            {
                response.Error = true;
                response.ErrorMsg = $"Email({register.Email}) is used or not valid";

                return response;
            }
            EfCaContact contact = new EfCaContact() {
                Email = register.Email,
                Firstname = register.Firstname,
                Lastname = register.Lastname,
                Birthday = register.Birthday
            };
            try
            {
                EfCaContactResp contactResp = _contactService.AddContacts(internalUserInfos, new List<EfCaContact>() { contact });
                if(contactResp.Error == true)
                {
                    response.Error = true;
                    response.ErrorMsg = contactResp.ErrorMsg;

                    return response;
                }
            }
            catch(Exception ex)
            {
                response.Error = true;
                response.ErrorMsg = $"Creating contact failed, {ex.Message}";

                return response;
            }

            EfCaUser user = new EfCaUser() {
                Email = register.Email,
                Role = "EfeuUser",
                ContactId = contact.Ident
            };
            userResp = AddUser(internalUserInfos, user, register.Password);
            if (userResp.Error == true)
            {
                response.Error = true;
                response.ErrorMsg = $"Email({register.Email}) register failed";

                return response;
            }
            response.FirstLoginPassword = userResp.FirstLoginPassword;
            if(userResp.Users != null || userResp.Users.Count > 0)
            {
                response.User = userResp.Users[0];
            }
            return response;
        }
        #endregion

        #region EfCaUser
        public EfCaUserResp AddUser(InternalUserInfos internalUserInfos, EfCaUser item, string passwordHash = null)
        {
            EfCaUserResp response = null;
            item.Ident = Guid.NewGuid().ToString();
            response = _mongoDBService.AddUser(internalUserInfos, item, passwordHash);
            if(response.Error == true)
            {
                item.Ident = null;
            }

            return response;
        }

        public EfCaUserResp FindUsersByFinder(InternalUserInfos internalUserInfos, EfCaUser finder)
        {
            EfCaUserResp response = null;
            response = _mongoDBService.FindUsersByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaUserResp PutUser(InternalUserInfos internalUserInfos, EfCaUser item)
        {
            EfCaUserResp response = null;
            response = _mongoDBService.UpdateUser(internalUserInfos, item);

            return response;
        }

        public EfCaUserResp DeleteUser(InternalUserInfos internalUserInfos, string ident)
        {
            EfCaUserResp response = null;
            response = _mongoDBService.DeleteUser(internalUserInfos, ident);

            return response;
        }

        public EfCaUserResp ChangePassword(InternalUserInfos internalUserInfos, EfCaChangePasswordRequest item)
        {
            EfCaUserResp response = new EfCaUserResp();

            EfCaUser finder = new EfCaUser() { Email = internalUserInfos.Email };
            response = _mongoDBService.FindUsersByFinder(internalUserInfos, finder);
            if (response.Error == true || response.Users.Count != 1)
            {
                return response;
            }
            EfCaUser user = response.Users[0];
            RandomPasswordGenerator randomPasswordGenerator = new RandomPasswordGenerator();
            LoginResp internalLoginResp = null;
            EfCaLogin checkLogin = new EfCaLogin() { Email = item.Email, Password = randomPasswordGenerator.ComputeSha256Hash(item.OldPassword) };
            internalLoginResp = _mongoDBService.UserLogin(checkLogin);


            if (internalLoginResp.Error == false)
            {
                response = _mongoDBService.UpdateUserPassword(internalUserInfos, user, randomPasswordGenerator.ComputeSha256Hash(item.Password));
            } else {
                response.Error = true;
                response.ErrorMsg = "Old password wrong";
            }

            return response;
        }

        public EfCaUserResp ResetPassword(InternalUserInfos internalUserInfos, EfCaLogin item)
        {
            EfCaUserResp response = null;

            EfCaUser finder = new EfCaUser() { Email = item.Email };
            response = _mongoDBService.FindUsersByFinder(internalUserInfos, finder);
            if (response.Error == true || response.Users.Count != 1)
            {
                return response;
            }
            EfCaUser user = response.Users[0];
            RandomPasswordGenerator randomPasswordGenerator = new RandomPasswordGenerator();
            PasswordDetails passwordDetails = new PasswordDetails();
            passwordDetails = randomPasswordGenerator.GeneratePassword(6);
            item.Password = passwordDetails.HashedPassword;

            response = _mongoDBService.UpdateUserPassword(internalUserInfos, user, item.Password);
            if(response.Error == false)
            {
                response.FirstLoginPassword = passwordDetails.Password;
            }
            return response;
        }

        public Response ResetPasswordWithToken(EfCaNewPasswordRequest rq) {
            Response response = new Response();
            response.Error = false;
            byte[] token = Enumerable.Range(0, rq.Token.Length)
                                        .Where(i => i % 2 == 0)
                                        .Select(c => Convert.ToByte(rq.Token.Substring(c, 2), 16))
                                        .ToArray();

                    using (MemoryStream tokenStream = new MemoryStream(token))
                    {
                        using (Aes aes = Aes.Create())
                        {
                            byte[] iv = new byte[aes.IV.Length];
                            tokenStream.Read(iv, 0, aes.IV.Length);
                            using (CryptoStream cs = new CryptoStream(tokenStream, aes.CreateDecryptor(_key, iv), CryptoStreamMode.Read))
                            {
                                using (StreamReader dr = new StreamReader(cs))
                                {
                                    string jsonString = dr.ReadToEnd();
                                    EfCaResetPasswordRequest checkrq = JsonConvert.DeserializeObject<EfCaResetPasswordRequest>(jsonString);
                                    DateTimeOffset rqTime = DateTimeOffset.FromUnixTimeSeconds(checkrq.DateTime);
                                    TimeSpan age = DateTimeOffset.Now - rqTime;
                                    if (age.CompareTo(new TimeSpan(0, 10, 0)) > 0) {
                                        response.ErrorMsg = "Token expired";
                                        response.Error = true;
                                        return response;
                                    }
                                    EfCaUserResp userResp = _mongoDBService.GetUserByEmail(checkrq.Email);
                                    if (userResp.Error || userResp.Users.Count != 1) {
                                        response.ErrorMsg = "User associated to token not found";
                                        response.Error = true;
                                        return response;
                                    }
                                    EfCaUser user = userResp.Users[0];
                                    if (user.Version != checkrq.Version) {
                                        response.ErrorMsg = "Token already used";
                                        response.Error = true;
                                        return response;
                                    }
                                    RandomPasswordGenerator randomPasswordGenerator = new RandomPasswordGenerator();
                                    userResp = _mongoDBService.UpdatePasswordByEmail(user.Email, randomPasswordGenerator.ComputeSha256Hash(rq.Password));
                                    return response;

                                }
                            }
                        }
                    }
        }

        public EfCaResetPasswordRequestResp RequestPasswordReset(EfCaResetPasswordRequest rq)
        {
            EfCaResetPasswordRequestResp response = new EfCaResetPasswordRequestResp();
            EfCaUserResp userresp = _mongoDBService.GetUserByEmail(rq.Email);
            if (userresp.Error == true || userresp.Users.Count != 1)
            {
                return response;
            }
            // use _key to create encrypted token from User.Email, User.Version and current time

            EfCaUser user = userresp.Users[0];
            rq.Version = user.Version;
            rq.DateTime = DateTimeOffset.Now.ToUnixTimeSeconds();

            try
            {
                using (MemoryStream token = new MemoryStream())
                {
                    using (Aes aes = Aes.Create())
                    {
                        aes.Key = _key;
                        byte[] iv = aes.IV;
                        token.Write(iv, 0, iv.Length);
                        using (CryptoStream cs = new CryptoStream(token, aes.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            string data = JsonConvert.SerializeObject(rq);

                            using (StreamWriter ew = new StreamWriter(cs))
                            {
                                ew.Write(data);
                            }
                        }
                    }
                    string tokenString = string.Join("", token.ToArray().Select(b => b.ToString("x2")));

                    response.Token = tokenString;
                }
            }
            catch (Exception ex)
            {
                response.Error = true;
                response.ErrorMsg = "Internal Server Error";
            }
            return response;
        }
        #endregion

        #region EfCaRole
        public EfCaRoleResp AddRole(InternalUserInfos internalUserInfos, EfCaRole item)
        {
            EfCaRoleResp response = null;
            response = _mongoDBService.AddRole(internalUserInfos, item);

            return response;
        }

        public EfCaRoleResp FindRolesByFinder(InternalUserInfos internalUserInfos, EfCaRole finder)
        {
            EfCaRoleResp response = null;
            response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaRoleResp PutRole(InternalUserInfos internalUserInfos, EfCaRole item)
        {
            EfCaRoleResp response = null;
            response = _mongoDBService.UpdateRole(internalUserInfos, item);

            return response;
        }

        public bool RoleContainsFunction(InternalUserInfos internalUserInfos, string right, string function, out string errorMsg)
        {
            bool valid = true; ;
            valid = _mongoDBService.RoleContainsFunction(internalUserInfos, right, function);
            if (valid == false)
            {
                errorMsg = $"Right={right} or Function={function} not assigned";

            }
            else
            {
                errorMsg = "";
            }

            return valid;
        }

        #endregion

        #region EfCaTenant
        /// <summary>
        /// Die folgenden Aktionen werden (solange kein Fehler auftritt) ausgefuehrt
        /// <para> Anlegen eines neuen Tenants. </para>
        /// <para> Abfragen der Template Roles und generieren der Rollen für den neuen Tenant</para>
        /// <para> Erzeugen des Admin Users für diesen Tenant</para>
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public EfCaTenantResp AddTenant(InternalUserInfos internalUserInfos, EfCaTenant item)
        {
            EfCaTenantResp response = null;
            item.Ident = Guid.NewGuid().ToString();
            response = _mongoDBService.AddTenant(internalUserInfos, item);

            if (response.Error == false)
            {
                /*
                 * Diese eindeutige ID wurde beim anlegen generiert und wird (muss) jetzt bei allen Tenant spezifischen
                 * Aktionen verwendet werden
                 */
                string tenantId = response.Tenants[0].Ident;
                InternalUserInfos tenantInternalUserInfos = new InternalUserInfos();
                tenantInternalUserInfos.TenantId = tenantId;

                EfCaRole finder = new EfCaRole();
                finder.TenantId = internalUserInfos.TenantId;
                EfCaRoleResp templateRolesResponse = FindRolesByFinder(internalUserInfos, finder);
                if (templateRolesResponse.Error == false)
                {
                    foreach (EfCaRole efCaRole in templateRolesResponse.Roles)
                    {
                        //efCaRole.Ident = null;
                        efCaRole.TenantId = null;
                        efCaRole.Version = null;
                        efCaRole.Description = $"({efCaRole.Ident}, Roles for Tenant.Description={response.Tenants[0].Description})";
                        EfCaRoleResp addRoleResponse = AddRole(tenantInternalUserInfos, efCaRole);
                        if (addRoleResponse.Error == true)
                        {
                            string errorMsg = $"AddTenant({tenantInternalUserInfos.TenantId}), Error adding a role={efCaRole.Ident} ";
                            log.Error(errorMsg);
                        }
                    }
                }
                else
                {
                    //JSt: ToDo: Fehler  behandeln
                }


                EfCaUser tenantAdminUser = new EfCaUser();
                tenantAdminUser.Email = item.EMail;
                tenantAdminUser.Role = "EfeuSystem";

                EfCaUserResp userResponse = AddUser(tenantInternalUserInfos, tenantAdminUser);
                if (userResponse.Error == true)
                {
                    string errorMsg = $"AddTenant({tenantInternalUserInfos.TenantId}), Error creating the Tenant admin user ";
                    log.Error(errorMsg);
                }
                response.TenantAdminUser = userResponse.Users[0];
                response.FirstLoginPassword = userResponse.FirstLoginPassword;
            }

            return response;
        }

        public EfCaTenantResp FindTenantsByFinder(InternalUserInfos internalUserInfos, EfCaTenant finder)
        {
            EfCaTenantResp response = null;
            response = _mongoDBService.FindTenantsByFinder(internalUserInfos, finder);

            return response;
        }

        /// <summary>
        /// Delete all Tenant specific content.
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="ident"></param>
        /// <returns></returns>
        public EfCaTenantResp DeleteTenant(InternalUserInfos internalUserInfos, string ident)
        {
            /*
             * JSt: ToDo:
             * Löschen der Daten => Vehicle usw
             * Löschen der User
             * Löschen der Rollen
             * Löschen des Tenants
             */
            EfCaTenantResp response = null;
            InternalUserInfos tempInternals = new InternalUserInfos();
            tempInternals.TenantId = ident;

            EfCaRoleResp efCaRoleResp = _mongoDBService.DeleteAllRolesOfTenant(tempInternals);
            EfCaUserResp efCaUserResp = _mongoDBService.DeleteAllUsersOfTenant(tempInternals);

            response = _mongoDBService.DeleteTenant(tempInternals, ident);

            return response;
        }

        public EfCaTenantResp PutTenant(InternalUserInfos internalUserInfos, EfCaTenant item)
        {
            EfCaTenantResp response = null;
            response = _mongoDBService.UpdateTenant(internalUserInfos, item);

            return response;
        }

        public EfCaConfig GetConfigOfTenant(InternalUserInfos internalUserInfos, string key)
        {
            EfCaConfig config = _mongoDBService.GetConfigOfTenant(internalUserInfos, key);

            return config;
        }
        #endregion

        #region EfCaConfig
        public List<SmartourQuality> GetTenantUnspecificData(InternalUserInfos internalUserInfos, string key)
        {
            List<SmartourQuality> data = _mongoDBService.GetTenantUnspecificData(internalUserInfos, key);

            return data;
        }

        public EfCaSystemResp AddConfig(InternalUserInfos internalUserInfos, EfCaConfig item)
        {
            EfCaSystemResp response = null;
            item.Ident = Guid.NewGuid().ToString();
            response = _mongoDBService.AddConfig(internalUserInfos, item);
            return response;
        }

        //public EfCaTenantResp FindTenantsByFinder(InternalUserInfos internalUserInfos, EfCaTenant finder)
        //{
        //    EfCaTenantResp response = null;
        //    response = _mongoDBService.FindTenantsByFinder(internalUserInfos, finder);

        //    return response;
        //}

        //public EfCaConfig GetConfigOfTenant(InternalUserInfos internalUserInfos, string key)
        //{
        //    EfCaConfig config = _mongoDBService.GetConfigOfTenant(internalUserInfos, key);

        //    return config;
        //}

        #endregion

        #region Updates
        public void Execute()
        {
            string infoMsg = $"Execute(...)";
            log.Info(infoMsg);
            CheckForSchemaUpdates(_appSettings.SchemaVersion);
        }

        internal void CheckForSchemaUpdates(int currentSchemaVersion)
        {
            string infoMsg = $"CheckForSchemaUpdate({currentSchemaVersion})";
            log.Info(infoMsg);

            EfCaSystem systemConfiguration = new EfCaSystem();
            systemConfiguration = _mongoDBService.FindSystemConfigurationByFinder(systemConfiguration);
            if (systemConfiguration == null)
            {
                systemConfiguration = new EfCaSystem();
                systemConfiguration.SchemaVersion = 0;

                ProviderUnspecificData smartourQuantities = new ProviderUnspecificData();
                smartourQuantities.Key = "SmartourQualities";
                smartourQuantities.SmartourQualities = new List<SmartourQuality>();
                for (int i = 1; i < 21; i++)
                {
                    SmartourQuality quantity = new SmartourQuality();
                    quantity.Name = $"QualiRecharge-{i}";
                    quantity.Number = i;
                    smartourQuantities.SmartourQualities.Add(quantity);
                }
                //smartourQuantities.Data = smartourQuantities;
                systemConfiguration.TenantUnspecificData = new List<ProviderUnspecificData>();
                systemConfiguration.TenantUnspecificData.Add(smartourQuantities);
                systemConfiguration = _mongoDBService.AddSystemConfiguration(systemConfiguration);

                systemConfiguration.SchemaVersion = Update_0_1();

                systemConfiguration.SchemaVersion = currentSchemaVersion;
                _mongoDBService.UpdateSystemConfiguration(systemConfiguration);
                return;
            }

            while (systemConfiguration.SchemaVersion < currentSchemaVersion)
            {
                if (systemConfiguration.SchemaVersion == 0)
                {
                    systemConfiguration.SchemaVersion = Update_0_1();
                }
                if (systemConfiguration.SchemaVersion == 1)
                {
                    systemConfiguration.SchemaVersion = Update_1_2();
                }
                if (systemConfiguration.SchemaVersion == 2)
                {
                    systemConfiguration.SchemaVersion = Update_2_3();
                }
                if (systemConfiguration.SchemaVersion == 3)
                {
                    systemConfiguration.SchemaVersion = Update_3_4();
                }
                if (systemConfiguration.SchemaVersion == 4)
                {
                    systemConfiguration.SchemaVersion = Update_4_5();
                }
                if (systemConfiguration.SchemaVersion == 5)
                {
                    systemConfiguration.SchemaVersion = Update_5_6(systemConfiguration);
                }
                if (systemConfiguration.SchemaVersion == 6)
                {
                    systemConfiguration.SchemaVersion = Update_6_7();
                }
                if (systemConfiguration.SchemaVersion == 7)
                {
                    systemConfiguration.SchemaVersion = Update_7_8();
                }
                if (systemConfiguration.SchemaVersion == 8)
                {
                    systemConfiguration.SchemaVersion = Update_8_9();
                }
                if (systemConfiguration.SchemaVersion == 9)
                {
                    systemConfiguration.SchemaVersion = Update_9_10();
                }
                if (systemConfiguration.SchemaVersion == 10)
                {
                    systemConfiguration.SchemaVersion = Update_10_11();
                }
                if (systemConfiguration.SchemaVersion == 11)
                {
                    systemConfiguration.SchemaVersion = Update_11_12();
                }
                if (systemConfiguration.SchemaVersion == 12)
                {
                    systemConfiguration.SchemaVersion = Update_12_13();
                }
                if (systemConfiguration.SchemaVersion == 13)
                {
                    systemConfiguration.SchemaVersion = Update_13_14();
                }
                if (systemConfiguration.SchemaVersion == 14)
                {
                    systemConfiguration.SchemaVersion = Update_14_15();
                }
                if (systemConfiguration.SchemaVersion == 15)
                {
                    systemConfiguration.SchemaVersion = Update_15_16();
                }
                if (systemConfiguration.SchemaVersion == 16)
                {
                    systemConfiguration.SchemaVersion = Update_16_17();
                }
                if (systemConfiguration.SchemaVersion == 17)
                {
                    systemConfiguration.SchemaVersion = Update_17_18();
                }
                if (systemConfiguration.SchemaVersion == 18)
                {
                    systemConfiguration.SchemaVersion = Update_18_19();
                }
                if (systemConfiguration.SchemaVersion == 19)
                {
                    systemConfiguration.SchemaVersion = Update_19_20();
                }
                if (systemConfiguration.SchemaVersion == 20)
                {
                    systemConfiguration.SchemaVersion = Update_20_21();
                }
                if (systemConfiguration.SchemaVersion == 21)
                {
                    systemConfiguration.SchemaVersion = Update_21_22();
                }
                if (systemConfiguration.SchemaVersion == 22)
                {
                    systemConfiguration.SchemaVersion = Update_22_23();
                }
                if (systemConfiguration.SchemaVersion == 23)
                {
                    systemConfiguration.SchemaVersion = Update_23_24();
                }
                if (systemConfiguration.SchemaVersion == 24)
                {
                    systemConfiguration.SchemaVersion = Update_24_25();
                }
                if (systemConfiguration.SchemaVersion == 25)
                {
                    systemConfiguration.SchemaVersion = Update_25_26();
                }
                if (systemConfiguration.SchemaVersion == 26)
                {
                    systemConfiguration.SchemaVersion = Update_26_27();
                }
                if (systemConfiguration.SchemaVersion == 27)
                {
                    systemConfiguration.SchemaVersion = Update_27_28();
                }
                if (systemConfiguration.SchemaVersion == 28)
                {
                    systemConfiguration.SchemaVersion = Update_28_29();
                }
                if (systemConfiguration.SchemaVersion == 29)
                {
                    systemConfiguration.SchemaVersion = Update_29_30();
                }
                if (systemConfiguration.SchemaVersion == 30)
                {
                    systemConfiguration.SchemaVersion = Update_30_31();
                }
                if (systemConfiguration.SchemaVersion == 31)
                {
                    systemConfiguration.SchemaVersion = Update_31_32();
                }
                if (systemConfiguration.SchemaVersion == 32)
                {
                    systemConfiguration.SchemaVersion = Update_32_33();
                }
                if (systemConfiguration.SchemaVersion == 33)
                {
                    systemConfiguration.SchemaVersion = Update_33_34();
                }
                if (systemConfiguration.SchemaVersion == 34)
                {
                    systemConfiguration.SchemaVersion = Update_34_35();
                }
                if (systemConfiguration.SchemaVersion == 35)
                {
                    systemConfiguration.SchemaVersion = Update_35_36();
                }
                if (systemConfiguration.SchemaVersion == 36)
                {
                    systemConfiguration.SchemaVersion = Update_36_37();
                }
                if (systemConfiguration.SchemaVersion == 37)
                {
                    systemConfiguration.SchemaVersion = Update_37_38();
                }
                if (systemConfiguration.SchemaVersion == 38)
                {
                    systemConfiguration.SchemaVersion = Update_38_39();
                }
                if (systemConfiguration.SchemaVersion == 39)
                {
                    systemConfiguration.SchemaVersion = Update_39_40();
                }
                if (systemConfiguration.SchemaVersion == 40)
                {
                    systemConfiguration.SchemaVersion = Update_40_41();
                }

                _mongoDBService.UpdateSystemConfiguration(systemConfiguration);
            }
        }

        /// <summary>
        /// Dieses Update wird dazu verwendet um die initiale Schema Version abzulegen
        ///
        /// <para>Erzeugen des EfeuAdministrator</para>
        /// <para>Erzeugen der EfeuAdministrator Rolle </para>
        /// <para></para>
        /// <para></para>
        /// <para></para>
        /// </summary>
        private int Update_0_1()
        {
            string infoMsg = $"Update_0_1(...), start creating the initial setup";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfo = new InternalUserInfos
            {
                TenantId = "EfeuAdministrator",
                InternalUserId = "EfeuAdministrator",
                Email = "efeuportal@efeucampus-bruchsal.de"
            };

            #region Add the EfeuAdministrator user
            EfCaUser efeuAdministrator = new EfCaUser()
            {
                Ident = "EfeuAdministrator",
                Role = "EfeuAdministrator",
                Email = "efeuportal@efeucampus-bruchsal.de"
            };
            AddUser(internalUserInfo, efeuAdministrator);
            #endregion

            #region Add roles
            string[] roles = new string[] {
                "EfeuAdministrator", "EfeuHardware",
                "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };
            foreach (string item in roles)
            {
                using (StreamReader file = File.OpenText($"./Import/Roles/{item}Role-initial.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    EfCaRole initRole = (EfCaRole)serializer.Deserialize(file, typeof(EfCaRole));
                    initRole.Ident = item;
                    AddRole(internalUserInfo, initRole);
                }
            }

            #endregion

            #region Add unique indexes
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-addresses");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-boxmountdevices");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-buildings");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-contacts");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-order-images");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-orders");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-plannedTrips");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-syncmeetingpoints");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-transportboxes");
            _mongoDBService.ExecuteCreateUniqueOriginatorIndex("efca-vehicles");
            _mongoDBService.ExecuteCreateUniqueUserIndex("efca-users");
            #endregion
            return 1;
        }

        /// <summary>
        /// New parameter "DeliveryAttempts" added to the clazz EfCaOrder,
        ///
        /// <para>Iterate all EfCaOrders and set the parameter "DeliveryAttempts" to 0</para>
        /// </summary>
        private int Update_1_2()
        {
            string infoMsg = $"Update_1_2(...)";
            log.Info(infoMsg);
            return _mongoDBService.Update_1_2_SchemaOrder();
        }

        /// <summary>
        /// Used to create SlotManagement Entries for
        /// </summary>
        /// <returns></returns>
        private int Update_2_3()
        {
            string infoMsg = $"Update_2_3(...)";
            log.Info(infoMsg);
            return _mongoDBService.Update_2_3_SchemaBoxMountingDevice();
        }

        private int Update_3_4()
        {
            string infoMsg = $"Update_3_4(...)";
            log.Info(infoMsg);
            return _mongoDBService.Update_3_4_SchemaOrderAggregationAllowed();
        }

        /// <summary>
        /// Parameter synchron removed
        /// </summary>
        /// <returns></returns>
        private int Update_4_5()
        {
            string infoMsg = $"Update_4_5(...)";
            log.Info(infoMsg);
            return _mongoDBService.Update_4_5_SchemaSynchron();
        }

        /// <summary>
        /// TenantUnspecificData
        /// </summary>
        /// <param name="systemConfiguration"></param>
        /// <returns></returns>
        private int Update_5_6(EfCaSystem systemConfiguration)
        {
            string infoMsg = $"Update_5_6(...)";
            log.Info(infoMsg);
            if (systemConfiguration.TenantUnspecificData == null)
            {
                systemConfiguration.TenantUnspecificData = new List<ProviderUnspecificData>();
            }
            ProviderUnspecificData smartourQuantities = new ProviderUnspecificData();
            smartourQuantities.Key = "SmartourQualities";
            smartourQuantities.SmartourQualities = new List<SmartourQuality>();
            for (int i = 1; i < 21; i++)
            {
                SmartourQuality quantity = new SmartourQuality();
                quantity.Name = $"QualiRecharge-{i}";
                quantity.Number = i;
                smartourQuantities.SmartourQualities.Add(quantity);
            }
            //smartourQuantities.Data = smartourQuantities;
            systemConfiguration.TenantUnspecificData.Add(smartourQuantities);
            return 6;
        }

        /// <summary>
        /// Added the parameter VehicleProfile in to EfCaVehicle.
        ///
        /// Default value = "sew-robby"
        /// </summary>
        /// <returns></returns>
        private int Update_6_7()
        {
            string infoMsg = $"Update_6_7(...)";
            log.Info(infoMsg);
            return _mongoDBService.Update_6_7_SchemaVehicleProfile();
        }

        private int Update_7_8()
        {
            string infoMsg = $"Update_7_8(...) hash all the passwords";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() { Email = "EfeuAdministrator", Role = "EfeuAdministrator" };
            EfCaUser finder = new EfCaUser();
            _mongoDBService.UpdateHashTheUserPasswords(internalUserInfos, finder);
            return 8;
        }

        /// <summary>
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/49
        /// </summary>
        /// <returns></returns>
        private int Update_8_9()
        {
            string infoMsg = $"Update_8_9(...) Process roles V2";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("App", new List<string>() {
                    "GetPackageImage;EfeuSystem,EfeuOperator,EfeuUser,EfeuUserCurrent"
                });
                sectionAddRights.Add("IntraLogApp", new List<string>() {
                    "PostResetDemo;EfeuSystem,EfeuOperator"
                });
                sectionAddRights.Add("UserApp", new List<string>() {
                    "GetCurrentOrdersOverview;EfeuSystem,EfeuUser,EfeuUserCurrent",
                    "GetFinishedOrdersOverview;EfeuSystem,EfeuUser,EfeuUserCurrent",
                    "PostModifySettings;EfeuSystem,EfeuUser,EfeuUserCurrent",
                    "PostResetDemo;EfeuSystem,EfeuUser,EfeuUserCurrent",
                    "GetSettings;EfeuSystem,EfeuUser,EfeuUserCurrent"
                });
                sectionAddRights.Add("Order", new List<string>() {
                    "PostAddOrderImages;EfeuSystem",
                    "GetOrderImages;EfeuSystem",
                    "DeleteOrderImages;EfeuSystem"
                });

                Dictionary<string, List<string>> sectionDeleteRights = new Dictionary<string, List<string>>();
                sectionDeleteRights.Add("IntraLogApp", new List<string>() { "GetParcelImage;EfeuSystem,EfeuOperator" });
                sectionDeleteRights.Add("ProcessMgmt", new List<string>() { "PostDemoReset;EfeuSystem,EfeuOperator" });
                sectionDeleteRights.Add("UserApp", new List<string>() { "GetVehicleStatusByOrder;EfeuSystem,EfeuOperator,EfeuUserCurrent" });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }

                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionDeleteRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight != null)
                                {
                                    if (efcaRight.Functions != null)
                                    {

                                        if (efcaRight.Functions.Contains(function))
                                        {
                                            efcaRight.Functions.Remove(function);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 9;
        }

        /// <summary>
        /// NUR TEILWEISE
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_9_10()
        {
            string infoMsg = $"Update_9_10(...) Process roles V2";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("IntraLogApp", new List<string>() {
                    "PostResetIntraDemo;EfeuSystem,EfeuOperator",
                    "PostOpenBoxIntra ;EfeuSystem,EfeuOperator"
                });
                sectionAddRights.Add("UserApp", new List<string>() {
                    "PostResetUserDemo;EfeuSystem,EfeuUser,EfeuUserCurrent"
                });
                sectionAddRights.Add("VehicleEvent", new List<string>() {
                    "PostRequestMountForEmpytBoxPickup;EfeuSystem,EfeuHardware"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 10;
        }

        /// <summary>
        /// REST von 9 auf 10
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_10_11()
        {
            string infoMsg = $"Update_10_11(...) roles and rights changes issue #53";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "GetVehicleInformations;EfeuSystem,EfeuOperator",
                    "GetTourOverview;EfeuSystem,EfeuOperator"
                });
                sectionAddRights.Add("IntraLogApp", new List<string>() {
                    "GetBoxInformations;EfeuSystem,EfeuOperator"
                });
                sectionAddRights.Add("DepotEvent", new List<string>() {
                    "PostDeactivateBox;EfeuSystem,EfeuOperator",
                    "PostStoreBox;EfeuSystem,EfeuOperator"
                });

                Dictionary<string, List<string>> sectionDeleteRights = new Dictionary<string, List<string>>();
                sectionDeleteRights.Add("AdminApp", new List<string>() {
                    "PostDeleteTour;EfeuAdministrator,EfeuHardware,EfeuOperator,EfeuSystem,EfeuUser,EfeuUserCurrent",
                    "PostCreateRegularWasteOrder;EfeuAdministrator,EfeuHardware,EfeuOperator,EfeuSystem,EfeuUser,EfeuUserCurrent",
                    "GetVehicleTourInformation;EfeuAdministrator,EfeuHardware,EfeuOperator,EfeuSystem,EfeuUser,EfeuUserCurrent"
                    });
                sectionDeleteRights.Add("IntraLogApp", new List<string>() {
                    "GetStoredTransportBoxes;EfeuAdministrator,EfeuHardware,EfeuOperator,EfeuSystem,EfeuUser,EfeuUserCurrent"
                    });
                sectionDeleteRights.Add("DepotEvent", new List<string>() {
                    "PostRemoveBoxFromMount;EfeuAdministrator,EfeuHardware,EfeuOperator,EfeuSystem,EfeuUser,EfeuUserCurrent"
                    });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }

                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionDeleteRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight != null)
                                {
                                    if (efcaRight.Functions != null)
                                    {

                                        if (efcaRight.Functions.Contains(function))
                                        {
                                            efcaRight.Functions.Remove(function);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 11;
        }

        /// <summary>
        /// REST von 11 auf 12
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_11_12()
        {
            string infoMsg = $"Update_11_12(...) roles and rights changes issue #53";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("UserApp", new List<string>() {
                    "GetUserSyncMeetingPoints;EfeuSystem,EfeuUser,EfeuUserCurrent",
                    "GetUserMounts;EfeuSystem,EfeuUser,EfeuUserCurrent",
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 12;
        }

        /// <summary>
        /// REST von 12 auf 13
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_12_13()
        {
            string infoMsg = $"Update_12_13(...) roles and rights changes issue #53";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("IntraLogApp", new List<string>() {
                    "PostResetTestData;EfeuSystem,EfeuOperator",
                    "PostCompleteTestCommissioning;EfeuSystem,EfeuOperator",
                    "PostCompleteTestCustomerStops;EfeuSystem,EfeuOperator",
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 13;
        }

        /// <summary>
        /// REST von 13 auf 14
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_13_14()
        {
            string infoMsg = $"Update_13_14(...) roles and rights changes issue #53";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("IntraLogApp", new List<string>() {
                    "PostTestRegisterPackages;EfeuSystem,EfeuOperator",

                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 14;
        }

        /// <summary>
        /// REST von 14 auf 15
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_14_15()
        {
            string infoMsg = $"Update_14_15(...) REST RequestMountForBoxPickup";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("VehicleEvent", new List<string>() {
                    "PostRequestMountForBoxPickup;EfeuSystem,EfeuHardware"
                });

                Dictionary<string, List<string>> sectionDeleteRights = new Dictionary<string, List<string>>();
                sectionDeleteRights.Add("VehicleEvent", new List<string>() { "PostRequestMountForEmpytBoxPickup;EfeuSystem,EfeuHardware" });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }

                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionDeleteRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight != null)
                                {
                                    if (efcaRight.Functions != null)
                                    {

                                        if (efcaRight.Functions.Contains(function))
                                        {
                                            efcaRight.Functions.Remove(function);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 15;
        }

        /// <summary>
        /// REST von 15 auf 16
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_15_16()
        {
            string infoMsg = $"Update_15_16(...) REST RequestMountForBoxPickup";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("DepotEvent", new List<string>() {
                    "GetPrintJobs;EfeuSystem,EfeuHardware",
                    "PostFinishPrintJob;EfeuSystem,EfeuHardware"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 16;
        }

        /// <summary>
        /// REST von 16 auf 17
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_16_17()
        {
            string infoMsg = $"Update_16_17(...) REST RequestMountForBoxPickup";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("DepotEvent", new List<string>() {
                    "GetBoxOrderInformations;EfeuSystem,EfeuOperator",
                    "GetPackageInformations;EfeuSystem,EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 17;
        }

        /// <summary>
        /// REST von 17 auf 18
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/53
        /// </summary>
        /// <returns></returns>
        private int Update_17_18()
        {
            string infoMsg = $"Update_17_18(...) REST RequestMountForBoxPickup";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "PostDeleteToursForVehicle;EfeuSystem,EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 18;
        }

        /// <summary>
        /// REST von 18 auf 19
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/69
        /// UserApp Apis Depot Öffnungszeiten
        /// </summary>
        /// <returns></returns>
        private int Update_18_19()
        {
            string infoMsg = $"Update_18_19(...) REST Apis";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("UserApp", new List<string>() {
                    "GetDepotAvailabilityTimeSlots;EfeuSystem,EfeuUser",
                    "GetMinimumTimeWindowLengthMinutes;EfeuSystem,EfeuUser"
                });
                sectionAddRights.Add("Warehouse", new List<string>() {
                    "PostAddWarehousePlaces;EfeuSystem",
                    "FindWarehousePlacesByFinder;EfeuSystem",
                    "DeleteWarehousePlace;EfeuSystem",
                    "PutWarehousePlace;EfeuSystem"
                });


                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 19;
        }

        /// <summary>
        /// REST von 19 auf 20
        /// BoxMountingDeviceApis für Rolle EfeuHardware, für IFL.
        /// </summary>
        /// <returns></returns>
        private int Update_19_20()
        {
            string infoMsg = $"Update_19_20(...) REST Apis";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
     
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("BoxMountingDevice", new List<string>() {
                    "FindBoxMountingDevicesByFinder;EfeuHardware",
                    "PutBoxMountingDevice;EfeuHardware"
                });


                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 20;
        }

        /// <summary>
        /// REST von 20 auf 21
        /// </summary>
        /// <returns></returns>
        private int Update_20_21()
        {
            string infoMsg = $"Update_20_21(...) REST Apis";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("UserApp", new List<string>() {
                    "GetAvailableBoxes;EfeuSystem,EfeuUser"
                });


                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 21;
        }

        private int Update_21_22()
        {
            string infoMsg = $"Update_21_22(...) REST Apis";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos() {
                Email = "EfeuAdministrator@mail.com", Role = "EfeuAdministrator", TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("ProcessMgmt", new List<string>() {
                    "PostWarehouseLog;EfeuSystem",
                    "PostSimulationConfig;EfeuSystem",
                });


                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach(EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if(updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 22;
        }

        private int Update_22_23()
        {
            string infoMsg = $"Update_22_23(...) add new function";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("User", new List<string>() {
                    "RegisterUser;EfeuOperator"
                });


                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 23;
        }

        private int Update_23_24()
        {
            string infoMsg = $"Update_23_24(...) add new function ChangePassword";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("User", new List<string>() {
                    "ChangePassword;EfeuUser"
                });


                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 24;
        }

        private int Update_24_25()
        {
            string infoMsg = $"Update_24_25(...) add new rights";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "PostAdminModifyOrder;EfeuSystem,EfeuOperator",
                    "PostDeleteOrder;EfeuSystem,EfeuOperator",
                });
                sectionAddRights.Add("UserApp", new List<string>() {
                    "GetAvailableBoxes;EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 25;
        }

        /// <summary>
        /// Der Operator bekommt die in der Sektion efeuOperator das Recht ResetUserPwd.
        /// </summary>
        /// <returns></returns>
        private int Update_25_26()
        {
            string infoMsg = $"Update_25_26(...) add new rights";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("User", new List<string>() {
                    "ResetUserPwd;EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 26;
        }

        /// <summary>
        /// Neue Schnittstelle für abgeschlossen Box-Aufträge, Rechte für den Operator
        /// </summary>
        /// <returns></returns>
        private int Update_26_27()
        {
            string infoMsg = $"Update_26_27(...) add new API function getFinishedBoxOrders";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "GetFinishedBoxOrders;EfeuSystem,EfeuOperator"
                });
                sectionAddRights.Add("Status", new List<string>() {
                    "GetBoxStatusList;EfeuSystem,EfeuOperator",
                    "GetMountStatusList;EfeuSystem,EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 27;
        }

        /// <summary>
        /// Neue Schnittstelle für Depot Öffnungszeiten
        /// </summary>
        /// <returns></returns>
        private int Update_27_28()
        {
            string infoMsg = $"Update_27_28(...) add new function getDepotOpeningHours";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }
            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("User", new List<string>() {
                    "GetDepotOpeningHours;EfeuSystem,EfeuUser,EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }
                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }
                }
            }
            return 28;
        }

        /// <summary>
        /// Neue Schnittstelle für Änderungen von Box-Aufträgen, Rechte für den Operator und den Administrator
        /// </summary>
        /// <returns></returns>
        private int Update_28_29()
        {
            string infoMsg = $"Update_28_29(...) add new API function postRemovePackagesFromOrder";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "PostRemovePackagesFromOrder;EfeuSystem,EfeuOperator,EfeuAdministrator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 29;
        }

        /// <summary>
        /// new API function to get customer list and corresponding rights
        /// </summary>
        /// <returns></returns>
        private int Update_29_30()
        {
            string infoMsg = $"Update_29_30(...) add new API function getCustomerList";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "GetCustomerList;EfeuSystem,EfeuOperator,EfeuAdministrator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 30;
        }

        /// <summary>
        /// new API functions to get single package/order + added rights
        /// </summary>
        /// <returns></returns>
        private int Update_30_31()
        {
            string infoMsg = $"Update_30_31(...) add new API functions to get single package/order";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "GetBoxOrder;EfeuSystem,EfeuOperator,EfeuAdministrator",
                    "GetPackage;EfeuSystem,EfeuOperator,EfeuAdministrator",
                    "GetFinishedPackages;EfeuSystem,EfeuOperator,EfeuAdministrator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 31;
        }

        /// <summary>
        /// new API function to get recharging orders + rights
        /// </summary>
        /// <returns></returns>
        private int Update_31_32()
        {
            string infoMsg = $"Update_31_32(...) add new API function to get recharging orders";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "GetRechargingOrders;EfeuSystem,EfeuOperator,EfeuAdministrator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 32;
        }

        /// <summary>
        /// new API function to cancel Pickup orders + rights
        /// </summary>
        /// <returns></returns>
        private int Update_32_33()
        {
            string infoMsg = $"Update_32_33(...) add new API function to cancel Pickup orders";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("UserApp", new List<string>() {
                    "PostCancelPickupOrder;EfeuSystem,EfeuOperator,EfeuUser",
                    "GetDepotOpeningHours;EfeuSystem,EfeuOperator,EfeuUser"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }
            return 33;
        }

        /// <summary>
        /// new API function for admin to return boxes
        /// </summary>
        /// <returns></returns>
        private int Update_33_34()
        {
            string infoMsg = $"Update_33_34(...) add new API function for admin to return box to depot";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("UserApp", new List<string>() {
                    "PostReturnBox;EfeuSystem,EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }
            return 34;
        }

        /// <summary>
        /// new API function for user app: getMinimumCommissioningTimeMinutes
        /// </summary>
        /// <returns></returns>
        private int Update_34_35()
        {
            string infoMsg = $"Update_34_35(...) new API function for user app getMinimumCommissioningTimeMinutes";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("UserApp", new List<string>() {
                    "GetMinimumCommissioningTimeMinutes;EfeuSystem,EfeuUser"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }
            return 35;
        }

        private int Update_35_36()
        {
            string infoMsg = $"Update_35_36(...) new API function for user app getPublicHolidaysBW2022To2028";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("UserApp", new List<string>() {
                    "GetPublicHolidaysBW2022To2028;EfeuSystem,EfeuUser"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }
            return 36;
        }

        private int Update_36_37()
        {
            string infoMsg = $"Update_36_37(...) roles and rights changes";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("Vehicle", new List<string>() {
                    "PostAddVehicles;EfeuOperator",
                    "PutVehicle;EfeuOperator",
                    "DeleteVehicle;EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 37;
        }

        private int Update_37_38()
        {
            string infoMsg = $"Update_37_38(...) roles and rights changes";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("SyncMeetingPoint", new List<string>() {
                    "PostAddSyncMeetingPoints;EfeuOperator",
                    "FindSyncMeetingPointsByFinder;EfeuOperator",
                    "PutSyncMeetingPoint;EfeuOperator",
                    "DeleteSyncMeetingPoint;EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 38;
        }

        private int Update_38_39()
        {
            string infoMsg = $"Update_38_39(...) roles and rights changes";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("TransportBox", new List<string>() {
                    "PostAddTransportBoxes;EfeuOperator",
                    "PutTransportBox;EfeuOperator",
                    "DeleteTransportBox;EfeuOperator",
                    "PostAddTransportBoxTypes;EfeuOperator",
                    "FindTransportBoxTypesByFinder;EfeuOperator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 39;
        }

        private int Update_39_40()
        {
            string infoMsg = $"Update_39_40(...) roles and rights changes";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("TransportBox", new List<string>() {
                    "DeleteTransportBoxType;EfeuOperator",
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }

            return 40;
        }

         /// <summary>
        /// new API function to retrieve planning status logs
        /// </summary>
        /// <returns></returns>
        private int Update_40_41()
        {
            string infoMsg = $"Update_40_41(...) add new API function retrieve planning status logs";
            log.Info(infoMsg);
            InternalUserInfos internalUserInfos = new InternalUserInfos()
            {
                Email = "EfeuAdministrator@mail.com",
                Role = "EfeuAdministrator",
                TenantId = "EfeuAdministrator"
            };

            EfCaRole finder = new EfCaRole();
            EfCaRoleResp response = _mongoDBService.FindRolesByFinder(internalUserInfos, finder);
            if (response.Error == true)
            {
                log.Error($"{infoMsg} failed. Msg: {response.ErrorMsg}");
                throw new Exception($"{infoMsg} failed. Msg: {response.ErrorMsg}");
            }

            {
                string[] roleNames = new string[] { "EfeuAdministrator", "EfeuHardware",
                    "EfeuOperator", "EfeuSystem", "EfeuUser", "EfeuUserCurrent" };

                Dictionary<string, List<string>> sectionAddRights = new Dictionary<string, List<string>>();
                sectionAddRights.Add("AdminApp", new List<string>() {
                    "GetPlanningStatusLogs;EfeuSystem,EfeuOperator,EfeuAdministrator"
                });

                foreach (string item in roleNames)
                {
                    // hier fügen wir Rechte dazu falls diese noch nicht existieren
                    foreach (KeyValuePair<string, List<string>> sectionAndRights in sectionAddRights)
                    {
                        string sectionName = sectionAndRights.Key;
                        foreach (string rights in sectionAndRights.Value)
                        {
                            string[] splittedAppRightContent = rights.Split(";");
                            if (splittedAppRightContent.Length != 2)
                            {
                                log.Error($"{infoMsg} failed. Msg: Split value not valid");
                                throw new Exception($"{infoMsg} failed. Msg: Split value not valid");
                            }
                            string[] splittedFunctions = splittedAppRightContent[1].Split(",");
                            if (!splittedFunctions.Contains(item))
                            {
                                continue;
                            }

                            string function = splittedAppRightContent[0];
                            List<EfCaRole> selecteRolesByName = response.Roles.Where(r => r.Ident.Equals(item)).ToList();
                            foreach (EfCaRole efcaRole in selecteRolesByName)
                            {
                                EfCaRight efcaRight = efcaRole.Rights.Where(r => r.Section.Equals(sectionName)).FirstOrDefault();
                                if (efcaRight == null)
                                {
                                    efcaRight = new EfCaRight() { Section = sectionName };
                                    if (efcaRole.Rights == null)
                                    {
                                        efcaRole.Rights = new List<EfCaRight>();
                                    }
                                    efcaRole.Rights.Add(efcaRight);
                                }
                                if (efcaRight.Functions == null)
                                {
                                    efcaRight.Functions = new List<string>();
                                }

                                if (!efcaRight.Functions.Contains(function))
                                {
                                    efcaRight.Functions.Add(function);
                                }
                            }
                        }
                    }
                }

                foreach (EfCaRole role in response.Roles)
                {
                    EfCaRoleResp updateRoleResponse = _mongoDBService.UpdateRole(internalUserInfos, role);
                    if (updateRoleResponse.Error == true)
                    {
                        log.Error($"{infoMsg}  Update of Role({role.Description}) failed. Msg: {updateRoleResponse.ErrorMsg}");
                    }

                }
            }
            return 41;
        }

        #endregion
    }
}
