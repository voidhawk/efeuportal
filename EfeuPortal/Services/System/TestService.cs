﻿using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.System
{
    public class TestService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(TestService));

        private readonly AppSettings _appSettings;
        private MongoDBSvc _mongoDBService;

        #region test members
        #region Address
        private AddressService _addressService = null;
        private EfCaAddress depotAddress = null;
        private EfCaAddress customerAddress = null;
        #endregion

        #region Building
        private BuildingService _buildingService = null;
        private EfCaBuilding depotBuilding = null;
        private EfCaBuilding customerBuilding = null;
        #endregion

        #region Contact
        private ContactService _contactService = null;
        private EfCaContact depotContact = null;
        private EfCaContact customerContact = null;
        #endregion

        #region Apartment alles auskommentiert
        //private ApartmentService _apartmentService = null;
        //private EfCaApartment apartmentEG = null;
        //private EfCaApartment apartmentOG = null;
        #endregion

        #region BoxMountingDevice
        private BoxMountingDeviceService _boxMountingDeviceService = null;
        private EfCaBoxMountingDevice depotBoxMountingDevice = null;
        private EfCaBoxMountingDevice customerBoxMountingDevice = null;
        #endregion

        #region TransportBoxes and Types
        private TransportBoxService _transportBoxService = null;
        private EfCaTransportBoxType smallTransportBoxType = null;
        private EfCaTransportBoxType largeTransportBoxType = null;
        private EfCaTransportBox smallTransportBox = null;
        private EfCaTransportBox largeTransportBox = null;
        #endregion

        #region Vehicle
        private VehicleService _vehicleService = null;
        private EfCaVehicle vehicle1 = null;
        private EfCaVehicle vehicle2 = null;
        #endregion
        #endregion

        public TestService(IConfiguration config, IOptions<AppSettings> appSettings,
            AddressService addressService, BuildingService buildingService, ContactService contactService, ApartmentService apartmentService,
            BoxMountingDeviceService boxMountingDeviceService, TransportBoxService transportBoxService, VehicleService vehicleService)
        {
            string infoMsg = $"TestService(...)";
            log.Info(infoMsg);
            _appSettings = appSettings.Value;
            _mongoDBService = new MongoDBSvc(config);

            _addressService = addressService;
            _buildingService = buildingService;
            _contactService = contactService;
            //_apartmentService = apartmentService;
            _boxMountingDeviceService = boxMountingDeviceService;
            _transportBoxService = transportBoxService;
            _vehicleService = vehicleService;
        }

        #region Tests
        /// <summary>
        /// ref: LateX
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        public SystemTestResponse ExecTests(InternalUserInfos internalUserInfos)
        {
            SystemTestResponse response = new SystemTestResponse();
            response.Error = true;
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            try
            {
                if (ExecCreateTests(internalUserInfos, detailInfos) == true)
                {
                    response.AddDetailInfos(detailInfos);
                    return response;
                }
                if (ExecReadTests(internalUserInfos, detailInfos) == true)
                {
                    response.AddDetailInfos(detailInfos);
                    return response;
                }
                if (ExecUpdateTests(internalUserInfos, detailInfos) == true)
                {
                    response.AddDetailInfos(detailInfos);
                    return response;
                }
                response.AddDetailInfos(detailInfos);

                //response.AddDetailInfos(ExecVehicleTests(internalUserInfos));
                //response.AddDetailInfos(ExecChargingStationTests(internalUserInfos));
                //response.AddDetailInfos(ExecKEPTests(internalUserInfos));
                //response.AddDetailInfos(ExecSyncMeetingPointTests(internalUserInfos));
                //response.AddDetailInfos(ExecOrderTests(internalUserInfos));
                //response.AddDetailInfos(ExecPlanningTests(internalUserInfos));
                //response.AddDetailInfos(ExecXServerTests(internalUserInfos));
                //response.AddDetailInfos(ExecDockTests(internalUserInfos));

                response.AddDetailInfos(ExecCleanTests(internalUserInfos));
            }
            catch (Exception ex)
            {
                response.ErrorMsg = ex.Message;
            }
            response.Error = false;
            return response;
        }

        protected bool ExecCreateTests(InternalUserInfos internalUserInfos, List<DetailInfo> detailInfos)
        {
            EfCaAddressResp createAddressResponse = CreateAddresses(internalUserInfos);
            detailInfos.AddRange(createAddressResponse.DetailInfos);
            if (createAddressResponse.Error == true)
            {
                return true;
            }

            EfCaBuildingResp createBuildingResponse = CreateBuildings(internalUserInfos);
            detailInfos.AddRange(createBuildingResponse.DetailInfos);
            if (createBuildingResponse.Error == true)
            {
                return true;
            }

            EfCaContactResp createContactResponse = CreateContacts(internalUserInfos);
            detailInfos.AddRange(createContactResponse.DetailInfos);
            if (createContactResponse.Error == true)
            {
                return true;
            }

            //EfCaApartmentResp createApartmentResponse = CreateApartments(internalUserInfos);
            //detailInfos.AddRange(createApartmentResponse.DetailInfos);
            //if (createContactResponse.Error == true)
            //{
            //    return true;
            //}

            EfCaBoxMountingDeviceResp createBoxMountingDeviceResponse = CreateBoxMountingDevices(internalUserInfos);
            detailInfos.AddRange(createBoxMountingDeviceResponse.DetailInfos);
            if (createBoxMountingDeviceResponse.Error == true)
            {
                return true;
            }

            EfCaTransportBoxResp createTransportBoxTypesResponse = CreateTransportBoxTypes(internalUserInfos);
            detailInfos.AddRange(createTransportBoxTypesResponse.DetailInfos);
            if (createTransportBoxTypesResponse.Error == true)
            {
                return true;
            }

            EfCaTransportBoxResp createTransportBoxResponse = CreateTransportBoxes(internalUserInfos);
            detailInfos.AddRange(createTransportBoxResponse.DetailInfos);
            if (createTransportBoxResponse.Error == true)
            {
                return true;
            }

            EfCaVehicleResp createVehicleResponse = CreateVehicles(internalUserInfos);
            detailInfos.AddRange(createVehicleResponse.DetailInfos);
            if (createVehicleResponse.Error == true)
            {
                return true;
            }

            //response.AddDetailInfos(ExecChargingStationTests(internalUserInfos));
            //response.AddDetailInfos(ExecKEPTests(internalUserInfos));
            //response.AddDetailInfos(ExecSyncMeetingPointTests(internalUserInfos));
            //response.AddDetailInfos(ExecOrderTests(internalUserInfos));
            //response.AddDetailInfos(ExecPlanningTests(internalUserInfos));
            //response.AddDetailInfos(ExecXServerTests(internalUserInfos));
            //response.AddDetailInfos(ExecDockTests(internalUserInfos));

            return false;
        }

        protected bool ExecReadTests(InternalUserInfos internalUserInfos, List<DetailInfo> detailInfos)
        {
            EfCaAddressResp readAddressResponse = ReadAddress(internalUserInfos);
            detailInfos.AddRange(readAddressResponse.DetailInfos);
            if (readAddressResponse.Error == true)
            {
                return true;
            }

            EfCaBuildingResp readBuildingResponse = ReadBuilding(internalUserInfos);
            detailInfos.AddRange(readBuildingResponse.DetailInfos);
            if (readBuildingResponse.Error == true)
            {
                return true;
            }

            EfCaContactResp readContactResponse = ReadContact(internalUserInfos);
            detailInfos.AddRange(readContactResponse.DetailInfos);
            if (readContactResponse.Error == true)
            {
                return true;
            }

            //EfCaApartmentResp readApartmentResponse = ReadApartment(internalUserInfos);
            //detailInfos.AddRange(readApartmentResponse.DetailInfos);
            //if (readApartmentResponse.Error == true)
            //{
            //    return true;
            //}

            EfCaBoxMountingDeviceResp readBoxMountingDeviceResponse = ReadBoxMountingDevice(internalUserInfos);
            detailInfos.AddRange(readBoxMountingDeviceResponse.DetailInfos);
            if (readBoxMountingDeviceResponse.Error == true)
            {
                return true;
            }

            EfCaTransportBoxResp readTransportBoxTypesResponse = ReadTransportBoxType(internalUserInfos);
            detailInfos.AddRange(readTransportBoxTypesResponse.DetailInfos);
            if (readTransportBoxTypesResponse.Error == true)
            {
                return true;
            }

            EfCaTransportBoxResp readTransportBoxResponse = ReadTransportBox(internalUserInfos);
            detailInfos.AddRange(readTransportBoxResponse.DetailInfos);
            if (readTransportBoxResponse.Error == true)
            {
                return true;
            }

            EfCaVehicleResp readVehicleResponse = ReadVehicle(internalUserInfos);
            detailInfos.AddRange(readVehicleResponse.DetailInfos);
            if (readVehicleResponse.Error == true)
            {
                return true;
            }

            //response.AddDetailInfos(ExecChargingStationTests(internalUserInfos));
            //response.AddDetailInfos(ExecKEPTests(internalUserInfos));
            //response.AddDetailInfos(ExecSyncMeetingPointTests(internalUserInfos));
            //response.AddDetailInfos(ExecOrderTests(internalUserInfos));
            //response.AddDetailInfos(ExecPlanningTests(internalUserInfos));
            //response.AddDetailInfos(ExecXServerTests(internalUserInfos));
            //response.AddDetailInfos(ExecDockTests(internalUserInfos));

            return false;
        }

        protected bool ExecUpdateTests(InternalUserInfos internalUserInfos, List<DetailInfo> detailInfos)
        {
            EfCaAddressResp updateAddressResponse = UpdateAddress(internalUserInfos);
            detailInfos.AddRange(updateAddressResponse.DetailInfos);
            if (updateAddressResponse.Error == true)
            {
                return true;
            }

            EfCaBuildingResp updateBuildingResponse = UpdateBuildings(internalUserInfos);
            detailInfos.AddRange(updateBuildingResponse.DetailInfos);
            if (updateBuildingResponse.Error == true)
            {
                return true;
            }

            EfCaContactResp updateContactResponse = UpdateContacts(internalUserInfos);
            detailInfos.AddRange(updateContactResponse.DetailInfos);
            if (updateContactResponse.Error == true)
            {
                return true;
            }

            //EfCaApartmentResp updateApartmentResponse = ReadApartment(internalUserInfos);
            //detailInfos.AddRange(updateApartmentResponse.DetailInfos);
            //if (updateApartmentResponse.Error == true)
            //{
            //    return true;
            //}

            //EfCaBoxMountingDeviceResp updateBoxMountingDeviceResponse = ReadBoxMountingDevice(internalUserInfos);
            //detailInfos.AddRange(updateBoxMountingDeviceResponse.DetailInfos);
            //if (updateBoxMountingDeviceResponse.Error == true)
            //{
            //    return true;
            //}

            //EfCaTransportBoxResp updateTransportBoxTypesResponse = ReadTransportBoxType(internalUserInfos);
            //detailInfos.AddRange(updateTransportBoxTypesResponse.DetailInfos);
            //if (updateTransportBoxTypesResponse.Error == true)
            //{
            //    return true;
            //}

            //EfCaTransportBoxResp updateTransportBoxResponse = ReadTransportBox(internalUserInfos);
            //detailInfos.AddRange(updateTransportBoxResponse.DetailInfos);
            //if (updateTransportBoxResponse.Error == true)
            //{
            //    return true;
            //}

            //EfCaVehicleResp updateVehicleResponse = ReadVehicle(internalUserInfos);
            //detailInfos.AddRange(updateVehicleResponse.DetailInfos);
            //if (updateVehicleResponse.Error == true)
            //{
            //    return true;
            //}

            //response.AddDetailInfos(ExecChargingStationTests(internalUserInfos));
            //response.AddDetailInfos(ExecKEPTests(internalUserInfos));
            //response.AddDetailInfos(ExecSyncMeetingPointTests(internalUserInfos));
            //response.AddDetailInfos(ExecOrderTests(internalUserInfos));
            //response.AddDetailInfos(ExecPlanningTests(internalUserInfos));
            //response.AddDetailInfos(ExecXServerTests(internalUserInfos));
            //response.AddDetailInfos(ExecDockTests(internalUserInfos));

            return false;
        }
        
        #region Address
        /// <summary>
        /// Erzeugen einer Depot und einer Customer Addresse
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected EfCaAddressResp CreateAddresses(InternalUserInfos internalUserInfos)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            response.Addresses = new List<EfCaAddress>();

            List<EfCaAddress> items = new List<EfCaAddress>();
            //Depot
            EfCaAddress newDepotItem = new EfCaAddress()
            {
                Label = "Das Depot",
                Type = "DEPOT",
                ZipCode = "76646",
                City = "Bruchsal",
                City2 = "Campus",
                Street = "International University Campus.",
                HouseNumber = "14",
                //Province = "Bruchsal",
                Country = "Germany",
                State = "BW",
                Longitude = 9.3,
                Latitude = 52.95
            };
            items.Add(newDepotItem);

            //Customer
            EfCaAddress newCustomerItem = new EfCaAddress()
            {
                Label = "Bei Max zu Hause",
                Type = "CUSTOMER",
                ZipCode = "76646",
                City = "Bruchsal",
                City2 = "Campus",
                Street = "International University Campus.",
                HouseNumber = "3",
                //Province = "Bruchsal",
                Country = "Germany",
                State = "BW",
                Longitude = 9.268203,
                Latitude = 52.924748
            };
            items.Add(newCustomerItem);

            EfCaAddressResp createResponse = _addressService.AddAddresses(internalUserInfos, items);
            if (createResponse.DetailInfos != null && createResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (createResponse.Error == true || createResponse.Addresses.Count != 2)
            {
                response.Error = true;

                DetailInfo createError = new DetailInfo() { Ident = "ExecAddressTests" };
                createError.Detail = $"CreateAddresses(..), FAILED";
                response.AddDetailInfos(createError);

                return response;
            }
            response.Addresses.AddRange(createResponse.Addresses);

            depotAddress = createResponse.Addresses[0];
            DetailInfo detailDepotAddress = new DetailInfo() { Ident = "ExecAddressTests" };
            detailDepotAddress.Detail = $"CreateAddresses(..), Depot.Ident({createResponse.Addresses[0].Ident}) CREATED";
            response.AddDetailInfos(detailDepotAddress);

            customerAddress = createResponse.Addresses[1];
            DetailInfo detailCustomerAddress = new DetailInfo() { Ident = "ExecAddressTests" };
            detailCustomerAddress.Detail = $"CreateAddresses(..), Customer.Ident({createResponse.Addresses[1].Ident}) CREATED";
            response.AddDetailInfos(detailCustomerAddress);

            return response;
        }

        protected EfCaAddressResp ReadAddress(InternalUserInfos internalUserInfos)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            response.Addresses = new List<EfCaAddress>();

            EfCaAddress finder = new EfCaAddress() { Ident = depotAddress.Ident };
            EfCaAddressResp readResponse = _addressService.FindAddressesByFinder(internalUserInfos, finder);
            if (readResponse.DetailInfos != null && readResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (readResponse.Error == true || readResponse.Addresses.Count != 1)
            {
                response.Error = true;

                DetailInfo error = new DetailInfo() { Ident = "ExecAddressTests" };
                error.Detail = $"ReadAddress(FAILED), ErrorMsg({readResponse.ErrorMsg}";
                response.AddDetailInfos(error);

                return response;
            }
            depotAddress = readResponse.Addresses[0];
            response.Addresses.AddRange(readResponse.Addresses);

            DetailInfo detailDepotAddress = new DetailInfo() { Ident = "ExecAddressTests" };
            detailDepotAddress.Detail = $"ReadAddress(SUCCESS), Depot.Ident({finder.Ident}) FOUND";
            response.AddDetailInfos(detailDepotAddress);

            return response;
        }

        /// <summary>
        /// Der Depot Adresse die BoxMountingDevice zuordnen.
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected EfCaAddressResp UpdateAddress(InternalUserInfos internalUserInfos)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            response.Addresses = new List<EfCaAddress>();

            depotAddress.BoxMountingDeviceIds = new List<string>();
            depotAddress.BoxMountingDeviceIds.Add(depotBoxMountingDevice.Ident);

            EfCaAddressResp addressResp = _mongoDBService.UpdateAddress(internalUserInfos, depotAddress);
            if (addressResp.Error == true || addressResp.Addresses.Count != 1)
            {
                response.Error = true;

                DetailInfo createError = new DetailInfo() { Ident = "ExecAddressTests" };
                createError.Detail = $"UpdateAddress(..), FAILED";
                response.AddDetailInfos(createError);

                return response;
            }

            depotAddress = addressResp.Addresses[0];
            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecAddressTests" };
            detailInfo.Detail = $"UpdateAddress(..), Ident({addressResp.Addresses[0].Ident}) UPDATED Version({addressResp.Addresses[0].Version})";
            response.AddDetailInfos(detailInfo);

            return response;
        }

        protected DetailInfo DeleteAddress(InternalUserInfos internalUserInfos, string ident)
        {
            DetailInfo detailInfo = new DetailInfo() { Ident = "DeleteAddress" };
            List<string> ids = new List<string>();
            ids.Add(ident);
            EfCaAddressResp response = _mongoDBService.DeleteAddresses(internalUserInfos, ids);
            if (response.Error == true)
            {
                throw new Exception($"DeleteAddress(..) failed. Msg: {response.ErrorMsg}");
            }
            detailInfo.Detail = $"DeleteAddress(..), Ident({ident}) DELETED";
            return detailInfo;
        }
        #endregion

        #region ToDo (UD): Apartment
        /*
        protected EfCaApartmentResp CreateApartments(InternalUserInfos internalUserInfos)
        {
            EfCaApartmentResp response = new EfCaApartmentResp();
            response.Apartments = new List<EfCaApartment>();

            List<EfCaApartment> items = new List<EfCaApartment>();
            // EG
            EfCaApartment newCustomerApartment_1 = new EfCaApartment()
            {
                AddressId = customerAddress.Ident,
                Info = "EG"
            };
            items.Add(newCustomerApartment_1);

            // OG
            EfCaApartment newCustomerApartment_2 = new EfCaApartment()
            {
                AddressId = customerAddress.Ident,
                Info = "OG"
            };
            items.Add(newCustomerApartment_2);

            EfCaApartmentResp createResponse = _apartmentService.AddApartments(internalUserInfos, items);
            if (createResponse.DetailInfos != null && createResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (createResponse.Error == true || createResponse.Apartments.Count != 2)
            {
                response.Error = true;

                DetailInfo createError = new DetailInfo() { Ident = "ExecApartmentTests" };
                createError.Detail = $"CreateApartments(FAILED), ErrorMsg({createResponse.ErrorMsg}";
                response.AddDetailInfos(createError);

                return response;
            }
            response.Apartments.AddRange(createResponse.Apartments);

            apartmentEG = createResponse.Apartments[0];
            DetailInfo detailDepotAddress = new DetailInfo() { Ident = "ExecApartmentTests" };
            detailDepotAddress.Detail = $"CreateApartments(SUCCESS), EG Apartment.Ident({createResponse.Apartments[0].Ident}) CREATED";
            response.AddDetailInfos(detailDepotAddress);

            apartmentOG = createResponse.Apartments[1];
            DetailInfo detailCustomerAddress = new DetailInfo() { Ident = "ExecApartmentTests" };
            detailCustomerAddress.Detail = $"CreateApartments(SUCCES), OG Apartment.Ident({createResponse.Apartments[1].Ident}) CREATED";
            response.AddDetailInfos(detailCustomerAddress);

            return response;
        }

        /// <summary>
        /// Read appartment EG
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected EfCaApartmentResp ReadApartment(InternalUserInfos internalUserInfos)
        {
            EfCaApartmentResp response = new EfCaApartmentResp();
            response.Apartments = new List<EfCaApartment>();

            EfCaApartment finder = new EfCaApartment() { Ident = apartmentEG.Ident };
            EfCaApartmentResp readResponse = _apartmentService.FindApartmentsByFinder(internalUserInfos, finder);
            if (readResponse.DetailInfos != null && readResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (readResponse.Error == true || readResponse.Apartments.Count != 1)
            {
                response.Error = true;

                DetailInfo errorDetailInfo = new DetailInfo() { Ident = "ExecApartmentTests" };
                errorDetailInfo.Detail = $"ReadApartment(FAILED), ErrorMsg({readResponse.ErrorMsg}";
                response.AddDetailInfos(errorDetailInfo);

                return response;
            }
            response.Apartments.AddRange(readResponse.Apartments);

            DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecApartmentTests" };
            detailCustomerBuilding.Detail = $"ReadApartment(SUCCESS), Depot.Ident({finder.Ident}) FOUND";
            response.AddDetailInfos(detailCustomerBuilding);

            return response;
        }
        */
        #endregion alles auskommentiert

        #region ToDo (UD): Building
        /// <summary>
        /// Anlegen der Buildings für Depot und Customer
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected EfCaBuildingResp CreateBuildings(InternalUserInfos internalUserInfos)
        {
            EfCaBuildingResp response = new EfCaBuildingResp();
            response.Buildings = new List<EfCaBuilding>();

            List<EfCaBuilding> items = new List<EfCaBuilding>();
            // Depot Building
            EfCaBuilding newDepotItem = new EfCaBuilding()
            {
                Type = "Do we really need this param?",
                AddressId = depotAddress.Ident,
                SyncMeetingPointIds = null,
                ApartmentIds = null,
                ContactIds = null,
                BoxMountingDeviceIds = null,
                ChargingStationIds = null
            };
            items.Add(newDepotItem);

            // Customer Building
            EfCaBuilding newCustomerItem = new EfCaBuilding()
            {
                Type = "Do we really need this param?",
                AddressId = customerAddress.Ident,
                SyncMeetingPointIds = null,
                ApartmentIds = null,
                ContactIds = null,
                BoxMountingDeviceIds = null,
                ChargingStationIds = null
            };
            items.Add(newCustomerItem);

            EfCaBuildingResp createResponse = _buildingService.AddBuildings(internalUserInfos, items);
            if (createResponse.DetailInfos != null && createResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (createResponse.Error == true || createResponse.Buildings.Count != 2)
            {
                response.Error = true;

                DetailInfo createBuildingsError = new DetailInfo() { Ident = "ExecBuildingTests" };
                createBuildingsError.Detail = $"CreateBuildings(..), FAILED";
                response.AddDetailInfos(createBuildingsError);

                return response;
            }
            response.Buildings.AddRange(createResponse.Buildings);

            depotBuilding = createResponse.Buildings[0];
            DetailInfo detailDepotBuilding = new DetailInfo() { Ident = "ExecBuildingTests" };
            detailDepotBuilding.Detail = $"CreateBuildings(..), Depot.Ident({createResponse.Buildings[0].Ident}) CREATED";
            response.AddDetailInfos(detailDepotBuilding);

            customerBuilding = createResponse.Buildings[1];
            DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecBuildingTests" };
            detailCustomerBuilding.Detail = $"CreateBuildings(..), Customer.Ident({createResponse.Buildings[1].Ident}) CREATED";
            response.AddDetailInfos(detailCustomerBuilding);

            return response;
        }

        /// <summary>
        /// Lesen des Depot Building
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected EfCaBuildingResp ReadBuilding(InternalUserInfos internalUserInfos)
        {
            EfCaBuildingResp response = new EfCaBuildingResp();
            response.Buildings = new List<EfCaBuilding>();

            EfCaBuilding finder = new EfCaBuilding() { Ident = depotBuilding.Ident };
            EfCaBuildingResp readResponse = _buildingService.FindBuildingsByFinder(internalUserInfos, finder);
            if (readResponse.DetailInfos != null && readResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (readResponse.Error == true || readResponse.Buildings.Count != 1)
            {
                response.Error = true;

                DetailInfo errorDetailInfo = new DetailInfo() { Ident = "ExecBuildingTests" };
                errorDetailInfo.Detail = $"ReadBuilding(FAILED), ErrorMsg({readResponse.ErrorMsg}";
                response.AddDetailInfos(errorDetailInfo);

                return response;
            }
            response.Buildings.AddRange(readResponse.Buildings);

            DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecBuildingTests" };
            detailCustomerBuilding.Detail = $"ReadBuilding(SUCCESS), Depot.Ident({finder.Ident}) FOUND";
            response.AddDetailInfos(detailCustomerBuilding);

            return response;
        }
        
        /// <summary>
        /// Den Buildings werden die vorher angelegten Kontakte zugeordnet.
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected EfCaBuildingResp UpdateBuildings(InternalUserInfos internalUserInfos)
        {
            EfCaBuildingResp response = new EfCaBuildingResp();
            response.Buildings = new List<EfCaBuilding>();

            {
                depotBuilding.ContactIds = new List<string>();
                depotBuilding.ContactIds.Add(depotContact.Ident);
                EfCaBuildingResp updateResponse = _buildingService.PutBuilding(internalUserInfos, depotBuilding);
                if (updateResponse.Error == true)
                {
                    response.Error = true;
                    DetailInfo detailInfo = new DetailInfo();
                    detailInfo.Ident = "ExecBuildingTests";
                    detailInfo.Detail = updateResponse.ErrorMsg;
                    response.AddDetailInfos(detailInfo);

                    return response;
                }

                if (updateResponse.Buildings[0].Version == 2)
                {
                    DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecBuildingTests" };
                    detailCustomerBuilding.Detail = $"UpdateBuildings(SUCCESS), Depot.Building.Version({updateResponse.Buildings[0].Version})";
                    response.AddDetailInfos(detailCustomerBuilding);
                }
                else
                {
                    DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecBuildingTests" };
                    detailCustomerBuilding.Detail = $"UpdateBuildings(ERROR), Depot.Building.Version({updateResponse.Buildings[0].Version} not valid)";
                    response.AddDetailInfos(detailCustomerBuilding);

                    return response;
                }
            }

            {
                customerBuilding.ContactIds = new List<string>();
                customerBuilding.ContactIds.Add(customerContact.Ident);
                EfCaBuildingResp updateResponse = _buildingService.PutBuilding(internalUserInfos, customerBuilding);
                if (updateResponse.Error == true)
                {
                    response.Error = true;
                    DetailInfo detailInfo = new DetailInfo();
                    detailInfo.Ident = "ExecBuildingTests";
                    detailInfo.Detail = updateResponse.ErrorMsg;
                    response.AddDetailInfos(detailInfo);

                    return response;
                }

                if (updateResponse.Buildings[0].Version == 2)
                {
                    DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecBuildingTests" };
                    detailCustomerBuilding.Detail = $"UpdateBuildings(SUCCESS), Customer.Building.Version({updateResponse.Buildings[0].Version})";
                    response.AddDetailInfos(detailCustomerBuilding);
                }
                else
                {
                    DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecBuildingTests" };
                    detailCustomerBuilding.Detail = $"UpdateBuildings(ERROR), Customer.Building.Version({updateResponse.Buildings[0].Version} not valid)";
                    response.AddDetailInfos(detailCustomerBuilding);

                    return response;
                }
            }

            return response;
        }

        #endregion

        #region ToDo (UD): BoxMountingDevice
        protected EfCaBoxMountingDeviceResp CreateBoxMountingDevices(InternalUserInfos internalUserInfos)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();

            List<EfCaBoxMountingDevice> items = new List<EfCaBoxMountingDevice>();
            // 
            EfCaBoxMountingDevice newDepotDevice = new EfCaBoxMountingDevice()
            {
                Info = "Depot BoxMountingDevice",
                MountCapacity = 1
            };
            items.Add(newDepotDevice);

            // 
            EfCaBoxMountingDevice newCustomerDevice = new EfCaBoxMountingDevice()
            {
                Info = "Customer BoxMountingDevice",
                MountCapacity = 1
            };
            items.Add(newCustomerDevice);

            EfCaBoxMountingDeviceResp createResponse = _boxMountingDeviceService.AddBoxMountingDevices(internalUserInfos, items);
            if (createResponse.DetailInfos != null && createResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(createResponse.DetailInfos);
            }

            if (createResponse.Error == true || createResponse.BoxMountingDevices.Count != 2)
            {
                response.Error = true;

                DetailInfo createError = new DetailInfo() { Ident = "ExecBoxMountingDeviceTests" };
                createError.Detail = $"CreateBoxMountingDevices(FAILED), ErrorMsg({createResponse.ErrorMsg}";
                response.AddDetailInfos(createError);

                return response;
            }
            response.BoxMountingDevices.AddRange(createResponse.BoxMountingDevices);

            depotBoxMountingDevice = createResponse.BoxMountingDevices[0];
            DetailInfo detailDepotAddress = new DetailInfo() { Ident = "ExecBoxMountingDeviceTests" };
            detailDepotAddress.Detail = $"CreateBoxMountingDevices(SUCCESS), Depot BoxMountingDevice.Ident({createResponse.BoxMountingDevices[0].Ident}) CREATED";
            response.AddDetailInfos(detailDepotAddress);

            customerBoxMountingDevice = createResponse.BoxMountingDevices[1];
            DetailInfo detailCustomerAddress = new DetailInfo() { Ident = "ExecBoxMountingDeviceTests" };
            detailCustomerAddress.Detail = $"CreateBoxMountingDevices(SUCCES), Customer BoxMountingDevice.Ident({createResponse.BoxMountingDevices[1].Ident}) CREATED";
            response.AddDetailInfos(detailCustomerAddress);

            return response;

        }

        protected EfCaBoxMountingDeviceResp ReadBoxMountingDevice(InternalUserInfos internalUserInfos)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();

            EfCaBoxMountingDevice finder = new EfCaBoxMountingDevice() { Ident = depotBoxMountingDevice.Ident };
            EfCaBoxMountingDeviceResp readResponse = _boxMountingDeviceService.FindBoxMountingDevicesByFinder(internalUserInfos, finder);
            if (readResponse.DetailInfos != null && readResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (readResponse.Error == true || readResponse.BoxMountingDevices.Count != 1)
            {
                response.Error = true;

                DetailInfo errorDetailInfo = new DetailInfo() { Ident = "ExecBoxMountingDeviceTests" };
                errorDetailInfo.Detail = $"ReadBoxMountingDevice(FAILED), ErrorMsg({readResponse.ErrorMsg}";
                response.AddDetailInfos(errorDetailInfo);

                return response;
            }
            response.BoxMountingDevices.AddRange(readResponse.BoxMountingDevices);

            DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecBoxMountingDeviceTests" };
            detailCustomerBuilding.Detail = $"ReadBoxMountingDevice(SUCCESS), Depot.Ident({finder.Ident}) FOUND";
            response.AddDetailInfos(detailCustomerBuilding);

            return response;
        }

        #endregion

        #region ToDo (CRUD): ChargingStation
        protected List<DetailInfo> ExecChargingStationTests(InternalUserInfos internalUserInfos, List<DetailInfo> detailInfos)
        {
            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecChargingStationTests", Detail = "NOT IMPLEMENTED" };
            detailInfos.Add(detailInfo);
            return detailInfos;
        }
        #endregion

        #region ToDo (UD): Contact
        protected EfCaContactResp CreateContacts(InternalUserInfos internalUserInfos)
        {
            EfCaContactResp response = new EfCaContactResp();
            response.Contacts = new List<EfCaContact>();

            List<EfCaContact> items = new List<EfCaContact>();
            //Depot
            EfCaContact newDepotContactItem = new EfCaContact()
            {
                Firstname = "Chef",
                Lastname = "vom depot",
                Email = "depot@big.com"
            };
            items.Add(newDepotContactItem);

            //Customer
            EfCaContact newCustomerContactItem = new EfCaContact()
            {
                Firstname = "Jürgen",
                Lastname = "Stolz",
                Email = "juergen.stolz@ptvgroup.com"
            };
            items.Add(newCustomerContactItem);

            EfCaContactResp createResponse = _contactService.AddContacts(internalUserInfos, items);
            if (createResponse.DetailInfos != null && createResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (createResponse.Error == true || createResponse.Contacts.Count != 2)
            {
                response.Error = true;

                DetailInfo detailError = new DetailInfo() { Ident = "ExecContactTests" };
                detailError.Detail = $"CreateContacts(FAILED), ErrorMsg({createResponse.ErrorMsg}";
                response.AddDetailInfos(detailError);

                return response;
            }
            response.Contacts.AddRange(createResponse.Contacts);
            depotContact = createResponse.Contacts[0];
            customerContact = createResponse.Contacts[1];

            depotContact = createResponse.Contacts[0];
            DetailInfo detailDepotBuilding = new DetailInfo() { Ident = "ExecContactTests" };
            detailDepotBuilding.Detail = $"CreateContacts(SUCCESS), Depot.Contact.Ident({createResponse.Contacts[0].Ident}) CREATED";
            response.AddDetailInfos(detailDepotBuilding);

            customerContact = createResponse.Contacts[1];
            DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecContactTests" };
            detailCustomerBuilding.Detail = $"CreateContacts(SUCCESS), Customer.Contact.Ident({createResponse.Contacts[1].Ident}) CREATED";
            response.AddDetailInfos(detailCustomerBuilding);

            return response;
        }

        /// <summary>
        /// Lesen des Contacts, der mit dem Depot verbunden werden soll
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected EfCaContactResp ReadContact(InternalUserInfos internalUserInfos)
        {
            EfCaContactResp response = new EfCaContactResp();
            response.Contacts = new List<EfCaContact>();

            EfCaContact finder = new EfCaContact() { Ident = depotContact.Ident };
            EfCaContactResp readResponse = _contactService.FindContactsByFinder(internalUserInfos, finder);
            if (readResponse.DetailInfos != null && readResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (readResponse.Error == true || readResponse.Contacts.Count != 1)
            {
                response.Error = true;

                DetailInfo errorDetailInfo = new DetailInfo() { Ident = "ExecContactTests" };
                errorDetailInfo.Detail = $"ReadContact(FAILED), ErrorMsg({readResponse.ErrorMsg}";
                response.AddDetailInfos(errorDetailInfo);

                return response;
            }
            response.Contacts.AddRange(readResponse.Contacts);

            DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecContactTests" };
            detailCustomerBuilding.Detail = $"ReadContact(SUCCESS), Depot.Contact.Ident({finder.Ident}) FOUND";
            response.AddDetailInfos(detailCustomerBuilding);

            return response;
        }

        protected EfCaContactResp UpdateContacts(InternalUserInfos internalUserInfos)
        {
            EfCaContactResp response = new EfCaContactResp();
            response.Contacts = new List<EfCaContact>();

            {
                depotContact.DefaultOrderMode = DefaultOrderMode.SELFSERVICE;
                EfCaContactResp updateResponse = _contactService.PutContact(internalUserInfos, depotContact);
                if (updateResponse.Error == true)
                {
                    response.Error = true;
                    DetailInfo detailInfo = new DetailInfo();
                    detailInfo.Ident = "ExecContactTests";
                    detailInfo.Detail = updateResponse.ErrorMsg;
                    response.AddDetailInfos(detailInfo);

                    return response;
                }

                if (updateResponse.Contacts[0].Version == 2)
                {
                    DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecContactTests" };
                    detailCustomerBuilding.Detail = $"UpdateContacts(SUCCESS), Depot.Contact.Version({updateResponse.Contacts[0].Version})";
                    response.AddDetailInfos(detailCustomerBuilding);
                }
                else
                {
                    DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecContactTests" };
                    detailCustomerBuilding.Detail = $"UpdateContacts(ERROR), Depot.Contact.Version({updateResponse.Contacts[0].Version} not valid)";
                    response.AddDetailInfos(detailCustomerBuilding);

                    return response;
                }
            }

            return response;
        }

        #endregion

        #region ToDo (CRUD): KEP
        protected List<DetailInfo> ExecKEPTests(InternalUserInfos internalUserInfos)
        {
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecKEPTests", Detail = "NOT IMPLEMENTED" };
            detailInfos.Add(detailInfo);
            return detailInfos;
        }
        #endregion

        #region ToDo (CRUD): SyncMeetingPoint
        protected List<DetailInfo> ExecSyncMeetingPointTests(InternalUserInfos internalUserInfos)
        {
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecSyncMeetingPointTests", Detail = "NOT IMPLEMENTED" };
            detailInfos.Add(detailInfo);
            return detailInfos;
        }
        #endregion

        #region ToDo (UD): TransportBox / TransportBoxTypes
        protected EfCaTransportBoxResp CreateTransportBoxTypes(InternalUserInfos internalUserInfos)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.TransportBoxTypes = new List<EfCaTransportBoxType>();

            List<EfCaTransportBoxType> items = new List<EfCaTransportBoxType>();
            // 
            EfCaTransportBoxType smallBoxtype = new EfCaTransportBoxType()
            {
                Description = "small box",
                Type = "SMALL_PACKAGE_BOX",
                Lenght = 400,
                Width = 450,
                Height = 300
            };
            items.Add(smallBoxtype);

            // 
            EfCaTransportBoxType largeBoxType = new EfCaTransportBoxType()
            {
                Description = "small box",
                Type = "LARGE_PACKAGE_BOX",
                Lenght = 600,
                Width = 500,
                Height = 400
            };
            items.Add(largeBoxType);

            EfCaTransportBoxResp createResponse = _transportBoxService.AddTransportBoxTypes(internalUserInfos, items);
            if (createResponse.DetailInfos != null && createResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (createResponse.Error == true || createResponse.TransportBoxTypes.Count != 2)
            {
                response.Error = true;

                DetailInfo createError = new DetailInfo() { Ident = "ExecTransportBoxTests" };
                createError.Detail = $"CreateTransportBoxTypes(FAILED), ErrorMsg({createResponse.ErrorMsg}";
                response.AddDetailInfos(createError);

                return response;
            }
            response.TransportBoxes.AddRange(createResponse.TransportBoxes);

            smallTransportBoxType = createResponse.TransportBoxTypes[0];
            DetailInfo smallTransportBoxTypeDetail = new DetailInfo() { Ident = "ExecTransportBoxTests" };
            smallTransportBoxTypeDetail.Detail = $"CreateTransportBoxTypes(SUCCESS), Small TransportBoxType.Ident({createResponse.TransportBoxTypes[0].Ident}) CREATED";
            response.AddDetailInfos(smallTransportBoxTypeDetail);

            largeTransportBoxType = createResponse.TransportBoxTypes[1];
            DetailInfo largeTransportBoxTypeDetail = new DetailInfo() { Ident = "ExecTransportBoxTests" };
            largeTransportBoxTypeDetail.Detail = $"CreateTransportBoxTypes(SUCCESS), Large TransportBoxType.Ident({createResponse.TransportBoxTypes[1].Ident}) CREATED";
            response.AddDetailInfos(largeTransportBoxTypeDetail);

            return response;
        }

        protected EfCaTransportBoxResp ReadTransportBoxType(InternalUserInfos internalUserInfos)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.TransportBoxTypes = new List<EfCaTransportBoxType>();

            EfCaTransportBoxType finder = new EfCaTransportBoxType() { Ident = smallTransportBoxType.Ident };
            EfCaTransportBoxResp readResponse = _transportBoxService.FindTransportBoxTypesByFinder(internalUserInfos, finder);
            if (readResponse.DetailInfos != null && readResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (readResponse.Error == true || readResponse.TransportBoxTypes.Count != 1)
            {
                response.Error = true;

                DetailInfo errorDetailInfo = new DetailInfo() { Ident = "ExecTransportBoxTests" };
                errorDetailInfo.Detail = $"ReadTransportBoxType(FAILED), ErrorMsg({readResponse.ErrorMsg}";
                response.AddDetailInfos(errorDetailInfo);

                return response;
            }
            response.TransportBoxTypes.AddRange(readResponse.TransportBoxTypes);

            DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecTransportBoxTests" };
            detailCustomerBuilding.Detail = $"ReadTransportBoxType(SUCCESS), Depot.Ident({finder.Ident}) FOUND";
            response.AddDetailInfos(detailCustomerBuilding);

            return response;
        }

        protected EfCaTransportBoxResp CreateTransportBoxes(InternalUserInfos internalUserInfos)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.TransportBoxes = new List<EfCaTransportBox>();

            List<EfCaTransportBox> items = new List<EfCaTransportBox>();
            // 
            EfCaTransportBox transportBoxSmall = new EfCaTransportBox()
            {
                TransportBoxTypeId = smallTransportBoxType.Ident
            };
            items.Add(transportBoxSmall);

            // 
            EfCaTransportBox transportBoxLarge = new EfCaTransportBox()
            {
                TransportBoxTypeId = largeTransportBoxType.Ident
            };
            items.Add(transportBoxLarge);

            EfCaTransportBoxResp createResponse = _transportBoxService.AddTransportBoxes(internalUserInfos, items);
            if (createResponse.DetailInfos != null && createResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (createResponse.Error == true || createResponse.TransportBoxes.Count != 2)
            {
                response.Error = true;

                DetailInfo createError = new DetailInfo() { Ident = "ExecTransportBoxTests" };
                createError.Detail = $"CreateTransportBoxes(FAILED), ErrorMsg({createResponse.ErrorMsg}";
                response.AddDetailInfos(createError);

                return response;
            }
            response.TransportBoxes.AddRange(createResponse.TransportBoxes);

            smallTransportBox = createResponse.TransportBoxes[0];
            DetailInfo detailTransportBoxSmall = new DetailInfo() { Ident = "ExecTransportBoxTests" };
            detailTransportBoxSmall.Detail = $"CreateTransportBoxes(SUCCESS), Small TransportBox.Ident({createResponse.TransportBoxes[0].Ident}) CREATED";
            response.AddDetailInfos(detailTransportBoxSmall);

            largeTransportBox = createResponse.TransportBoxes[1];
            DetailInfo detailDepotAddress = new DetailInfo() { Ident = "ExecTransportBoxTests" };
            detailDepotAddress.Detail = $"CreateTransportBoxes(SUCCESS), Large TransportBox.Ident({createResponse.TransportBoxes[1].Ident}) CREATED";
            response.AddDetailInfos(detailDepotAddress);

            return response;

        }

        protected EfCaTransportBoxResp ReadTransportBox(InternalUserInfos internalUserInfos)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.TransportBoxes = new List<EfCaTransportBox>();

            EfCaTransportBox finder = new EfCaTransportBox() { Ident = smallTransportBox.Ident };
            EfCaTransportBoxResp readResponse = _transportBoxService.FindTransportBoxesByFinder(internalUserInfos, finder);
            if (readResponse.DetailInfos != null && readResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (readResponse.Error == true || readResponse.TransportBoxes.Count != 1)
            {
                response.Error = true;

                DetailInfo errorDetailInfo = new DetailInfo() { Ident = "ExecTransportBoxTests" };
                errorDetailInfo.Detail = $"ReadTransportBox(FAILED), ErrorMsg({readResponse.ErrorMsg}";
                response.AddDetailInfos(errorDetailInfo);

                return response;
            }
            response.TransportBoxes.AddRange(readResponse.TransportBoxes);

            DetailInfo detailCustomerBuilding = new DetailInfo() { Ident = "ExecTransportBoxTests" };
            detailCustomerBuilding.Detail = $"ReadTransportBox(SUCCESS), Depot.Ident({finder.Ident}) FOUND";
            response.AddDetailInfos(detailCustomerBuilding);

            return response;
        }

        #endregion

        #region ToDo (UD): Vehicle
        protected EfCaVehicleResp CreateVehicles(InternalUserInfos internalUserInfos)
        {
            EfCaVehicleResp response = new EfCaVehicleResp();
            response.Vehicles = new List<EfCaVehicle>();

            List<EfCaVehicle> items = new List<EfCaVehicle>();
            // 
            EfCaVehicle vehicle_1 = new EfCaVehicle()
            {
                VehicleType = "Standard",
                MaxSpeed = 1.6,
                MaxWeight = 75000,
                VehicleProfile = "sew-robby"
            };
            items.Add(vehicle_1);

            EfCaVehicle vehicle_2 = new EfCaVehicle()
            {
                VehicleType = "Standard",
                MaxSpeed = 1.6,
                MaxWeight = 75000,
                VehicleProfile = "sew-robby"
            };
            items.Add(vehicle_2);

            EfCaVehicleResp createResponse = _vehicleService.AddVehicles(internalUserInfos, items);
            if (createResponse.DetailInfos != null && createResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (createResponse.Error == true || createResponse.Vehicles.Count != 2)
            {
                response.Error = true;
                response.ErrorMsg = createResponse.ErrorMsg;

                DetailInfo createError = new DetailInfo() { Ident = "ExecVehicleTests" };
                createError.Detail = $"CreateVehicles(FAILED), ErrorMsg({createResponse.ErrorMsg})";
                response.AddDetailInfos(createError);

                return response;
            }
            response.Vehicles.AddRange(createResponse.Vehicles);

            vehicle1 = createResponse.Vehicles[0];
            DetailInfo detailDepotAddress = new DetailInfo() { Ident = "ExecVehicleTests" };
            detailDepotAddress.Detail = $"CreateVehicles(SUCCESS), Vehicle1.Ident({createResponse.Vehicles[0].Ident}) CREATED";
            response.AddDetailInfos(detailDepotAddress);

            vehicle2 = createResponse.Vehicles[1];
            DetailInfo detailCustomerAddress = new DetailInfo() { Ident = "ExecVehicleTests" };
            detailCustomerAddress.Detail = $"CreateVehicles(SUCCESS), Vehicle2.Ident({createResponse.Vehicles[1].Ident}) CREATED";
            response.AddDetailInfos(detailCustomerAddress);

            return response;
        }

        /// <summary>
        /// Vehicle 1 abfragen
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected EfCaVehicleResp ReadVehicle(InternalUserInfos internalUserInfos)
        {
            EfCaVehicleResp response = new EfCaVehicleResp();
            response.Vehicles = new List<EfCaVehicle>();

            EfCaVehicle finder = new EfCaVehicle() { Ident = vehicle1.Ident };
            EfCaVehicleResp readResponse = _vehicleService.FindVehiclesByFinder(internalUserInfos, finder);
            if (readResponse.DetailInfos != null && readResponse.DetailInfos.Count > 0)
            {
                response.AddDetailInfos(response.DetailInfos);
            }

            if (readResponse.Error == true || readResponse.Vehicles.Count != 1)
            {
                response.Error = true;

                DetailInfo errorDetailInfo = new DetailInfo() { Ident = "ExecVehicleTests" };
                errorDetailInfo.Detail = $"ReadVehicle(FAILED), ErrorMsg({readResponse.ErrorMsg}";
                response.AddDetailInfos(errorDetailInfo);

                return response;
            }
            response.Vehicles.AddRange(readResponse.Vehicles);

            DetailInfo detailVehicle = new DetailInfo() { Ident = "ExecVehicleTests" };
            detailVehicle.Detail = $"ReadVehicle(SUCCESS), Vehicle1.Ident({finder.Ident}) FOUND";
            response.AddDetailInfos(detailVehicle);

            return response;
        }
        #endregion

        #region ToDo (CRUD): Order
        protected List<DetailInfo> ExecOrderTests(InternalUserInfos internalUserInfos)
        {
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecOrderTests", Detail = "NOT IMPLEMENTED" };
            detailInfos.Add(detailInfo);
            return detailInfos;
        }
        #endregion

        #region ToDo (...): Planning
        protected List<DetailInfo> ExecPlanningTests(InternalUserInfos internalUserInfos)
        {
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecPlanningTests", Detail = "NOT IMPLEMENTED" };
            detailInfos.Add(detailInfo);
            return detailInfos;
        }
        #endregion

        #region ToDo: xServer
        protected List<DetailInfo> ExecXServerTests(InternalUserInfos internalUserInfos)
        {
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecXServerTests", Detail = "NOT IMPLEMENTED" };
            detailInfos.Add(detailInfo);
            return detailInfos;
        }
        #endregion

        #region ToDo: Dock reservation
        protected List<DetailInfo> ExecDockTests(InternalUserInfos internalUserInfos)
        {
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecDockTests", Detail = "NOT IMPLEMENTED" };
            detailInfos.Add(detailInfo);
            return detailInfos;
        }
        #endregion

        #region ToDo: Dock reservation
        #endregion

        /// <summary>
        /// Aufräumen und zurücksetzen der Inhalte
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        protected List<DetailInfo> ExecCleanTests(InternalUserInfos internalUserInfos)
        {
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            //detailInfos.Add(DeleteAddress(internalUserInfos, depotAddress.Ident));
            //detailInfos.Add(DeleteAddress(internalUserInfos, customerAddress.Ident));

            DetailInfo detailInfo = new DetailInfo() { Ident = "ExecCleanTests", Detail = "NOT IMPLEMENTED" };
            detailInfos.Add(detailInfo);
            return detailInfos;
        }

        #endregion

    }
}
