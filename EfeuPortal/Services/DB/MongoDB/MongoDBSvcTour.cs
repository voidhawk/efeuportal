﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.TourData;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcTour
    public partial class MongoDBSvc
    {
        public EfCaTourResp AddTours(InternalUserInfos internalUserInfos, List<EfCaPlannedTrip> items)
        {
            EfCaTourResp response = new EfCaTourResp();
            response.Error = false;
            response.PlannedTrips = new List<EfCaPlannedTrip>();

            foreach (EfCaPlannedTrip item in items)
            {
                try
                {
                    EfCaPlannedTripDB addItemToDB = new EfCaPlannedTripDB(internalUserInfos, item);
                    _plannedToursCollection.InsertOne(addItemToDB);

                    EfCaPlannedTrip newValueObject = new EfCaPlannedTrip(addItemToDB);
                    response.PlannedTrips.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddOrders({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddOrders(..) Message not processed in detail: {ex.Message}");
                    }
                    response.AddDetailInfos(detailInfo);
                }
            }

            return response;
        }

        public EfCaTourResp FindToursByFinder(InternalUserInfos internalUserInfos, EfCaPlannedTrip finder, List<string> orderIds)
        {
            EfCaTourResp response = new EfCaTourResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaPlannedTripDB>> filterDefinitions = new List<FilterDefinition<EfCaPlannedTripDB>>();
                    filterDefinitions.Add(Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    var builderFilter = Builders<EfCaPlannedTripDB>.Filter;
                    if (finder != null)
                    {
                        filterDefinitions.Add(Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.owner", true));
                        if (finder.Ident != null)
                        {
                            filterDefinitions.Add(Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                        }
                        if (finder.TourExtId != null)
                        {
                            filterDefinitions.Add(Builders<EfCaPlannedTripDB>.Filter.Eq(t => t.TourExtId, finder.TourExtId));
                        }
                    }
                    else if (orderIds != null && orderIds.Count > 0)
                    {
                        filterDefinitions.Add(Builders<EfCaPlannedTripDB>.Filter.In("extIdDetails.extId", orderIds));
                    }
                    else
                    {
                        response.Error = true;
                        response.ErrorMsg = "Invalid Finder(s)";
                        return response;
                    }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaPlannedTripDB> dbItemsFound = FindAllByFilter(filter, _plannedToursCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.PlannedTrips = new List<EfCaPlannedTrip>();
                        foreach (EfCaPlannedTripDB item in dbItemsFound)
                        {
                            EfCaPlannedTrip newValueObject = new EfCaPlannedTrip(item);
                            response.PlannedTrips.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindOrdersByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }

        public EfCaTourResp UpdateTour(InternalUserInfos internalUserInfos, EfCaPlannedTrip item)
        {
            EfCaTourResp response = new EfCaTourResp();
            response.Error = true;
            response.ErrorMsg = "UpdateTour(..) not implemented";

            //var builder = Builders<EfCaPlannedTripDB>.Filter;
            //var filter = builder.And(
            //    Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
            //    Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.extId", item.Ident),
            //    Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.owner", true)
            //    );
            //EfCaPlannedTripDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _ordersCollectionName);
            //if (uniqueDBItemToUpdateFound == null)
            //{
            //    string logErrorMsg = $"UpdateOrder({internalUserInfos.TenantId}) failed. Order({item.Ident}) not found";
            //    log.Error(logErrorMsg);

            //    response.Error = true;
            //    string errorMsg = $"UpdateOrder(..) failed. Order({item.Ident}) not found";
            //    response.ErrorMsg = errorMsg;
            //    return response;
            //}

            //try
            //{
            //    uniqueDBItemToUpdateFound.Update(item);
            //}
            //catch (Exception ex)
            //{
            //    string logErrorMsg = $"UpdateOrder({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
            //    log.Error(logErrorMsg);

            //    response.Error = true;
            //    response.ErrorMsg = ex.Message;
            //    return response;
            //}

            //try
            //{
            //    uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _ordersCollectionName);
            //    if (uniqueDBItemToUpdateFound != null)
            //    {
            //        EfCaPlannedTrip newValueObject = new EfCaPlannedTrip(uniqueDBItemToUpdateFound);
            //        response.PlannedTrips.Add(newValueObject);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    string errorMsg = $"UpdateOrder(..) failed, Exception: {ex.Message}";
            //    log.Error(errorMsg);

            //    response.Error = true;
            //    response.ErrorMsg = "Please refer to the details";
            //}

            return response;
        }

        public EfCaTourResp DeleteTours(InternalUserInfos internalUserInfos, List<string> itemIdents)
        {
            EfCaTourResp response = new EfCaTourResp();
            response.Error = true;
            response.ErrorMsg = "DeleteTours(..), not implemented";
            //try
            //{
            //    foreach (string ident in itemIdents)
            //    {
            //        var builder = Builders<EfCaPlannedTripDB>.Filter;
            //        var filter = builder.And(
            //            Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
            //            Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.extId", ident),
            //            Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.owner", true)
            //            );
            //        EfCaPlannedTripDB uniqueDBItemFound = FindUniqueByFilter(filter, _ordersCollectionName);

            //        if (uniqueDBItemFound == null)
            //        {
            //            response.Error = true;
            //            string logErrorMsg = $"DeleteOrders({internalUserInfos.TenantId}) failed. Order({ident}) not found";
            //            log.Error(logErrorMsg);
            //            string errorMsg = $"DeleteOrders(..) failed. Order({ident}) not found";
            //            response.ErrorMsg = errorMsg;
            //            if (response.DetailInfos == null)
            //            {
            //                response.DetailInfos = new List<DetailInfo>();
            //            }
            //            DetailInfo detailInfo = new DetailInfo() { Ident = ident };
            //            detailInfo.Detail = $"Order.Ident({ident}) did not exist. Deletion failed.";
            //            response.AddDetailInfos(detailInfo);
            //        }
            //        else
            //        {
            //            try
            //            {
            //                _plannedToursCollection.DeleteOne(filter);
            //            }
            //            catch (Exception ex)
            //            {
            //                if (response.DetailInfos == null)
            //                {
            //                    response.DetailInfos = new List<DetailInfo>();
            //                }
            //                DetailInfo detailInfo = new DetailInfo() { Ident = ident };
            //                log.Error($"AddDeleteOrders(${ident}) Exception: {ex.Message}");
            //                detailInfo.Detail = $"Delete Order.Ident({ident})  failed: {ex.Message}";
            //                response.AddDetailInfos(detailInfo);
            //            }
            //        }
            //    }
            //    return response;
            //}
            //catch (Exception ex)
            //{
            //    string errorMsg = $"DeleteOrders(..) failed, Exception: {ex.Message}";
            //    log.Error(errorMsg);

            //    response.Error = true;
            //    response.ErrorMsg = "Please refer to the details";
            //}

            return response;
        }

        public EfCaTourResp PatchTours(InternalUserInfos internalUserInfos, List<string> tourExtIds, string state)
        {
            EfCaTourResp response = new EfCaTourResp();

            var filterBuilder = Builders<EfCaPlannedTripDB>.Filter;
            var filter = filterBuilder.And(
                Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaPlannedTripDB>.Filter.In(tour => tour.TourExtId, tourExtIds),
                Builders<EfCaPlannedTripDB>.Filter.Eq("extIdDetails.owner", true)
                );
            UpdateDefinitionBuilder<EfCaPlannedTripDB> updateDefinition = new UpdateDefinitionBuilder<EfCaPlannedTripDB>();

            var updateBuilder = Builders<EfCaPlannedTripDB>.Update;
            var updates = new List<UpdateDefinition<EfCaPlannedTripDB>>();
            updates.Add(updateBuilder.Set(plannedTrip => plannedTrip.State, state));
            UpdateResult updateResult = PatchByFilterInternal(filter, updateBuilder.Combine(updates), _plannedToursCollectionName);

            return response;
        }
    }
}
