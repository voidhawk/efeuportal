﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcBuilding
    public partial class MongoDBSvc
    {
        #region efeuCampus building
        public EfCaBuildingResp AddBuildings(InternalUserInfos internalUserInfos, List<EfCaBuilding> buildings)
        {
            EfCaBuildingResp response = new EfCaBuildingResp();
            response.Error = false;
            response.Buildings = new List<EfCaBuilding>();

            foreach (EfCaBuilding item in buildings)
            {
                try
                {
                    EfCaBuildingDB addItemToDB = new EfCaBuildingDB(internalUserInfos, item);
                    _buildingCollection.InsertOne(addItemToDB);

                    EfCaBuilding newValueObject = new EfCaBuilding(addItemToDB);
                    response.Buildings.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddBuilding({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddBuilding(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaBuildingResp FindBuildingsByFinder(InternalUserInfos internalUserInfos, EfCaBuilding finder)
        {
            EfCaBuildingResp response = new EfCaBuildingResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaBuildingDB>> filterDefinitions = new List<FilterDefinition<EfCaBuildingDB>>();
                    filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaBuildingDB>.Filter;
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.Type != null)
                    {
                        filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.Eq("type", finder.Type));
                    }
                    if (finder.AddressId != null)
                    {
                        filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.Eq("addressId", finder.AddressId));
                    }
                    if (finder.BoxMountingDeviceIds != null && finder.BoxMountingDeviceIds.Count > 0)
                    {
                        filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.In("boxMountingDeviceIds", finder.BoxMountingDeviceIds));
                        //filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.In(building => building.BoxMountingDeviceIds, finder.BoxMountingDeviceIds));
                        //filterDefinitions.Add(Builders<EfCaPlannedTripDB>.Filter.Eq(t => t.TourExtId, finder.TourExtId));
                    }
                    if (finder.ContactIds != null && finder.ContactIds.Count > 0)
                    {
                        filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.In("contactIds", finder.ContactIds));
                        //filterDefinitions.Add(Builders<EfCaBuildingDB>.Filter.In(building => building.BoxMountingDeviceIds, finder.BoxMountingDeviceIds));
                        //filterDefinitions.Add(Builders<EfCaPlannedTripDB>.Filter.Eq(t => t.TourExtId, finder.TourExtId));
                    }


                    //public List<string> SyncMeetingPointIds { get; set; }
                    //public List<string> ApartmentIds { get; set; }
                    //public List<string> ContactIds { get; set; }
                    //public List<string> BoxMountingDeviceIds { get; set; }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaBuildingDB> dbItemsFound = FindAllByFilter(filter, _buildingCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.Buildings = new List<EfCaBuilding>();
                        foreach (EfCaBuildingDB item in dbItemsFound)
                        {
                            EfCaBuilding newValueObject = new EfCaBuilding(item);
                            response.Buildings.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindBuildingsByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaBuildingResp ReadAllBuildings(InternalUserInfos internalUserInfo)
        {
            EfCaBuildingResp response = new EfCaBuildingResp();

            try
            {
                var builder = Builders<EfCaBuildingDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.originator", internalUserInfo.TenantId),
                    Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaBuildingDB> buildingsDB = FindAllByFilter(filter, _buildingCollectionName);
                if (buildingsDB != null && buildingsDB.Count > 0)
                {
                    response.Buildings = new List<EfCaBuilding>();
                    foreach (EfCaBuildingDB item in buildingsDB)
                    {
                        EfCaBuilding building = new EfCaBuilding(item);
                        response.Buildings.Add(building);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllEfeuBuildings(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaBuildingResp UpdateBuilding(InternalUserInfos internalUserInfos, EfCaBuilding building)
        {
            EfCaBuildingResp response = new EfCaBuildingResp();

            var builder = Builders<EfCaBuildingDB>.Filter;
            var filter = builder.And(
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.extId", building.Ident),
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaBuildingDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _buildingCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateBuilding({internalUserInfos.TenantId}) failed. Building({building.Ident}) not found";
                log.Error(logErrorMsg);
                string errorMsg = $"UpdateBuilding(..) failed. Building({building.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(building);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateBuilding({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _buildingCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaBuilding newValueObject = new EfCaBuilding(uniqueDBItemToUpdateFound);
                    response.Buildings.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateBuilding(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaBuildingResp DeleteBuilding(InternalUserInfos internalUserInfo, List<string> buildingIdents)
        {
            EfCaBuildingResp response = new EfCaBuildingResp();

            try
            {
                foreach (string ident in buildingIdents)
                {
                    var builder = Builders<EfCaBuildingDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.originator", internalUserInfo.TenantId),
                        Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaBuildingDB uniqueDBItemFound = FindUniqueByFilter(filter, _buildingCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        response.Error = true;
                        string logErrorMsg = $"DeleteBuilding({internalUserInfo.TenantId}) failed. Building({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteBuilding(..) failed. Building({ident}) not found";
                        response.ErrorMsg = errorMsg;
                        return response;
                    }
                    else
                    {
                        _buildingCollection.DeleteOne(filter);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteBuilding(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        /// <summary>
        /// Such eine Buildings über dessen eindeutige ID (Ident)
        /// </summary>
        /// <param name="internalUserInfo"></param>
        /// <param name="ident"></param>
        /// <returns></returns>
        private EfCaBuildingDB FindBuilding(InternalUserInfos internalUserInfo, string ident)
        {
            var builder = Builders<EfCaBuildingDB>.Filter;
            var filter = builder.And(
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.originator", internalUserInfo.TenantId),
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.extId", ident),
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaBuildingDB uniqueDBItemFound = FindUniqueByFilter(filter, _buildingCollectionName);

            return uniqueDBItemFound;
        }
        #endregion

        #region references
        /// <summary>
        /// 
        /// </summary>
        /// <param name="internalUserInfo"></param>
        /// <param name="ident"></param>
        /// <param name="type">boxMountingDevice, contact, apartment</param>
        /// <returns></returns>
        private EfCaBuildingDB FindBuildingByRef(InternalUserInfos internalUserInfo, string type, string ident)
        {
            var builder = Builders<EfCaBuildingDB>.Filter;
            var filter = builder.And(
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.originator", internalUserInfo.TenantId),
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.owner", true),
                Builders<EfCaBuildingDB>.Filter.Eq(type + "Ids", ident)
                );
            EfCaBuildingDB uniqueDBItemFound = FindUniqueByFilter(filter, _buildingCollectionName);

            return uniqueDBItemFound;
        }

        public EfCaBuildingResp FindBuildingsByRef(InternalUserInfos internalUserInfo, string type, string ident)
        {
            EfCaBuildingResp buildingResp = new EfCaBuildingResp();

            var builder = Builders<EfCaBuildingDB>.Filter;
            var filter = builder.And(
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.originator", internalUserInfo.TenantId),
                Builders<EfCaBuildingDB>.Filter.Eq("extIdDetails.owner", true),
                Builders<EfCaBuildingDB>.Filter.Eq(type + "Ids", ident)
                );
            List<EfCaBuildingDB> buildingsDB = FindAllByFilter(filter, _buildingCollectionName);
            if (buildingsDB != null && buildingsDB.Count > 0)
            {
                buildingResp.Buildings = new List<EfCaBuilding>();
                foreach (EfCaBuildingDB item in buildingsDB)
                {
                    EfCaBuilding building = new EfCaBuilding(item);
                    buildingResp.Buildings.Add(building);
                }
            }
            else
            {
                buildingResp.Error = true;
            }

            return buildingResp;
        }
        #endregion
    }
}
