﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.ChargingStation;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcChargingStationService
    public partial class MongoDBSvc
    {
        #region efeuCampus DockingStation
        public EfCaChargingStationResp AddChargingStations(InternalUserInfos internalUserInfos, List<EfCaChargingStation> addItems)
        {
            EfCaChargingStationResp response = new EfCaChargingStationResp();
            response.Error = false;
            response.ChargingStations = new List<EfCaChargingStation>();

            foreach (EfCaChargingStation item in addItems)
            {
                try
                {
                    EfCaChargingStationDB addItemToDB = new EfCaChargingStationDB(internalUserInfos, item);
                    _chargingStationCollection.InsertOne(addItemToDB);

                    EfCaChargingStation newValueObject = new EfCaChargingStation(addItemToDB);
                    response.ChargingStations.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddChargingStations({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddChargingStations(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaChargingStationResp FindChargingStationsByFinder(InternalUserInfos internalUserInfos, EfCaChargingStation finder)
        {
            EfCaChargingStationResp response = new EfCaChargingStationResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaChargingStationDB>> filterDefinitions = new List<FilterDefinition<EfCaChargingStationDB>>();
                    filterDefinitions.Add(Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaChargingStationDB>.Filter;
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.Type != null)
                    {
                        filterDefinitions.Add(Builders<EfCaChargingStationDB>.Filter.Eq("type", finder.Type));
                    }
                    if (finder.Info != null)
                    {
                        filterDefinitions.Add(Builders<EfCaChargingStationDB>.Filter.Regex("info", $"{finder.Info}.*"));
                    }

                    //public List<string> SupportedVehicleTypes { get; set; }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaChargingStationDB> dbItemsFound = FindAllByFilter(filter, _chargingStationCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.ChargingStations = new List<EfCaChargingStation>();
                        foreach (EfCaChargingStationDB item in dbItemsFound)
                        {
                            EfCaChargingStation newValueObject = new EfCaChargingStation(item);
                            response.ChargingStations.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindChargingStationsByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaChargingStationResp ReadAllChargingStations(InternalUserInfos internalUserInfos)
        {
            EfCaChargingStationResp response = new EfCaChargingStationResp();

            try
            {
                var builder = Builders<EfCaChargingStationDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaChargingStationDB> dbItems = FindAllByFilter(filter, _chargingStationCollectionName);
                if (dbItems != null && dbItems.Count > 0)
                {
                    response.ChargingStations = new List<EfCaChargingStation>();
                    foreach (EfCaChargingStationDB dbItem in dbItems)
                    {
                        EfCaChargingStation item = new EfCaChargingStation(dbItem);
                        response.ChargingStations.Add(item);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllChargingStations(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaChargingStationResp UpdateChargingStation(InternalUserInfos internalUserInfos, EfCaChargingStation updateItem)
        {
            EfCaChargingStationResp response = new EfCaChargingStationResp();

            var builder = Builders<EfCaChargingStationDB>.Filter;
            var filter = builder.And(
                Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.extId", updateItem.Ident),
                Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaChargingStationDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _chargingStationCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateChargingStation({internalUserInfos.TenantId}) failed. ChargingStation({updateItem.Ident}) not found";
                log.Error(logErrorMsg);
                string errorMsg = $"UpdateChargingStation(..) failed. ChargingStation({updateItem.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(updateItem);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateChargingStation({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _chargingStationCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaChargingStation newValueObject = new EfCaChargingStation(uniqueDBItemToUpdateFound);
                    response.ChargingStations.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateChargingStation(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaChargingStationResp DeleteChargingStations(InternalUserInfos internalUserInfos, List<string> itemIdents)
        {
            EfCaChargingStationResp response = new EfCaChargingStationResp();

            try
            {
                foreach (string ident in itemIdents)
                {
                    var builder = Builders<EfCaChargingStationDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaChargingStationDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaChargingStationDB uniqueDBItemFound = FindUniqueByFilter(filter, _chargingStationCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        //JSt: ToDo: das Verhalten ist nicht richtig, es kann ja nur ein Löschvorgang fehlschlagen
                        response.Error = true;
                        string logErrorMsg = $"DeleteChargingStations({internalUserInfos.TenantId}) failed. DockingStation({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteChargingStations(..) failed. DockingStation({ident}) not found";
                        response.ErrorMsg = errorMsg;
                        return response;
                    }
                    else
                    {
                        _chargingStationCollection.DeleteOne(filter);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteChargingStations(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }
        #endregion
    }
}
