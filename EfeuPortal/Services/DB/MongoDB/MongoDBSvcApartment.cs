﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Apartment;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcApartment
    public partial class MongoDBSvc
    {
        #region efeuCampus apartment data
        public EfCaApartmentResp AddApartment(InternalUserInfos internalUserInfos, List<EfCaApartment> apartments)
        {
            EfCaApartmentResp response = new EfCaApartmentResp();
            response.Error = false;
            response.Apartments = new List<EfCaApartment>();

            foreach (EfCaApartment item in apartments)
            {
                try
                {
                    EfCaApartmentDB addItemToDB = new EfCaApartmentDB(internalUserInfos, item);
                    _apartmentCollection.InsertOne(addItemToDB);

                    EfCaApartment newValueObject = new EfCaApartment(addItemToDB);
                    response.Apartments.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddApartment({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddApartment(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaApartmentResp FindApartmentsByFinder(InternalUserInfos internalUserInfos, EfCaApartment finder)
        {
            EfCaApartmentResp response = new EfCaApartmentResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaApartmentDB>> filterDefinitions = new List<FilterDefinition<EfCaApartmentDB>>();
                    filterDefinitions.Add(Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaApartmentDB>.Filter;
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.AddressId != null)
                    {
                        filterDefinitions.Add(Builders<EfCaApartmentDB>.Filter.Eq("addressId", finder.AddressId));
                    }

                    //public string  { get; set; }
                    //public string Info { get; set; }
                    //public List<string> ContactIds { get; set; }
                    //public List<string> BoxMountingDeviceIds { get; set; }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaApartmentDB> dbItemsFound = FindAllByFilter(filter, _apartmentCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.Apartments = new List<EfCaApartment>();
                        foreach (EfCaApartmentDB item in dbItemsFound)
                        {
                            EfCaApartment newValueObject = new EfCaApartment(item);
                            response.Apartments.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindApartmentsByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaApartmentResp ReadAllApartments(InternalUserInfos internalUserInfos)
        {
            EfCaApartmentResp response = new EfCaApartmentResp();

            try
            {
                var builder = Builders<EfCaApartmentDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaApartmentDB> apartmentsDB = FindAllByFilter(filter, _apartmentCollectionName);
                if (apartmentsDB != null && apartmentsDB.Count > 0)
                {
                    response.Apartments = new List<EfCaApartment>();
                    foreach (EfCaApartmentDB item in apartmentsDB)
                    {
                        EfCaApartment apartment = new EfCaApartment(item);
                        response.Apartments.Add(apartment);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllApartments(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaApartmentResp UpdateApartment(InternalUserInfos internalUserInfos, EfCaApartment apartment)
        {
            EfCaApartmentResp response = new EfCaApartmentResp();

            var builder = Builders<EfCaApartmentDB>.Filter;
            var filter = builder.And(
                Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.extId", apartment.Ident),
                Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaApartmentDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _apartmentCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateApartment({internalUserInfos.TenantId}) failed. Apartment({apartment.Ident}) not found";
                log.Error(logErrorMsg);
                string errorMsg = $"UpdateApartment(..) failed. Apartment({apartment.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(apartment);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateApartment({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _apartmentCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaApartment newValueObject = new EfCaApartment(uniqueDBItemToUpdateFound);
                    response.Apartments.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateApartment(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaApartmentResp DeleteApartments(InternalUserInfos internalUserInfos, List<string> apartmentIdents)
        {
            EfCaApartmentResp response = new EfCaApartmentResp();

            try
            {
                foreach (string ident in apartmentIdents)
                {
                    var builder = Builders<EfCaApartmentDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaApartmentDB uniqueDBItemFound = FindUniqueByFilter(filter, _apartmentCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        response.Error = true;
                        string logErrorMsg = $"DeleteApartment({internalUserInfos.TenantId}) failed. Apartment({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteApartment(..) failed. Apartment({ident}) not found";
                        response.ErrorMsg = errorMsg;
                        return response;
                    }
                    else
                    {
                        _apartmentCollection.DeleteOne(filter);
                    }
                }

                //if (type.Equals("BoxMountingDevice"))
                //{
                //    apartmentResp = ModifyBoundingBoxReference(tenantId, action, ident, apartmentDB);
                //}
                //else if (type.Equals("Contact"))
                //{
                //    apartmentResp = ModifyContactReference(tenantId, action, ident, apartmentDB);
                //}
                //else
                //{
                //    apartmentResp = new EC_ApartmentResp();
                //    apartmentResp.Error = true;
                //    apartmentResp.ErrorMsg = $"Type({type}) not supported";
                //    return apartmentResp;
                //}

            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteApartment(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        private EfCaApartmentDB FindEfeuApartment(InternalUserInfos internalUserInfos, string ident)
        {
            var builder = Builders<EfCaApartmentDB>.Filter;
            var filter = builder.And(
                Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.extId", ident),
                Builders<EfCaApartmentDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaApartmentDB uniqueDBItemFound = FindUniqueByFilter(filter, _apartmentCollectionName);

            return uniqueDBItemFound;
        }
        #endregion

        #region apartment modify references
        public EfCaApartmentResp PutModifyApartmentReferences(InternalUserInfos internalUserInfos, string action, string type, string ident, EfCaApartment apartment)
        {
            EfCaApartmentResp response = new EfCaApartmentResp();
            response = new EfCaApartmentResp();
            response.Error = true;
            response.ErrorMsg = "PutModifyApartmentReferences(..) not implemented.";

            // JSt: ToDo: Parameter check: action, type, ident
            //EfCaApartmentDB apartmentDB = _mongoDBService.FindEfeuApartment(tenantId, apartment.Ident);
            //if (apartmentDB == null)
            //{
            //    response = new EfCaApartmentResp();
            //    response.Error = true;
            //    response.ErrorMsg = "Apartment not valid.";
            //    return response;
            //}

            //if (apartmentDB.CurrentVersion != apartment.Version)
            //{
            //    response = new EfCaApartmentResp();
            //    response.Error = true;
            //    response.ErrorMsg = "Version outdated.";
            //    return response;
            //}

            //if (type.Equals("BoxMountingDevice"))
            //{
            //    response = ModifyBoundingBoxReference(tenantId, action, ident, apartmentDB);
            //}
            //else if (type.Equals("Contact"))
            //{
            //    response = ModifyContactReference(tenantId, action, ident, apartmentDB);
            //}
            //else
            //{
            //    response = new EfCaApartmentResp();
            //    response.Error = true;
            //    response.ErrorMsg = $"Type({type}) not supported";
            //    return response;
            //}

            return response;
        }

        public EfCaApartmentResp ModifyBoundingBoxReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaApartment apartmentDB)
        {
            EfCaApartmentResp response = null;
            response = new EfCaApartmentResp();
            response.Error = true;
            response.ErrorMsg = "ModifyBoundingBoxReference(..) not implemented.";

            //EfCaBoxMountingDeviceDB boxMountingDeviceDB = _mongoDBService.FindEfeuBoxMountingDevice(tenantId, ident);
            //if (boxMountingDeviceDB == null)
            //{
            //    response.Error = true;
            //    response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is not valid.";
            //    return response;
            //}

            //if (action.Equals("ADD"))
            //{
            //    if (apartmentDB.BoxMountingDeviceIds == null)
            //    {
            //        apartmentDB.BoxMountingDeviceIds = new List<string>();
            //    }
            //    if (apartmentDB.BoxMountingDeviceIds.Contains(ident))
            //    {
            //        response = new EfCaApartmentResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is already assigned.";
            //        return response;
            //    }
            //    apartmentDB.BoxMountingDeviceIds.Add(ident);
            //}
            //else if (action.Equals("REMOVE"))
            //{
            //    if (apartmentDB.BoxMountingDeviceIds != null && !apartmentDB.BoxMountingDeviceIds.Contains(ident))
            //    {
            //        response = new EfCaApartmentResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is not available.";
            //        return response;
            //    }
            //    apartmentDB.BoxMountingDeviceIds.Remove(ident);
            //}

            //EfCaApartment apartment = new EfCaApartment(apartmentDB);
            //response = _mongoDBService.UpdateEfeuApartment(tenantId, apartment);
            return response;
        }

        public EfCaApartmentResp ModifyContactReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaApartment apartmentDB)
        {
            EfCaApartmentResp response = null;
            response = new EfCaApartmentResp();
            response.Error = true;
            response.ErrorMsg = "ModifyContactReference(..) not implemented.";


            //EfCaContactDB contactDB = _mongoDBService.FindEfeuContact(tenantId, ident);
            //if (contactDB == null)
            //{
            //    response.Error = true;
            //    response.ErrorMsg = $"Contact.Id({ident}) is not valid.";
            //    return response;
            //}

            //if (action.Equals("ADD"))
            //{
            //    if (apartmentDB.ContactIds == null)
            //    {
            //        apartmentDB.ContactIds = new List<string>();
            //    }
            //    if (apartmentDB.ContactIds.Contains(ident))
            //    {
            //        response = new EfCaApartmentResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"Contact.Id({ident}) is already assigned.";
            //        return response;
            //    }
            //    apartmentDB.ContactIds.Add(ident);
            //}
            //else if (action.Equals("REMOVE"))
            //{
            //    if (apartmentDB.ContactIds != null && !apartmentDB.ContactIds.Contains(ident))
            //    {
            //        response = new EfCaApartmentResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"Contact.Id({ident}) is not available.";
            //        return response;
            //    }
            //    apartmentDB.ContactIds.Remove(ident);
            //}

            //EfCaApartment apartment = new EfCaApartment(apartmentDB);
            //response = _mongoDBService.UpdateEfeuApartment(tenantId, apartment);
            return response;
        }

        #endregion
    }
}
