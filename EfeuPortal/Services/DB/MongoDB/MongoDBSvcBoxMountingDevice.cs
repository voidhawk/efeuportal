﻿using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcBoxMountingDevice

    /// <summary>
    /// Indexes:
    /// idx-tenant-extId:
    /// {
    ///    "extIdDetails.originator" : 1,
    ///    "extIdDetails.extId" : 1
    /// }
    /// </summary>
    public partial class MongoDBSvc
    {
        #region efeuCampus Übergabebock (BoxMounting)
        /// <summary>
        /// Index:
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="boxMountingDevices"></param>
        /// <returns></returns>
        public EfCaBoxMountingDeviceResp AddBoxMountingDevices(InternalUserInfos internalUserInfos, List<EfCaBoxMountingDevice> boxMountingDevices)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;
            response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();

            foreach (EfCaBoxMountingDevice item in boxMountingDevices)
            {
                try
                {
                    EfCaBoxMountingDeviceDB addItemToDB = new EfCaBoxMountingDeviceDB(internalUserInfos, item);
                    _boxMountingDeviceCollection.InsertOne(addItemToDB);

                    EfCaBoxMountingDevice newValueObject = new EfCaBoxMountingDevice(addItemToDB);
                    response.BoxMountingDevices.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddBoxMountingDevices({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident};
                    response.AddDetailInfos(detailInfo);
                    if(ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddBoxMountingDevices(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaBoxMountingDeviceResp FindBoxMountingDevicesByFinder(InternalUserInfos internalUserInfos, EfCaBoxMountingDevice finder, List<string> boxMountingDeviceIds = null)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaBoxMountingDeviceDB>> filterDefinitions = new List<FilterDefinition<EfCaBoxMountingDeviceDB>>();
                    filterDefinitions.Add(Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaBoxMountingDeviceDB>.Filter;
                    if (finder != null)
                    {
                        if (finder.Ident != null)
                        {
                            filterDefinitions.Add(Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                        }
                        if (finder.Type != null)
                        {
                            filterDefinitions.Add(Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("type", finder.Type));
                        }
                        if (finder.MountCapacity != null)
                        {
                            filterDefinitions.Add(Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("mountCapacity", finder.MountCapacity));
                        }
                    }
                    else if (boxMountingDeviceIds != null && boxMountingDeviceIds.Count > 0)
                    {
                        filterDefinitions.Add(Builders<EfCaBoxMountingDeviceDB>.Filter.In("extIdDetails.extId", boxMountingDeviceIds));
                    }
                    else
                    {
                        response.Error = true;
                        response.ErrorMsg = "Invalid Finder(s)";
                        return response;
                    }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaBoxMountingDeviceDB> dbItemsFound = FindAllByFilter(filter, _boxMountingDeviceCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();
                        foreach (EfCaBoxMountingDeviceDB item in dbItemsFound)
                        {
                            EfCaBoxMountingDevice newValueObject = new EfCaBoxMountingDevice(item);
                            response.BoxMountingDevices.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindBoxMountingDevicesByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = $"FindBoxMountingDevicesByFinder(..) failed, Hint: {ex.Message}";
            }

            return response;
        }

        public EfCaBoxMountingDeviceResp ReadAllBoxMountingDevices(InternalUserInfos internalUserInfos)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;
            try
            {
                var builder = Builders<EfCaBoxMountingDeviceDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaBoxMountingDeviceDB> boxMountingDevicesDB = FindAllByFilter(filter, _boxMountingDeviceCollectionName);
                if (boxMountingDevicesDB != null && boxMountingDevicesDB.Count > 0) 
                {
                    response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();
                    foreach (EfCaBoxMountingDeviceDB item in boxMountingDevicesDB)
                    {
                        EfCaBoxMountingDevice boxMountingDevice = new EfCaBoxMountingDevice(item);
                        response.BoxMountingDevices.Add(boxMountingDevice);
                    }
                }
                else
                {
                    response.ErrorMsg = "ReadAllBoxMountingDevices(..), no entries available.";
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllBoxMountingDevices(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaBoxMountingDeviceResp UpdateBoxMountingDevice(InternalUserInfos internalUserInfos, EfCaBoxMountingDevice boxMountingDevice)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;

            var builder = Builders<EfCaBoxMountingDeviceDB>.Filter;
            var filter = builder.And(
                Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.extId", boxMountingDevice.Ident),
                Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaBoxMountingDeviceDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _boxMountingDeviceCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateBoxMountingDevice({internalUserInfos.TenantId}, {internalUserInfos.Email}) failed. MountingDevice({boxMountingDevice.Ident}) not found";
                log.Error(logErrorMsg);

                string errorMsg = $"UpdateBoxMountingDevice(..) failed. MountingDevice({boxMountingDevice.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(boxMountingDevice);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateEfeuMountingDevice({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _boxMountingDeviceCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaBoxMountingDevice newValueObject = new EfCaBoxMountingDevice(uniqueDBItemToUpdateFound);
                    if(response.BoxMountingDevices == null)
                    {
                        response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();
                    }
                    response.BoxMountingDevices.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateEfeuMountingDevice(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaBoxMountingDeviceResp DeleteBoxMountingDevice(InternalUserInfos internalUserInfos, List<string> transportBoxIdents)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;
            try
            {
                foreach (string ident in transportBoxIdents)
                {
                    var builder = Builders<EfCaBoxMountingDeviceDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaBoxMountingDeviceDB uniqueDBItemFound = FindUniqueByFilter(filter, _boxMountingDeviceCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        response.Error = true;
                        string logErrorMsg = $"DeleteBoxMountingDevice({internalUserInfos.TenantId}, {internalUserInfos.Email}) failed. BoxMountingDevice({ident}) not found";
                        log.Error(logErrorMsg);

                        string errorMsg = $"DeleteBoxMountingDevice(..) failed. BoxMountingDevice({ident}) not found";
                        response.ErrorMsg = errorMsg;
                        return response;
                    }
                    else
                    {
                        _boxMountingDeviceCollection.DeleteOne(filter);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteBoxMountingDevice(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaBoxMountingDeviceResp GetBoxMountingDeviceDetails(InternalUserInfos internalUserInfos, string ident)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;
            response.ErrorMsg = "GetBoxMountingDeviceDetails(..) not finally implemented. Check the result, especially the BuildingIDs";

            EfCaBoxMountingDeviceDB boxMountingDeviceDB = FindBoxMountingDevice(internalUserInfos, ident);
            if (boxMountingDeviceDB == null)
            {
                string errorMsg = $"GetBoxMountingDeviceDetails({internalUserInfos.TenantId}, {ident}) is not known";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = $"BoxMountingDevice({ident}) is not known"; ;
                return response;
            }

            EfCaBoxMountingDevice boxMountingDevice = new EfCaBoxMountingDevice(boxMountingDeviceDB);
            response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();
            response.BoxMountingDevices.Add(boxMountingDevice);

            //EfCaBuildingResp buildingResp = _efeuBuildingService.FindAllEfeuBuildingsByRef(tenantId, "BoxMountingDevice", ident);
            //if (buildingResp.Error == false && buildingResp.Buildings != null && buildingResp.Buildings.Count > 0)
            //{
            //    response.BuildingIds = new List<string>();
            //    foreach (EC_Building building in buildingResp.Buildings)
            //    {
            //        response.BuildingIds.Add(building.Ident);
            //    }
            //}

            return response;
        }

        private EfCaBoxMountingDeviceDB FindBoxMountingDevice(InternalUserInfos internalUserInfos, string ident)
        {
            var builder = Builders<EfCaBoxMountingDeviceDB>.Filter;
            var filter = builder.And(
                Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.extId", ident),
                Builders<EfCaBoxMountingDeviceDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaBoxMountingDeviceDB uniqueDBItemFound = FindUniqueByFilter(filter, _boxMountingDeviceCollectionName);

            return uniqueDBItemFound;
        }

        public EfCaBoxMountingDeviceResp FindBoxMountingDeviceReservationsByFinder(InternalUserInfos internalUserInfos, EfCaBoxMountingDeviceSlot finder)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaBoxMountingDeviceSlotDB>> filterDefinitions = new List<FilterDefinition<EfCaBoxMountingDeviceSlotDB>>();
                    filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSlotDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSlotDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaBoxMountingDeviceSlotDB>.Filter;
                    if (finder.BockIdent != null)
                    {
                        filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSlotDB>.Filter.Eq("bockIdent", finder.BockIdent));
                    }
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSlotDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.OrderIdent != null)
                    {
                        filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSlotDB>.Filter.Eq(o => o.OrderIdent, finder.OrderIdent));
                    }

                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaBoxMountingDeviceSlotDB> dbItemsFound = FindAllByFilter(filter, _boxMountEntryCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        //response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();
                        //foreach (EfCBoxMountEntryDB item in dbItemsFound)
                        //{
                        //    EfCaBoxMountingDevice newValueObject = new EfCaBoxMountingDevice(item);
                        //    response.BoxMountingDevices.Add(newValueObject);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindBoxMountingDeviceReservationsByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaBoxMountingDeviceResp AddBoxMountingDeviceReservations(InternalUserInfos internalUserInfos, List<EfCaBoxMountingDeviceSlot> boxMountEntrys)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;
            response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();

            foreach (EfCaBoxMountingDeviceSlot item in boxMountEntrys)
            {
                try
                {
                    EfCaBoxMountingDeviceSlotDB addItemToDB = new EfCaBoxMountingDeviceSlotDB(internalUserInfos, item);
                    _boxMountEntryCollection.InsertOne(addItemToDB);

                    //EfCBoxMountEntry newValueObject = new EfCBoxMountEntry(addItemToDB);
                    //response.BoxMountingDevices.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddBoxMountingDevices({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddBoxMountingDevices(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaBoxMountingDeviceResp UpdateBoxMountingDeviceReservation(InternalUserInfos internalUserInfos, EfCaBoxMountingDeviceSlot boxMountingDeviceSlot)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;

            var builder = Builders<EfCaBoxMountingDeviceSlotDB>.Filter;
            var filter = builder.And(
                Builders<EfCaBoxMountingDeviceSlotDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaBoxMountingDeviceSlotDB>.Filter.Eq("extIdDetails.extId", boxMountingDeviceSlot.Ident),
                Builders<EfCaBoxMountingDeviceSlotDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaBoxMountingDeviceSlotDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _boxMountEntryCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateBoxMountingDeviceReservation({internalUserInfos.TenantId}, {internalUserInfos.Email}) failed. MountingDevice({boxMountingDeviceSlot.Ident}) not found";
                log.Error(logErrorMsg);

                string errorMsg = $"UpdateBoxMountingDeviceReservation(..) failed. MountingDevice({boxMountingDeviceSlot.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(boxMountingDeviceSlot);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateBoxMountingDeviceReservation({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _boxMountingDeviceCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaBoxMountingDeviceSlot newValueObject = new EfCaBoxMountingDeviceSlot(uniqueDBItemToUpdateFound);
                    response.BoxMountingDeviceSlots = new List<EfCaBoxMountingDeviceSlot>();
                    response.BoxMountingDeviceSlots.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateBoxMountingDeviceReservation(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        #region EfCaBoxMountingDeviceScheduler
        public EfCaBoxMountingDeviceResp AddBoxMountingDeviceSchedulers(InternalUserInfos internalUserInfos, List<EfCaBoxMountingDeviceScheduler> boxMountingDeviceSchedulers)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;
            response.BoxMountingDeviceSchedulers = new List<EfCaBoxMountingDeviceScheduler>();

            foreach (EfCaBoxMountingDeviceScheduler item in boxMountingDeviceSchedulers)
            {
                try
                {
                    EfCaBoxMountingDeviceSchedulerDB addItemToDB = new EfCaBoxMountingDeviceSchedulerDB(internalUserInfos, item);
                    _boxMountingDeviceSchedulerCollection.InsertOne(addItemToDB);

                    EfCaBoxMountingDeviceScheduler newValueObject = new EfCaBoxMountingDeviceScheduler(addItemToDB);
                    response.BoxMountingDeviceSchedulers.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddBoxMountingDeviceScheduler({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddBoxMountingDeviceScheduler(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaBoxMountingDeviceResp FindBoxMountingDeviceSchedulersByFinder(InternalUserInfos internalUserInfos, EfCaBoxMountingDeviceScheduler finder, List<string> boxMountingDeviceSchedulerIds)
        {
            //EfCaSlotReservation
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaBoxMountingDeviceSchedulerDB>> filterDefinitions = new List<FilterDefinition<EfCaBoxMountingDeviceSchedulerDB>>();
                    filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    var builderFilter = Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter;
                    if (finder != null)
                    {
                        filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq("extIdDetails.owner", true));
                        if (finder.Ident != null)
                        {
                            filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                        }
                        if (finder.BoxMountingDeviceId != null)
                        {
                            filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq(find => find.BoxMountingDeviceId, finder.BoxMountingDeviceId));
                        }
                        if (finder.SlotDescription != null)
                        {
                            filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq(find => find.SlotDescription, finder.SlotDescription));
                        }
                    }
                    else if (boxMountingDeviceSchedulerIds != null && boxMountingDeviceSchedulerIds.Count > 0)
                    {
                        filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.In("extIdDetails.extId", boxMountingDeviceSchedulerIds));
                    }
                    else
                    {
                        response.Error = true;
                        response.ErrorMsg = "Invalid Finder(s)";
                        return response;
                    }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaBoxMountingDeviceSchedulerDB> dbItemsFound = FindAllByFilter(filter, _boxMountingDeviceSchedulerCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.BoxMountingDeviceSchedulers = new List<EfCaBoxMountingDeviceScheduler>();
                        foreach (EfCaBoxMountingDeviceSchedulerDB item in dbItemsFound)
                        {
                            EfCaBoxMountingDeviceScheduler newValueObject = new EfCaBoxMountingDeviceScheduler(item);
                            response.BoxMountingDeviceSchedulers.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindBoxMountingDeviceSchedulersByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }


        /// <summary>
        /// MongoDB aggregation finder
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="finder"></param>
        /// <returns></returns>
        public EfCaBoxMountingDeviceResp FindBoxMountingDeviceSchedulersByReservationFinder(InternalUserInfos internalUserInfos, EfCaSlotReservation finder)
        {
            //EfCaSlotReservation
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            string errorMsg = $"FindBoxMountingDeviceSchedulersByReservationFinder(..) failed, not implemented";
            log.Error(errorMsg);

            response.Error = true;
            response.ErrorMsg = errorMsg;

            //response.Error = false;

            //try
            //{
            //    {
            //        #region create the find Filter
            //        List<FilterDefinition<EfCaBoxMountingDeviceSchedulerDB>> filterDefinitions = new List<FilterDefinition<EfCaBoxMountingDeviceSchedulerDB>>();
            //        filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
            //        if (finder != null)
            //        {
            //            filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq("extIdDetails.owner", true));
            //            if (finder.BoxMountingDeviceId != null)
            //            {
            //                filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq(find => find.BoxMountingDeviceId, finder.BoxMountingDeviceId));
            //            }
            //            if (finder.MountingSlot.GetValueOrDefault(-1) != -1)
            //            {
            //                filterDefinitions.Add(Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq(find => find.SlotDescription, "Slot-" + finder.MountingSlot));
            //            }

            //            if (finder.CalendarWeek == null)
            //            {
            //                if (finder.Start == DateTimeOffset.MinValue || finder.Start == DateTimeOffset.MaxValue)
            //                {
            //                    // Jetzt müssen Die DateTimeOffsets gesetzt sein
            //                    response.Error = true;
            //                    response.ErrorMsg = "FindBoxMountingDeviceSchedulersByReservationFinder(..) failed. Invalid Finder(s) Start is not processed";
            //                    return response;
            //                }
            //                int calendarWeek = DataHelper.GetWeekNumber(finder.Start);
            //                finder.CalendarWeek = calendarWeek.ToString();
            //            }
            //        }
            //        else
            //        {
            //            response.Error = true;
            //            response.ErrorMsg = "Invalid Finder(s)";
            //            return response;
            //        }
            //        var builderFilter = Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter;
            //        var filter = builderFilter.And(filterDefinitions.ToArray());
            //        #endregion

            //        #region builder Projection
            //        //int test = Convert.ToInt16("Slot-1".Split("-")[1]);
            //        ProjectionDefinition<EfCaBoxMountingDeviceSchedulerDB, EfCaSlotReservations> projection =
            //            Builders<EfCaBoxMountingDeviceSchedulerDB>.Projection.Expression(sdb => new EfCaSlotReservations
            //            {
            //                BoxMountingDeviceId = sdb.BoxMountingDeviceId,
            //                MountingSlot = sdb.SlotDescription, //Slot-0
            //                Scheduler = sdb.PtvSlotReservations.GetValueOrDefault(finder.CalendarWeek)
            //            }); ;

            //        #endregion

            //        //List<EfCaBoxMountingDeviceSchedulerDB> dbItemsFound = FindAllByFilter(filter, _boxMountingDeviceSchedulerCollectionName);
            //        List<EfCaSlotReservations> dbItemsFound = FindAllByAggregation(filter, projection, _boxMountingDeviceSchedulerCollectionName);

            //        if (dbItemsFound != null && dbItemsFound.Count > 0)
            //        {
            //            foreach (EfCaSlotReservations item in dbItemsFound)
            //            {
            //                item.CalendarWeek = finder.CalendarWeek;
            //            }
            //            response.SlotReservations = dbItemsFound;
            //        }
            //        else
            //        {
            //            response.SlotReservations = new List<EfCaSlotReservations>();
            //            response.ErrorMsg = $"FindBoxMountingDeviceSchedulersByReservationFinder(..), no entries available";
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    string errorMsg = $"FindBoxMountingDeviceSchedulersByReservationFinder(..) failed, Exception: {ex.Message}";
            //    log.Error(errorMsg);

            //    response.Error = true;
            //    response.ErrorMsg = errorMsg;
            //}

            return response;
        }

        public EfCaBoxMountingDeviceResp UpdateBoxMountingDeviceScheduler(InternalUserInfos internalUserInfos, EfCaBoxMountingDeviceScheduler boxMountingDeviceScheduler)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.Error = false;

            var builder = Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter;
            var filter = builder.And(
                Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq("extIdDetails.extId", boxMountingDeviceScheduler.Ident),
                Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq("extIdDetails.owner", true),
                Builders<EfCaBoxMountingDeviceSchedulerDB>.Filter.Eq(find => find.SlotDescription, boxMountingDeviceScheduler.SlotDescription)
                );
            EfCaBoxMountingDeviceSchedulerDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _boxMountingDeviceSchedulerCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateBoxMountingDeviceScheduler({internalUserInfos.TenantId}, {internalUserInfos.Email}) failed. MountingDevice({boxMountingDeviceScheduler.Ident}) not found";
                log.Error(logErrorMsg);

                string errorMsg = $"UpdateBoxMountingDeviceScheduler(..) failed. MountingDevice({boxMountingDeviceScheduler.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(boxMountingDeviceScheduler);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateBoxMountingDeviceScheduler({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _boxMountingDeviceSchedulerCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaBoxMountingDeviceScheduler newValueObject = new EfCaBoxMountingDeviceScheduler(uniqueDBItemToUpdateFound);
                    response.BoxMountingDeviceSchedulers = new List<EfCaBoxMountingDeviceScheduler>();
                    response.BoxMountingDeviceSchedulers.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateBoxMountingDeviceScheduler(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        #endregion
        #endregion
    }
}
