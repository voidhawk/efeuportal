﻿using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Models.System.MongoDB;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcSystem
    public partial class MongoDBSvc
    {
        #region auth
        public LoginResp UserLogin(EfCaLogin login)
        {
            LoginResp response = new LoginResp();
            response.Error = false;

            try
            {
                #region create the Filter
                List<FilterDefinition<EfCaUserDB>> filterDefinitions = new List<FilterDefinition<EfCaUserDB>>();
                filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Eq("email", login.Email));
                filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Eq("password", login.Password));
                #endregion

                var builderFilter = Builders<EfCaUserDB>.Filter;
                var filter = builderFilter.And(filterDefinitions.ToArray());
                EfCaUserDB dbItemFound = FindUniqueByFilter(filter, _usersCollectionName);
                if (dbItemFound == null)
                {
                    response.Error = true;
                    response.ErrorMsg = "Login invalid";
                    return response;
                }
                EfCaUser newValueObject = new EfCaUser(dbItemFound);
                response.User = newValueObject;
                response.TenantId = dbItemFound.TenantId;
            }
            catch (Exception ex)
            {
                string errorMsg = $"UserLogin(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }
        #endregion

        public EfCaUserResp GetUserByEmail(string email)
        {
            EfCaUserResp response = new EfCaUserResp();
            response.Error = false;
            try
            {
                #region create the Filter
                List<FilterDefinition<EfCaUserDB>> filterDefinitions = new List<FilterDefinition<EfCaUserDB>>();
                filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Eq("email", email));
                #endregion

                var builderFilter = Builders<EfCaUserDB>.Filter;
                var filter = builderFilter.And(filterDefinitions.ToArray());
                EfCaUserDB dbItemFound = FindUniqueByFilter(filter, _usersCollectionName);
                if (dbItemFound == null)
                {
                    response.Error = true;
                    response.ErrorMsg = "Login invalid";
                    return response;
                }
                EfCaUser newValueObject = new EfCaUser(dbItemFound);
                response.Users.Add(newValueObject);
            }
            catch (Exception ex)
            {
                string errorMsg = $"UserLogin(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaUserResp UpdatePasswordByEmail(string email, string password)
        {
            EfCaUserResp response = new EfCaUserResp();
            response.Error = false;

            #region create the Filter
            List<FilterDefinition<EfCaUserDB>> filterDefinitions = new List<FilterDefinition<EfCaUserDB>>();
            filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Eq("email", email));
            #endregion

            var builderFilter = Builders<EfCaUserDB>.Filter;
            var filter = builderFilter.And(filterDefinitions.ToArray());
            EfCaUserDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _usersCollectionName);
            EfCaUser item = new EfCaUser(uniqueDBItemToUpdateFound);
            if (uniqueDBItemToUpdateFound == null)
            {
                string logErrorMsg = $"UpdatePasswordByEmail({email}) not found";
                log.Error(logErrorMsg);

                response.Error = true;
                string errorMsg = $"UpdatePasswordByEmail(..) failed. Address({email}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(item, password);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdatePasswordByEmail({email}) failed. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _usersCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaUser newValueObject = new EfCaUser(uniqueDBItemToUpdateFound);
                    response.Users.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdatePasswordByEmail(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }




        #region user
        public EfCaUserResp AddUser(InternalUserInfos internalUserInfos, EfCaUser item, string passwordHash = null)
        {
            EfCaUserResp response = new EfCaUserResp();
            response.Error = false;

            try
            {
                EfCaUserDB addItemToDB = new EfCaUserDB(internalUserInfos, item);
                RandomPasswordGenerator randomPasswordGenerator = new RandomPasswordGenerator();
                PasswordDetails passwordDetails = new PasswordDetails();
                if (item.Role.Equals("EfeuAdministrator"))
                {
                    passwordDetails.Password = masterPassword;
                    passwordDetails.HashedPassword = randomPasswordGenerator.ComputeSha256Hash(passwordDetails.Password);
                }
                else
                {
                    if(passwordHash != null)
                    {
                        //passwordDetails = new PasswordDetails();
                        passwordDetails.HashedPassword = passwordHash;
                    }
                    else
                    {
                        passwordDetails = randomPasswordGenerator.GeneratePassword(6);
                    }
                }
                addItemToDB.Password = passwordDetails.HashedPassword;

                _usersCollection.InsertOne(addItemToDB);

                EfCaUser newValueObject = new EfCaUser(addItemToDB);
                if (passwordHash == null)
                {
                    response.FirstLoginPassword = passwordDetails.Password;
                }
                response.Users.Add(newValueObject);
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddUser({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
                DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                response.AddDetailInfos(detailInfo);
                if (ex.Message.Contains("E11000 duplicate key error"))
                {
                    detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                }
                else
                {
                    detailInfo.Detail = ex.Message;
                    log.Error($"AddUser(..) Message not processed in detail: {ex.Message}");
                }
            }

            return response;
        }

        public EfCaUserResp FindUsersByFinder(InternalUserInfos internalUserInfos, EfCaUser finder)
        {
            EfCaUserResp response = new EfCaUserResp();
            response.Error = false;

            try
            {
                #region create the Filter
                List<FilterDefinition<EfCaUserDB>> filterDefinitions = new List<FilterDefinition<EfCaUserDB>>();
                if (internalUserInfos.Role == "EfeuAdministrator")
                {
                    filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Empty);
                }
                else
                {
                    filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Eq("tenantId", internalUserInfos.TenantId));
                }
                if (finder.Ident != null)
                {
                    filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Eq("userId", finder.Ident));
                }
                if (finder.ContactId != null)
                {
                    filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Eq("contactId", finder.ContactId));
                }
                if (finder.Email != null)
                {
                    filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Eq("email", finder.Email));
                    //filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Regex("email", $"{finder.Email}.*"));
                }
                #endregion

                var builderFilter = Builders<EfCaUserDB>.Filter;
                var filter = builderFilter.And(filterDefinitions.ToArray());
                List<EfCaUserDB> dbItemsFound = FindAllByFilter(filter, _usersCollectionName);

                if (dbItemsFound != null && dbItemsFound.Count > 0)
                {
                    response.Users = new List<EfCaUser>();
                    foreach (EfCaUserDB item in dbItemsFound)
                    {
                        EfCaUser newValueObject = new EfCaUser(item);
                        response.Users.Add(newValueObject);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindUsersByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaUserResp UpdateUser(InternalUserInfos internalUserInfos, EfCaUser item)
        {
            EfCaUserResp response = new EfCaUserResp();

            var builder = Builders<EfCaUserDB>.Filter;
            var filter = builder.And(
                Builders<EfCaUserDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaUserDB>.Filter.Eq("extIdDetails.extId", item.Ident),
                Builders<EfCaUserDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaUserDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _usersCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                string logErrorMsg = $"UpdateUser({internalUserInfos.TenantId}) failed. Address({item.Ident}) not found";
                log.Error(logErrorMsg);

                response.Error = true;
                string errorMsg = $"UpdateUser(..) failed. Address({item.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(item);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateUser({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _usersCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaUser newValueObject = new EfCaUser(uniqueDBItemToUpdateFound);
                    response.Users.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateUser(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaUserResp UpdateUserPassword(InternalUserInfos internalUserInfos, EfCaUser item, string password)
        {
            EfCaUserResp response = new EfCaUserResp();

            var builder = Builders<EfCaUserDB>.Filter;
            var filter = builder.And(
                Builders<EfCaUserDB>.Filter.Eq("tenantId", internalUserInfos.TenantId),
                Builders<EfCaUserDB>.Filter.Eq("userId", item.Ident) 
                );
            EfCaUserDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _usersCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                string logErrorMsg = $"UpdateUserPassword({internalUserInfos.TenantId}) failed. UpdateUser({item.Ident}) not found";
                log.Error(logErrorMsg);

                response.Error = true;
                string errorMsg = $"UpdateUserPassword(..) failed. Address({item.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(item, password);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateUserPassword({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _usersCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaUser newValueObject = new EfCaUser(uniqueDBItemToUpdateFound);
                    response.Users.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateUserPassword(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaUserResp DeleteUser(InternalUserInfos internalUserInfos, string ident)
        {
            EfCaUserResp response = new EfCaUserResp();
            response.Error = false;
            try
            {
                var builder = Builders<EfCaUserDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaUserDB>.Filter.Eq("tenantId", internalUserInfos.TenantId),
                    Builders<EfCaUserDB>.Filter.Eq("userId", ident)
                );

                EfCaUserDB uniqueDBItemFound = FindUniqueByFilter(filter, _usersCollectionName);

                if (uniqueDBItemFound == null)
                {
                    response.Error = true;
                    string logErrorMsg = $"DeleteUser({internalUserInfos.TenantId}) failed. User({ident}) not found";
                    log.Error(logErrorMsg);
                    string errorMsg = $"DeleteUser(..) failed. User({ident}) not found";
                    response.ErrorMsg = errorMsg;
                    return response;
                }
                else
                {
                    _usersCollection.DeleteOne(filter);
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteUser(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaUserResp DeleteAllUsersOfTenant(InternalUserInfos internalUserInfos)
        {
            EfCaUserResp response = new EfCaUserResp();
            response.Error = false;
            try
            {
                var builder = Builders<EfCaUserDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaUserDB>.Filter.Eq("tenantId", internalUserInfos.TenantId)
                    );
                DeleteResult deleteResult = _usersCollection.DeleteMany(filter);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteAllUsersOfTenant(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }
        #endregion

        #region role
        public EfCaRoleResp AddRole(InternalUserInfos internalUserInfos, EfCaRole item)
        {
            EfCaRoleResp response = new EfCaRoleResp();
            response.Error = false;

            try
            {
                EfCaRoleDB addItemToDB = new EfCaRoleDB(internalUserInfos, item);
                _rolesCollection.InsertOne(addItemToDB);

                EfCaRole newValueObject = new EfCaRole(addItemToDB);
                response.Roles.Add(newValueObject);
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddRole({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
                DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                response.AddDetailInfos(detailInfo);
                if (ex.Message.Contains("E11000 duplicate key error"))
                {
                    detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                }
                else
                {
                    detailInfo.Detail = ex.Message;
                    log.Error($"AddRole(..) Message not processed in detail: {ex.Message}");
                }
            }

            return response;
        }

        /// <summary>
        /// Wenn der anfragende Tenant der "EfeuAdministrator" ist werden die Default Rollen die zum kopieren
        /// verwendet werden zurückgeliefert.
        /// 
        /// <para>Ansonsten werden die Rollen des Users der sich angemeldet hat zurück gegeben</para>
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="finder"></param>
        /// <returns></returns>
        public EfCaRoleResp FindRolesByFinder(InternalUserInfos internalUserInfos, EfCaRole finder)
        {
            EfCaRoleResp response = new EfCaRoleResp();
            response.Error = false;

            try
            {
                #region create the Filter
                List<FilterDefinition<EfCaRoleDB>> filterDefinitions = new List<FilterDefinition<EfCaRoleDB>>();
                if (internalUserInfos.Role == "EfeuAdministrator")
                {
                    if (finder != null && finder.TenantId == "EfeuAdministrator")
                    {
                        filterDefinitions.Add(Builders<EfCaRoleDB>.Filter.Eq("extIdDetails.originator", "EfeuAdministrator"));
                        filterDefinitions.Add(Builders<EfCaRoleDB>.Filter.Ne("extIdDetails.extId", "EfeuAdministrator"));
                    }
                    else
                    {
                        filterDefinitions.Add(Builders<EfCaRoleDB>.Filter.Empty);
                    }
                }
                else
                {
                    filterDefinitions.Add(Builders<EfCaRoleDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                }
                var builderFilter = Builders<EfCaRoleDB>.Filter;
                #endregion

                var filter = builderFilter.And(filterDefinitions.ToArray());
                List<EfCaRoleDB> dbItemsFound = FindAllByFilter(filter, _rolesCollectionName);

                if (dbItemsFound != null && dbItemsFound.Count > 0)
                {
                    foreach (EfCaRoleDB item in dbItemsFound)
                    {
                        EfCaRole newValueObject = new EfCaRole(item);
                        response.Roles.Add(newValueObject);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"InternalUserInfos(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaRoleResp UpdateRole(InternalUserInfos internalUserInfos, EfCaRole item)
        {
            EfCaRoleResp response = new EfCaRoleResp();

            List<FilterDefinition<EfCaRoleDB>> filterDefinitions = new List<FilterDefinition<EfCaRoleDB>>();
            filterDefinitions.Add(Builders<EfCaRoleDB>.Filter.Eq("extIdDetails.originator", item.TenantId));
            filterDefinitions.Add(Builders<EfCaRoleDB>.Filter.Eq("extIdDetails.extId", item.Ident));

            item.TenantId = null;

            var builderFilter = Builders<EfCaRoleDB>.Filter;
            var filter = builderFilter.And(filterDefinitions.ToArray());

            EfCaRoleDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _rolesCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                string logErrorMsg = $"UpdateRole({internalUserInfos.TenantId}) failed. Role({item.Ident}) not found";
                log.Error(logErrorMsg);

                response.Error = true;
                string errorMsg = $"UpdateRole(..) failed. Role({item.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(item);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateRole({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _rolesCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaRole newValueObject = new EfCaRole(uniqueDBItemToUpdateFound);
                    response.Roles.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateRole(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaRoleResp DeleteAllRolesOfTenant(InternalUserInfos internalUserInfos)
        {
            EfCaRoleResp response = new EfCaRoleResp();
            response.Error = false;
            try
            {
                var builder = Builders<EfCaRoleDB>.Filter;
                var filter = builder.And(
                Builders<EfCaRoleDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId)
                );
                DeleteResult deleteResult = _rolesCollection.DeleteMany(filter);
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteAllRolesOfTenant(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }

        public bool RoleContainsFunction(InternalUserInfos internalUserInfos, string right, string function)
        {
            var builder = Builders<EfCaRoleDB>.Filter;
            var filter = builder.And(
                Builders<EfCaRoleDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaRoleDB>.Filter.Eq("extIdDetails.extId", internalUserInfos.Role),
                Builders<EfCaRoleDB>.Filter.Eq("rights.section", right),
                Builders<EfCaRoleDB>.Filter.Eq("rights.functions", function)
                );
            try
            {
                EfCaRoleDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _rolesCollectionName);
                if (uniqueDBItemToUpdateFound == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"RoleContainsFunction({internalUserInfos.TenantId}, Right={right}, Function={function}). Error({ex.Message})";
                log.Error(logErrorMsg);

                return false;
            }
        }
        #endregion

        #region tenant
        /// <summary>
        /// Creates a new Tenant in the database
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public EfCaTenantResp AddTenant(InternalUserInfos internalUserInfos, EfCaTenant item)
        {
            EfCaTenantResp response = new EfCaTenantResp();
            response.Error = false;

            try
            {
                EfCaTenantDB addItemToDB = new EfCaTenantDB(internalUserInfos, item);
                _tenantsCollection.InsertOne(addItemToDB);

                EfCaTenant newValueObject = new EfCaTenant(addItemToDB);
                response.Tenants.Add(newValueObject);
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddTenant({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
                DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                response.AddDetailInfos(detailInfo);
                if (ex.Message.Contains("E11000 duplicate key error"))
                {
                    detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                }
                else
                {
                    detailInfo.Detail = ex.Message;
                    log.Error($"AddRole(..) Message not processed in detail: {ex.Message}");
                }
            }

            return response;
        }

        public EfCaTenantResp FindTenantsByFinder(InternalUserInfos internalUserInfos, EfCaTenant finder)
        {
            EfCaTenantResp response = new EfCaTenantResp();
            response.Error = false;

            try
            {
                #region create the Filter
                List<FilterDefinition<EfCaTenantDB>> filterDefinitions = new List<FilterDefinition<EfCaTenantDB>>();
                if (internalUserInfos.Role == "EfeuAdministrator")
                {
                    filterDefinitions.Add(Builders<EfCaTenantDB>.Filter.Empty);
                }
                else
                {
                    filterDefinitions.Add(Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.extId", internalUserInfos.TenantId));

                }
                var builderFilter = Builders<EfCaTenantDB>.Filter;
                #endregion

                var filter = builderFilter.And(filterDefinitions.ToArray());
                List<EfCaTenantDB> dbItemsFound = FindAllByFilter(filter, _tenantsCollectionName);

                if (dbItemsFound != null && dbItemsFound.Count > 0)
                {
                    foreach (EfCaTenantDB item in dbItemsFound)
                    {
                        EfCaTenant newValueObject = new EfCaTenant(item);
                        response.Tenants.Add(newValueObject);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"InternalUserInfos(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaTenantResp DeleteTenant(InternalUserInfos internalUserInfos, string ident)
        {
            EfCaTenantResp response = new EfCaTenantResp();
            response.Error = false;
            try
            {
                var builder = Builders<EfCaTenantDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.originator", "EfeuAdministrator"),
                    Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.extId", ident),
                    Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                EfCaTenantDB uniqueDBItemFound = FindUniqueByFilter(filter, _tenantsCollectionName);

                if (uniqueDBItemFound == null)
                {
                    response.Error = true;
                    string logErrorMsg = $"DeleteTenant({internalUserInfos.TenantId}) failed. Tenant({ident}) not found";
                    log.Error(logErrorMsg);
                    string errorMsg = $"DeleteTenant(..) failed. Tenant({ident}) not found";
                    response.ErrorMsg = errorMsg;
                    return response;
                }
                else
                {
                    DeleteResult deleteResult = _tenantsCollection.DeleteOne(filter);
                    return response;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteTenant(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaTenantResp UpdateTenant(InternalUserInfos internalUserInfos, EfCaTenant item)
        {
            EfCaTenantResp response = new EfCaTenantResp();

            var builder = Builders<EfCaTenantDB>.Filter;
            var filter = builder.And(
                Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.extId", item.Ident),
                Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaTenantDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _tenantsCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                string logErrorMsg = $"UpdateTenant({internalUserInfos.TenantId}) failed. Role({item.Ident}) not found";
                log.Error(logErrorMsg);

                response.Error = true;
                string errorMsg = $"UpdateTenant(..) failed. Role({item.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(item);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateTenant({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _tenantsCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaTenant newValueObject = new EfCaTenant(uniqueDBItemToUpdateFound);
                    response.Tenants.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateTenant(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaConfig GetConfigOfTenant(InternalUserInfos internalUserInfos, string key)
        {
            EfCaConfig config = null;
            try
            {
                var builder = Builders<EfCaTenantDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.extId", internalUserInfos.TenantId),
                    Builders<EfCaTenantDB>.Filter.Eq("configs.key", key)
                    );
                EfCaTenantDB uniqueDBItemFound = FindUniqueByFilter(filter, _tenantsCollectionName);
                if (uniqueDBItemFound == null)
                {
                    string errorMsg = $"GetConfigOfTenant(..) failed, no result received";
                    log.Error(errorMsg);
                    return config;
                }
                foreach (EfCaConfig item in uniqueDBItemFound.Configs)
                {
                    if (item.Key == key)
                    {
                        config = item;
                        if (log.IsDebugEnabled && config != null)
                        {
                            string configAsJSON = JsonConvert.SerializeObject(config, Formatting.Indented);
                            log.Debug($"GetConfigOfTenant(..), Tenant({internalUserInfos.TenantId}, EfCaConfig({configAsJSON})");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"GetConfigOfTenant(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);
            }
            return config;
        }
        #endregion

        public List<SmartourQuality> GetTenantUnspecificData(InternalUserInfos internalUserInfos, string key)
        {
            EfCaSystem systemConfiguration = new EfCaSystem();
            systemConfiguration = FindSystemConfigurationByFinder(systemConfiguration);
            if (systemConfiguration == null)
            {
                return null;
            }
            ProviderUnspecificData data = systemConfiguration.TenantUnspecificData.First(data => data.Key.Equals(key));

            //EfCaConfig config = null;
            //if (log.IsDebugEnabled && config != null)
            //{
            //    string configAsJSON = JsonConvert.SerializeObject(config, Formatting.Indented);
            //    log.Debug($"GetConfigOfTenant(..), Tenant({internalUserInfos.TenantId}, EfCaConfig({configAsJSON})");
            //}

            return data.SmartourQualities;
        }

        #region config
        public EfCaSystemResp AddConfig(InternalUserInfos internalUserInfos, EfCaConfig item)
        {
            EfCaSystemResp response = new EfCaSystemResp();
            response.Error = false;

            try
            {
                EfCaConfigDB addItemToDB = new EfCaConfigDB(internalUserInfos, item);
                _configsCollection.InsertOne(addItemToDB);

                EfCaConfig newValueObject = new EfCaConfig(addItemToDB);
                response.Configs = new List<EfCaConfig>();
                response.Configs.Add(newValueObject);
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddConfig({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
                DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                response.AddDetailInfos(detailInfo);
                if (ex.Message.Contains("E11000 duplicate key error"))
                {
                    detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                }
                else
                {
                    detailInfo.Detail = ex.Message;
                    log.Error($"AddRole(..) Message not processed in detail: {ex.Message}");
                }
            }

            return response;
        }

        //public EfCaTenantResp FindTenantsByFinder(InternalUserInfos internalUserInfos, EfCaTenant finder)
        //{
        //    EfCaTenantResp response = new EfCaTenantResp();
        //    response.Error = false;

        //    try
        //    {
        //        #region create the Filter
        //        List<FilterDefinition<EfCaTenantDB>> filterDefinitions = new List<FilterDefinition<EfCaTenantDB>>();
        //        if (internalUserInfos.Role == "EfeuAdministrator")
        //        {
        //            filterDefinitions.Add(Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.owner", true));
        //        }
        //        else
        //        {
        //            filterDefinitions.Add(Builders<EfCaTenantDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));

        //        }
        //        var builderFilter = Builders<EfCaTenantDB>.Filter;
        //        #endregion

        //        var filter = builderFilter.And(filterDefinitions.ToArray());
        //        List<EfCaTenantDB> dbItemsFound = FindAllByFilter(filter, _tenantsCollectionName);

        //        if (dbItemsFound != null && dbItemsFound.Count > 0)
        //        {
        //            foreach (EfCaTenantDB item in dbItemsFound)
        //            {
        //                EfCaTenant newValueObject = new EfCaTenant(item);
        //                response.Tenants.Add(newValueObject);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMsg = $"InternalUserInfos(..) failed, Exception: {ex.Message}";
        //        log.Error(errorMsg);

        //        response.Error = true;
        //        response.ErrorMsg = "Please refer to the details";
        //    }

        //    return response;
        //}
        #endregion

        public EfCaSystem AddSystemConfiguration(EfCaSystem item)
        {
            EfCaUserResp response = new EfCaUserResp();
            response.Error = false;

            try
            {
                EfCaSystemDB addItemToDB = new EfCaSystemDB(item);

                _systemCollection.InsertOne(addItemToDB);

                EfCaSystem newValueObject = new EfCaSystem(addItemToDB);
                return newValueObject;
            }
            catch (Exception ex)
            {
                string errorMsg = $"AddSystemConfiguration(...); failed, Exception: {ex.Message}";
                log.Error(errorMsg);
            }

            return null;
        }

        public EfCaSystem FindSystemConfigurationByFinder(EfCaSystem finder)
        {
            try
            {
                #region create the Filter
                List<FilterDefinition<EfCaSystemDB>> filterDefinitions = new List<FilterDefinition<EfCaSystemDB>>();
                filterDefinitions.Add(Builders<EfCaSystemDB>.Filter.Empty);
                var builderFilter = Builders<EfCaSystemDB>.Filter;
                var filter = builderFilter.And(filterDefinitions.ToArray());
                #endregion

                List<EfCaSystemDB> dbItemsFound = FindAllByFilter(filter, _systemCollectionName);

                if (dbItemsFound != null && dbItemsFound.Count > 0)
                {
                    EfCaSystem system = new EfCaSystem(dbItemsFound[0]);
                    return system;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindSystemConfigurationByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);
            }

            return null;
        }

        public EfCaSystem UpdateSystemConfiguration(EfCaSystem item)
        {
            var builder = Builders<EfCaSystemDB>.Filter;
            var filter = builder.And(Builders<EfCaSystemDB>.Filter.Empty);

            EfCaSystemDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _systemCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                string logErrorMsg = $"UpdateSystemConfiguration(...) failed. SystemConfiguration not found";
                log.Error(logErrorMsg);

                throw new Exception("Unique SystemConfiguration not valid");
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(item);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateSystemConfiguration(...) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                return null;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _systemCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaSystem newValueObject = new EfCaSystem(uniqueDBItemToUpdateFound);
                    return newValueObject;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateSystemConfiguration(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);
            }

            return null;
        }

        public int Update_1_2_SchemaOrder()
        {
            var builder = Builders<EfCaOrderDB>.Filter;
            var filter = builder.And(Builders<EfCaOrderDB>.Filter.Empty);

            UpdateDefinitionBuilder<EfCaOrderDB> updateDefinition = new UpdateDefinitionBuilder<EfCaOrderDB>();

            var updateBuilder = Builders<EfCaOrderDB>.Update;
            var updates = new List<UpdateDefinition<EfCaOrderDB>>();
            updates.Add(updateBuilder.Set(order => order.DeliveryAttempts, -1));
            UpdateResult updateResult = PatchByFilterInternal(filter, updateBuilder.Combine(updates), _ordersCollectionName);

            return 2;
        }

        internal int Update_2_3_SchemaBoxMountingDevice()
        {
            var builder = Builders<EfCaBoxMountingDeviceDB>.Filter;
            var filter = builder.And(Builders<EfCaBoxMountingDeviceDB>.Filter.Exists("SlotSchedulerIdents", false));
            List<EfCaBoxMountingDeviceDB> dbItemsFound = FindAllByFilter(filter, _boxMountingDeviceCollectionName);

            UpdateDefinitionBuilder<EfCaBoxMountingDeviceDB> updateDefinition = new UpdateDefinitionBuilder<EfCaBoxMountingDeviceDB>();
            var updateBuilder = Builders<EfCaBoxMountingDeviceDB>.Update;
            var updates = new List<UpdateDefinition<EfCaBoxMountingDeviceDB>>();
            updates.Add(updateBuilder.Set(boxMountingDevice => boxMountingDevice.SlotSchedulerIdents, null));
            UpdateResult updateResult = PatchByFilterInternal(filter, updateBuilder.Combine(updates), _boxMountingDeviceCollectionName);

            return 3;
        }

        internal int Update_3_4_SchemaOrderAggregationAllowed()
        {
            var builder = Builders<EfCaContactDB>.Filter;
            var filter = builder.And(Builders<EfCaContactDB>.Filter.Empty);
            //var filter = builder.And(Builders<EfCaContactDB>.Filter.Exists("SlotSchedulerIdents", false));
            List<EfCaContactDB> dbItemsFound = FindAllByFilter(filter, _contactCollectionName);

            UpdateDefinitionBuilder<EfCaContactDB> updateDefinition = new UpdateDefinitionBuilder<EfCaContactDB>();
            var updateBuilder = Builders<EfCaContactDB>.Update;
            var updates = new List<UpdateDefinition<EfCaContactDB>>();
            updates.Add(updateBuilder.Set(contact => contact.OrderAggregationAllowed, true));
            UpdateResult updateResult = PatchByFilterInternal(filter, updateBuilder.Combine(updates), _contactCollectionName);

            return 4;
        }

        internal int Update_4_5_SchemaSynchron()
        {
            var builder = Builders<EfCaContactDB>.Filter;
            var filter = builder.And(Builders<EfCaContactDB>.Filter.Empty);
            //List<EfCaContactDB> dbItemsFound = FindAllByFilter(filter, _contactCollectionName);

            UpdateDefinitionBuilder<EfCaContactDB> updateDefinition = new UpdateDefinitionBuilder<EfCaContactDB>();
            var updateBuilder = Builders<EfCaContactDB>.Update;
            var updates = new List<UpdateDefinition<EfCaContactDB>>();
            updates.Add(updateBuilder.Unset("synchron"));
            UpdateResult updateResult = PatchByFilterInternal(filter, updateBuilder.Combine(updates), _contactCollectionName);

            return 5;
        }

        internal int Update_6_7_SchemaVehicleProfile()
        {
            var builder = Builders<EfCaVehicleDB>.Filter;
            var filter = builder.And(Builders<EfCaVehicleDB>.Filter.Empty);

            UpdateDefinitionBuilder<EfCaVehicleDB> updateDefinition = new UpdateDefinitionBuilder<EfCaVehicleDB>();
            var updateBuilder = Builders<EfCaVehicleDB>.Update;
            var updates = new List<UpdateDefinition<EfCaVehicleDB>>();
            updates.Add(updateBuilder.Set("vehicleProfile", "sew-robby"));
            UpdateResult updateResult = PatchByFilterInternal(filter, updateBuilder.Combine(updates), _vehiclesCollectionName);

            return 7;
        }

         internal EfCaUserResp UpdateHashTheUserPasswords(InternalUserInfos internalUserInfos, EfCaUser finder)
        {
            EfCaUserResp response = new EfCaUserResp();
            response.Error = false;
            RandomPasswordGenerator randomPasswordGenerator = new RandomPasswordGenerator();

            try
            {
                #region create the Filter
                List<FilterDefinition<EfCaUserDB>> filterDefinitions = new List<FilterDefinition<EfCaUserDB>>();
                if (internalUserInfos.Role == "EfeuAdministrator")
                {
                    filterDefinitions.Add(Builders<EfCaUserDB>.Filter.Empty);
                }
                #endregion

                var builderFilter = Builders<EfCaUserDB>.Filter;
                var filter = builderFilter.And(filterDefinitions.ToArray());
                List<EfCaUserDB> dbItemsFound = FindAllByFilter(filter, _usersCollectionName);

                if (dbItemsFound != null && dbItemsFound.Count > 0)
                {
                    foreach (EfCaUserDB item in dbItemsFound)
                    {
                        item.Password = randomPasswordGenerator.ComputeSha256Hash(item.Password);
                        var builder = Builders<EfCaUserDB>.Filter;
                        var updateFilter = builder.And(
                            Builders<EfCaUserDB>.Filter.Eq("tenantId", item.TenantId),
                            Builders<EfCaUserDB>.Filter.Eq("role", item.Role)
                            );

                        EfCaUserDB updateItem = UpdateByFilterInternal(updateFilter, item, _usersCollectionName);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateHashTheUserPasswords(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        //internal void Update_2_3_SchemaBoxMountingDevice(int update, EfCaBoxMountingDevice finder)
        //{

        //    #region create the Filter
        //    List<FilterDefinition<EfCaBoxMountingDeviceDB>> filterDefinitions = new List<FilterDefinition<EfCaBoxMountingDeviceDB>>();
        //    var builderFilter = Builders<EfCaBoxMountingDeviceDB>.Filter;
        //    switch (update)
        //    {
        //        case 3:
        //            filterDefinitions.Add(Builders<EfCaBoxMountingDeviceDB>.Filter.Not(item => item.SlotSchedulerIdents, null));
        //            break;
        //        default:
        //            throw new NotImplementedException();
        //    }
        //    #endregion

        //    var filter = builderFilter.And(filterDefinitions.ToArray());
        //    List<EfCaBoxMountingDeviceDB> dbItemsFound = FindAllByFilter(filter, _boxMountingDeviceCollectionName);

        //    if (dbItemsFound != null && dbItemsFound.Count > 0)
        //    {
        //        response.BoxMountingDevices = new List<EfCaBoxMountingDevice>();
        //        foreach (EfCaBoxMountingDeviceDB item in dbItemsFound)
        //        {
        //            EfCaBoxMountingDevice newValueObject = new EfCaBoxMountingDevice(item);
        //            response.BoxMountingDevices.Add(newValueObject);
        //        }
        //    }
        //}
    }
}
