﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Kep;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcKep
    public partial class MongoDBSvc
    {
        #region efeuCampus Kep ( Transport Dienstleister)
        public EfCaKepProviderResp AddLogisticServiceProviders(InternalUserInfos internalUserInfos, List<EfCaServiceProvider> addItems)
        {
            EfCaKepProviderResp response = new EfCaKepProviderResp();
            response.Error = false;
            response.KepProviders = new List<EfCaServiceProvider>();

            foreach (EfCaServiceProvider item in addItems)
            {
                try
                {
                    EfCaServiceProviderDB addItemToDB = new EfCaServiceProviderDB(internalUserInfos, item);
                    _kepProviderCollection.InsertOne(addItemToDB);

                    EfCaServiceProvider newValueObject = new EfCaServiceProvider(addItemToDB);
                    response.KepProviders.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddLogisticServiceProviders({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddLogisticServiceProviders(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaKepProviderResp FindLogisticServiceProvidersByFinder(InternalUserInfos internalUserInfos, EfCaServiceProvider finder)
        {
            EfCaKepProviderResp response = new EfCaKepProviderResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaServiceProviderDB>> filterDefinitions = new List<FilterDefinition<EfCaServiceProviderDB>>();
                    filterDefinitions.Add(Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaServiceProviderDB>.Filter;
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.Name != null)
                    {
                        filterDefinitions.Add(Builders<EfCaServiceProviderDB>.Filter.Regex("name", $"{finder.Name}.*"));
                    }

                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaServiceProviderDB> dbItemsFound = FindAllByFilter(filter, _kepProviderCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.KepProviders = new List<EfCaServiceProvider>();
                        foreach (EfCaServiceProviderDB item in dbItemsFound)
                        {
                            EfCaServiceProvider newValueObject = new EfCaServiceProvider(item);
                            response.KepProviders.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindLogisticServiceProvidersByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaKepProviderResp ReadAllLogisticServiceProviders(InternalUserInfos internalUserInfos)
        {
            EfCaKepProviderResp response = new EfCaKepProviderResp();
            response.Error = false;
            try
            {
                var builder = Builders<EfCaServiceProviderDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaServiceProviderDB> dbItems = FindAllByFilter(filter, _kepProviderCollectionName);
                if (dbItems != null && dbItems.Count > 0)
                {
                    response.KepProviders = new List<EfCaServiceProvider>();
                    foreach (EfCaServiceProviderDB dbItem in dbItems)
                    {
                        EfCaServiceProvider item = new EfCaServiceProvider(dbItem);
                        response.KepProviders.Add(item);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllLogisticServiceProviders(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaKepProviderResp UpdateLogisticServiceProvider(InternalUserInfos internalUserInfos, EfCaServiceProvider updateItem)
        {
            EfCaKepProviderResp response = new EfCaKepProviderResp();
            response.Error = false;

            var builder = Builders<EfCaServiceProviderDB>.Filter;
            var filter = builder.And(
                Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.extId", updateItem.Ident),
                Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaServiceProviderDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _kepProviderCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateLogisticServiceProvider({internalUserInfos.TenantId}) failed. LogisticServiceProvider({updateItem.Ident}) not found";
                log.Error(logErrorMsg);
                string errorMsg = $"UpdateLogisticServiceProvider(..) failed. LogisticServiceProvider({updateItem.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(updateItem);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateLogisticServiceProvider({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _kepProviderCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaServiceProvider newValueObject = new EfCaServiceProvider(uniqueDBItemToUpdateFound);
                    response.KepProviders.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateLogisticServiceProvider(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaKepProviderResp DeleteLogisticServiceProviders(InternalUserInfos internalUserInfos, List<string> itemIdents)
        {
            EfCaKepProviderResp response = new EfCaKepProviderResp();
            response.Error = false;
            try
            {
                foreach (string ident in itemIdents)
                {
                    var builder = Builders<EfCaServiceProviderDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaServiceProviderDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaServiceProviderDB uniqueDBItemFound = FindUniqueByFilter(filter, _kepProviderCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        //JSt: ToDo: das Verhalten ist nicht richtig, es kann ja nur ein Löschvorgang fehlschlagen
                        response.Error = true;
                        string logErrorMsg = $"DeleteLogisticServiceProviders({internalUserInfos.TenantId}) failed. LogisticServiceProvider({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteLogisticServiceProviders(..) failed. LogisticServiceProvider({ident}) not found";
                        response.ErrorMsg = errorMsg;
                        return response;
                    }
                    else
                    {
                        _kepProviderCollection.DeleteOne(filter);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteLogisticServiceProviders(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }
        #endregion
    }
}
