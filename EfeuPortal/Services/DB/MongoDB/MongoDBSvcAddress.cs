﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcAddress
    public partial class MongoDBSvc
    {
        public EfCaAddressResp AddAddresses(InternalUserInfos internalUserInfos, List<EfCaAddress> addresses)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            response.Error = false;
            response.Addresses = new List<EfCaAddress>();

            foreach (EfCaAddress item in addresses)
            {
                try
                {
                    EfCaAddressDB addItemToDB = new EfCaAddressDB(internalUserInfos, item);
                    _addressCollection.InsertOne(addItemToDB);

                    EfCaAddress newValueObject = new EfCaAddress(addItemToDB);
                    response.Addresses.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddAddresses({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddAddresses(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaAddressResp FindAddressesByFinder(InternalUserInfos internalUserInfos, EfCaAddress finder)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaAddressDB>> filterDefinitions = new List<FilterDefinition<EfCaAddressDB>>();
                    filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.owner", true));
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.ZipCode != null)
                    {
                        filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Eq("zipCode", finder.ZipCode));
                    }
                    if (finder.City != null)
                    {
                        filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Regex("city", $"{finder.City}.*"));
                    }
                    if (finder.Street != null)
                    {
                        filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Regex("street", $"{finder.Street}.*"));
                    }
                    if (finder.Label != null)
                    {
                        filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Regex("label", $"{finder.Label}.*"));
                    }
                    if (finder.Type != null)
                    {
                        filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Eq("type", finder.Type));
                    }
                    #endregion

                    var builderFilter = Builders<EfCaAddressDB>.Filter;
                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaAddressDB> dbItemsFound = FindAllByFilter(filter, _addressesCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.Addresses = new List<EfCaAddress>();
                        foreach (EfCaAddressDB item in dbItemsFound)
                        {
                            EfCaAddress newValueObject = new EfCaAddress(item);
                            response.Addresses.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindAddressesByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaAddressResp ReadAllAddresses(InternalUserInfos internalUserInfos)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            response.Error = false;
            try
            {
                var builder = Builders<EfCaAddressDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaAddressDB> addressesDB = FindAllByFilter(filter, _addressesCollectionName);
                if (addressesDB != null && addressesDB.Count > 0)
                {
                    response.Addresses = new List<EfCaAddress>();
                    foreach (EfCaAddressDB item in addressesDB)
                    {
                        EfCaAddress address = new EfCaAddress(item);
                        response.Addresses.Add(address);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllAddresses(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaAddressResp UpdateAddress(InternalUserInfos internalUserInfos, EfCaAddress address)
        {
            EfCaAddressResp response = new EfCaAddressResp();

            var builder = Builders<EfCaAddressDB>.Filter;
            var filter = builder.And(
                Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.extId", address.Ident),
                Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaAddressDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _addressesCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                string logErrorMsg = $"UpdateAddress({internalUserInfos.TenantId}) failed. Address({address.Ident}) not found";
                log.Error(logErrorMsg);

                response.Error = true;
                string errorMsg = $"UpdateAddress(..) failed. Address({address.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(address);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateAddress({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _addressesCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaAddress newValueObject = new EfCaAddress(uniqueDBItemToUpdateFound);
                    response.Addresses.Add(newValueObject );
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateAddress(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaAddressResp DeleteAddresses(InternalUserInfos internalUserInfos, List<string> addressIdents)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            response.Error = false;
            string errorMsg = $"DeleteAddresses(..) please check the details";
            response.ErrorMsg = errorMsg;
            try
            {
                foreach (string ident in addressIdents)
                {
                    var builder = Builders<EfCaAddressDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaAddressDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaAddressDB uniqueDBItemFound = FindUniqueByFilter(filter, _addressesCollectionName);

                    if (uniqueDBItemFound == null)
                    {
                        string logErrorMsg = $"DeleteAddresses({internalUserInfos.TenantId}) failed. Address({ident}) not found";
                        log.Error(logErrorMsg);

                        response.AddDetailInfos(new DetailInfo() { Ident = ident, Detail = "AddressId not valid" });

                        response.Error = true;
                    }
                    else
                    {
                        _addressCollection.DeleteOne(filter);
                        response.AddDetailInfos(new DetailInfo() { Ident = ident, Detail = "AddressId deleted" });
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                errorMsg = $"DeleteAddress(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.AddDetailInfos(new DetailInfo() { Ident = "undefined", Detail = ex.Message });
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }
    }
}
