﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcContact
    public partial class MongoDBSvc
    {
        #region efeuCampus contact
        public EfCaContactResp AddContacts(InternalUserInfos internalUserInfos, List<EfCaContact> contacts)
        {
            EfCaContactResp response = new EfCaContactResp();
            response.Error = false;
            response.Contacts = new List<EfCaContact>();

            foreach (EfCaContact item in contacts)
            {
                try
                {
                    EfCaContactDB addItemToDB = new EfCaContactDB(internalUserInfos, item);
                    _contactCollection.InsertOne(addItemToDB);

                    EfCaContact newValueObject = new EfCaContact(addItemToDB);
                    response.Contacts.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddContacts({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddContacts(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }
            return response;
        }

        public EfCaContactResp FindContactsByFinder(InternalUserInfos internalUserInfos, EfCaContact finder)
        {
            EfCaContactResp response = new EfCaContactResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaContactDB>> filterDefinitions = new List<FilterDefinition<EfCaContactDB>>();
                    filterDefinitions.Add(Builders<EfCaContactDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaContactDB>.Filter.Eq("extIdDetails.owner", true));

                    /*
                     * JSt: ToDo: Überlegen was ich hier wollte
                     */
                    //if (internalUserInfos.Role != "EfeuDepot")
                    //{
                    //    filterDefinitions.Add(Builders<EfCaContactDB>.Filter.Eq("email", internalUserInfos.Email));
                    //}

                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaContactDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.Email != null)
                    {
                        filterDefinitions.Add(Builders<EfCaContactDB>.Filter.Eq("email", finder.Email));
                    }
                    if (finder.Firstname != null)
                    {
                        filterDefinitions.Add(Builders<EfCaContactDB>.Filter.Regex("firstname", $"{finder.Firstname}.*"));
                    }
                    if (finder.Lastname != null)
                    {
                        filterDefinitions.Add(Builders<EfCaContactDB>.Filter.Regex("lastname", $"{finder.Lastname}.*"));
                    }

                    //public List<EfCaSlotsPerDay> SyncAvailabilityTimeSlots { get; set; }
                    //public List<EfCaSlotsPerDay> AsyncAvailabilityTimeSlots { get; set; }
                    //public List<EfCaDateTimeSlot> NoAvailabilities { get; set; }
                    //public DateTimeOffset Birthday { get; set; }
                    //public string  { get; set; }
                    #endregion

                    var builderFilter = Builders<EfCaContactDB>.Filter;
                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaContactDB> dbItemsFound = FindAllByFilter(filter, _contactCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.Contacts = new List<EfCaContact>();
                        foreach (EfCaContactDB item in dbItemsFound)
                        {
                            EfCaContact newValueObject = new EfCaContact(item);
                            response.Contacts.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindContactsByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaContactResp ReadAllContacts(InternalUserInfos internalUserInfos)
        {
            EfCaContactResp contactResp = new EfCaContactResp();
            contactResp.Error = false;
            try
            {
                var builder = Builders<EfCaContactDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaContactDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaContactDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaContactDB> contactsDB = FindAllByFilter(filter, _contactCollectionName);
                if (contactsDB != null && contactsDB.Count > 0)
                {
                    contactResp.Contacts = new List<EfCaContact>();
                    foreach (EfCaContactDB item in contactsDB)
                    {
                        EfCaContact contact = new EfCaContact(item);
                        contactResp.Contacts.Add(contact);
                    }
                }
                return contactResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllContacts(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                contactResp.Error = true;
                contactResp.ErrorMsg = "Please refer to the details";
            }

            return contactResp;
        }

        public EfCaContactResp UpdateContact(InternalUserInfos internalUserInfos, EfCaContact contact)
        {
            EfCaContactResp response = new EfCaContactResp();
            response.Error = false;

            var builder = Builders<EfCaContactDB>.Filter;
            var filter = builder.And(
                Builders<EfCaContactDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaContactDB>.Filter.Eq("extIdDetails.extId", contact.Ident),
                Builders<EfCaContactDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaContactDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _contactCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateContact({internalUserInfos.TenantId}) failed. Contact({contact.Ident}) not found";
                log.Error(logErrorMsg);
                string errorMsg = $"UpdateContact(..) failed. Contact({contact.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(contact);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateContact({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _contactCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaContact newValueObject = new EfCaContact(uniqueDBItemToUpdateFound);
                    response.Contacts.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateContact(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaContactResp DeleteContact(InternalUserInfos internalUserInfos, List<string> contactIdents)
        {
            EfCaContactResp contactResp = new EfCaContactResp();
            contactResp.Error = false;
            try
            {
                foreach (string ident in contactIdents)
                {
                    var builder = Builders<EfCaContactDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaContactDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaContactDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaContactDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaContactDB uniqueDBItemFound = FindUniqueByFilter(filter, _contactCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        contactResp.Error = true;
                        string logErrorMsg = $"DeleteContact({internalUserInfos.TenantId}) failed. Contact({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteContact(..) failed. Contact({ident}) not found";
                        contactResp.ErrorMsg = errorMsg;
                        return contactResp;
                    }
                    else
                    {
                        string email = uniqueDBItemFound.Email;
                        try
                        {
                            DeleteResult deleteResult = _contactCollection.DeleteOne(filter);
                            if(deleteResult.DeletedCount == 1)
                            {
                                contactResp.AddDetailInfos(new DetailInfo() { Ident = ident, Detail = "SUCCESS" });
                            }
                        }
                        catch(Exception ex)
                        {
                            contactResp.AddDetailInfos(new DetailInfo() { Ident = ident, Detail = "ERROR" });
                        }
                    }
                }
                return contactResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteContact(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                contactResp.Error = true;
                contactResp.ErrorMsg = "Please refer to the details";
            }

            return contactResp;
        }

        public EfCaContactDB FindContact(InternalUserInfos internalUserInfos, string ident)
        {
            var builder = Builders<EfCaContactDB>.Filter;
            var filter = builder.And(
                Builders<EfCaContactDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaContactDB>.Filter.Eq("extIdDetails.extId", ident),
                Builders<EfCaContactDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaContactDB uniqueDBItemFound = FindUniqueByFilter(filter, _contactCollectionName);

            return uniqueDBItemFound;
        }
        #endregion
    }
}
