﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Warehouse;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcWarehouse
    public partial class MongoDBSvc
    {
        public EfCaWarehousePlaceResp AddWarehousePlace(InternalUserInfos internalUserInfos, List<EfCaWarehousePlace> warehousePlaces)
        {
            EfCaWarehousePlaceResp response = new EfCaWarehousePlaceResp();
            response.Error = false;
            response.WarehousePlaces = new List<EfCaWarehousePlace>();

            foreach (EfCaWarehousePlace item in warehousePlaces)
            {
                try
                {
                    EfCaWarehousePlaceDB addItemToDB = new EfCaWarehousePlaceDB(internalUserInfos, item);
                    _warehousePlaceCollection.InsertOne(addItemToDB);

                    EfCaWarehousePlace newValueObject = new EfCaWarehousePlace(addItemToDB);
                    response.WarehousePlaces.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddWarehousePlace({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddWarehousePlace(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaWarehousePlaceResp FindWarehousePlacesByFinder(InternalUserInfos internalUserInfos, EfCaWarehousePlace finder)
        {
            EfCaWarehousePlaceResp response = new EfCaWarehousePlaceResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaWarehousePlaceDB>> filterDefinitions = new List<FilterDefinition<EfCaWarehousePlaceDB>>();
                    filterDefinitions.Add(Builders<EfCaWarehousePlaceDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaWarehousePlaceDB>.Filter.Eq("extIdDetails.owner", true));
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaWarehousePlaceDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    //if (finder.City != null)
                    //{
                    //    filterDefinitions.Add(Builders<EfCaAddressDB>.Filter.Regex("city", $"{finder.City}.*"));
                    //}
                    #endregion

                    var builderFilter = Builders<EfCaWarehousePlaceDB>.Filter;
                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaWarehousePlaceDB> dbItemsFound = FindAllByFilter(filter, _warehousePlaceCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.WarehousePlaces = new List<EfCaWarehousePlace>();
                        foreach (EfCaWarehousePlaceDB item in dbItemsFound)
                        {
                            EfCaWarehousePlace newValueObject = new EfCaWarehousePlace(item);
                            response.WarehousePlaces.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindWarehousePlacesByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaWarehousePlaceResp DeleteWarehousePlaces(InternalUserInfos internalUserInfo, List<string> warehousePlaceIdents)
        {
            EfCaWarehousePlaceResp response = new EfCaWarehousePlaceResp();

            try
            {
                foreach (string ident in warehousePlaceIdents)
                {
                    var builder = Builders<EfCaWarehousePlaceDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaWarehousePlaceDB>.Filter.Eq("extIdDetails.originator", internalUserInfo.TenantId),
                        Builders<EfCaWarehousePlaceDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaWarehousePlaceDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaWarehousePlaceDB uniqueDBItemFound = FindUniqueByFilter(filter, _warehousePlaceCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        response.Error = true;
                        string logErrorMsg = $"DeleteWarehousePlace({internalUserInfo.TenantId}) failed. WarehousePlace({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteWarehousePlace(..) failed. WarehousePlace({ident}) not found";
                        response.ErrorMsg = errorMsg;
                        return response;
                    }
                    else
                    {
                        _warehousePlaceCollection.DeleteOne(filter);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteWarehousePlace(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }
    }
}
