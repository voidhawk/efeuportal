﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.SyncMeetingPoint;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcSyncmeetingPoint
    public partial class MongoDBSvc
    {
        #region efeuCampus MeetingPoints
        public EfCaSyncMeetingPointResp AddSyncMeetingPoints(InternalUserInfos internalUserInfos, List<EfCaSyncMeetingPoint> syncMeetingPoints)
        {
            EfCaSyncMeetingPointResp response = new EfCaSyncMeetingPointResp();
            response.Error = false;
            response.SyncMeetingPoints = new List<EfCaSyncMeetingPoint>();

            foreach (EfCaSyncMeetingPoint item in syncMeetingPoints)
            {
                try
                {
                    EfCaSyncMeetingPointDB addItemToDB = new EfCaSyncMeetingPointDB(internalUserInfos, item);
                    _syncMeetingPointCollection.InsertOne(addItemToDB);

                    EfCaSyncMeetingPoint newValueEntry = new EfCaSyncMeetingPoint(addItemToDB);
                    response.SyncMeetingPoints.Add(newValueEntry);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddSyncMeetingPoints({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddSyncMeetingPoints(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaSyncMeetingPointResp FindSyncMeetingPointsByFinder(InternalUserInfos internalUserInfos, EfCaSyncMeetingPoint finder)
        {
            EfCaSyncMeetingPointResp response = new EfCaSyncMeetingPointResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaSyncMeetingPointDB>> filterDefinitions = new List<FilterDefinition<EfCaSyncMeetingPointDB>>();
                    filterDefinitions.Add(Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaSyncMeetingPointDB>.Filter;
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.Info != null)
                    {
                        filterDefinitions.Add(Builders<EfCaSyncMeetingPointDB>.Filter.Regex("info", $"{finder.Info}.*"));
                    }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaSyncMeetingPointDB> dbItemsFound = FindAllByFilter(filter, _syncMeetingPointCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.SyncMeetingPoints = new List<EfCaSyncMeetingPoint>();
                        foreach (EfCaSyncMeetingPointDB item in dbItemsFound)
                        {
                            EfCaSyncMeetingPoint newValueObject = new EfCaSyncMeetingPoint(item);
                            response.SyncMeetingPoints.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindSyncMeetingPointsByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaSyncMeetingPointResp ReadAllSyncMeetingPoints(InternalUserInfos internalUserInfos)
        {
            EfCaSyncMeetingPointResp syncMeetingPointResp = new EfCaSyncMeetingPointResp();
            syncMeetingPointResp.Error = false;
            try
            {
                var builder = Builders<EfCaSyncMeetingPointDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaSyncMeetingPointDB> allDBEntries = FindAllByFilter(filter, _syncMeetingPointCollectionName);
                if (allDBEntries != null && allDBEntries.Count > 0)
                {
                    syncMeetingPointResp.SyncMeetingPoints = new List<EfCaSyncMeetingPoint>();
                    foreach (EfCaSyncMeetingPointDB item in allDBEntries)
                    {
                        EfCaSyncMeetingPoint newValueEntry = new EfCaSyncMeetingPoint(item);
                        syncMeetingPointResp.SyncMeetingPoints.Add(newValueEntry);
                    }
                }
                return syncMeetingPointResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllSyncMeetingPoints(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                syncMeetingPointResp.Error = true;
                syncMeetingPointResp.ErrorMsg = "Please refer to the details";
            }

            return syncMeetingPointResp;
        }

        public EfCaSyncMeetingPointResp UpdateSyncMeetingPoint(InternalUserInfos internalUserInfos, EfCaSyncMeetingPoint syncMeetingPoint)
        {
            EfCaSyncMeetingPointResp response = new EfCaSyncMeetingPointResp();
            response.Error = false;

            var builder = Builders<EfCaSyncMeetingPointDB>.Filter;
            var filter = builder.And(
                Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.extId", syncMeetingPoint.Ident),
                Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaSyncMeetingPointDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _syncMeetingPointCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateSyncMeetingPoint({internalUserInfos.TenantId}) failed. MeetingPoint({syncMeetingPoint.Ident}) not found";
                log.Error(logErrorMsg);
                string errorMsg = $"UpdateSyncMeetingPoint(..) failed. MeetingPoint({syncMeetingPoint.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(syncMeetingPoint);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateSyncMeetingPoint({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _syncMeetingPointCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaSyncMeetingPoint newValueObject = new EfCaSyncMeetingPoint(uniqueDBItemToUpdateFound);
                    response.SyncMeetingPoints.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateSyncMeetingPoint(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaSyncMeetingPointResp DeleteSyncMeetingPoints(InternalUserInfos internalUserInfos, List<string> syncMeetingPointIdents)
        {
            EfCaSyncMeetingPointResp syncMeetingPointResp = new EfCaSyncMeetingPointResp();
            syncMeetingPointResp.Error = false;
            try
            {
                foreach (string ident in syncMeetingPointIdents)
                {
                    var builder = Builders<EfCaSyncMeetingPointDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaSyncMeetingPointDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaSyncMeetingPointDB uniqueDBItemFound = FindUniqueByFilter(filter, _syncMeetingPointCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        syncMeetingPointResp.Error = true;
                        string logErrorMsg = $"DeleteSyncMeetingPoints({internalUserInfos.TenantId}) failed. MeetingPoint({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteSyncMeetingPoints(..) failed. MeetingPoint({ident}) not found";
                        syncMeetingPointResp.ErrorMsg = errorMsg;
                        return syncMeetingPointResp;
                    }
                    else
                    {
                        _syncMeetingPointCollection.DeleteOne(filter);
                    }
                }
                return syncMeetingPointResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteEfeuMeetingPoints(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                syncMeetingPointResp.Error = true;
                syncMeetingPointResp.ErrorMsg = "Please refer to the details";
            }

            return syncMeetingPointResp;
        }
        #endregion

    }
}
