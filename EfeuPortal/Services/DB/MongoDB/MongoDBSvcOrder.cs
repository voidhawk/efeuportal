﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcOrder
    public partial class MongoDBSvc
    {
        #region Order
        public EfCaOrderResp AddOrders(InternalUserInfos internalUserInfos, List<EfCaOrder> items)
        {
            EfCaOrderResp response = new EfCaOrderResp();
            response.Error = false;
            if (response.Orders == null)
            {
                response.Orders = new List<EfCaOrder>();
            }

            foreach (EfCaOrder item in items)
            {
                try
                {
                    EfCaOrderDB addItemToDB = new EfCaOrderDB(internalUserInfos, item);
                    _ordersCollection.InsertOne(addItemToDB);

                    EfCaOrder newValueObject = new EfCaOrder(addItemToDB);
                    response.Orders.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddOrders({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddOrders(..) Message not processed in detail: {ex.Message}");
                    }
                    response.AddDetailInfos(detailInfo);
                }
            }

            return response;
        }

        public EfCaOrderResp FindOrdersByFinder(InternalUserInfos internalUserInfos, EfCaOrder finder, List<string> orderIds)
        {
            EfCaOrderResp response = new EfCaOrderResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaOrderDB>> filterDefinitions = new List<FilterDefinition<EfCaOrderDB>>();
                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    var builderFilter = Builders<EfCaOrderDB>.Filter;
                    if (finder != null)
                    {
                        filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.owner", true));
                        if (finder.Ident != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                        }
                        if (finder.ProviderIds != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.In("providerIds", finder.ProviderIds));
                        }
                        if (finder.TrackingId != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("trackingId", finder.TrackingId));
                        }
                        if (finder.State != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("state", finder.State));
                        }
                        if (finder.ExternalOrderId != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("externalOrderId", finder.ExternalOrderId));
                        }
                        if (finder.OrderType != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("orderType", finder.OrderType));
                        }
                        if (finder.OrderUnit != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("orderUnit", finder.OrderUnit));
                        }
                        if (finder.OrderAggregation != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("orderAggregation", finder.OrderAggregation));
                        }
                        if (finder.MainOrder != null)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("mainOrder", finder.MainOrder));
                        }
                        if (finder.OnlySelfService.HasValue)
                        {
                            filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("onlySelfService", finder.OnlySelfService));
                        }

                        

                        if (finder.Pickup != null)
                        {
                            if (finder.Pickup.StorageIds != null)
                            {
                                if (finder.Pickup.StorageIds.AddressId != null)
                                {
                                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("pickup.storageIds.addressId", finder.Pickup.StorageIds.AddressId));
                                }
                                if (finder.Pickup.StorageIds.BuildingId != null)
                                {
                                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("pickup.storageIds.buildingId", finder.Pickup.StorageIds.BuildingId));
                                }
                                if (finder.Pickup.StorageIds.ContactId != null)
                                {
                                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("pickup.storageIds.contactId", finder.Pickup.StorageIds.ContactId));
                                }
                                if (finder.Pickup.StorageIds.SyncMeetingPointId != null)
                                {
                                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("pickup.storageIds.syncMeetingPointId", finder.Pickup.StorageIds.SyncMeetingPointId));
                                }
                            }
                        }

                        if (finder.Delivery != null)
                        {
                            if (finder.Delivery.StorageIds != null)
                            {
                                if (finder.Delivery.StorageIds.AddressId != null)
                                {
                                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("delivery.storageIds.addressId", finder.Delivery.StorageIds.AddressId));
                                }
                                if (finder.Delivery.StorageIds.BuildingId != null)
                                {
                                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("delivery.storageIds.buildingId", finder.Delivery.StorageIds.BuildingId));
                                }
                                if (finder.Delivery.StorageIds.ContactId != null)
                                {
                                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("delivery.storageIds.contactId", finder.Delivery.StorageIds.ContactId));
                                }
                                if (finder.Delivery.StorageIds.SyncMeetingPointId != null)
                                {
                                    filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.Eq("delivery.storageIds.syncMeetingPointId", finder.Delivery.StorageIds.SyncMeetingPointId));
                                }
                            }
                        }
                    }
                    else if (orderIds != null && orderIds.Count > 0)
                    {
                        filterDefinitions.Add(Builders<EfCaOrderDB>.Filter.In("extIdDetails.extId", orderIds));
                    }
                    else
                    {
                        response.Error = true;
                        response.ErrorMsg = "Invalid Finder(s)";
                        return response;
                    }

                    //ProjectionDefinition<EfCaOrderDB> projectionBuilder = Builders<EfCaOrderDB>.Projection.Exclude(exclude => exclude.Images);
                    #endregion

                    FilterDefinition<EfCaOrderDB> filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaOrderDB> dbItemsFound = FindAllByFilter(filter, _ordersCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.Orders = new List<EfCaOrder>();
                        foreach (EfCaOrderDB item in dbItemsFound)
                        {
                            EfCaOrder newValueObject = new EfCaOrder(item);
                            response.Orders.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindOrdersByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }

        public EfCaOrderResp UpdateOrder(InternalUserInfos internalUserInfos, EfCaOrder item)
        {
            EfCaOrderResp response = new EfCaOrderResp();

            var builder = Builders<EfCaOrderDB>.Filter;
            var filter = builder.And(
                Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.extId", item.Ident),
                Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaOrderDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _ordersCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                string logErrorMsg = $"UpdateOrder({internalUserInfos.TenantId}) failed. Order({item.Ident}) not found";
                log.Error(logErrorMsg);

                response.Error = true;
                string errorMsg = $"UpdateOrder(..) failed. Order({item.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(item);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateOrder({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _ordersCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaOrder newValueObject = new EfCaOrder(uniqueDBItemToUpdateFound);
                    response.Orders.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateOrder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaOrderResp DeleteOrders(InternalUserInfos internalUserInfos, List<string> itemIdents)
        {
            EfCaOrderResp response = new EfCaOrderResp();
            response.Error = false;
            try
            {
                foreach (string ident in itemIdents)
                {
                    var builder = Builders<EfCaOrderDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaOrderDB uniqueDBItemFound = FindUniqueByFilter(filter, _ordersCollectionName);

                    if (uniqueDBItemFound == null)
                    {
                        response.Error = true;
                        string logErrorMsg = $"DeleteOrders({internalUserInfos.TenantId}) failed. Order({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteOrders(..) failed. Order({ident}) not found";
                        response.ErrorMsg = errorMsg;
                        DetailInfo detailInfo = new DetailInfo() { Ident = ident };
                        detailInfo.Detail = $"Order.Ident({ident}) did not exist. Deletion failed.";
                        response.AddDetailInfos(detailInfo);
                    }
                    else
                    {
                        try
                        {
                            _ordersCollection.DeleteOne(filter);
                        }
                        catch (Exception ex)
                        {
                            DetailInfo detailInfo = new DetailInfo() { Ident = ident };
                            log.Error($"AddDeleteOrders(${ident}) Exception: {ex.Message}");
                            detailInfo.Detail = $"Delete Order.Ident({ident})  failed: {ex.Message}";
                            response.AddDetailInfos(detailInfo);
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteOrders(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaOrderResp PatchOrder(InternalUserInfos internalUserInfos, List<string> idents, EfCaOrder item)
        {
            EfCaOrderResp response = new EfCaOrderResp();
            if (item == null)
            {
                string logErrorMsg = $"PatchOrder({internalUserInfos.TenantId}) failed. Order(NULL) not valid";
                log.Error(logErrorMsg);

                response.Error = true;
                string errorMsg = $"PatchOrder(..) failed. Order(NULL) not valid";
                response.ErrorMsg = errorMsg;

                return response;
            }

            var filterBuilder = Builders<EfCaOrderDB>.Filter;
            var filter = filterBuilder.And(
                Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaOrderDB>.Filter.In("extIdDetails.extId", idents),
                Builders<EfCaOrderDB>.Filter.Eq("extIdDetails.owner", true)
                );
            //UpdateDefinitionBuilder<EfCaOrderDB> updateDefinition = new UpdateDefinitionBuilder<EfCaOrderDB>();

            var updateBuilder = Builders<EfCaOrderDB>.Update;
            var updates = new List<UpdateDefinition<EfCaOrderDB>>();
            if (item.State != null)
            {
                updates.Add(updateBuilder.Set("State", item.State));
            }
            else
            {
                updates.Add(updateBuilder.Set(order => order.State, item.State));
            }
            UpdateResult updateResult = PatchByFilterInternal(filter, updateBuilder.Combine(updates), _ordersCollectionName);

            //EfCaOrderDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _ordersCollectionName);
            //if (uniqueDBItemToUpdateFound == null)
            //{
            //    string logErrorMsg = $"PatchOrder({internalUserInfos.TenantId}) failed. Order({item.Ident}) not found";
            //    log.Error(logErrorMsg);

            //    response.Error = true;
            //    string errorMsg = $"PatchOrder(..) failed. Order({item.Ident}) not found";
            //    response.ErrorMsg = errorMsg;
            //    return response;
            //}

            return response;
        }
        #endregion Order

        #region Order images
        public EfCaOrderResp AddOrderImageDocumentations(InternalUserInfos internalUserInfos, List<EfCaImageDocumentation> items)
        {
            EfCaOrderResp response = new EfCaOrderResp();
            response.Error = false;
            foreach (EfCaImageDocumentation item in items)
            {
                try
                {
                    EfCaImageDocumentationDB addItemToDB = new EfCaImageDocumentationDB(internalUserInfos, item);
                    _imageDescriptionsCollection.InsertOne(addItemToDB);

                    EfCaImageDocumentation newValueObject = new EfCaImageDocumentation(addItemToDB);
                    response.AddImageDocumentation(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddOrderImageDocumentations({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddOrderImageDocumentations(..) Message not processed in detail: {ex.Message}");
                    }
                    response.AddDetailInfos(detailInfo);
                }
            }

            return response;
        }

        internal EfCaOrderResp FindOrderImageDocumentations(InternalUserInfos internalUserInfos, EfCaModelCollector finder)
        {
            //https://stackoverflow.com/questions/32227284/mongo-c-sharp-driver-building-filter-dynamically-with-nesting/32253436

            EfCaOrderResp response = new EfCaOrderResp();
            response.Error = false;
            int finderCounter = 0;
            try
            {
                #region create the Filter
                //List<FilterDefinition<EfCaImageDocumentationDB>> filterDefinitions = new List<FilterDefinition<EfCaImageDocumentationDB>>();
                //filterDefinitions.Add(Builders<EfCaImageDocumentationDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                var builderFilter = Builders<EfCaImageDocumentationDB>.Filter;

                //var filterCollector = builderFilter.Empty;
                FilterDefinition<EfCaImageDocumentationDB> filterCollector = builderFilter.Empty;
                //var tenantFilter = builderFilter.Eq("extIdDetails.originator", internalUserInfos.TenantId);
                //filterCollector &= tenantFilter;

                FilterDefinition<EfCaImageDocumentationDB> identFilter = builderFilter.Empty;
                if (finder.Idents != null && finder.Idents.Count > 0)
                {
                    //filterDefinitions.Add(Builders<EfCaImageDocumentationDB>.Filter.In(f => f.OrderId, finder.Idents));
                    identFilter &= builderFilter.In(f => f.OrderId, finder.Idents);
                    //foreach (string item in finder.Idents)
                    //{
                    //    filterCollector |= builderFilter.Eq(f => f.OrderId, item);
                    //}
                    finderCounter++;
                }

                FilterDefinition<EfCaImageDocumentationDB> imageDocumentationFilter = builderFilter.Empty;
                if (finder.ImageDocumentation != null && finder.ImageDocumentation.Count > 0)
                {
                    foreach (EfCaImageDocumentation item in finder.ImageDocumentation)
                    {
                        //var imageDocumentation = builderFilter.Empty;
                        if (!string.IsNullOrWhiteSpace(item.Ident))
                        {
                            //filterDefinitions.Add(Builders<EfCaImageDocumentationDB>.Filter.Eq("extIdDetails.extId", item.Ident));
                            //var temp = builderFilter.Eq("extIdDetails.extId", item.Ident);
                            imageDocumentationFilter &= builderFilter.Eq("extIdDetails.extId", item.Ident);
                            //filterCollector |= temp;
                        }
                        if (!string.IsNullOrWhiteSpace(item.OrderId))
                        {
                            //filterCollector |= builderFilter.Regex(find => find.Description, $"{item.Description}.*");
                            imageDocumentationFilter &= builderFilter.Eq(find => find.OrderId, item.OrderId);
                        }
                        if (!string.IsNullOrWhiteSpace(item.Description))
                        {
                            //filterCollector |= builderFilter.Regex(find => find.Description, $"{item.Description}.*");
                            imageDocumentationFilter &= builderFilter.Eq(find => find.Description, item.Description);
                        }
                        //filterCollector |= imageDocumentation;
                    }

                    finderCounter++;
                }

                if (finderCounter == 0)
                {
                    response.Error = true;
                    response.ErrorMsg = "Empty Finder not allowed";
                    return response;
                }

                FilterDefinition<EfCaImageDocumentationDB> combinedFilter = builderFilter.Empty;
                if (identFilter != builderFilter.Empty)
                {
                    combinedFilter &= identFilter;
                }
                if (imageDocumentationFilter != builderFilter.Empty)
                {
                    if (combinedFilter != builderFilter.Empty)
                    {
                        combinedFilter |= imageDocumentationFilter;
                    }
                    else
                    {
                        combinedFilter &= imageDocumentationFilter;
                    }
                }

                //FilterDefinition<EfCaImageDocumentationDB> combinedFilter1 = builderFilter.OfType<EfCaImageDocumentationDB>();
                //combinedFilter1 = identFilter | imageDocumentationFilter;

                var tenantFilter = builderFilter.Eq("extIdDetails.originator", internalUserInfos.TenantId);
                filterCollector &= tenantFilter;
                filterCollector &= combinedFilter;
                //ProjectionDefinition<EfCaOrderDB> projectionBuilder = Builders<EfCaOrderDB>.Projection.Exclude(exclude => exclude.Images);
                #endregion
                List<EfCaImageDocumentationDB> dbItemsFound = FindAllByFilter(filterCollector, _imageDescriptionsCollectionName);

                //FilterDefinition<EfCaImageDocumentationDB> filter = builderFilter.And(filterDefinitions.ToArray());
                //dbItemsFound = FindAllByFilter(filter, _imageDescriptionsCollectionName);

                if (dbItemsFound != null && dbItemsFound.Count > 0)
                {
                    foreach (EfCaImageDocumentationDB item in dbItemsFound)
                    {
                        EfCaImageDocumentation newValueObject = new EfCaImageDocumentation(item);
                        response.AddImageDocumentation(newValueObject);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindOrderImageDocumentations(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }

        /// <summary>
        /// Es wird nur einer von beiden Parametern Idents ODER ImageDocumentation(s) ausgewertet
        /// <para>Wenn die EfCaModelCollector.Idents gesetzt ist, werden diese als EfCaOrder.Ident verwendet.</para>
        /// 
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="deleteImageDocumentations"></param>
        /// <returns></returns>
        public EfCaOrderResp DeleteOrderImageDocumentations(InternalUserInfos internalUserInfos, EfCaModelCollector deleteImageDocumentations)
        {
            EfCaOrderResp response = new EfCaOrderResp();
            response.Error = false;
            if (deleteImageDocumentations== null)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "Parameter Idents not valid";

                return response;
            }

            try
            {
                var builderFilter = Builders<EfCaImageDocumentationDB>.Filter;
                List<FilterDefinition<EfCaImageDocumentationDB>> addFilterDefinitions = new List<FilterDefinition<EfCaImageDocumentationDB>>();
                List<FilterDefinition<EfCaImageDocumentationDB>> orFilterDefinitions = new List<FilterDefinition<EfCaImageDocumentationDB>>();
                addFilterDefinitions.Add(Builders<EfCaImageDocumentationDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                int filterCount = 1;

                if (deleteImageDocumentations.Idents != null && deleteImageDocumentations.Idents.Count > 0)
                {
                    addFilterDefinitions.Add(Builders<EfCaImageDocumentationDB>.Filter.In(item => item.OrderId, deleteImageDocumentations.Idents));
                    filterCount++;
                }
                else if (deleteImageDocumentations.ImageDocumentation != null && deleteImageDocumentations.ImageDocumentation.Count > 0)
                {
                    List<string> imageDocumentationIdents = imageDocumentationIdents = deleteImageDocumentations.ImageDocumentation.Select(item => item.Ident).ToList();
                    addFilterDefinitions.Add(Builders<EfCaImageDocumentationDB>.Filter.In("extIdDetails.extId", imageDocumentationIdents));
                    //orFilterDefinitions.Add(Builders<EfCaImageDocumentationDB>.Filter.In("extIdDetails.extId", imageDocumentationIdents));
                    //builderFilter.Or(Builders<EfCaImageDocumentationDB>.Filter.Or("extIdDetails.extId", imageDocumentationIdents));
                    //builderFilter.Or(Builders<EfCaImageDocumentationDB>.Filter.In("extIdDetails.extId", imageDocumentationIdents));
                    filterCount++;
                }

                if (filterCount == 1)
                {
                    response.Error = true;
                    response.ErrorMsg = "No valid filter detected";
                    return response;
                }

                FilterDefinition<EfCaImageDocumentationDB> filter = builderFilter.And(addFilterDefinitions.ToArray());

                try
                {
                    DeleteResult deleteResult = _imageDescriptionsCollection.DeleteMany(filter);
                }
                catch (Exception ex)
                {
                    log.Error($"DeleteOrderImageDocumentations(...) Exception: {ex.Message}");
                    response.Error = true;
                    response.ErrorMsg = $"DeleteOrderImageDocumentations(...) Exception: {ex.Message}";
                }
                return response;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteOrderImageDocumentations(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }
        #endregion
    }
}