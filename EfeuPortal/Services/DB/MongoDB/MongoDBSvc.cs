﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.System.MongoDB;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace EfeuPortal.Services.DB.MongoDB
{
    public partial class MongoDBSvc
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(MongoDBSvc));

        private readonly string masterPassword;

        #region System
        private IMongoDatabase _dbSystem_efeuCampus;

        private readonly IMongoCollection<EfCaBoxMountingDeviceDB> _boxMountingDeviceCollection;
        private readonly string _boxMountingDeviceCollectionName = "efca-boxmountdevices";

        private readonly IMongoCollection<EfCaBoxMountingDeviceSchedulerDB> _boxMountingDeviceSchedulerCollection;
        private readonly string _boxMountingDeviceSchedulerCollectionName = "efca-boxmountdeviceschedulers";

        private readonly IMongoCollection<EfCaBoxMountingDeviceSlotDB> _boxMountEntryCollection;
        private readonly string _boxMountEntryCollectionName = "efca-boxmountdevicereservations";

        private readonly IMongoCollection<EfCaAddressDB> _addressCollection;
        private readonly string _addressesCollectionName = "efca-addresses";

        private readonly IMongoCollection<EfCaVehicleDB> _vehicleCollection;
        private readonly string _vehiclesCollectionName = "efca-vehicles";

        private readonly IMongoCollection<EfCaApartmentDB> _apartmentCollection;
        private readonly string _apartmentCollectionName = "efca-apartments";

        private readonly IMongoCollection<EfCaBuildingDB> _buildingCollection;
        private readonly string _buildingCollectionName = "efca-buildings";

        private readonly IMongoCollection<EfCaChargingStationDB> _chargingStationCollection;
        private readonly string _chargingStationCollectionName = "efca-chargingstations";

        private readonly IMongoCollection<EfCaContactDB> _contactCollection;
        private readonly string _contactCollectionName = "efca-contacts";

        private readonly IMongoCollection<EfCaServiceProviderDB> _kepProviderCollection;
        private readonly string _kepProviderCollectionName = "efca-kepproviders";

        private readonly IMongoCollection<EfCaSyncMeetingPointDB> _syncMeetingPointCollection;
        private readonly string _syncMeetingPointCollectionName = "efca-syncmeetingpoints";

        private readonly IMongoCollection<EfCaTransportBoxDB> _transportBoxCollection;
        private readonly string _transportBoxCollectionName = "efca-transportboxes";

        private readonly IMongoCollection<EfCaTransportBoxTypeDB> _transportBoxTypeCollection;
        private readonly string _transportBoxTypeCollectionName = "efca-transportboxtypes";

        private readonly IMongoCollection<EfCaOrderDB> _ordersCollection;
        private readonly string _ordersCollectionName = "efca-orders";
        private readonly IMongoCollection<EfCaImageDocumentationDB> _imageDescriptionsCollection;
        private readonly string _imageDescriptionsCollectionName = "efca-order-images";

        private readonly IMongoCollection<EfCaPlannedTripDB> _plannedToursCollection;
        private readonly string _plannedToursCollectionName = "efca-plannedTrips";

        private readonly IMongoCollection<EfCaUserDB> _usersCollection;
        private readonly string _usersCollectionName = "efca-users";

        private readonly IMongoCollection<EfCaRoleDB> _rolesCollection;
        private readonly string _rolesCollectionName = "efca-roles";

        private readonly IMongoCollection<EfCaTenantDB> _tenantsCollection;
        private readonly string _tenantsCollectionName = "efca-tenants";

        private readonly IMongoCollection<EfCaConfigDB> _configsCollection;
        private readonly string _configsCollectionName = "efca-configs";

        private readonly IMongoCollection<EfCaSystemDB> _systemCollection;
        private readonly string _systemCollectionName = "efca-system";

        private readonly IMongoCollection<EfCaWarehousePlaceDB> _warehousePlaceCollection;
        private readonly string _warehousePlaceCollectionName = "efca-warehouse";
        #endregion

        public MongoDBSvc(IConfiguration config)
        {
            masterPassword = config.GetSection("AppSettings:DefaultAdminPwd").Value;

            #region data
            var clientEfeuCampus = new MongoClient(config.GetConnectionString("mongodbConnection"));
            _dbSystem_efeuCampus = clientEfeuCampus.GetDatabase(config.GetConnectionString("mongodbName"));

            _boxMountingDeviceCollection = _dbSystem_efeuCampus.GetCollection<EfCaBoxMountingDeviceDB>(_boxMountingDeviceCollectionName);
            _boxMountingDeviceSchedulerCollection = _dbSystem_efeuCampus.GetCollection<EfCaBoxMountingDeviceSchedulerDB>(_boxMountingDeviceSchedulerCollectionName);
            _boxMountEntryCollection = _dbSystem_efeuCampus.GetCollection<EfCaBoxMountingDeviceSlotDB>(_boxMountEntryCollectionName);
            _addressCollection = _dbSystem_efeuCampus.GetCollection<EfCaAddressDB>(_addressesCollectionName);
            _vehicleCollection = _dbSystem_efeuCampus.GetCollection<EfCaVehicleDB>(_vehiclesCollectionName);
            _apartmentCollection = _dbSystem_efeuCampus.GetCollection<EfCaApartmentDB>(_apartmentCollectionName);
            _buildingCollection = _dbSystem_efeuCampus.GetCollection<EfCaBuildingDB>(_buildingCollectionName);
            _chargingStationCollection = _dbSystem_efeuCampus.GetCollection<EfCaChargingStationDB>(_chargingStationCollectionName);
            _contactCollection = _dbSystem_efeuCampus.GetCollection<EfCaContactDB>(_contactCollectionName);
            _kepProviderCollection = _dbSystem_efeuCampus.GetCollection<EfCaServiceProviderDB>(_kepProviderCollectionName);
            _syncMeetingPointCollection = _dbSystem_efeuCampus.GetCollection<EfCaSyncMeetingPointDB>(_syncMeetingPointCollectionName);
            _transportBoxCollection = _dbSystem_efeuCampus.GetCollection<EfCaTransportBoxDB>(_transportBoxCollectionName);
            _transportBoxTypeCollection = _dbSystem_efeuCampus.GetCollection<EfCaTransportBoxTypeDB>(_transportBoxTypeCollectionName);
            _ordersCollection = _dbSystem_efeuCampus.GetCollection<EfCaOrderDB>(_ordersCollectionName);
            _imageDescriptionsCollection = _dbSystem_efeuCampus.GetCollection<EfCaImageDocumentationDB>(_imageDescriptionsCollectionName);
            _plannedToursCollection = _dbSystem_efeuCampus.GetCollection<EfCaPlannedTripDB>(_plannedToursCollectionName);
            _usersCollection = _dbSystem_efeuCampus.GetCollection<EfCaUserDB>(_usersCollectionName);
            _rolesCollection = _dbSystem_efeuCampus.GetCollection<EfCaRoleDB>(_rolesCollectionName);
            _tenantsCollection = _dbSystem_efeuCampus.GetCollection<EfCaTenantDB>(_tenantsCollectionName);
            _configsCollection = _dbSystem_efeuCampus.GetCollection<EfCaConfigDB>(_configsCollectionName);
            _systemCollection = _dbSystem_efeuCampus.GetCollection<EfCaSystemDB>(_systemCollectionName);
            _warehousePlaceCollection = _dbSystem_efeuCampus.GetCollection<EfCaWarehousePlaceDB>(_warehousePlaceCollectionName);
            #endregion
        }

        internal T FindUniqueByFilter<T>(FilterDefinition<T> filter, string collection)
        {
            List<T> objectsFound = FindAllByFilter(filter, collection);
            if (objectsFound != null && objectsFound.Count == 1)
            {
                return objectsFound.ElementAt(0);
            }

            //return default(T);
            return default;
        }

        internal string ExecuteCreateUniqueOriginatorIndex(string collectionName)
        {
            var collection = _dbSystem_efeuCampus.GetCollection<BsonDocument>(collectionName);

            var indexKeysDefinition = Builders<BsonDocument>.IndexKeys.Ascending("extIdDetails.originator").Ascending("extIdDetails.extId");
            var indexModel = new CreateIndexModel<BsonDocument>(indexKeysDefinition, new CreateIndexOptions { Unique = true });
            var result = collection.Indexes.CreateOne(indexModel);
            return result;
        }

        internal string ExecuteCreateUniqueUserIndex(string collectionName)
        {
            var collection = _dbSystem_efeuCampus.GetCollection<BsonDocument>(collectionName);

            var indexKeysDefinition = Builders<BsonDocument>.IndexKeys.Ascending("email").Ascending("role");
            var indexModel = new CreateIndexModel<BsonDocument>(indexKeysDefinition, new CreateIndexOptions { Unique = true });
            var result = collection.Indexes.CreateOne(indexModel);
            return result;
        }

        internal string ExecuteCreateUniqueOriginatorIndex_org<T>(string collectionName)
        {
            var collection = _dbSystem_efeuCampus.GetCollection<T>(collectionName);

            var indexKeysDefinition = Builders<T>.IndexKeys.Ascending("extIdDetails.originator").Ascending("extIdDetails.extId");
            var indexModel = new CreateIndexModel<T>(indexKeysDefinition, new CreateIndexOptions { Unique = true });
            var result = collection.Indexes.CreateOne(indexModel);
            return result;
        }

        private List<T> FindAllByFilter<T>(FilterDefinition<T> filter, string collection)
        {
            IMongoCollection<T> _collection;
            _collection = _dbSystem_efeuCampus.GetCollection<T>(collection);

            List<T> objectsFound = _collection.Find(filter).ToList<T>();
            return objectsFound;
        }

        private List<T> FindAllByFilter<T>(FilterDefinition<T> filter, string collection, ProjectionDefinition<T> exclude)
        {
            IMongoCollection<T> _collection;
            _collection = _dbSystem_efeuCampus.GetCollection<T>(collection);

            List<T> objectsFound = _collection.Find(filter).Project<T>(exclude).ToList<T>();
            return objectsFound;
        }

        /// <summary>
        /// Die folgenden Parameter MÜSSEN von aussen übergeben also richtig gesetzt werden
        /// 
        /// CurrentVersion++, UpdateTime
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter"></param>
        /// <param name="updateDB"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        private T UpdateByFilterInternal<T>(FilterDefinition<T> filter, T updateDB, string collection)
        {
            IMongoCollection<T> _collection;
            _collection = _dbSystem_efeuCampus.GetCollection<T>(collection);
            ReplaceOptions options = new ReplaceOptions();
            options.IsUpsert = false;

            ReplaceOneResult replaceOneResult = _collection.ReplaceOne(filter, updateDB, options);
            if (replaceOneResult.IsAcknowledged == true && replaceOneResult.MatchedCount == 1 && replaceOneResult.ModifiedCount == 1)
            {
                return updateDB;
            }

            return default(T);
        }

        private UpdateResult PatchByFilterInternal<T>(FilterDefinition<T> filter, UpdateDefinition<T> updateDBItems, string collection)
        {
            IMongoCollection<T> _collection;
            _collection = _dbSystem_efeuCampus.GetCollection<T>(collection);
            UpdateOptions options = new UpdateOptions();
            options.IsUpsert = false;

            UpdateResult updateResult = _collection.UpdateMany(filter, updateDBItems, options);
            if (updateResult.IsAcknowledged == true)
            {
                return updateResult;
            }

            return null;
        }

        private List<K> FindAllByAggregation<T, K>(FilterDefinition<T> filter, ProjectionDefinition<T, K> projection, string collection)
        {
            IMongoCollection<T> _collection;
            _collection = _dbSystem_efeuCampus.GetCollection<T>(collection);

            List<K> objectsFound = _collection.Aggregate().Match(filter).Project(projection).ToList();
            return objectsFound;
        }
    }
}
