﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcVehicle
    public partial class MongoDBSvc
    {
        #region efeuCampus Vehicle
        public EfCaVehicleResp AddVehicles(InternalUserInfos internalUserInfos, List<EfCaVehicle> vehicles)
        {
            EfCaVehicleResp response = new EfCaVehicleResp();
            response.Error = false;
            response.Vehicles = new List<EfCaVehicle>();

            foreach (EfCaVehicle item in vehicles)
            {
                try
                {
                    EfCaVehicleDB addItemToDB = new EfCaVehicleDB(internalUserInfos, item);
                    _vehicleCollection.InsertOne(addItemToDB);

                    EfCaVehicle newValueObject = new EfCaVehicle(addItemToDB);
                    response.Vehicles.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddVehicles({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddVehicles(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaVehicleResp FindVehiclesByFinder(InternalUserInfos internalUserInfos, EfCaVehicle finder)
        {
            EfCaVehicleResp response = new EfCaVehicleResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaVehicleDB>> filterDefinitions = new List<FilterDefinition<EfCaVehicleDB>>();
                    filterDefinitions.Add(Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaVehicleDB>.Filter;
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.VehicleType != null)
                    {
                        filterDefinitions.Add(Builders<EfCaVehicleDB>.Filter.Eq("vehicleType", finder.VehicleType));
                    }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaVehicleDB> dbItemsFound = FindAllByFilter(filter, _vehiclesCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.Vehicles = new List<EfCaVehicle>();
                        foreach (EfCaVehicleDB item in dbItemsFound)
                        {
                            EfCaVehicle newValueObject = new EfCaVehicle(item);
                            response.Vehicles.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindVehiclesByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaVehicleResp ReadAllVehicles(InternalUserInfos internalUserInfos)
        {
            EfCaVehicleResp vehicleBotResp = new EfCaVehicleResp();
            vehicleBotResp.Error = false;
            try
            {
                var builder = Builders<EfCaVehicleDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaVehicleDB> vehicleBotsDB = FindAllByFilter(filter, _vehiclesCollectionName);
                if (vehicleBotsDB != null && vehicleBotsDB.Count > 0)
                {
                    vehicleBotResp.Vehicles = new List<EfCaVehicle>();
                    foreach (EfCaVehicleDB item in vehicleBotsDB)
                    {
                        EfCaVehicle vehicleBot = new EfCaVehicle(item);
                        vehicleBotResp.Vehicles.Add(vehicleBot);
                    }
                }
                return vehicleBotResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllVehicles(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                vehicleBotResp.Error = true;
                vehicleBotResp.ErrorMsg = "Please refer to the details";
            }

            return vehicleBotResp;
        }

        public EfCaVehicleResp UpdateVehicle(InternalUserInfos internalUserInfos, EfCaVehicle vehicle)
        {
            EfCaVehicleResp response = new EfCaVehicleResp();
            response.Error = false;

            var builder = Builders<EfCaVehicleDB>.Filter;
            var filter = builder.And(
                Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.extId", vehicle.Ident),
                Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaVehicleDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _vehiclesCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateVehicle({internalUserInfos.TenantId}) failed. Vehicle({vehicle.Ident}) not found";
                log.Error(logErrorMsg);
                string errorMsg = $"UpdateVehicle(..) failed. Vehicle({vehicle.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(vehicle);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateVehicle({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _vehiclesCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaVehicle newValueObject = new EfCaVehicle(uniqueDBItemToUpdateFound);
                    response.Vehicles.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateVehicle(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaVehicleResp DeleteVehicles(InternalUserInfos internalUserInfos, List<string> vehicleBotIdents)
        {
            EfCaVehicleResp vehicleBotResp = new EfCaVehicleResp();
            vehicleBotResp.Error = false;
            try
            {
                foreach (string ident in vehicleBotIdents)
                {
                    var builder = Builders<EfCaVehicleDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaVehicleDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaVehicleDB uniqueDBItemFound = FindUniqueByFilter(filter, _vehiclesCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        vehicleBotResp.Error = true;
                        string logErrorMsg = $"DeleteVehicles({internalUserInfos.TenantId}) failed. VehicleBot({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteVehicles(..) failed. VehicleBot({ident}) not found";
                        vehicleBotResp.ErrorMsg = errorMsg;
                        return vehicleBotResp;
                    }
                    else
                    {
                        _vehicleCollection.DeleteOne(filter);
                    }
                }
                return vehicleBotResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteVehicles(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                vehicleBotResp.Error = true;
                vehicleBotResp.ErrorMsg = "Please refer to the details";
            }

            return vehicleBotResp;
        }
        #endregion
    }
}
