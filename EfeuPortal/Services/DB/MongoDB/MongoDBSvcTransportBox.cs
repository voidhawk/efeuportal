using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.DB.MongoDB
{
    //public class MongoDBSvcTransportBox
    public partial class MongoDBSvc
    {
        #region efeuCampus TransportBoxes
        public EfCaTransportBoxResp AddTransportBoxes(InternalUserInfos internalUserInfos, List<EfCaTransportBox> transportBoxes)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.Error = false;
            response.TransportBoxes = new List<EfCaTransportBox>();

            foreach (EfCaTransportBox item in transportBoxes)
            {
                try
                {
                    EfCaTransportBoxDB addItemToDB = new EfCaTransportBoxDB(internalUserInfos, item);
                    _transportBoxCollection.InsertOne(addItemToDB);
                    
                    EfCaTransportBox newValueObject = new EfCaTransportBox(addItemToDB);
                    response.TransportBoxes.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddTransportBoxes({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddTransportBoxes(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaTransportBoxResp FindTransportBoxesByFinder(InternalUserInfos internalUserInfos, EfCaTransportBox finder)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaTransportBoxDB>> filterDefinitions = new List<FilterDefinition<EfCaTransportBoxDB>>();
                    filterDefinitions.Add(Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaTransportBoxDB>.Filter;
                    if (finder.Ident != null)
                    {
                        filterDefinitions.Add(Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                    }
                    if (finder.TransportBoxTypeId != null)
                    {
                        filterDefinitions.Add(Builders<EfCaTransportBoxDB>.Filter.Eq("transportBoxTypeId", finder.TransportBoxTypeId));
                    }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaTransportBoxDB> dbItemsFound = FindAllByFilter(filter, _transportBoxCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.TransportBoxes = new List<EfCaTransportBox>();
                        foreach (EfCaTransportBoxDB item in dbItemsFound)
                        {
                            EfCaTransportBox newValueObject = new EfCaTransportBox(item);
                            response.TransportBoxes.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindTransportBoxesByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaTransportBoxResp ReadAllTransportBoxes(InternalUserInfos internalUserInfos)
        {
            EfCaTransportBoxResp transportBoxResp = new EfCaTransportBoxResp();
            transportBoxResp.Error = false;
            try
            {
                var builder = Builders<EfCaTransportBoxDB>.Filter;
                var filter = builder.And(
                    Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                    Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.owner", true)
                    );
                List<EfCaTransportBoxDB> transportBoxesDB = FindAllByFilter(filter, _transportBoxCollectionName);
                if (transportBoxesDB != null && transportBoxesDB.Count > 0)
                {
                    transportBoxResp.TransportBoxes = new List<EfCaTransportBox>();
                    foreach (EfCaTransportBoxDB item in transportBoxesDB)
                    {
                        EfCaTransportBox transportBox = new EfCaTransportBox(item);
                        transportBoxResp.TransportBoxes.Add(transportBox);
                    }
                }
                return transportBoxResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"ReadAllTransportBoxes(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                transportBoxResp.Error = true;
                transportBoxResp.ErrorMsg = "Please refer to the details";
            }

            return transportBoxResp;
        }

        public EfCaTransportBoxResp UpdateTransportBox(InternalUserInfos internalUserInfos, EfCaTransportBox transportBox)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.Error = false;

            var builder = Builders<EfCaTransportBoxDB>.Filter;
            var filter = builder.And(
                Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.extId", transportBox.Ident),
                Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.owner", true)
                );
            EfCaTransportBoxDB uniqueDBItemToUpdateFound = FindUniqueByFilter(filter, _transportBoxCollectionName);
            if (uniqueDBItemToUpdateFound == null)
            {
                response.Error = true;
                string logErrorMsg = $"UpdateTransportBox({internalUserInfos.TenantId}) failed. TransportBox({transportBox.Ident}) not found";
                log.Error(logErrorMsg);
                string errorMsg = $"UpdateTransportBox(..) failed. TransportBox({transportBox.Ident}) not found";
                response.ErrorMsg = errorMsg;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound.Update(transportBox);
            }
            catch (Exception ex)
            {
                string logErrorMsg = $"UpdateTransportBox({internalUserInfos.TenantId}) outdated. Error({ex.Message})";
                log.Error(logErrorMsg);

                response.Error = true;
                response.ErrorMsg = ex.Message;
                return response;
            }

            try
            {
                uniqueDBItemToUpdateFound = UpdateByFilterInternal(filter, uniqueDBItemToUpdateFound, _transportBoxCollectionName);
                if (uniqueDBItemToUpdateFound != null)
                {
                    EfCaTransportBox newValueObject = new EfCaTransportBox(uniqueDBItemToUpdateFound);
                    response.TransportBoxes.Add(newValueObject);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"UpdateTransportBox(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }

        public EfCaTransportBoxResp DeleteTransportBoxes(InternalUserInfos internalUserInfos, List<string> transportBoxIdents)
        {
            EfCaTransportBoxResp transportBoxResp = new EfCaTransportBoxResp();
            transportBoxResp.Error = false;
            try
            {
                foreach (string ident in transportBoxIdents)
                {
                    var builder = Builders<EfCaTransportBoxDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaTransportBoxDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaTransportBoxDB uniqueDBItemFound = FindUniqueByFilter(filter, _transportBoxCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        transportBoxResp.Error = true;
                        string logErrorMsg = $"DeleteTransportBoxes({internalUserInfos.TenantId}) failed. TransportBox({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteTransportBoxes(..) failed. TransportBox({ident}) not found";
                        transportBoxResp.ErrorMsg = errorMsg;
                        return transportBoxResp;
                    }
                    else
                    {
                        _transportBoxCollection.DeleteOne(filter);
                    }
                }
                return transportBoxResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteTransportBoxes(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                transportBoxResp.Error = true;
                transportBoxResp.ErrorMsg = "Please refer to the details";
            }

            return transportBoxResp;
        }
        #endregion

        #region efeuCampus TransportBoxTypes
        public EfCaTransportBoxResp AddTransportBoxTypes(InternalUserInfos internalUserInfos, List<EfCaTransportBoxType> transportBoxTypes)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.Error = false;
            response.TransportBoxTypes = new List<EfCaTransportBoxType>();

            foreach (EfCaTransportBoxType item in transportBoxTypes)
            {
                try
                {
                    EfCaTransportBoxTypeDB addItemToDB = new EfCaTransportBoxTypeDB(internalUserInfos, item);
                    _transportBoxTypeCollection.InsertOne(addItemToDB);

                    EfCaTransportBoxType newValueObject = new EfCaTransportBoxType(addItemToDB);
                    response.TransportBoxTypes.Add(newValueObject);
                }
                catch (Exception ex)
                {
                    string errorMsg = $"AddTransportBoxTypes({internalUserInfos.TenantId}, {internalUserInfos.Email}) Ident({item.Ident}); failed, Exception: {ex.Message}";
                    log.Error(errorMsg);

                    response.Error = true;
                    response.ErrorMsg = "Please refer to the details";
                    DetailInfo detailInfo = new DetailInfo() { Ident = item.Ident };
                    response.AddDetailInfos(detailInfo);
                    if (ex.Message.Contains("E11000 duplicate key error"))
                    {
                        //A write operation resulted in an error. E11000 duplicate key error index: thehaulier.efca - boxmountdevices.$tenant - extId dup key: { : null, : null }
                        detailInfo.Detail = $"E11000 duplicate key error: Index(...)";
                    }
                    else
                    {
                        detailInfo.Detail = ex.Message;
                        log.Error($"AddTransportBoxTypes(..) Message not processed in detail: {ex.Message}");
                    }
                }
            }

            return response;
        }

        public EfCaTransportBoxResp FindTransportBoxTypesByFinder(InternalUserInfos internalUserInfos, EfCaTransportBoxType finder, List<string> transportBoxTypeIds)
        {
            EfCaTransportBoxResp response = new EfCaTransportBoxResp();
            response.Error = false;

            try
            {
                {
                    #region create the Filter
                    List<FilterDefinition<EfCaTransportBoxTypeDB>> filterDefinitions = new List<FilterDefinition<EfCaTransportBoxTypeDB>>();
                    filterDefinitions.Add(Builders<EfCaTransportBoxTypeDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId));
                    filterDefinitions.Add(Builders<EfCaTransportBoxTypeDB>.Filter.Eq("extIdDetails.owner", true));
                    var builderFilter = Builders<EfCaTransportBoxTypeDB>.Filter;
                    if (finder != null)
                    {
                        if (finder.Ident != null)
                        {
                            filterDefinitions.Add(Builders<EfCaTransportBoxTypeDB>.Filter.Eq("extIdDetails.extId", finder.Ident));
                        }
                        if (finder.Type != null)
                        {
                            filterDefinitions.Add(Builders<EfCaTransportBoxTypeDB>.Filter.Eq(find => find.Type, $"{finder.Type}.*"));
                        }
                        if (finder.Description != null)
                        {
                            filterDefinitions.Add(Builders<EfCaTransportBoxTypeDB>.Filter.Regex(find => find.Description, $"{finder.Description}.*"));
                        }
                    }
                    else if (transportBoxTypeIds != null && transportBoxTypeIds.Count > 0)
                    {
                        filterDefinitions.Add(Builders<EfCaTransportBoxTypeDB>.Filter.In("extIdDetails.extId", transportBoxTypeIds));
                    }
                    else
                    {
                        response.Error = true;
                        response.ErrorMsg = "Invalid Finder(s)";
                        return response;
                    }
                    #endregion

                    var filter = builderFilter.And(filterDefinitions.ToArray());
                    List<EfCaTransportBoxTypeDB> dbItemsFound = FindAllByFilter(filter, _transportBoxTypeCollectionName);

                    if (dbItemsFound != null && dbItemsFound.Count > 0)
                    {
                        response.TransportBoxTypes = new List<EfCaTransportBoxType>();
                        foreach (EfCaTransportBoxTypeDB item in dbItemsFound)
                        {
                            EfCaTransportBoxType newValueObject = new EfCaTransportBoxType(item);
                            response.TransportBoxTypes.Add(newValueObject);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = $"FindTransportBoxTypesByFinder(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                response.Error = true;
                response.ErrorMsg = "Please refer to the details";
            }

            return response;
        }
 
        public EfCaTransportBoxResp DeleteTransportBoxTypes(InternalUserInfos internalUserInfos, List<string> transportBoxTypeIdents)
        {
            EfCaTransportBoxResp transportBoxResp = new EfCaTransportBoxResp();
            transportBoxResp.Error = false;
            try
            {
                foreach (string ident in transportBoxTypeIdents)
                {
                    var builder = Builders<EfCaTransportBoxTypeDB>.Filter;
                    var filter = builder.And(
                        Builders<EfCaTransportBoxTypeDB>.Filter.Eq("extIdDetails.originator", internalUserInfos.TenantId),
                        Builders<EfCaTransportBoxTypeDB>.Filter.Eq("extIdDetails.extId", ident),
                        Builders<EfCaTransportBoxTypeDB>.Filter.Eq("extIdDetails.owner", true)
                        );
                    EfCaTransportBoxTypeDB uniqueDBItemFound = FindUniqueByFilter(filter, _transportBoxTypeCollectionName);
                    if (uniqueDBItemFound == null)
                    {
                        transportBoxResp.Error = true;
                        string logErrorMsg = $"DeleteTransportBoxTypes({internalUserInfos.TenantId}) failed. TransportBox({ident}) not found";
                        log.Error(logErrorMsg);
                        string errorMsg = $"DeleteTransportBoxTypes(..) failed. TransportBoxType({ident}) not found";
                        transportBoxResp.ErrorMsg = errorMsg;
                        return transportBoxResp;
                    }
                    else
                    {
                        _transportBoxTypeCollection.DeleteOne(filter);
                    }
                }
                return transportBoxResp;
            }
            catch (Exception ex)
            {
                string errorMsg = $"DeleteTransportBoxTypes(..) failed, Exception: {ex.Message}";
                log.Error(errorMsg);

                transportBoxResp.Error = true;
                transportBoxResp.ErrorMsg = "Please refer to the details";
            }

            return transportBoxResp;
        }
        #endregion
    }
}
