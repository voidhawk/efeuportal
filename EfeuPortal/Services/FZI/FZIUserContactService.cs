
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Services.FZI
{
    public class FZIUserContactService
    {
        private SystemService _systemService;

        private ContactService _contactService;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIAuthorizationService));
        
        public FZIUserContactService(IConfiguration config, SystemService systemService, ContactService contactService)
        {
            _systemService = systemService;
            _contactService = contactService;
        }

        public EfCaContact GetContactForUser(InternalUserInfos internalUserInfos)
        {
            var userFinder = new EfCaUser();
            userFinder.Email = internalUserInfos.Email;
            var user = _systemService.FindUsersByFinder(internalUserInfos, userFinder).Users[0];
            
            if (user.ContactId != null) {
                var contactFinder = new EfCaContact();
                contactFinder.Ident = user.ContactId;
                var contact = _contactService.FindContactsByFinder(internalUserInfos, contactFinder).Contacts[0];
                return contact;
            } else {
                return null;
            }
        }
    }
}