using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Client;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using System;
using System.Collections.Generic;

namespace EfeuPortal.Services.FZI
{
    public class FZIAdminAppService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIUserAppService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private TourService _tourService;

        private Dictionary<String, AdminAppApi> adminAppApiByUrl;

        public FZIAdminAppService(IConfiguration config, MongoDBSvc mongoDBSvc, SystemService systemService, TourService tourService)
        {
            adminAppApiByUrl = new Dictionary<string, AdminAppApi>();
            _mongoDBService = mongoDBSvc;
            _systemService = systemService;
            _tourService = tourService;
        }

        private ResourceCalendarsApi GetResourceCalendarsApi(InternalUserInfos internalUserInfos) {
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            EfCaConfig roPlanningArea = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-PlanningArea");
            return new ResourceCalendarsApi(roWebAPIUrl.Url);
        }
        
        private AdminAppApi GetAdminAppApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!adminAppApiByUrl.ContainsKey(processManagementUrl)) {
                adminAppApiByUrl[processManagementUrl] = new AdminAppApi(processManagementUrl);
            } 
            return adminAppApiByUrl[processManagementUrl];
        }

        internal bool ModifyOrder(AdminModifyOrderRequest request, InternalUserInfos internalUserInfos)
        {
            try {
                GetAdminAppApi(internalUserInfos).ModifyOrderAdmin(request);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool DeleteOrder(AdminDeleteOrderRequest request, InternalUserInfos internalUserInfos)
        {
            try {
                GetAdminAppApi(internalUserInfos).DeleteOrderAdmin(request);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool ReturnBox(AdminReturnBoxRequest request, InternalUserInfos internalUserInfos)
        {
            try {
                GetAdminAppApi(internalUserInfos).ReturnBox(request);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool CreateBoxRequestAdmin(String contactId, BoxRequest request, InternalUserInfos internalUserInfos)
        {
            try {
                GetAdminAppApi(internalUserInfos).CreateBoxRequestAdmin(contactId, request);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal VehicleLock LockVehicle(LockVehicleRequest request, InternalUserInfos internalUserInfos)
        {
            ResourceCalendarsApi calendarApi = GetResourceCalendarsApi(internalUserInfos);

            List<ResourceCalendarHeader> calendarHeaderList = new List<ResourceCalendarHeader>();
            string ident = Guid.NewGuid().ToString();
            request.LockIdent = ident;
            ResourceCalendarHeader calendarHeader = new ResourceCalendarHeader(ident, ResourceCalendarHeader.ActionCodeEnum.NEW, 
                ResourceCalendarHeader.TypeEnum.BLOCKTIME, request.Start.UtcDateTime.ToLocalTime(), request.End.UtcDateTime.ToLocalTime());
            calendarHeaderList.Add(calendarHeader);

            List<ResourceCalendar> calendars = new List<ResourceCalendar>();
            ResourceCalendar calendar = new ResourceCalendar(calendarHeaderList, request.VehicleIdent, DateTime.UtcNow, ResourceCalendar.ActionCodeEnum.UPDATE, null);
            calendars.Add(calendar);
            ImportResults result = calendarApi.ResourceCalendarsPostAsync(calendars);
            try {
                return GetAdminAppApi(internalUserInfos).LockVehicle(request);
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal bool UnlockVehicle(UnlockVehicleRequest request, InternalUserInfos internalUserInfos)
        {
            ResourceCalendarsApi calendarApi = GetResourceCalendarsApi(internalUserInfos);
            
            List<ResourceCalendarHeader> calendarHeaderList = new List<ResourceCalendarHeader>();
            ResourceCalendarHeader calendarHeader = new ResourceCalendarHeader(request.LockIdent, ResourceCalendarHeader.ActionCodeEnum.DELETE);
            calendarHeaderList.Add(calendarHeader);

            List<ResourceCalendar> calendars = new List<ResourceCalendar>();
            ResourceCalendar calendar = new ResourceCalendar(calendarHeaderList, request.VehicleIdent, DateTime.UtcNow, ResourceCalendar.ActionCodeEnum.UPDATE, null);
            calendars.Add(calendar);
            ImportResults result = calendarApi.ResourceCalendarsPostAsync(calendars);
            try {
                GetAdminAppApi(internalUserInfos).UnlockVehicle(request);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal List<PackageInformationAdmin> GetPackageInformations(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetPackageInformations();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal List<PackageInformationAdmin> GetFinishedPackages(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetFinishedPackages();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal PackageInformationAdmin GetPackage(string orderId, InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetPackage(orderId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal List<BoxOrderInformationAdmin> GetBoxOrderInformations(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetBoxOrderInformations();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal List<BoxOrderInformationAdmin> GetFinishedBoxOrders(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetFinishedBoxOrders();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal BoxOrderInformationAdmin GetBoxOrder(string orderId, InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetBoxOrder(orderId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal List<RechargeOrderInfoAdmin> GetRechargingOrders(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetRechargingOrders();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal TourDeletionResult DeleteToursForVehicle(InternalUserInfos internalUserInfos, string vehicleId)
        {
            try {
                return GetAdminAppApi(internalUserInfos).DeleteToursForVehicle(vehicleId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal ActionResult<List<TourInformation>> GetTourOverview(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetTourOverview();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal ActionResult<List<VehicleInformation>> GetVehicleInformations(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetVehicleInformations();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal List<VehicleLock> GetVehicleLocks(string vehicleIdent, InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetVehicleLocks(vehicleIdent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal bool RemovePackagesFromOrder(AdminRemovePackagesFromOrderRequest request, InternalUserInfos internalUserInfos)
        {
            try {
                GetAdminAppApi(internalUserInfos).RemovePackagesFromOrder(request);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal ActionResult<List<CustomerInfoAdmin>> GetCustomerList(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetCustomerList();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }

        internal ActionResult<List<PlanningStatus>> GetPlanningStatusLogs(InternalUserInfos internalUserInfos)
        {
            try {
                return GetAdminAppApi(internalUserInfos).GetPlanningStatusLogs();
            } catch (ApiException e) {
                log.Error(e.ToString());
                throw;
            }
        }
    }
}
