using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Client;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using static EfeuPortal.Clients.ProcessManagement.Model.BoxStatus;
using static EfeuPortal.Clients.ProcessManagement.Model.MountStatus;

namespace EfeuPortal.Services.FZI
{
    public class FZIStatusService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIStatusService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private TourService _tourService;

        private Dictionary<String, MountStatusApi> mountStatusApiByUrl;
        private Dictionary<String, BoxStatusApi> boxStatusApiByUrl;
        private Dictionary<String, VehicleStatusApi> vehicleStatusApiByUrl;
        private Dictionary<String, TourStatusApi> tourStatusApiByUrl;

        public FZIStatusService(IConfiguration config, MongoDBSvc mongoDBSvc, SystemService systemService, TourService tourService)
        {
            mountStatusApiByUrl = new Dictionary<String, MountStatusApi>();
            boxStatusApiByUrl = new Dictionary<String, BoxStatusApi>();
            vehicleStatusApiByUrl = new Dictionary<String, VehicleStatusApi>();
            tourStatusApiByUrl = new Dictionary<string, TourStatusApi>();
            _mongoDBService = mongoDBSvc;
            _systemService = systemService;
            _tourService = tourService;
        }

        
        private MountStatusApi GetMountStatusApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!mountStatusApiByUrl.ContainsKey(processManagementUrl)) {
                mountStatusApiByUrl[processManagementUrl] = new MountStatusApi(processManagementUrl);
            } 
            return mountStatusApiByUrl[processManagementUrl];
        }

        private TourStatusApi GetTourStatusApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!tourStatusApiByUrl.ContainsKey(processManagementUrl)) {
                tourStatusApiByUrl[processManagementUrl] = new TourStatusApi(processManagementUrl);
            } 
            return tourStatusApiByUrl[processManagementUrl];
        }

        private BoxStatusApi GetBoxStatusApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!boxStatusApiByUrl.ContainsKey(processManagementUrl)) {
                boxStatusApiByUrl[processManagementUrl] = new BoxStatusApi(processManagementUrl);
            } 
            return boxStatusApiByUrl[processManagementUrl];
        }

        private VehicleStatusApi GetVehicleStatusApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!vehicleStatusApiByUrl.ContainsKey(processManagementUrl)) {
                vehicleStatusApiByUrl[processManagementUrl] = new VehicleStatusApi(processManagementUrl);
            } 
            return vehicleStatusApiByUrl[processManagementUrl];
        }

        internal List<MountStatus> GetMountStatusList(InternalUserInfos internalUserInfos)
        {
            try {
                return GetMountStatusApi(internalUserInfos).GetMountStatusList();
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }
        
        internal MountStatus GetMountStatus(string mountId, InternalUserInfos internalUserInfos)
        {
            try {
                return GetMountStatusApi(internalUserInfos).GetMountStatus(mountId);
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal BoxInDepotResponse IsBoxInDepot(InternalUserInfos internalUserInfos, string lockerId, string lockId)
        {
            try {
                BoxInDepotResponse response = GetBoxStatusApi(internalUserInfos).IsBoxInDepot(lockerId, lockId);
                return response;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal VehicleStatus GetVehicleStatus(InternalUserInfos internalUserInfos, string vehicleId)
        {
           try {
                VehicleStatus vehicleStatus = GetVehicleStatusApi(internalUserInfos).GetVehicleStatus(vehicleId);
                return vehicleStatus;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal List<VehicleStatus> GetVehicleStatusList(InternalUserInfos internalUserInfos)
        {
           try {
                List<VehicleStatus> vehicleStatusList = GetVehicleStatusApi(internalUserInfos).GetVehicleStatusList();
                return vehicleStatusList;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool UpdateMountStatus(InternalUserInfos _internalUserInfos, MountStatus mountStatus)
        {
            try {
                GetMountStatusApi(_internalUserInfos).UpdateMountStatus(mountStatus.Id, mountStatus);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool UpdateBoxStatus(InternalUserInfos _internalUserInfos, BoxStatus boxStatus)
        {
            try {
                GetBoxStatusApi(_internalUserInfos).UpdateBoxStatus(boxStatus.Id, boxStatus);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal List<BoxStatus> GetBoxStatusList(InternalUserInfos internalUserInfos)
        {
            try {
                List<BoxStatus> boxStatusList = GetBoxStatusApi(internalUserInfos).GetBoxStatusList();
                return boxStatusList;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal BoxStatus GetBoxStatus(InternalUserInfos internalUserInfos, string boxId)
        {
            try {
                BoxStatus boxStatus = GetBoxStatusApi(internalUserInfos).GetBoxStatus(boxId);
                return boxStatus;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal List<TourStatus> GetTourStatusList(InternalUserInfos internalUserInfos)
        {
            try {
                List<TourStatus> tourStatusList = GetTourStatusApi(internalUserInfos).GetTourStatusList();
                return tourStatusList;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal TourStatus CreateTourStatus(InternalUserInfos internalUserInfos, TourStatus status)
        {
            try {
                TourStatus resStatus = GetTourStatusApi(internalUserInfos).CreateTourStatus(status);
                return resStatus;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool UpdateVehicleStatus(InternalUserInfos _internalUserInfos, VehicleStatus vehicleStatus)
        {
            try {
                GetVehicleStatusApi(_internalUserInfos).UpdateVehicleStatus(vehicleStatus.Id, vehicleStatus);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool AddBoxMountingDevicesStatus(InternalUserInfos _internalUserInfos, EfCaBoxMountingDeviceResp response)
        {
            foreach (EfCaBoxMountingDevice mount in response.BoxMountingDevices) {
                try {
                    MountStatus status = new MountStatus(mount.Ident, DateTime.Now);
                    status.VehicleOccupationState = VehicleOccupationStateEnum.FREE;
                    status.VehicleId = null;
                    status.BoxId = null;
                    status.MountBoxOccupationState = MountBoxOccupationStateEnum.FREE;
                    GetMountStatusApi(_internalUserInfos).CreateMountStatus(mount.Ident, status);
                } catch (ApiException e) {
                    log.Error(e.ToString());
                    return true;
                }
            }
            
            return false;
        }

        internal bool DeleteBoxMountingDeviceStatus(InternalUserInfos _internalUserInfos, String ident)
        {
            try {
                GetMountStatusApi(_internalUserInfos).DeleteMountStatus(ident);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            
            return false;
        }

        internal bool AddVehiclesStatus(InternalUserInfos _internalUserInfos, EfCaVehicleResp response)
        {
            foreach (EfCaVehicle vehicle in response.Vehicles) {
                try {
                    GetVehicleStatusApi(_internalUserInfos).CreateVehicleStatus(vehicle.Ident, 
                    new VehicleStatus(vehicle.Ident, VehicleStateEnum.Idle, true, 1.0f, 1.0f, null, 100, 100.0f, DateTime.Now));
                } catch (ApiException e) {
                    log.Error(e.ToString());
                    return true;
                }
            }
            
            return false;
        }

        internal object DeleteVehicleStatus(InternalUserInfos _internalUserInfos,  String ident)
        {
            try {
                GetVehicleStatusApi(_internalUserInfos).DeleteVehicleStatus(ident);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            
            return false;
        }

        internal bool AddTransportBoxesStatus(InternalUserInfos _internalUserInfos, EfCaTransportBoxResp response)
        {
            foreach (EfCaTransportBox box in response.TransportBoxes) {
                try {
                    GetBoxStatusApi(_internalUserInfos).CreateBoxStatus(box.Ident, 
                    new BoxStatus(box.Ident, BoxStateEnum.Stored, BoxLoadEnum.Empty, DateTime.Now, null, null));
                } catch (ApiException e) {
                    log.Error(e.ToString());
                    return true;
                }
            }
            
            return false;
        }

        internal object DeleteTransportBoxStatus(InternalUserInfos _internalUserInfos,  String ident)
        {
            try {
                GetBoxStatusApi(_internalUserInfos).DeleteBoxStatus(ident);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            
            return false;
        }
    }
}
