using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Client;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using EfeuPortal.Models;
using EfeuPortal.Services.System;
using EfeuPortal.Models.ROPlanning;
using EfeuPortal.Models.Campus.TourData;
using EfeuPortal.Services.Campus;
using Microsoft.AspNetCore.Mvc;
using EfeuPortal.Models.Shared;

namespace EfeuPortal.Services.FZI
{  
    public class FZIVehicleEventService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIVehicleEventService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;

        private Dictionary<String, VehicleEventApi> eventApiByUrl;

        public FZIVehicleEventService(IConfiguration config, MongoDBSvc mongoDBSvc, SystemService systemService)
        {
            eventApiByUrl = new Dictionary<String, VehicleEventApi>();
            _mongoDBService = mongoDBSvc;
            _systemService = systemService;
        }

        private VehicleEventApi GetEventApi(InternalUserInfos internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(internalUserInfos, "ProcessMgmtService").StringValue;
            if (!eventApiByUrl.ContainsKey(processManagementUrl)) {
                eventApiByUrl[processManagementUrl] = new VehicleEventApi(processManagementUrl);
            } 
            return eventApiByUrl[processManagementUrl];
        }

        internal bool DeliverBoxToMount(InternalUserInfos internalUserInfos, VehicleBoxDeliveryEvent vehicleBoxDeliveryEvent)
        {
            try {
                GetEventApi(internalUserInfos).DeliverBoxToMount(vehicleBoxDeliveryEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool PickupBoxFromMount(InternalUserInfos internalUserInfos, VehicleBoxPickupEvent vehicleBoxPickupEvent)
        {
            try {
                GetEventApi(internalUserInfos).PickupBoxFromMount(vehicleBoxPickupEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal RequestMountForBoxDeliveryResponse RequestMountForBoxDelivery(InternalUserInfos internalUserInfos, RequestMountForBoxDeliveryEvent requestMountForBoxDeliveryEvent)
        {
            try {
                return GetEventApi(internalUserInfos).RequestMountForBoxDelivery(requestMountForBoxDeliveryEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool UpdateVehicle(InternalUserInfos internalUserInfos, VehicleUpdate vehicleUpdate)
        {
            try {
                GetEventApi(internalUserInfos).UpdateVehicle(vehicleUpdate);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool FinishTour(InternalUserInfos internalUserInfos, FinishTourEvent finishTourEvent)
        {
            try {
                GetEventApi(internalUserInfos).FinishTour(finishTourEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal SyncStopStateResponse RequestSyncStopState(InternalUserInfos internalUserInfos, SyncStopStateRequest syncStopStateRequest)
        {
            try {
                return GetEventApi(internalUserInfos).RequestSyncStopState(syncStopStateRequest);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool SyncStopArrival(InternalUserInfos internalUserInfos, SyncStopArrivalMessage syncStopArrivalMessage)
        {
            try {
                GetEventApi(internalUserInfos).SyncStopArrival(syncStopArrivalMessage);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal RequestMountForWaitingResponse RequestMountForWaiting(InternalUserInfos internalUserInfos, RequestMountForWaitingEvent requestMountForWaitingEvent)
        {
            try {
                return GetEventApi(internalUserInfos).RequestMountForWaiting(requestMountForWaitingEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool DepartFromMountWaitingCharging(InternalUserInfos internalUserInfos, DepartFromMountWaitingChargingEvent departFromMountWaitingChargingEvent)
        {
            try {
                GetEventApi(internalUserInfos).DepartFromMountWaitingCharging(departFromMountWaitingChargingEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal RequestMountForChargingResponse RequestMountForCharging(InternalUserInfos internalUserInfos, RequestMountForChargingEvent requestMountForChargingEvent)
        {
            try {
                return GetEventApi(internalUserInfos).RequestMountForCharging(requestMountForChargingEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool StartTour(InternalUserInfos internalUserInfos, StartTourEvent startTourEvent)
        {
            try {
                GetEventApi(internalUserInfos).StartTour(startTourEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal RequestMountForBoxPickupResponse RequestMountForBoxPickup(InternalUserInfos internalUserInfos, RequestMountForBoxPickupEvent requestMountForBoxPickupEvent)
        {
            try {
                return GetEventApi(internalUserInfos).RequestMountForBoxPickup(requestMountForBoxPickupEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }
    }
}