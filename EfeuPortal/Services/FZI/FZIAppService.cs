using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Client;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using System;
using sio = System.IO;
using System.Collections.Generic;

namespace EfeuPortal.Services.FZI
{
    public class FZIAppService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIAppService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private Dictionary<String, AppApi> _appApiByUrl;

        public FZIAppService(IConfiguration config, MongoDBSvc mongoDBSvc, SystemService systemService)
        {
            _appApiByUrl = new Dictionary<string, AppApi>();
            _mongoDBService = mongoDBSvc;
            _systemService = systemService;
        }
        
        private AppApi GetAppApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!_appApiByUrl.ContainsKey(processManagementUrl)) {
                _appApiByUrl[processManagementUrl] = new AppApi(processManagementUrl);
            } 
            return _appApiByUrl[processManagementUrl];
        }

        internal ImageDownload GetPackageImages(string packageOrderIdent, InternalUserInfos internalUserInfos)
        {
            try {
                return GetAppApi(internalUserInfos).GetPackageImages(packageOrderIdent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }
    }
}
