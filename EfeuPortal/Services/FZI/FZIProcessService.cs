using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Client;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Campus.TourData;
using EfeuPortal.Models.ROPlanning;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using System;
using System.Collections.Generic;

namespace EfeuPortal.Services.FZI
{
    public class FZIProcessService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIProcessService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private TourService _tourService;

        private Dictionary<String, ProcessApi> processApiByUrl;
        private Dictionary<String, TourStatusApi> tourStatusApiByUrl;

        public FZIProcessService(IConfiguration config, MongoDBSvc mongoDBSvc, SystemService systemService, TourService tourService)
        {
            processApiByUrl = new Dictionary<string, ProcessApi>();
            tourStatusApiByUrl = new Dictionary<String, TourStatusApi>();
            _mongoDBService = mongoDBSvc;
            _systemService = systemService;
            _tourService = tourService;
        }

        private TourStatusApi GetTourStatusApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!tourStatusApiByUrl.ContainsKey(processManagementUrl)) {
                tourStatusApiByUrl[processManagementUrl] = new TourStatusApi(processManagementUrl);
            } 
            return tourStatusApiByUrl[processManagementUrl];
        }

        private ProcessApi GetProcessApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!processApiByUrl.ContainsKey(processManagementUrl)) {
                processApiByUrl[processManagementUrl] = new ProcessApi(processManagementUrl);
            } 
            return processApiByUrl[processManagementUrl];
        }

        internal List<string> GetVehicleTours(string vehicleId, InternalUserInfos internalUserInfos)
        {
            try {
                List<String> tourStrings = GetProcessApi(internalUserInfos).GetAllToursForVehicle(vehicleId);
                return tourStrings;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool DeleteAllReservations(InternalUserInfos internalUserInfos)
        {
            try {
                GetProcessApi(internalUserInfos).DeleteAllReservations();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal List<MountReservation> GetReservationsForMount(string mountId, InternalUserInfos internalUserInfos)
        {
            try {
                List<MountReservation> result = GetProcessApi(internalUserInfos).GetAllReservationsForMount(mountId);
                return result;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool DeleteReservation(string extId, InternalUserInfos internalUserInfos)
        {
            try {
                GetProcessApi(internalUserInfos).DeleteReservation(extId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal List<MountReservation> GetAllReservations(InternalUserInfos internalUserInfos)
        {
            try {
                List<MountReservation> result = GetProcessApi(internalUserInfos).GetAllReservations();
                return result;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool UnfixAllTrips(InternalUserInfos internalUserInfos)
        {
            try {
                var tourStatusList = GetTourStatusApi(internalUserInfos).GetTourStatusList();
                foreach (TourStatus tourStatus in tourStatusList) {
                    var fixation = new EfCaTourFixation();
                    fixation.TourId = tourStatus.Id;
                    fixation.Fix = false;
                    var fixationList = new List<EfCaTourFixation>();
                    fixationList.Add(fixation);
                    FixUnfixTours(internalUserInfos, fixationList);
                    GetTourStatusApi(internalUserInfos).DeleteTourStatus(tourStatus.Id);
                }
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal MountReservation AddReservation(MountReservation mountReservation, InternalUserInfos internalUserInfos)
        {
            try {
                MountReservation result = GetProcessApi(internalUserInfos).AddReservation(mountReservation);
                return result;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }
        
        internal bool Plan(InternalUserInfos _internalUserInfos)
        {
            try {
                GetProcessApi(_internalUserInfos).Plan();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        #region Planning
        internal EfCaPlanningResp ExecutePlanning(InternalUserInfos internalUserInfos, string planningId)
        {
            EfCaPlanningResp response = new EfCaPlanningResp();
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (planningId == null)
            {
                EfCaConfig roPlanningArea = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-PlanningArea");
                planningId = roPlanningArea.StringValue;
            }
            if (roWebAPIUrl == null)
            {
                response.Error = true;
                response.ErrorMsg = "ExecutePlanning(..), EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }

            TourPlanningApi tourPlanningApi = new TourPlanningApi(roWebAPIUrl.Url);
            WebApiResponse planTourResult = tourPlanningApi.TourPlanningPostAllOrdersAsync(planningId);
            if (planTourResult == null || planTourResult.PlanResults == null)
            {
                response.Error = true;
                response.ErrorMsg = "ExecutePlanning(..), empty result received.";
                log.Error(response.ErrorMsg);
                return response;
            }
            /*
             * Planung aufarbeiten
             * - Status der in der Planung verwendeten Aufträge aktualisieren (JSt) >= OK
             * - Tour speichern
             * - gelöschte TourIds => die Daten auch löschen
             * - Touren für das Fahrzeug mit den Bock Koordinaten überarbeiten
             * - Bockreservierung(en) aktualisieren (PTV) => Tommy fragen
             */
            UpdateModifiedOrdersInPlanning(internalUserInfos, planTourResult.PlanResults);

            List<Tour> tours = planTourResult.PlanResults.Tours;
            foreach (Tour item in tours)
            {
                EfCaTour efCaTour = new EfCaTour(item);
                response.PlannedTours.Add(efCaTour);
            }
            response.DeletedTours = planTourResult.PlanResults.DeletedTours;

            //AddPlannedTrips(internalUserInfos, response.PlannedTours);
            //SetPlannedTripsState(internalUserInfos, response.DeletedTours, "DELETED");
            return response;
        }

        internal EfCaPlanningResp FixUnfixTours(InternalUserInfos internalUserInfos, List<EfCaTourFixation> tourFixations)
        {
            EfCaPlanningResp response = new EfCaPlanningResp();
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response.Error = true;
                response.ErrorMsg = "ExecutePlanning(..), EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }
            EfCaConfig roPlanningArea = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-PlanningArea");
            if(roPlanningArea == null)
            {
                response.Error = true;
                response.ErrorMsg = "ExecutePlanning(..), EfCaConfig(key==PTV-RO-PlanningArea) is not valid";
                return response;
            }

            List<FixationParam> fixationParams = new List<FixationParam>();
            List<string> fixTourExtIds = new List<string>();
            List<string> releaseTourExtIds = new List<string>();
            foreach (EfCaTourFixation tourToFix in tourFixations)
            {
                FixationParam fixationParam = new FixationParam();
                if (string.IsNullOrEmpty(tourToFix.PlanningId))
                {
                    fixationParam.PlanningAreaExtId2 = roPlanningArea.StringValue;
                }
                else
                {
                    fixationParam.PlanningAreaExtId2 = tourToFix.PlanningId;
                }

                fixationParam.TourExtId1 = tourToFix.TourId;
                fixationParam.Active = !tourToFix.Fix;

                fixationParams.Add(fixationParam);
                if (tourToFix.Fix)
                {
                    GetProcessApi(internalUserInfos).FixTrip(tourToFix.TourId);
                    fixTourExtIds.Add(tourToFix.TourId);
                }else
                {
                    GetProcessApi(internalUserInfos).UnfixTrip(tourToFix.TourId);
                    releaseTourExtIds.Add(tourToFix.TourId);
                }
            }
            TourPlanningApi tourPlanningApi = new TourPlanningApi(roWebAPIUrl.Url);
            WebApiResponse fixationResult = tourPlanningApi.TourPlanningPutFixOrUnfixTours(fixationParams);
            if (fixationResult == null || fixationResult.FixationResult == null)
            {
                response.Error = true;
                response.ErrorMsg = "FixUnfixTours(..), empty result received.";
                log.Error(response.ErrorMsg);
                return response;
            }
            //if (fixTourExtIds.Count > 0)
            //{
            //    _tourService.UpdatePlannedTripsState(internalUserInfos, fixTourExtIds, "FIXED");
            //}
            //if (releaseTourExtIds.Count > 0)
            //{
            //    _tourService.UpdatePlannedTripsState(internalUserInfos, releaseTourExtIds, "UN_FIXED");
            //}

            return response;
        }

        private void UpdateModifiedOrdersInPlanning(InternalUserInfos internalUserInfos, PlanResults planResults)
        {
            if (planResults.IgnoredOrders != null && planResults.IgnoredOrders.Count > 0)
            {
                EfCaOrder efCaOrder = new EfCaOrder();
                efCaOrder.State = "IGNORED";
                _mongoDBService.PatchOrder(internalUserInfos, planResults.IgnoredOrders, efCaOrder);
            }
            if (planResults.ModifiedOrders != null && planResults.ModifiedOrders.Count > 0)
            {
                EfCaOrder efCaOrder = new EfCaOrder();
                efCaOrder.State = "PLANNED";
                _mongoDBService.PatchOrder(internalUserInfos, planResults.ModifiedOrders, efCaOrder);
            }
            if (planResults.UnscheduledOrders != null && planResults.UnscheduledOrders.Count > 0)
            {
                EfCaOrder efCaOrder = new EfCaOrder();
                efCaOrder.State = "UNSCHEDULED";
                _mongoDBService.PatchOrder(internalUserInfos, planResults.UnscheduledOrders, efCaOrder);
            }
        }

        /// <summary>
        /// This method is used to store the planned trip data in the MongoDB.
        /// Perhaps the data is useful in the future.
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="plannedTrips"></param>
        private void AddPlannedTrips(InternalUserInfos internalUserInfos, List<EfCaTour> plannedTrips)
        {
            _tourService.AddPlannedTrips(internalUserInfos, plannedTrips);
        }

        private void SetPlannedTripsState(InternalUserInfos internalUserInfos, List<string> tripExIds, string state)
        {
            _tourService.UpdatePlannedTripsState(internalUserInfos, tripExIds, state);
        }

        internal SimulationInformation PostSimulationConfig(InternalUserInfos internalUserInfos, SimulationConfig simulationConfig)
        {
            try {
                SimulationInformation result = GetProcessApi(internalUserInfos).PostSimulationConfig(simulationConfig);
                return result;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool PostWarehouseLog(InternalUserInfos internalUserInfos, WarehouseLog warehouseLog)
        {
            try {
                GetProcessApi(internalUserInfos).PostWarehouseLog(warehouseLog);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }
        #endregion
    }
}
