using EfeuPortal.Clients.LoggingService.Api;
using EfeuPortal.Models.Campus;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace EfeuPortal.Services.FZI
{
    public class FZILoggingService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZILoggingService));

        private SystemService _systemService;

        private LoggingApi _loggingApi;

        private bool _enabled;

        public FZILoggingService(IConfiguration config, SystemService systemService)
        {
            _systemService = systemService;
            _loggingApi = new LoggingApi("http://localhost:8006");
            _enabled = false;

        }

        public void LogOrderSerialized(EfCaOrder efCaOrder)
        {
            if (_enabled) {
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(efCaOrder);
                _loggingApi.PostLog("EFEU_PORTAL Order_Update", JSONString);
            }
            

        }

        public void LogOrder(EfCaOrder efCaOrder)
        {
            if (_enabled) {
                dynamic JSONObject = new JObject();
                JSONObject.Order_Id             = efCaOrder.Ident;
                JSONObject.Order_Mode           = efCaOrder.OrderMode;
                JSONObject.Order_Type           = efCaOrder.OrderType;
                JSONObject.Order_Unit           = efCaOrder.OrderUnit;
                JSONObject.State                = efCaOrder.State;
                JSONObject.Creation_Time        = efCaOrder.UserCreationTime;
                JSONObject.RelatedBoxOrderIds   = new JArray(efCaOrder.RelatedBoxOrderIds);
                JSONObject.LinkedBoxOrderId     = efCaOrder.LinkedBoxOrderId;

                EfCaDateTimeSlot deliveryTimeSlot = efCaOrder.DeliveryTimeSlots.FirstOrDefault();

                if (deliveryTimeSlot != null) {
                    DateTimeOffset deliveryStart = deliveryTimeSlot.Start;
                    DateTimeOffset deliveryEnd = deliveryTimeSlot.End;
                    JSONObject.DeliveryStart   = deliveryStart;
                    JSONObject.DeliveryEnd = deliveryEnd;
                } else {
                    JSONObject.DeliveryStart   = null;
                    JSONObject.DeliveryEnd = null;
                }

                EfCaDateTimeSlot pickupTimeSlot = efCaOrder.PickupTimeSlots.FirstOrDefault();

                if (pickupTimeSlot != null) {
                    DateTimeOffset pickupStart = pickupTimeSlot.Start;
                    DateTimeOffset pickupEnd = pickupTimeSlot.End;
                    JSONObject.PickupStart   = pickupStart;
                    JSONObject.PickupEnd = pickupEnd;
                } else {
                    JSONObject.PickupStart   = null;
                    JSONObject.PickupEnd = null;
                }

                string test = JSONObject.ToString(Formatting.None);
                string test2 = JSONObject.ToString(Newtonsoft.Json.Formatting.None);
                string jsonString = JsonConvert.SerializeObject(JSONObject);
                
                _loggingApi.PostLog("EFEU_PORTAL Order_Update", $"{jsonString}");
            }
            

        }

        public void LogOrderTest(EfCaOrder efCaOrder)
        {
            if (_enabled) {
                var JSONObject = new JObject();
                JSONObject.Add("Order_ID", efCaOrder.Ident);       
                
                string test = Regex.Unescape(JSONObject.ToString(Formatting.None));
                _loggingApi.PostLog("EFEU_PORTAL Order_Update", Regex.Unescape(JSONObject.ToString(Formatting.None)));
            }
            

        }

        public void LogOrderDeleted(String id)
        {
            if (_enabled) {
                _loggingApi.PostLog("EFEU_PORTAL Order_Update", "deleted_Order_Id:" + id);
            }

        }

    }
}


