using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Client;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace EfeuPortal.Services.FZI
{
    public class FZIUserAppService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIUserAppService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private TourService _tourService;
        private FZIUserContactService _fziUserContactService;
        private Dictionary<String, UserAppApi> userAppApiByUrl;

        public FZIUserAppService(IConfiguration config, MongoDBSvc mongoDBSvc, SystemService systemService, TourService tourService, FZIUserContactService fziUserContactService)
        {
            userAppApiByUrl = new Dictionary<string, UserAppApi>();
            _mongoDBService = mongoDBSvc;
            _systemService = systemService;
            _tourService = tourService;
            _fziUserContactService = fziUserContactService;
        }

        private UserAppApi GetUserAppApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!userAppApiByUrl.ContainsKey(processManagementUrl)) {
                userAppApiByUrl[processManagementUrl] = new UserAppApi(processManagementUrl);
            } 
            return userAppApiByUrl[processManagementUrl];
        }

        internal ActionResult<BoxRequestResponse> RequestBox(string customerContactIdent, BoxRequest boxRequest, InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                if (contact != null) {
                    customerContactIdent = contact.Ident;
                }
                BoxRequestResponse response = GetUserAppApi(internalUserInfos).RequestBox(customerContactIdent, boxRequest);
                return response;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool ModifyOrder(ModifyOrderRequest modifyOrderRequest, InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                GetUserAppApi(internalUserInfos).ModifyOrder(contact.Ident, modifyOrderRequest);
                return false;
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
        }

        internal bool DeleteBoxRequest(DeleteBoxRequest deleteBoxRequest, InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                GetUserAppApi(internalUserInfos).DeleteBoxRequest(contact.Ident, deleteBoxRequest);
                return false;
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
        }

        internal bool CancelPickupOrder(CancelPickupOrderRequest cancelPickupOrderRequest, InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                GetUserAppApi(internalUserInfos).CancelPickupOrder(contact.Ident, cancelPickupOrderRequest);
                return false;
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
        }

        internal bool ReceivePackage(string customerContactIdent, InternalUserInfos internalUserInfos, CustomerReceivePackageEvent customerReceivePackageEvent)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                if (contact != null) {
                    customerContactIdent = contact.Ident;
                }
                GetUserAppApi(internalUserInfos).ReceivePackage(customerContactIdent, customerReceivePackageEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal ActionResult<CustomerReuseBoxResponse> ReuseBox(InternalUserInfos internalUserInfos, CustomerReuseBoxEvent customerReuseBoxEvent)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                return GetUserAppApi(internalUserInfos).ReuseBox(contact.Ident, customerReuseBoxEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool PostOpenBox(InternalUserInfos internalUserInfos, string boxId)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                GetUserAppApi(internalUserInfos).OpenBox(contact.Ident, boxId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool SendPackage(string customerContactIdent, InternalUserInfos internalUserInfos, CustomerSendPackageEvent customerSendPackageEvent)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                if (contact != null) {
                    customerContactIdent = contact.Ident;
                }
                GetUserAppApi(internalUserInfos).SendPackage(customerContactIdent, customerSendPackageEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool PlaceWasteInBox(InternalUserInfos internalUserInfos, CustomerPlaceWasteEvent customerPlaceWasteEvent)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                GetUserAppApi(internalUserInfos).PlaceWasteInBox(contact.Ident, customerPlaceWasteEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool ModifySettings(InternalUserInfos internalUserInfos, CustomerSettings request)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                GetUserAppApi(internalUserInfos).ModifySettings(contact.Ident, request);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal List<OrderInformation> GetFinishedOrdersOverview(InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                return GetUserAppApi(internalUserInfos).GetFinishedOrdersOverview(contact.Ident);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal List<OrderInformation> GetCurrentOrdersOverview(InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                return GetUserAppApi(internalUserInfos).GetCurrentOrdersOverview(contact.Ident);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal CustomerSettings GetSettings(InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                return GetUserAppApi(internalUserInfos).GetSettings(contact.Ident);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<SyncMeetingPointInformation>> GetSyncMeetingPoints(InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                return GetUserAppApi(internalUserInfos).GetSyncMeetingPoints(contact.Ident);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool ResetDemo(InternalUserInfos internalUserInfos)
        {
            try {
                GetUserAppApi(internalUserInfos).ResetUserDemo();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal ActionResult<List<MountInformation>> GetMounts(InternalUserInfos internalUserInfos)
        {
            try {
                var contact = _fziUserContactService.GetContactForUser(internalUserInfos);
                return GetUserAppApi(internalUserInfos).GetMounts(contact.Ident);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<MinimumTimeWindowLength> GetMinimumTimeWindowLengthMinutes(InternalUserInfos internalUserInfos)
        {
            try {
                return GetUserAppApi(internalUserInfos).GetMinimumTimeWindowLengthMinutes();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<MinimumCommissioningTimeMinutes> GetMinimumCommissioningTimeMinutes(InternalUserInfos internalUserInfos)
        {
            try {
                return GetUserAppApi(internalUserInfos).GetMinimumCommissioningTimeMinutes();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal List<AvailableBox> GetAvailableBoxes(InternalUserInfos internalUserInfos)
        {
            try {
                return GetUserAppApi(internalUserInfos).GetAvailableBoxes();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<DepotAvailabilityTimeSlots> GetDepotAvailabilityTimeSlots(InternalUserInfos internalUserInfos)
        {
            try {
                return GetUserAppApi(internalUserInfos).GetDepotAvailabilityTimeSlots();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<DepotAvailabilityTimeSlots> GetDepotOpeningHours(InternalUserInfos internalUserInfos)
        {
            try {
                return GetUserAppApi(internalUserInfos).GetDepotOpeningHours();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal List<DateTimeOffset> GetPublicHolidaysBW2022To2028(InternalUserInfos internalUserInfos)
        {
            try {
                return GetUserAppApi(internalUserInfos).GetPublicHolidaysBW2022To2028();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }
    }
}
