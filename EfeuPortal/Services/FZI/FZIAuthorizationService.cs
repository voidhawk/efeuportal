
using EfeuPortal.Models;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;

namespace EfeuPortal.Services.FZI
{
    public class FZIAuthorizationService
    {
        private SystemService _systemService;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIAuthorizationService));
        
        public FZIAuthorizationService(IConfiguration config, SystemService systemService)
        {
            _systemService = systemService;
        }

        public bool isAuthorized(InternalUserInfos internalUserInfos, string right, string function, Response response)
        {
            string roleContainsFunctionMsg;
            if (!_systemService.RoleContainsFunction(internalUserInfos, right, function, out roleContainsFunctionMsg))
            {
                log.Error(roleContainsFunctionMsg);

                if (response != null) {
                    response.Error = true;
                    response.ErrorMsg = roleContainsFunctionMsg;
                }
                return false;
            }
            return true;
        }
    }
}