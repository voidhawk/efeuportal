using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Client;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using EfeuPortal.Models;
using EfeuPortal.Services.System;
using EfeuPortal.Models.ROPlanning;
using EfeuPortal.Models.Campus.TourData;
using EfeuPortal.Services.Campus;
using Microsoft.AspNetCore.Mvc;
using EfeuPortal.Models.Shared;

namespace EfeuPortal.Services.FZI
{  
    public class FZIDepotEventService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIDepotEventService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;

        private Dictionary<String, DepotEventApi> eventApiByUrl;

        public FZIDepotEventService(IConfiguration config, MongoDBSvc mongoDBSvc, SystemService systemService)
        {
            eventApiByUrl = new Dictionary<String, DepotEventApi>();
            _mongoDBService = mongoDBSvc;
            _systemService = systemService;
        }

        private DepotEventApi GetEventApi(InternalUserInfos internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(internalUserInfos, "ProcessMgmtService").StringValue;
            if (!eventApiByUrl.ContainsKey(processManagementUrl)) {
                eventApiByUrl[processManagementUrl] = new DepotEventApi(processManagementUrl);
            } 
            return eventApiByUrl[processManagementUrl];
        }
        
        internal CheckCustomerResponse CheckCustomer(CheckCustomerRequest checkCustomerRequest, InternalUserInfos internalUserInfos)
        {
            try {
                return GetEventApi(internalUserInfos).CheckCustomer(checkCustomerRequest);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool AddImageToPackage(string packageOrderIdent, ImageUpload body, InternalUserInfos internalUserInfos)
        {
            try {
                GetEventApi(internalUserInfos).AddImageToPackage(packageOrderIdent, body);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal ActionResult<RegisterPackageResponse> PostRegisterPackage(PackageInformation packageInformation, InternalUserInfos internalUserInfos)
        {
            try {
                RegisterPackageResponse response = GetEventApi(internalUserInfos).RegisterPackage(packageInformation);
                return response;
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool AssignBoxToOrder(InternalUserInfos internalUserInfos, AssignBoxToOrderEvent assignBoxToOrderEvent)
        {
            try {
                GetEventApi(internalUserInfos).AssignBoxToOrder(assignBoxToOrderEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }
        
        internal bool PlaceBoxOnMount(InternalUserInfos internalUserInfos, PlaceBoxOnMountEvent placeBoxOnMountEvent)
        {
            try {
                GetEventApi(internalUserInfos).PlaceBoxOnMount(placeBoxOnMountEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool DeactivateBox(InternalUserInfos internalUserInfos, DeactivateBoxEvent deactivateBoxEvent)
        {
            try {
                GetEventApi(internalUserInfos).DeactivateBox(deactivateBoxEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool PlacePackageInBoxDepot(InternalUserInfos internalUserInfos, DepotSendPackageEvent depotSendPackageEvent)
        {
            try {
                GetEventApi(internalUserInfos).PlacePackageInBox(depotSendPackageEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool ReceivePackageFromBox(InternalUserInfos internalUserInfos, DepotReceivePackageEvent depotReceivePackageEvent)
        {
            try {
                GetEventApi(internalUserInfos).ReceivePackageFromBox(depotReceivePackageEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool RemoveWasteFromBox(InternalUserInfos internalUserInfos, DepotRemoveWasteEvent depotRemoveWasteEvent)
        {
            try {
                GetEventApi(internalUserInfos).RemoveWasteFromBox(depotRemoveWasteEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool CompleteCommissioning(InternalUserInfos internalUserInfos, CompleteCommissioningEvent completeCommissioningEvent)
        {
            try {
                GetEventApi(internalUserInfos).CompleteCommissioning(completeCommissioningEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool LabelParcel(InternalUserInfos internalUserInfos, LabelParcelEvent labelParcelEvent)
        {
            try {
                GetEventApi(internalUserInfos).LabelParcel(labelParcelEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal RegisterPackageResponse LabelOutboundParcel(InternalUserInfos internalUserInfos, OutboundParcelInformation outboundParcelInformation)
        {
            try {
                return GetEventApi(internalUserInfos).LabelOutboundParcel(outboundParcelInformation);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool CompleteOutboundParcelStorage(InternalUserInfos internalUserInfos, CompleteOutboundParcelStorageEvent completeOutboundParcelStorageEvent)
        {
            try {
                GetEventApi(internalUserInfos).CompleteOutboundParcelStorage(completeOutboundParcelStorageEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool CompleteParcelStorage(InternalUserInfos internalUserInfos, CompleteParcelStorageEvent completeParcelStorageEvent)
        {
            try {
                GetEventApi(internalUserInfos).CompleteParcelStorage(completeParcelStorageEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool ConfirmSelfPickup(InternalUserInfos internalUserInfos, ConfirmSelfPickupEvent confirmSelfPickupEvent)
        {
            try {
                GetEventApi(internalUserInfos).ConfirmSelfPickup(confirmSelfPickupEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool ConfirmKepPickup(InternalUserInfos internalUserInfos, ConfirmKepPickupEvent confirmKepPickupEvent)
        {
            try {
                GetEventApi(internalUserInfos).ConfirmKepPickup(confirmKepPickupEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool AddInformationToOutboundPackage(InternalUserInfos internalUserInfos, string packageOrderIdent, PackageInformation packageInformation)
        {
            try {
                GetEventApi(internalUserInfos).AddInformationToOutboundPackage(packageOrderIdent, packageInformation);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool StoreBox(InternalUserInfos internalUserInfos, StoreBoxEvent storeBoxEvent)
        {
            try {
                GetEventApi(internalUserInfos).StoreBox(storeBoxEvent);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal List<PrintJob> GetPrintJobs(InternalUserInfos internalUserInfos)
        {
            try {
                return GetEventApi(internalUserInfos).GetPrintJobs();
            } catch (ApiException e) {
                log.Error(e.ToString());
            }
            return null;
        }

        internal bool FinishPrintJob(InternalUserInfos internalUserInfos, string extId)
        {
            try {
                GetEventApi(internalUserInfos).FinishPrintJob(extId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }
    }
}