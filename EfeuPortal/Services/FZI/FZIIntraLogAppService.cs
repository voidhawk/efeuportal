using EfeuPortal.Clients.ProcessManagement.Api;
using EfeuPortal.Clients.ProcessManagement.Client;
using EfeuPortal.Clients.ProcessManagement.Model;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.Campus;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using System;
using sio = System.IO;
using System.Collections.Generic;

namespace EfeuPortal.Services.FZI
{
    public class FZIIntraLogAppService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FZIIntraLogAppService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private OrderService _orderService;

        private Dictionary<String, IntraLogApi> intraLogAppApiByUrl;

        public FZIIntraLogAppService(IConfiguration config, MongoDBSvc mongoDBSvc, SystemService systemService, OrderService orderService)
        {
            intraLogAppApiByUrl = new Dictionary<string, IntraLogApi>();
            _mongoDBService = mongoDBSvc;
            _systemService = systemService;
            _orderService = orderService;
        }
        
        private IntraLogApi GetIntraLogAppApi(InternalUserInfos _internalUserInfos) {
            var processManagementUrl = _mongoDBService.GetConfigOfTenant(_internalUserInfos, "ProcessMgmtService").StringValue;
            if (!intraLogAppApiByUrl.ContainsKey(processManagementUrl)) {
                intraLogAppApiByUrl[processManagementUrl] = new IntraLogApi(processManagementUrl);
            } 
            return intraLogAppApiByUrl[processManagementUrl];
        }

        internal ActionResult<Commission> GetCommissioning(string commissioningId, InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetCommission(commissioningId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<Commission>> GetCommissionings(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetOpenCommissions();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool DeleteCreatedPackageOrder(string efeuPackageId, InternalUserInfos internalUserInfos)
        {
            try {
                GetIntraLogAppApi(internalUserInfos).DeleteCreatedPackageOrder(efeuPackageId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal ActionResult<List<Parcel>> GetTransitionParcels(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetTransitionParcels();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<MountingDevice>> GetBoxMountingDevices(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetMountingDevices();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<AppBarNumbers> GetAppBarNumbers(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetAppBarNumbers();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<Parcel>> GetParcels(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetIncomingParcels();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<WarehousePlace>> GetWarehousePlaces(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetWarehousePlaces();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<IncomingBox>> GetIncomingBoxes(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetIncomingBoxes();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<Parcel>> GetOutboundParcels(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetOutboundParcels();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool CleanupIntralogData(InternalUserInfos internalUserInfos)
        {
            try {
                GetIntraLogAppApi(internalUserInfos).CleanupIntralogData();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal ActionResult<List<Contact>> GetContacts(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetContacts();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<Parcel>> GetSelfPickupParcelsForContact(string contactId, InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetSelfPickupParcelsForContact(contactId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<Parcel>> GetKepParcels(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetKepParcels();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<BoxInformation>> GetBoxInformations(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetBoxInformations();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal ActionResult<List<Parcel>> GetAllParcels(InternalUserInfos internalUserInfos)
        {
            try {
                return GetIntraLogAppApi(internalUserInfos).GetAllParcels();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return null;
            }
        }

        internal bool ResetDemo(InternalUserInfos internalUserInfos)
        {
            try {
                GetIntraLogAppApi(internalUserInfos).ResetIntraDemo();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool PostOpenBox(InternalUserInfos internalUserInfos, string boxId)
        {
            try {
                GetIntraLogAppApi(internalUserInfos).OpenBoxIntra(boxId);
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool ResetTestData(InternalUserInfos internalUserInfos)
        {
            try {
                GetIntraLogAppApi(internalUserInfos).ResetTestData();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool CompleteTestCommissioning(InternalUserInfos internalUserInfos)
        {
            try {
                GetIntraLogAppApi(internalUserInfos).CompleteTestCommissioning();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool CompleteTestCustomerStops(InternalUserInfos internalUserInfos)
        {
            try {
                GetIntraLogAppApi(internalUserInfos).CompleteTestCustomerStops();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }

        internal bool TestRegisterPackages(InternalUserInfos internalUserInfos)
        {
            try {
                GetIntraLogAppApi(internalUserInfos).RegisterPackages();
            } catch (ApiException e) {
                log.Error(e.ToString());
                return true;
            }
            return false;
        }
    }
}
