﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace EfeuPortal.Services.Campus
{
    public class ContactService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ContactService));

        private MongoDBSvc _mongoDBService;
        private BuildingService _buildingService;

        public ContactService(IConfiguration config, BuildingService buildingService)
        {
            _mongoDBService = new MongoDBSvc(config);
            _buildingService = buildingService;

        }

        public EfCaContactResp AddContacts(InternalUserInfos internalUserInfos, List<EfCaContact> contacts)
        {
            EfCaContactResp response = null;
            foreach(EfCaContact item in contacts)
            {
                item.Ident = Guid.NewGuid().ToString();
            }
            response = _mongoDBService.AddContacts(internalUserInfos, contacts);

            return response;
        }

        public EfCaContactResp FindContactsByFinder(InternalUserInfos internalUserInfos, EfCaContact finder)
        {
            EfCaContactResp response = null;
            response = _mongoDBService.FindContactsByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaContactResp GetAllContacts(InternalUserInfos internalUserInfos)
        {
            EfCaContactResp response = null;
            response = _mongoDBService.ReadAllContacts(internalUserInfos);

            return response;
        }

        public EfCaContactResp PutContact(InternalUserInfos internalUserInfos, EfCaContact contact)
        {
            EfCaContactResp response = null;
            response = _mongoDBService.UpdateContact(internalUserInfos, contact);

            return response;
        }

        /// <summary>
        /// Steps
        /// - Delete a EfCaContact
        /// - if successfull 
        /// -- delete the EfCaUser
        /// -- remove the reference at the EfCaBuilding
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="ids">Ident(s) of a EfCAContact</param>
        /// <returns></returns>
        public EfCaContactResp DeleteContacts(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaContactResp response = null;
            response = _mongoDBService.DeleteContact(internalUserInfos, ids);
            if (response.DetailInfos != null)
            {
                foreach (DetailInfo item in response.DetailInfos)
                {
                    if (item.Detail.Equals("SUCCESS"))
                    {
                        /*
                         * Löschen des zugeordneten EfCaUser
                         */
                        EfCaUser userFinder = new EfCaUser() { ContactId = item.Ident };
                        EfCaUserResp efCaUserResp = _mongoDBService.FindUsersByFinder(internalUserInfos, userFinder);
                        if (efCaUserResp.Error == false && efCaUserResp.Users.Count == 1)
                        {
                            _mongoDBService.DeleteUser(internalUserInfos, efCaUserResp.Users[0].Ident);
                        }

                        /*
                         * Entfernen 
                         */
                        EfCaBuilding buildingsFinder = new EfCaBuilding() { ContactIds = new List<string> { item.Ident } };
                        EfCaBuildingResp findEfCaBuildingResp = _buildingService.FindBuildingsByFinder(internalUserInfos, buildingsFinder);
                        if(findEfCaBuildingResp.Error == false && findEfCaBuildingResp.Buildings != null && findEfCaBuildingResp.Buildings.Count > 0)
                        {
                            foreach(EfCaBuilding building in findEfCaBuildingResp.Buildings)
                            {
                                if(building.ContactIds != null && building.ContactIds.Count > 0)
                                {
                                    building.ContactIds.Remove(item.Ident);
                                    _buildingService.PutBuilding(internalUserInfos, building);
                                }
                            }
                        }
                    }
                }
            }

            return response;
        }

        public EfCaContactResp PutModifyContactReferences(InternalUserInfos internalUserInfos, string action, string type, string ident, EfCaContact contact)
        {
            EfCaContactResp response = null;
            response = new EfCaContactResp();
            response.Error = true;
            response.ErrorMsg = "PutModifyContactReferences(..) not implemented.";

            // JSt: ToDo: Parameter check: action, type, ident
            //EfCaContactDB contactDB = _mongoDBService.FindEfeuContact(tenantId, contact.Ident);
            //if (contactDB == null)
            //{
            //    response = new EfCaContactResp();
            //    response.Error = true;
            //    response.ErrorMsg = "Contact not valid.";
            //    return response;
            //}

            //if (contactDB.CurrentVersion != contact.Version)
            //{
            //    response = new EfCaContactResp();
            //    response.Error = true;
            //    response.ErrorMsg = "Version outdated.";
            //    return response;
            //}

            //if (type.Equals("BoxMountingDevice"))
            //{
            //    response = ModifyBoundingBoxReference(internalUserInfos.TenantId, action, ident, contactDB);
            //}
            //else
            //{
            //    response = new EfCaContactResp();
            //    response.Error = true;
            //    response.ErrorMsg = $"Type({type}) not supported";
            //    return response;
            //}
            return response;
        }

        private EfCaContactResp ModifyBoundingBoxReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaContactDB contactDB)
        {
            EfCaContactResp response = null;
            response = new EfCaContactResp();
            response.Error = true;
            response.ErrorMsg = "ModifyBoundingBoxReference(..) not implemented.";

            //EfCaBoxMountingDeviceDB boxMountingDeviceDB = _mongoDBService.FindEfeuBoxMountingDevice(tenantId, ident);
            //if (boxMountingDeviceDB == null)
            //{
            //    response.Error = true;
            //    response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is not valid.";
            //    return response;
            //}

            //if (action.Equals("ADD"))
            //{
            //    if (contactDB.BoxMountingDeviceIds == null)
            //    {
            //        contactDB.BoxMountingDeviceIds = new List<string>();
            //    }
            //    if (contactDB.BoxMountingDeviceIds.Contains(ident))
            //    {
            //        response = new EfCaContactResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is already assigned.";
            //        return response;
            //    }
            //    contactDB.BoxMountingDeviceIds.Add(ident);
            //}
            //else if (action.Equals("REMOVE"))
            //{
            //    if (contactDB.BoxMountingDeviceIds != null && !contactDB.BoxMountingDeviceIds.Contains(ident))
            //    {
            //        response = new EfCaContactResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is not available.";
            //        return response;
            //    }
            //    contactDB.BoxMountingDeviceIds.Remove(ident);
            //}
            //else
            //{
            //    response = new EfCaContactResp();
            //    response.Error = true;
            //    response.ErrorMsg = $"Action({action}) is not available.";
            //    return response;
            //}

            //EfCaContact contact = new EfCaContact(contactDB);
            //response = _mongoDBService.UpdateEfeuContact(tenantId, contact);
            return response;
        }
    }
}
