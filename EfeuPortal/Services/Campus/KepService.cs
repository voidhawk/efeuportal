﻿using EfeuPortal.Models.Campus.Kep;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class KepService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(KepService));

        private MongoDBSvc _mongoDBService;

        //private RabbitMQService _rabbitMQService;

        public KepService(IConfiguration config)
        {
            _mongoDBService = new MongoDBSvc(config);
            //_rabbitMQService = new RabbitMQService(config);
        }


        public EfCaKepProviderResp AddLogisticServiceProviders(InternalUserInfos internalUserInfos, List<EfCaServiceProvider> logisticServiceProviders)
        {
            EfCaKepProviderResp response = null;
            foreach(EfCaServiceProvider item in logisticServiceProviders)
            {
                item.Version = null;
            }
            response = _mongoDBService.AddLogisticServiceProviders(internalUserInfos, logisticServiceProviders);

            return response;
        }

        public EfCaKepProviderResp FindLogisticServiceProvidersByFinder(InternalUserInfos internalUserInfos, EfCaServiceProvider finder)
        {
            EfCaKepProviderResp response = null;
            response = _mongoDBService.FindLogisticServiceProvidersByFinder(internalUserInfos, finder);

            return response;
        }

        internal EfCaKepProviderResp GetAllLogisticServiceProviders(InternalUserInfos internalUserInfos)
        {
            EfCaKepProviderResp response = null;
            response = _mongoDBService.ReadAllLogisticServiceProviders(internalUserInfos);

            return response;
        }

        internal EfCaKepProviderResp PutLogisticServiceProvider(InternalUserInfos internalUserInfos, EfCaServiceProvider logisticServiceProvider)
        {
            EfCaKepProviderResp response = null;
            response = _mongoDBService.UpdateLogisticServiceProvider(internalUserInfos, logisticServiceProvider);

            return response;
        }

        internal EfCaKepProviderResp DeleteLogisticServiceProviders(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaKepProviderResp response = null;

            response = _mongoDBService.DeleteLogisticServiceProviders(internalUserInfos, ids);
            return response;
        }
    }
}
