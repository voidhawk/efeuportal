﻿using EfeuPortal.Models.Campus.TourData;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class TourService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(TourService));

        private MongoDBSvc _mongoDBService;

        public TourService(IConfiguration config)
        {
            _mongoDBService = new MongoDBSvc(config);
        }

        public EfCaTourResp AddPlannedTrips(InternalUserInfos internalUserInfos, List<EfCaTour> items)
        {
            EfCaTourResp response = null;
            List<EfCaPlannedTrip> plannedTrips = new List<EfCaPlannedTrip>();
            foreach (EfCaTour item in items)
            {
                EfCaPlannedTrip efCaPlannedTrip = new EfCaPlannedTrip();
                efCaPlannedTrip.Ident = Guid.NewGuid().ToString();
                efCaPlannedTrip.TourExtId = item.TourHeader.ExtId1;
                efCaPlannedTrip.PlannedTour = item;
                efCaPlannedTrip.State = "PLANNED";

                plannedTrips.Add(efCaPlannedTrip);
            }
            response = _mongoDBService.AddTours(internalUserInfos, plannedTrips);

            return response;
        }

        public EfCaTourResp UpdatePlannedTripsState(InternalUserInfos internalUserInfos, List<string> idents, string state)
        {
            EfCaTourResp response = null;
            response = _mongoDBService.PatchTours(internalUserInfos, idents, state);

            return response;
        }

        public EfCaTourResp GetVehicleTour(InternalUserInfos internalUserInfos, EfCaPlannedTrip finder)
        {
            EfCaTourResp response = null;
            response = _mongoDBService.FindToursByFinder(internalUserInfos, finder, null);
            if(response.Error == true || response.PlannedTrips == null || response.PlannedTrips.Count != 1)
            {
                return response;
            }

            EfCaPlannedTrip plannedTrip = response.PlannedTrips[0];
            EfCaVehicleTrip vehicleTrip = CreateVehicleTrip(plannedTrip);
            response = new EfCaTourResp();
            response.VehicleTrips = new List<EfCaVehicleTrip>();
            response.VehicleTrips.Add(vehicleTrip);
            return response;
        }

        private EfCaVehicleTrip CreateVehicleTrip(EfCaPlannedTrip plannedTrip)
        {
            EfCaVehicleTrip efCaVehicleTrip = new EfCaVehicleTrip(plannedTrip);
            return efCaVehicleTrip;
        }
    }
}
