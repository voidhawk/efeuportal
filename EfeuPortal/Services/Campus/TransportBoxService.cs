﻿using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.FZI;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class TransportBoxService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(TransportBoxService));

        private FZIStatusService _fziService;

        private MongoDBSvc _mongoDBService;
        //private RabbitMQService _rabbitMQService;
        //private EfeuBuildingService _efeuBuildingService;
        //private EfeuAddressService _efeuAddressService;

        public TransportBoxService(IConfiguration config, FZIStatusService fziService)
        {
            _mongoDBService = new MongoDBSvc(config);
            _fziService = fziService;
            //_rabbitMQService = new RabbitMQService(config);
            //_efeuBuildingService = new EfeuBuildingService(config);
            //_efeuAddressService = new EfeuAddressService(config);
        }

        public EfCaTransportBoxResp AddTransportBoxes(InternalUserInfos internalUserInfos, List<EfCaTransportBox> transportBoxes)
        {
            EfCaTransportBoxResp response = null;
            foreach (EfCaTransportBox item in transportBoxes)
            {
                if (item.Ident == null)
                {
                    item.Ident = Guid.NewGuid().ToString();
                }
                else
                {
                    EfCaTransportBox finder = new EfCaTransportBox();
                    finder.Ident = item.Ident;
                    EfCaTransportBoxResp findResponse = _mongoDBService.FindTransportBoxesByFinder(internalUserInfos, finder);
                    if (findResponse.TransportBoxes.Count > 0)
                    {
                        string errorMsg = $"AddTransportBoxes(..) failed, duplicate transport box ID";
                        log.Error(errorMsg);
                        response.Error = true;
                        response.ErrorMsg = errorMsg;
                        return response;
                    }
                }
            }
            response = _mongoDBService.AddTransportBoxes(internalUserInfos, transportBoxes);
            if (!response.Error) {
                var fziResponse = _fziService.AddTransportBoxesStatus(internalUserInfos, response);
            }

            return response;
        }

        public EfCaTransportBoxResp FindTransportBoxesByFinder(InternalUserInfos internalUserInfos, EfCaTransportBox finder)
        {
            EfCaTransportBoxResp response = null;
            response = _mongoDBService.FindTransportBoxesByFinder(internalUserInfos, finder);

            return response;
        }

        internal EfCaTransportBoxResp GetAllTransportBoxes(InternalUserInfos internalUserInfos)
        {
            EfCaTransportBoxResp response = null;
            response = _mongoDBService.ReadAllTransportBoxes(internalUserInfos);

            return response;
        }

        internal EfCaTransportBoxResp PutTransportBox(InternalUserInfos internalUserInfos, EfCaTransportBox transportBox)
        {
            EfCaTransportBoxResp response = null;
            response = _mongoDBService.UpdateTransportBox(internalUserInfos, transportBox);

            return response;
        }

        internal EfCaTransportBoxResp DeleteTransportBoxes(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaTransportBoxResp response = null;
            response = _mongoDBService.DeleteTransportBoxes(internalUserInfos, ids);
            if (!response.Error) {
                var fziResponse = _fziService.DeleteTransportBoxStatus(internalUserInfos, ids[0]);
            }

            return response;
        }

        internal EfCaTransportBoxResp DeleteTransportBoxTypes(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaTransportBoxResp response = null;
            response = _mongoDBService.DeleteTransportBoxTypes(internalUserInfos, ids);

            return response;
        }
        #region TransportBoxTypes
        public EfCaTransportBoxResp AddTransportBoxTypes(InternalUserInfos internalUserInfos, List<EfCaTransportBoxType> transportBoxTypes)
        {
            EfCaTransportBoxResp response = null;
            foreach (EfCaTransportBoxType item in transportBoxTypes)
            {
                item.Ident = Guid.NewGuid().ToString();
            }
            response = _mongoDBService.AddTransportBoxTypes(internalUserInfos, transportBoxTypes);

            return response;
        }

        public EfCaTransportBoxResp FindTransportBoxTypesByFinder(InternalUserInfos internalUserInfos, EfCaTransportBoxType finder)
        {
            EfCaTransportBoxResp response = null;
            response = _mongoDBService.FindTransportBoxTypesByFinder(internalUserInfos, finder, null);

            return response;
        }
        #endregion
    }
}
