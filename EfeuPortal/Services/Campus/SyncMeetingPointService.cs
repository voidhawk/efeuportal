﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.SyncMeetingPoint;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class SyncMeetingPointService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SyncMeetingPointService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;

        public SyncMeetingPointService(IConfiguration config, SystemService systemService)
        {
            _mongoDBService = new MongoDBSvc(config);
            _systemService = systemService;
            //_rabbitMQService = new RabbitMQService(config);
        }

        internal EfCaSyncMeetingPointResp AddSyncMeetingPoints(InternalUserInfos internalUserInfos, List<EfCaSyncMeetingPoint> syncMeetingPoints)
        {
            foreach (EfCaSyncMeetingPoint item in syncMeetingPoints)
            {
                item.Ident = Guid.NewGuid().ToString();
            }
            EfCaSyncMeetingPointResp response = null;
            response = _mongoDBService.AddSyncMeetingPoints(internalUserInfos, syncMeetingPoints);
            if (response.Error == false)
            {
                #region https://lsogit.fzi.de/efeu/efeuportal/-/issues/74
                /*
                * https://lsogit.fzi.de/efeu/efeuportal/-/issues/74
                */
                EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
                if (roWebAPIUrl == null)
                {
                    response = new EfCaSyncMeetingPointResp();
                    response.Error = true;
                    response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                    return response;
                }
                EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
                if (roTaskField == null)
                {
                    response = new EfCaSyncMeetingPointResp();
                    response.Error = true;
                    response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                    return response;
                }
                try
                {
                    List<Location> locationsList = ExportROWebAPILocations(response.SyncMeetingPoints, "NEW", roTaskField.StringValue);
                    LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
                    var locationResult = locationsApi.LocationsPostAsync(locationsList);
                }
                catch(Exception ex)
                {
                    response.AddDetailInfos(new DetailInfo() { Ident = "ExportROWebAPILocations", Detail = ex.Message});
                }
                #endregion
            }
            return response;
        }

        internal EfCaSyncMeetingPointResp FindSyncMeetingPointsByFinder(InternalUserInfos internalUserInfos, EfCaSyncMeetingPoint finder)
        {
            EfCaSyncMeetingPointResp response = null;
            response = _mongoDBService.FindSyncMeetingPointsByFinder(internalUserInfos, finder);

            return response;
        }

        internal EfCaSyncMeetingPointResp GetAllSyncMeetingPoints(InternalUserInfos internalUserInfos)
        {
            EfCaSyncMeetingPointResp response = null;
            response = _mongoDBService.ReadAllSyncMeetingPoints(internalUserInfos);

            return response;
        }

        internal EfCaSyncMeetingPointResp PutSyncMeetingPoint(InternalUserInfos internalUserInfos, EfCaSyncMeetingPoint syncMeetingPoint)
        {
            EfCaSyncMeetingPointResp response = null;
            response = _mongoDBService.UpdateSyncMeetingPoint(internalUserInfos, syncMeetingPoint);

            return response;
        }

        internal EfCaSyncMeetingPointResp DeleteSyncMeetingPoints(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaSyncMeetingPointResp response = null;
            response = _mongoDBService.DeleteSyncMeetingPoints(internalUserInfos, ids);

            return response;
        }

        private List<Location> ExportROWebAPILocations(List<EfCaSyncMeetingPoint> syncMeeetingPoints, string actionCode, string taskFields)
        {
            List<Location> locationsList = new List<Location>();

            Location.ActionCodeEnum action;
            if (actionCode == "NEW")
            {
                action = Location.ActionCodeEnum.NEW;
            }
            else if (actionCode == "UPDATE")
            {
                action = Location.ActionCodeEnum.UPDATE;
            }
            else if (actionCode == "DELETE")
            {
                action = Location.ActionCodeEnum.DELETE;
            }
            else
            {
                return null;
            }


            foreach (EfCaSyncMeetingPoint item in syncMeeetingPoints)
            {
                Location location = new Location(null, null, null, null, item.Ident, DateTime.Now, action, null);
                if (action == Location.ActionCodeEnum.DELETE)
                {

                }
                if (action == Location.ActionCodeEnum.NEW || action == Location.ActionCodeEnum.UPDATE)
                {
                    location.LocationHeader = new LocationHeader();
                    LocationHeader.FunctionEnum function = LocationHeader.FunctionEnum.CUSTOMER;

                    location.LocationHeader.Function = function;
                    location.LocationHeader.Taskfields = taskFields;
                    location.LocationHeader.Country = "DEU";
                    location.LocationHeader.ShortDescription = "SyncMeetingPoint, name=" + item.Name;
                    location.LocationHeader.HandlingTimeClassPickup = 0;
                    location.LocationHeader.HandlingTimeClassDelivery = 0;
                    location.LocationHeader.Description = item.Info;

                    location.LocationHeader.CoordinateType = LocationHeader.CoordinateTypeEnum.GEODECIMAL;
                    location.LocationHeader.Latitude = item.Latitude;
                    location.LocationHeader.Longitude = item.Longitude;
                }
                locationsList.Add(location);
            }

            return locationsList;
        }

    }
}
