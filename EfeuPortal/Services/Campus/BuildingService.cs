﻿using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class BuildingService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(BuildingService));

        private MongoDBSvc _mongoDBService;

        //private RabbitMQService _rabbitMQService;

        public BuildingService(IConfiguration config)
        {
            _mongoDBService = new MongoDBSvc(config);
            //_rabbitMQService = new RabbitMQService(config);
        }

        public EfCaBuildingResp AddBuildings(InternalUserInfos internalUserInfos, List<EfCaBuilding> buildings)
        {
            EfCaBuildingResp response = null;
            foreach(EfCaBuilding item in buildings)
            {
                item.Ident = Guid.NewGuid().ToString();
            }
            response = _mongoDBService.AddBuildings(internalUserInfos, buildings);

            return response;
        }

        public EfCaBuildingResp FindBuildingsByFinder(InternalUserInfos internalUserInfos, EfCaBuilding finder)
        {
            EfCaBuildingResp response = null;
            response = _mongoDBService.FindBuildingsByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaBuildingResp GetAllBuildings(InternalUserInfos internalUserInfos)
        {
            EfCaBuildingResp response = null;
            response = _mongoDBService.ReadAllBuildings(internalUserInfos);

            return response;
        }

        public EfCaBuildingResp PutBuilding(InternalUserInfos internalUserInfos, EfCaBuilding building)
        {
            EfCaBuildingResp response = null;
            response = _mongoDBService.UpdateBuilding(internalUserInfos, building);

            return response;
        }

        /// <summary>
        /// Wir aktualisieren nicht alle Parameter!!!!
        /// 
        /// z.B. Wird der AdressIdent nicht geändert
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="building"></param>
        /// <returns></returns>
        public EfCaBuildingResp PatchBuilding(InternalUserInfos internalUserInfos, EfCaBuilding building)
        {
            EfCaBuildingResp response = null;
            EfCaBuilding finder = new EfCaBuilding() { Ident = building.Ident};
            EfCaBuildingResp finderResp = _mongoDBService.FindBuildingsByFinder(internalUserInfos, finder);
            if(finderResp.Error == true)
            {
                return finderResp;
            }
            if (finderResp.Buildings == null || finderResp.Buildings.Count != 1 || finderResp.Buildings[0].Version != building.Version)
            {
                finderResp.Error = true;
                finderResp.ErrorMsg = "Searched data not valid or outdated";
                return finderResp;
            }
            EfCaBuilding updateBuilding = finderResp.Buildings[0];

            if (building.Type != null)
            {
                updateBuilding.Type= building.Type;
            }
            if (building.SyncMeetingPointIds != null)
            {
                updateBuilding.SyncMeetingPointIds = building.SyncMeetingPointIds;
            }
            if (building.ApartmentIds != null)
            {
                updateBuilding.ApartmentIds = building.ApartmentIds;
            }
            if (building.ContactIds != null)
            {
                updateBuilding.ContactIds = building.ContactIds;
            }
            if (building.BoxMountingDeviceIds != null)
            {
                updateBuilding.BoxMountingDeviceIds = building.BoxMountingDeviceIds;
            }
            if (building.ChargingStationIds != null)
            {
                updateBuilding.ChargingStationIds = building.ChargingStationIds;
            }

            response = _mongoDBService.UpdateBuilding(internalUserInfos, building);

            return response;
        }

        public EfCaBuildingResp DeleteBuilding(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaBuildingResp response = null;
            response = _mongoDBService.DeleteBuilding(internalUserInfos, ids);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="action">ADD or REMOVE</param>
        /// <param name="type">BoxMountingDevice, Apartment, Contact</param>
        /// <param name="ident">Id of the type object</param>
        /// <param name="building"></param>
        /// <returns></returns>
        public EfCaBuildingResp PutModifyBuildingReferences(InternalUserInfos internalUserInfos, string action, string type, string ident, EfCaBuilding building)
        {
            EfCaBuildingResp response = null;

            // JSt: ToDo: Parameter check: action, type, ident
            EfCaBuilding finder = new EfCaBuilding() { Ident = building.Ident};
            response = _mongoDBService.FindBuildingsByFinder(internalUserInfos, finder);
            if (response.Error == true || response.Buildings == null || response.Buildings.Count != 1)
            {
                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = "Building not valid.";
                return response;
            }


            //if (buildingDB.CurrentVersion != building.Version)
            //{
            //    response = new EfCaBuildingResp();
            //    response.Error = true;
            //    response.ErrorMsg = "Version outdated.";
            //    return response;
            //}

            if (type.Equals("BoxMountingDevice"))
            {
                response = ModifyBoxMountingDeviceReference(internalUserInfos, action, ident, response.Buildings[0]);
            }
            else if (type.Equals("SyncMeetingPoint"))
            {
                response = ModifySyncMeetingPointReference(internalUserInfos, action, ident, response.Buildings[0]);
            }
            //else if (type.Equals("Apartment"))
            //{
            //    response = ModifyApartmentReference(tenantId, action, ident, buildingDB);
            //}
            //else if (type.Equals("Contact"))
            //{
            //    response = ModifyContactReference(tenantId, action, ident, buildingDB);
            //}
            else
            {
                response = new EfCaBuildingResp();
                response.Error = true;
                response.ErrorMsg = $"Type({type}) not supported";
                return response;
            }
            return response;
        }

        private EfCaBuildingResp ModifyBoxMountingDeviceReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaBuilding efCaBuilding)
        {
            EfCaBuildingResp response = null;


            if (action.Equals("ADD"))
            {
                if (efCaBuilding.BoxMountingDeviceIds == null)
                {
                    efCaBuilding.BoxMountingDeviceIds = new List<string>();
                }
                if (efCaBuilding.BoxMountingDeviceIds.Contains(ident))
                {
                    response = new EfCaBuildingResp();
                    response.Error = true;
                    response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is already assigned.";
                    return response;
                }
                efCaBuilding.BoxMountingDeviceIds.Add(ident);
            }
            else if (action.Equals("REMOVE"))
            {
                if (efCaBuilding.BoxMountingDeviceIds != null && !efCaBuilding.BoxMountingDeviceIds.Contains(ident))
                {
                    response = new EfCaBuildingResp();
                    response.Error = true;
                    response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is not available.";
                    return response;
                }
                efCaBuilding.BoxMountingDeviceIds.Remove(ident);
            }
            else
            {
                response = new EfCaBuildingResp() { Error = true, ErrorMsg = $"Action({action}) not supported"};
                return response;
            }
            response = _mongoDBService.UpdateBuilding(internalUserInfos, efCaBuilding);

            return response;
        }

        private EfCaBuildingResp ModifySyncMeetingPointReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaBuilding efCaBuilding)
        {
            EfCaBuildingResp response = null;


            if (action.Equals("ADD"))
            {
                if (efCaBuilding.SyncMeetingPointIds == null)
                {
                    efCaBuilding.SyncMeetingPointIds = new List<string>();
                }
                if (efCaBuilding.SyncMeetingPointIds.Contains(ident))
                {
                    response = new EfCaBuildingResp();
                    response.Error = true;
                    response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is already assigned.";
                    return response;
                }
                efCaBuilding.SyncMeetingPointIds.Add(ident);
            }
            else if (action.Equals("REMOVE"))
            {
                if (efCaBuilding.SyncMeetingPointIds != null && !efCaBuilding.SyncMeetingPointIds.Contains(ident))
                {
                    response = new EfCaBuildingResp();
                    response.Error = true;
                    response.ErrorMsg = $"BoxMountingDevice.Id({ident}) is not available.";
                    return response;
                }
                efCaBuilding.SyncMeetingPointIds.Remove(ident);
            }
            else
            {
                response = new EfCaBuildingResp() { Error = true, ErrorMsg = $"Action({action}) not supported" };
                return response;
            }
            response = _mongoDBService.UpdateBuilding(internalUserInfos, efCaBuilding);

            return response;
        }

        private EfCaBuildingResp ModifyApartmentReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaBuilding buildingDB)
        {
            EfCaBuildingResp response = null;
            response = new EfCaBuildingResp();
            response.Error = true;
            response.ErrorMsg = "ModifyApartmentReference(..) not implemented.";

            //EfCaApartmentDB apartmentDB = _mongoDBService.FindEfeuApartment(tenantId, ident);
            //if (apartmentDB == null)
            //{
            //    response.Error = true;
            //    response.ErrorMsg = $"Apartment.Id({ident}) is not valid.";
            //    return response;
            //}

            //if (action.Equals("ADD"))
            //{
            //    if (buildingDB.ApartmentIds == null)
            //    {
            //        buildingDB.ApartmentIds = new List<string>();
            //    }
            //    if (buildingDB.ApartmentIds.Contains(ident))
            //    {
            //        response = new EfCaBuildingResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"Apartment.Id({ident}) is already assigned.";
            //        return response;
            //    }
            //    buildingDB.ApartmentIds.Add(ident);
            //}
            //else if (action.Equals("REMOVE"))
            //{
            //    if (buildingDB.ApartmentIds != null && !buildingDB.ApartmentIds.Contains(ident))
            //    {
            //        response = new EfCaBuildingResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"Apartment.Id({ident}) is not available.";
            //        return response;
            //    }
            //    buildingDB.ApartmentIds.Remove(ident);
            //}

            //EfCaBuilding building = new EfCaBuilding(buildingDB);
            //response = _mongoDBService.UpdateEfeuBuilding(tenantId, building);
            return response;
        }

        private EfCaBuildingResp ModifyContactReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaBuilding buildingDB)
        {
            EfCaBuildingResp response = null;
            response = new EfCaBuildingResp();
            response.Error = true;
            response.ErrorMsg = "ModifyContactReference(..) not implemented.";

            //EfCaContactDB contactDB = _mongoDBService.FindEfeuContact(tenantId, ident);
            //if (contactDB == null)
            //{
            //    response.Error = true;
            //    response.ErrorMsg = $"Contact.Id({ident}) is not valid.";
            //    return response;
            //}

            //if (action.Equals("ADD"))
            //{
            //    if (buildingDB.ContactIds == null)
            //    {
            //        buildingDB.ContactIds = new List<string>();
            //    }
            //    if (buildingDB.ContactIds.Contains(ident))
            //    {
            //        response = new EfCaBuildingResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"Contact.Id({ident}) is already assigned.";
            //        return response;
            //    }
            //    buildingDB.ContactIds.Add(ident);
            //}
            //else if (action.Equals("REMOVE"))
            //{
            //    if (buildingDB.ContactIds != null && !buildingDB.ContactIds.Contains(ident))
            //    {
            //        response = new EfCaBuildingResp();
            //        response.Error = true;
            //        response.ErrorMsg = $"Contact.Id({ident}) is not available.";
            //        return response;
            //    }
            //    buildingDB.ContactIds.Remove(ident);
            //}

            //EfCaBuilding building = new EfCaBuilding(buildingDB);
            //response = _mongoDBService.UpdateEfeuBuilding(tenantId, building);
            return response;
        }

        public EfCaBuildingResp FindAllEfeuBuildingsByRef(InternalUserInfos internalUserInfos, string type, string ident)
        {
            EfCaBuildingResp response = null;
            response = _mongoDBService.FindBuildingsByRef(internalUserInfos, type, ident);

            return response;
        }
    }
}
