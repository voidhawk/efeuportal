﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus;
using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Campus.TourData;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.Campus.XServer2;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PTVGROUP.xServer2;
using PTVGROUP.xServer2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class XServer2Service
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(XServer2Service));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private TourService _tourService;
        private OrderService _orderService;
        private TransportBoxService _transportBoxService;
        private VehicleService _vehicleService;

        //private RabbitMQService _rabbitMQService;

        public XServer2Service(IConfiguration config, SystemService systemService, TourService tourService, OrderService orderService,
            TransportBoxService transportBoxService, VehicleService vehicleService)
        {
            _mongoDBService = new MongoDBSvc(config);
            _systemService = systemService;
            _tourService = tourService;
            _orderService = orderService;
            _transportBoxService = transportBoxService;
            _vehicleService = vehicleService;
        }

        #region location
        public async Task<EfCaXServerResp> SearchByAddressRequestAsync(InternalUserInfos internalUserInfos, EfCaAddress searchByAddress)
        {
            EfCaConfig xServer2Config = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-xServer2");
            if (xServer2Config == null)
            {
                EfCaXServerResp xServer2ConfigResponse = new EfCaXServerResp();
                xServer2ConfigResponse.Error = true;
                xServer2ConfigResponse.ErrorMsg = "EfCaConfig(key==PTV-xServer2) is not valid";
                return xServer2ConfigResponse;
            }

            XServer xServer = new XServer(new Uri(xServer2Config.XServerConfig.Url), new HttpClientHandler
            {
                Credentials = new NetworkCredential
                {
                    Password = xServer2Config.XServerConfig.Token,
                    UserName = xServer2Config.XServerConfig.User
                }
            });

            EfCaXServerResp response = new EfCaXServerResp();
            EfCaXServer2SearchAddress searchLocationsRequest = new EfCaXServer2SearchAddress(searchByAddress);
            if (searchLocationsRequest.SearchByAddressRequest != null)
            {
                try
                {
                    LocationsResponse locationsResponse = await xServer.SearchLocationsAsync(searchLocationsRequest.SearchByAddressRequest);
                    response.LocationsResponse = locationsResponse;
                }
                catch(Exception e)
                {
                    log.Error($"SearchByAddressRequestAsync(..), failed with Exception({e.Message})");

                    response.Error = true;
                    response.ErrorMsg = e.Message;
                }
            }
            else
            {
                string errorMsg = $"SearchByAddressRequestAsync(..), creation of the xServer2 request SearchByAddressRequest failed.";
                log.Error(errorMsg);
                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }

        public async Task<EfCaXServerResp> SearchByTextRequestAsync(InternalUserInfos internalUserInfos, string addressAsText)
        {
            EfCaConfig xServer2Config = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-xServer2");
            if (xServer2Config == null)
            {
                EfCaXServerResp xServer2ConfigResponse = new EfCaXServerResp();
                xServer2ConfigResponse.Error = true;
                xServer2ConfigResponse.ErrorMsg = "EfCaConfig(key==PTV-xServer2) is not valid";
                return xServer2ConfigResponse;
            }

            XServer xServer = new XServer(new Uri(xServer2Config.XServerConfig.Url), new HttpClientHandler
            {
                Credentials = new NetworkCredential
                {
                    Password = xServer2Config.XServerConfig.Token,
                    UserName = xServer2Config.XServerConfig.User
                }
            });

            EfCaXServerResp response = new EfCaXServerResp();
            EfCaXServer2SearchAddress searchLocationsRequest = new EfCaXServer2SearchAddress(addressAsText);
            if (searchLocationsRequest.SearchByTextRequest != null)
            {
                try
                {
                    LocationsResponse locationsResponse = await xServer.SearchLocationsAsync(searchLocationsRequest.SearchByTextRequest);
                    response.LocationsResponse = locationsResponse;
                }
                catch (Exception e)
                {
                    log.Error($"SearchByTextRequestAsync(..), failed with Exception({e.Message})");

                    response.Error = true;
                    response.ErrorMsg = e.Message;
                }
            }
            else
            {
                string errorMsg = $"SearchByTextRequestAsync(..), creation of the xServer2 request SearchByTextRequest failed.";
                log.Error(errorMsg);
                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }

        public async Task<EfCaXServerResp> SearchByPositionRequestAsync(InternalUserInfos internalUserInfos, EfCaCoordinate position)
        {
            EfCaConfig xServer2Config = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-xServer2");
            if (xServer2Config == null)
            {
                EfCaXServerResp xServer2ConfigResponse = new EfCaXServerResp();
                xServer2ConfigResponse.Error = true;
                xServer2ConfigResponse.ErrorMsg = "EfCaConfig(key==PTV-xServer2) is not valid";
                return xServer2ConfigResponse;
            }
            //SearchByPositionRequest searchByPositionRequest = new SearchByPositionRequest();
            //searchByPositionRequest.coordinate = new Coordinate();
            //searchByPositionRequest.coordinate.x = 6.130806;
            //searchByPositionRequest.coordinate.y = 49.61055;

            XServer xServer = new XServer(new Uri(xServer2Config.XServerConfig.Url), new HttpClientHandler
            {
                Credentials = new NetworkCredential
                {
                    Password = xServer2Config.XServerConfig.Token,
                    UserName = xServer2Config.XServerConfig.User
                }
            });

            EfCaXServerResp response = new EfCaXServerResp();
            EfCaXServer2SearchAddress searchLocationsRequest = new EfCaXServer2SearchAddress(position);
            if (searchLocationsRequest.SearchByPositionRequest != null)
            {
                try
                {
                    LocationsResponse locationsResponse = await xServer.SearchLocationsAsync(searchLocationsRequest.SearchByPositionRequest);
                    response.LocationsResponse = locationsResponse;
                }
                catch (Exception e)
                {
                    log.Error($"SearchByTextRequestAsync(..), failed with Exception({e.Message})");

                    response.Error = true;
                    response.ErrorMsg = e.Message;
                }
            }
            else
            {
                string errorMsg = $"SearchByTextRequestAsync(..), creation of the xServer2 request SearchByPositionRequest failed.";
                log.Error(errorMsg);
                response.Error = true;
                response.ErrorMsg = errorMsg;
            }

            return response;
        }
        #endregion

        #region xRoute
        public async Task<EfCaXServerResp> CalculateRoute(InternalUserInfos internalUserInfos, EfCaRouteCalculation routeCalculation)
        {
            EfCaConfig xServer2Config = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-xServer2");
            if (xServer2Config == null)
            {
                EfCaXServerResp response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = "CalculateRoute(..), EfCaConfig(key==PTV-xServer2) is not valid";
                return response;
            }

            XServer xServer = new XServer(new Uri(xServer2Config.XServerConfig.Url), new HttpClientHandler
            {
                Credentials = new NetworkCredential
                {
                    Password = xServer2Config.XServerConfig.Token,
                    UserName = xServer2Config.XServerConfig.User
                }
            });

            EfCaPlannedTrip finder = new EfCaPlannedTrip();
            finder.Ident = routeCalculation.TourIdent;
            finder.TourExtId = routeCalculation.TourExtId;
            EfCaTourResp vehicleTourResp = _tourService.GetVehicleTour(internalUserInfos, finder);
            if(vehicleTourResp.Error == true)
            {
                EfCaXServerResp response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = $"CalculateRoute(..), Unable to create trip for Tour.ExtId({finder.TourExtId})";
                return response;
            }

            EfCaRouteRequest efCaRouteRequest = new EfCaRouteRequest(vehicleTourResp.VehicleTrips[0], routeCalculation.ResultFields);
            RouteRequest routeRequest = efCaRouteRequest.RouteRequest;
            string routerResponseAsJSON = JsonConvert.SerializeObject(routeRequest, Formatting.Indented);
            log.Debug($"CalculateRoute(..), EfCaXServerResp({routerResponseAsJSON})");

            RouteResponse res = await xServer.CalculateRouteAsync(routeRequest);

            EfCaXServerResp efCaXServerResp = new EfCaXServerResp();
            efCaXServerResp.RouteResponse = res;

            return efCaXServerResp;
        }

        public async Task<EfCaXServerResp> CalculateRouteStartEnd(InternalUserInfos internalUserInfos, EfCaRouteCalculationStartEnd routeCalculation)
        {
            EfCaConfig xServer2Config = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-xServer2");
            if (xServer2Config == null)
            {
                EfCaXServerResp response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = "CalculateRoute(..), EfCaConfig(key==PTV-xServer2) is not valid";
                return response;
            }

            XServer xServer = new XServer(new Uri(xServer2Config.XServerConfig.Url), new HttpClientHandler
            {
                Credentials = new NetworkCredential
                {
                    Password = xServer2Config.XServerConfig.Token,
                    UserName = xServer2Config.XServerConfig.User
                }
            });

            String profile = "sew-robo";
            //String profile = "pedestrian";
            if (routeCalculation.VehicleIdent != null && routeCalculation.VehicleIdent.Length > 0) 
            {
                EfCaVehicle finder = new EfCaVehicle();
                finder.Ident = routeCalculation.VehicleIdent;
                EfCaVehicleResp vehicleResp = _vehicleService.FindVehiclesByFinder(internalUserInfos, finder);
                if(vehicleResp.Error == true)
                {
                    EfCaXServerResp response = new EfCaXServerResp();
                    response.Error = true;
                    response.ErrorMsg = $"CalculateRoute(..), Unable to find vehicle for Vehicle.Ident({finder.Ident})";
                    return response;
                }
                profile = vehicleResp.Vehicles[0].VehicleProfile;
            }

            EfCaRouteRequest efCaRouteRequest = new EfCaRouteRequest(routeCalculation, profile);
            RouteRequest routeRequest = efCaRouteRequest.RouteRequest;
            string routerResponseAsJSON = JsonConvert.SerializeObject(routeRequest, Formatting.Indented);
            log.Debug($"CalculateRoute(..), EfCaXServerResp({routerResponseAsJSON})");

            RouteResponse res = await xServer.CalculateRouteAsync(routeRequest);

            EfCaXServerResp efCaXServerResp = new EfCaXServerResp();
            efCaXServerResp.RouteResponse = res;

            return efCaXServerResp;
        }

        #endregion

        #region xLoad
        public async Task<EfCaXServerResp> PackBins(InternalUserInfos internalUserInfos, EfCaModelCollector packBins)
        {
            EfCaConfig xServer2Config = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-xServer2");
            if (xServer2Config == null)
            {
                EfCaXServerResp response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = "PackBins(..), EfCaConfig(key==PTV-xServer2) is not valid";
                return response;
            }

            XServer xServer = new XServer(new Uri(xServer2Config.XServerConfig.Url), new HttpClientHandler
            {
                Credentials = new NetworkCredential
                {
                    Password = xServer2Config.XServerConfig.Token,
                    UserName = xServer2Config.XServerConfig.User
                }
            });
            List<string> orderIds = packBins.Idents;
            EfCaOrderResp efCaOrderResp = _orderService.FindOrdersByFinder(internalUserInfos, null, orderIds);
            if (efCaOrderResp.Error == true)
            {
                EfCaXServerResp response = new EfCaXServerResp();
                response.Error = true;
                response.ErrorMsg = $"PackBins(..), query the orders failed, {efCaOrderResp.ErrorMsg}";
                return response;
            }

            List<EfCaTransportBox> transportBoxes = packBins.TransportBoxes;
            List<string> distinctBoxTypeIds = transportBoxes.Select(p => p.TransportBoxTypeId).Distinct().ToList();

            EfCaTransportBoxResp efCaTransportBoxResp = _mongoDBService.FindTransportBoxTypesByFinder(internalUserInfos, null, distinctBoxTypeIds);
            List<BinType> binTypes = CreateBins(efCaTransportBoxResp.TransportBoxTypes);
            PackBinsRequest packBinsRequest = new PackBinsRequest();
            packBinsRequest.Bins = binTypes;

            List<ItemType> itemTypes = CreateItems(efCaOrderResp.Orders);
            packBinsRequest.Items = itemTypes;
            EfCaXServerResp efCaXServerResp = new EfCaXServerResp();
            try
            {
                PackedBinsResponse result = await xServer.PackBinsAsync(packBinsRequest);
                //string routerResponseAsJSON = JsonConvert.SerializeObject(routeRequest, Formatting.Indented);
                //log.Debug($"CalculateRoute(..), EfCaXServerResp({routerResponseAsJSON})");
                efCaXServerResp.PackedBinsResponse = result;
            }
            catch (Exception ex)
            {
                string modelCollectorAsJSON = JsonConvert.SerializeObject(packBins, Formatting.Indented);
                log.Error($"PackBins({internalUserInfos.TenantId}), EfCaModelCollector({modelCollectorAsJSON})");

                string packBinsRequestAsJSON = JsonConvert.SerializeObject(packBinsRequest, Formatting.Indented);
                log.Error($"PackBins({internalUserInfos.TenantId}), PackBinsRequest({packBinsRequestAsJSON})");

                efCaXServerResp.Error = true;
                efCaXServerResp.ErrorMsg = ex.Message;
            }

            return efCaXServerResp;
        }

        private List<BinType> CreateBins(List<EfCaTransportBoxType> transportBoxTypes)
        {
            List<BinType> bins = new List<BinType>();
            foreach(EfCaTransportBoxType item in transportBoxTypes)
            {
                BinType binType = new BinType();
                binType.Id = item.Ident;
                binType.NumberOfBins = 10;
                binType.Dimensions = new BoxDimensions();
                binType.Dimensions.X = item.Lenght * 10;
                binType.Dimensions.Y = item.Width * 10;
                binType.Dimensions.Z = item.Height * 10;
                binType.MaximumVolumeCapacity = item.Volume * 1000;
                binType.MaximumWeightCapacity = item.Weight/1000;

                bins.Add(binType);
            }

            return bins;
        }

        private List<ItemType> CreateItems(List<EfCaOrder> orders)
        {
            List<ItemType> itemTypes = new List<ItemType>();
            foreach(EfCaOrder item in orders)
            {
                ItemType itemType = new ItemType();
                itemType.Id = item.Ident;
                itemType.NumberOfItems = 1;
                itemType.Dimensions = new BoxDimensions();
                itemType.Dimensions.X = item.Quantities.Length * 10;
                itemType.Dimensions.Y = item.Quantities.Width * 10;
                itemType.Dimensions.Z = item.Quantities.Height * 10;
                itemType.Weight = item.Quantities.Weight/1000;
                //itemType.MaximumSurfaceLoads;
                //itemType.AllowedOrientations;

                itemTypes.Add(itemType);
            }

            return itemTypes;
        }
        #endregion
    }
}
