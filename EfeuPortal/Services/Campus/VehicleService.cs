﻿using EfeuPortal.Helpers;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class VehicleService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(VehicleService));
        private readonly AppSettings _appSettings;

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private FZIStatusService _fziService;

        //private RabbitMQService _rabbitMQService;

        public VehicleService(IConfiguration config, IOptions<AppSettings> appSettings, SystemService systemService, FZIStatusService fziService)
        {
            _mongoDBService = new MongoDBSvc(config);
            //_rabbitMQService = new RabbitMQService(config);
            _systemService = systemService;

            _appSettings = appSettings.Value;
            _fziService = fziService;
        }

        public EfCaVehicleResp AddVehicles(InternalUserInfos internalUserInfos, List<EfCaVehicle> vehicles)
        {
            EfCaVehicleResp response = null;

            List<SmartourQuality> smartourQualities = _systemService.GetTenantUnspecificData(internalUserInfos, "SmartourQualities");
            List<string> allAvailableSmartourQualities = smartourQualities.Select(qual => qual.Name).ToList();

            EfCaVehicle finder = new EfCaVehicle();
            EfCaVehicleResp vehicleResp = _mongoDBService.FindVehiclesByFinder(internalUserInfos, finder);
            foreach (EfCaVehicle item in vehicles)
            {
                item.Ident = Guid.NewGuid().ToString();

                if (allAvailableSmartourQualities != null && allAvailableSmartourQualities.Count > 0)
                {
                    if (vehicleResp.Error == false && vehicleResp.Vehicles != null && vehicleResp.Vehicles.Count > 0)
                    {
                        List<string> usedVehQualities = vehicleResp.Vehicles.Where(vehQual => vehQual.SmartourQualityReference != null).Select(res => res.SmartourQualityReference).ToList();
                        List<string> notUsedQualities = allAvailableSmartourQualities.Except(usedVehQualities).ToList();

                        if (notUsedQualities != null && notUsedQualities.Count > 0)
                        {
                            item.SmartourQualityReference = notUsedQualities[0];
                            allAvailableSmartourQualities.Remove(notUsedQualities[0]);
                        }
                        else
                        {
                            response = new EfCaVehicleResp();
                            response.Error = true;
                            response.ErrorMsg = "Not enough available Smartour quantities";
                            return response;
                        }
                    }
                    else
                    {
                        item.SmartourQualityReference = allAvailableSmartourQualities[0];
                        allAvailableSmartourQualities.RemoveAt(0);
                    }
                }
                else
                {
                    response = new EfCaVehicleResp();
                    response.Error = true;
                    response.ErrorMsg = "Not enough available Smartour quantities";
                    return response;
                }
            }
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            if (roTaskField == null)
            {
                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                return response;
            }

            response = _mongoDBService.AddVehicles(internalUserInfos, vehicles);
            if (!response.Error) {
                var fziResponse = _fziService.AddVehiclesStatus(internalUserInfos, response);

                List<Resource> resourcesList = ExportROWebAPIVehicles(response.Vehicles, "NEW", roTaskField.StringValue, smartourQualities);
                ResourcesApi resourceApi = new ResourcesApi(roWebAPIUrl.Url);
                var locationResult = resourceApi.ResourcesPostAsync(resourcesList);
                /*
                 * JSt: ToDo: das Ergebnis auswerten und zurückliefern. Irgendwann müssen wir mal von true/false weg, da 
                 * das Ergebnis auch tilweise OK sein kann.
                 */
            }

            return response;
        }

        public EfCaVehicleResp FindVehiclesByFinder(InternalUserInfos internalUserInfos, EfCaVehicle finder)
        {
            EfCaVehicleResp response = null;
            response = _mongoDBService.FindVehiclesByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaVehicleResp PutVehicle(InternalUserInfos internalUserInfos, EfCaVehicle vehicle)
        {
            EfCaVehicleResp response = null;
            response = _mongoDBService.UpdateVehicle(internalUserInfos, vehicle);
            
            return response;
        }

        public EfCaVehicleResp GetAllVehicle(InternalUserInfos internalUserInfos)
        {
            EfCaVehicleResp response = null;
            response = _mongoDBService.ReadAllVehicles(internalUserInfos);

            return response;
        }

        public EfCaVehicleResp DeleteVehicle(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaVehicleResp response = null;
            response = _mongoDBService.DeleteVehicles(internalUserInfos, ids);
            if (!response.Error) {
                var fziResponse = _fziService.DeleteVehicleStatus(internalUserInfos, ids[0]);
            }
            if (response.Error == true)
            {
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            if (roTaskField == null)
            {
                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                return response;
            }

            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response = new EfCaVehicleResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }

            List<EfCaVehicle> vehicles = new List<EfCaVehicle>();
            foreach (string id in ids)
            {
                EfCaVehicle efCaVehicle = new EfCaVehicle();
                efCaVehicle.Ident = id;
                vehicles.Add(efCaVehicle);
            }
            List<Resource> resourcesList = ExportROWebAPIVehicles(vehicles, "DELETE", roTaskField.StringValue, null);
            ResourcesApi resourceApi = new ResourcesApi(roWebAPIUrl.Url);
            var locationResult = resourceApi.ResourcesPostAsync(resourcesList);

            return response;
        }

        private List<Resource> ExportROWebAPIVehicles(List<EfCaVehicle> vehicles, string actionCode, string taskFields, List<SmartourQuality> smartourQualities)
        {
            Resource.ActionCodeEnum action;
            if (actionCode == "NEW")
            {
                action = Resource.ActionCodeEnum.NEW;
            }
            else if (actionCode == "UPDATE")
            {
                action = Resource.ActionCodeEnum.UPDATE;
            }
            else if (actionCode == "DELETE")
            {
                action = Resource.ActionCodeEnum.DELETE;
            }
            else
            {
                return null;
            }

            List<Resource> vehiclesList = new List<Resource>();
            foreach (EfCaVehicle item in vehicles)
            {
                Resource resource = new Resource(null, null, item.Ident, DateTime.Now, action, null);
                resource.ResourceHeader = new ResourceHeader();
                resource.ResourceHeader.ResourceKind = ResourceHeader.ResourceKindEnum.VEHICLE;
                resource.ResourceHeader.Category = ResourceHeader.CategoryEnum.TRANSPORTER;
                resource.ResourceHeader.RoutingProfileExtId = "sew-robby";
                resource.ResourceHeader.Taskfields = taskFields;
                resource.ResourceHeader.QuantityWeight = item.MaxWeight;
                resource.ResourceHeader.QuantityVolume = 1;

                // https://lsogit.fzi.de/efeu/efeuportal/-/issues/45
                resource.ResourceHeader.StartDepot = item.StartDepot;
                resource.ResourceHeader.DestinationDepot = item.EndDepot;
                resource.ResourceHeader.Name = item.Name;
                //resource.ResourceHeader.QuantityLoadingMeter = 1;

                if (action == Resource.ActionCodeEnum.NEW)
                {
                    #region ResourceAvailabilities
                    resource.Availabilities = new List<ResourceAvailability>();
                    ResourceAvailability resourceAvailability = new ResourceAvailability();
                    resourceAvailability.FromWeekday = ResourceAvailability.FromWeekdayEnum.MON;
                    resourceAvailability.UntilWeekday = ResourceAvailability.UntilWeekdayEnum.MON;
                    resourceAvailability.From = "0200";
                    resourceAvailability.Until = "2200";
                    resource.Availabilities.Add(resourceAvailability);

                    resourceAvailability = new ResourceAvailability();
                    resourceAvailability.FromWeekday = ResourceAvailability.FromWeekdayEnum.TUE;
                    resourceAvailability.UntilWeekday = ResourceAvailability.UntilWeekdayEnum.TUE;
                    resourceAvailability.From = "0200";
                    resourceAvailability.Until = "2200";
                    resource.Availabilities.Add(resourceAvailability);

                    resourceAvailability = new ResourceAvailability();
                    resourceAvailability.FromWeekday = ResourceAvailability.FromWeekdayEnum.WED;
                    resourceAvailability.UntilWeekday = ResourceAvailability.UntilWeekdayEnum.WED;
                    resourceAvailability.From = "0200";
                    resourceAvailability.Until = "2200";
                    resource.Availabilities.Add(resourceAvailability);

                    resourceAvailability = new ResourceAvailability();
                    resourceAvailability.FromWeekday = ResourceAvailability.FromWeekdayEnum.THU;
                    resourceAvailability.UntilWeekday = ResourceAvailability.UntilWeekdayEnum.THU;
                    resourceAvailability.From = "0200";
                    resourceAvailability.Until = "2200";
                    resource.Availabilities.Add(resourceAvailability);

                    resourceAvailability = new ResourceAvailability();
                    resourceAvailability.FromWeekday = ResourceAvailability.FromWeekdayEnum.FRI;
                    resourceAvailability.UntilWeekday = ResourceAvailability.UntilWeekdayEnum.FRI;
                    resourceAvailability.From = "0200";
                    resourceAvailability.Until = "2200";
                    resource.Availabilities.Add(resourceAvailability);

                    resourceAvailability = new ResourceAvailability();
                    resourceAvailability.FromWeekday = ResourceAvailability.FromWeekdayEnum.SAT;
                    resourceAvailability.UntilWeekday = ResourceAvailability.UntilWeekdayEnum.SAT;
                    resourceAvailability.From = "0200";
                    resourceAvailability.Until = "1800";
                    resource.Availabilities.Add(resourceAvailability);
                    #endregion
                    SmartourQuality smartourQuality = smartourQualities.Find(qual => qual.Name.Equals(item.SmartourQualityReference));
                    resource.ResourceHeader.Qualifications = CreateSmartourQualityReference(smartourQuality.Number);
                }

                vehiclesList.Add(resource);
            }

            return vehiclesList;
        }

        internal string CreateSmartourQualityReference(InternalUserInfos internalUserInfos, string vehicleIdent)
        {
            EfCaVehicle finder = new EfCaVehicle() { Ident= vehicleIdent};
            EfCaVehicleResp vehicleResp = FindVehiclesByFinder(internalUserInfos, finder);
            if(vehicleResp == null || vehicleResp.Vehicles == null || vehicleResp.Vehicles.Count != 1)
            {
                return string.Empty;
            }
            List<SmartourQuality> smartourQualities = _systemService.GetTenantUnspecificData(internalUserInfos, "SmartourQualities");
            List<string> allAvailableSmartourQualities = smartourQualities.Select(qual => qual.Name).ToList();
            SmartourQuality smartourQuality = smartourQualities.Find(qual => qual.Name.Equals(vehicleResp.Vehicles[0].SmartourQualityReference));
            string smartourQualityFormat = CreateSmartourQualityReference(smartourQuality.Number);

            return smartourQualityFormat;
        }

        private string CreateSmartourQualityReference(int number)
        {
            string quality = string.Empty;
            byte[] numberAsBytes = new byte[number];
            string txt = "";
            for (int i = 1; i <= numberAsBytes.Length; i++)
            {
                if (i == number)
                {
                    txt += 1;
                }
                else
                {
                    txt += 0;
                }
            }
            return quality = txt;
        }
    }
}
