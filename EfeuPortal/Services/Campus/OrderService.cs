﻿using EfeuPortal.Models.Campus;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using EfeuPortal.Helpers;
using EfeuPortal.Models.ROPlanning;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using EfeuPortal.Services.FZI;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using static PTVGROUP.ROWebApi.Model.Order;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus.SyncMeetingPoint;

namespace EfeuPortal.Services.Campus
{
    public class OrderService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AddressService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private AddressService _addressService;
        private BuildingService _buildingService;
        private ContactService _contactService;
        private VehicleService _vehicleService;
        private SyncMeetingPointService _syncMeetingPointService;

        private FZILoggingService _FZILoggingService;

        //private RabbitMQService _rabbitMQService;

        public OrderService(IConfiguration config, SystemService systemService, AddressService addressService, BuildingService buildingService,
            ContactService contactService, VehicleService vehicleService, SyncMeetingPointService syncMeetingPointService, FZILoggingService FZILoggingService)
        {
            _mongoDBService = new MongoDBSvc(config);
            //_rabbitMQService = new RabbitMQService(config);
            _systemService = systemService;
            _addressService = addressService;
            _buildingService = buildingService;
            _contactService = contactService;
            _vehicleService = vehicleService;
            _syncMeetingPointService = syncMeetingPointService;
            _FZILoggingService = FZILoggingService;
        }

        #region Order
        public EfCaOrderResp AddOrders(InternalUserInfos internalUserInfos, List<EfCaOrder> items)
        {
            EfCaOrderResp response = null;
            foreach (EfCaOrder item in items)
            {
                if (item.Ident == null)
                {
                    item.Ident = Guid.NewGuid().ToString();
                }
                else
                {
                    EfCaOrder finder = new EfCaOrder();
                    finder.Ident = item.Ident;
                    EfCaOrderResp findResponse = _mongoDBService.FindOrdersByFinder(internalUserInfos, finder, null);
                    if (findResponse.Orders.Count > 0)
                    {
                        string errorMsg = $"AddOrders(..) failed, duplicate order ID";
                        log.Error(errorMsg);
                        response.Error = true;
                        response.ErrorMsg = errorMsg;
                        return response;
                    }
                }
            }
            response = _mongoDBService.AddOrders(internalUserInfos, items);

            foreach (EfCaOrder item in items)
            {
                _FZILoggingService.LogOrder(item);
            }

            return response;
        }

        public EfCaOrderResp FindOrdersByFinder(InternalUserInfos internalUserInfos, EfCaOrder finder, List<string> idents)
        {
            EfCaOrderResp response = null;
            response = _mongoDBService.FindOrdersByFinder(internalUserInfos, finder, idents);

            return response;
        }

        public EfCaOrderResp PutOrder(InternalUserInfos internalUserInfos, EfCaOrder item)
        {
            EfCaOrderResp response = null;
            response = _mongoDBService.UpdateOrder(internalUserInfos, item);
            _FZILoggingService.LogOrder(item);
        

            return response;
        }

        /// <summary>
        /// Liste der ids enthält EfCaOrder.Ident
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public EfCaOrderResp DeleteOrders(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaOrderResp response = null;
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if(roWebAPIUrl == null)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }

            /*
             * Falls es Bilder gibt müssen diese ebenfalls gelöscht werden
             * https://lsogit.fzi.de/efeu/efeuportal/-/issues/50
             */
            EfCaModelCollector deleteBmpsFromOrders = new EfCaModelCollector() { Idents = ids };
            DeleteOrderImages(internalUserInfos, deleteBmpsFromOrders);

            List<string> dummyOrder2Delete = new List<string>();
            EfCaOrderResp orders2DeleteResp =  _mongoDBService.FindOrdersByFinder(internalUserInfos, null, ids);
            if (orders2DeleteResp.Error == false && orders2DeleteResp.Orders.Count > 0)
            {
                foreach(EfCaOrder order2Delete in orders2DeleteResp.Orders)
                {
                    if(order2Delete.SpecialCodes  != null)
                    {
                        foreach(SpecialCode specialCode in order2Delete.SpecialCodes)
                        {
                            if(specialCode.Key.Equals("DUMMY-ORDER-IDENT"))
                            {
                                dummyOrder2Delete.Add(specialCode.Value);
                            }
                        }
                    }
                }
            }

            response = _mongoDBService.DeleteOrders(internalUserInfos, ids);
            OrdersApi ordersApi = new OrdersApi(roWebAPIUrl.Url);
            if(dummyOrder2Delete.Count > 0){

                ids.AddRange(dummyOrder2Delete);
            }
            foreach (String ident in ids) {
                ordersApi.OrdersDelete(ident);
            }

            foreach (string id in ids)
            {
                _FZILoggingService.LogOrderDeleted(id);
            }

            return response;
        }

        /// <summary>
        /// Diese Methode wird im Moment ausschlieslich dazu verwendet um BOX Orders zu erzeugen und zuzuweisen.
        /// 
        /// ToDo: Aktuele wird der Status READY_FOR_PLANNING hardcoded gesetzt, das ist noch falsch, die Logik muss
        /// aus dem ProcessMgmt kommen
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public EfCaOrderResp PostCreateDependentOrders(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaOrderResp response = new EfCaOrderResp();
            EfCaOrderResp responseFoundOrders = _mongoDBService.FindOrdersByFinder(internalUserInfos, null, ids);
            if (responseFoundOrders.Error == false && responseFoundOrders.Orders != null && responseFoundOrders.Orders.Count > 0)
            {
                EfCaOrderResp createOrderResponse = null;
                foreach (EfCaOrder item in responseFoundOrders.Orders)
                {
                    List<string> relatedOrderIds = new List<string>();
                    if (item.MainOrder.GetValueOrDefault(true))
                    {
                        /*
                        INBOUND Anlieferung ans Depot => DELIVERY
                        PICKUP = Am Depot
                        DELIVERY = zum Kunden
                        */
                        //EfCaStorage pickup = null;
                        //EfCaStorage delivery = null;

                        createOrderResponse = CreateOrder(internalUserInfos, item, "DELIVERY", "PACKAGE_BOX", "READY_FOR_PLANNING", false);
                        if (createOrderResponse.Error == true)
                        {
                            return createOrderResponse;
                        }
                        relatedOrderIds.Add(createOrderResponse.Orders[0].Ident);

                        /*
                        INBOUND Anlieferung ans Depot => DELIVERY
                        PICKUP = beim Kunden
                        DELIVERY = zum Depot
                        */
                        createOrderResponse = CreateOrder(internalUserInfos, item, "PICKUP", "PACKAGE_BOX", "READY_FOR_PLANNING", false);
                        if (createOrderResponse.Error == true)
                        {
                            return createOrderResponse;
                        }
                        relatedOrderIds.Add(createOrderResponse.Orders[0].Ident);
                    }
                    else
                    {
                        createOrderResponse = new EfCaOrderResp();
                        createOrderResponse.Error = true;
                        createOrderResponse.ErrorMsg = "Only mainOrders could produce dependent EfCaOrders";
                        return createOrderResponse;
                    }

                    createOrderResponse = UpdateMainOrder(internalUserInfos, item, "IN_PROCESS", relatedOrderIds);
                    if (createOrderResponse.Error == true)
                    {
                        /*
                         * JSt: ToDo: Nach einem Fehler nicht abbrechen, sondern detaillierte Info liefern.
                         */
                        return createOrderResponse;
                    }
                    response.Orders.Add(item);
                }
            }

            return response;
        }

        public EfCaOrderResp ProcessPlanningOrders(InternalUserInfos internalUserInfos, List<string> ids, ImportActionType importAction)
        {
            EfCaOrderResp response = null;
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if(roWebAPIUrl == null)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            if (roTaskField == null)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                return response;
            }

            List<Order> roOrders = new List<Order>();
            response = _mongoDBService.FindOrdersByFinder(internalUserInfos, null, ids);
            if (response.Error == true)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "MongoDB.FindOrdersByFinder(..) failed";
                return response;
            }

            foreach (EfCaOrder efcaOrder in response.Orders)
            {
                OrderHeader orderHeader = CreateROOrderHeader(internalUserInfos, efcaOrder, roTaskField.StringValue);
                if (orderHeader == null)
                {
                    response = new EfCaOrderResp();
                    response.Error = true;
                    response.ErrorMsg = $"Create Smartour Order({efcaOrder.Ident}) failed, nothing exported";
                    return response;
                }
                List<ActionPoint> actionPoints = CreateROActionPoints(internalUserInfos, efcaOrder);

                List<OrderItem> orderItems = new List<OrderItem>();
                OrderItem orderItem = new OrderItem();
                //orderItems.Add(orderItem);
                ActionCodeEnum actionCodeEnum = ActionCodeEnum.UPDATE;
                if (importAction == ImportActionType.NEW)
                {
                    actionCodeEnum = ActionCodeEnum.NEW;
                }
                else if (importAction == ImportActionType.DELETE)
                {
                    actionCodeEnum = ActionCodeEnum.DELETE;
                }
                Order order = new Order(orderHeader, actionPoints, null, null, 0, null, efcaOrder.Ident, DateTime.Now, actionCodeEnum, null);
                roOrders.Add(order);

                Order dummyOrder = CreateDummyOrder(order, efcaOrder);
                if(dummyOrder != null)
                {
                    if(efcaOrder.SpecialCodes == null)
                    {
                        efcaOrder.SpecialCodes = new List<SpecialCode>();
                    }
                    SpecialCode specialCode = new SpecialCode() { Key = "DUMMY-ORDER-IDENT", Value = dummyOrder.ExtId };
                    efcaOrder.SpecialCodes.Add(specialCode);
                    _mongoDBService.UpdateOrder(internalUserInfos, efcaOrder);

                    roOrders.Add(dummyOrder);
                }
                //string orderAsJSON = JsonConvert.SerializeObject(order, Formatting.Indented);
                //string infoMsg = $"ProcessPlanningOrders({internalUserInfos.TenantId}), Order: {orderAsJSON}";
                //log.Debug(infoMsg);
            }

            OrdersApi ordersApi = new OrdersApi(roWebAPIUrl.Url);
            ImportResults result = ordersApi.OrdersPostAsync(roOrders);
            if(result != null && result.ImportDetailedResults != null && result.ImportDetailedResults.Count > 0)
            {
                response.Error = false;
                foreach(EfCaOrder efCaOrder in response.Orders)
                {
                    ImportResult importResultSUCCESS = result.ImportDetailedResults.Find(order => order.Key == efCaOrder.Ident && order.Result == "SUCCESS");
                    if (importResultSUCCESS != null)
                    {
                        efCaOrder.State = "IN_PLANNING";
                        _mongoDBService.UpdateOrder(internalUserInfos, efCaOrder);
                    }
                    ImportResult importResultERROR = result.ImportDetailedResults.Find(order => order.Key == efCaOrder.Ident && order.Result == "ERROR");
                    if (importResultERROR != null)
                    {
                        efCaOrder.State = "EXPORT_FAILED";
                        _mongoDBService.UpdateOrder(internalUserInfos, efCaOrder);
                    }
                }
            }
            
            return response;
        }

        public EfCaOrderResp DeletePlanningOrders(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaOrderResp response = null;
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            if (roTaskField == null)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                return response;
            }

            List<Order> roOrders = new List<Order>();
            response = _mongoDBService.FindOrdersByFinder(internalUserInfos, null, ids);
            if (response.Error == true || response.Orders.Count <= 0)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "MongoDB.FindOrdersByFinder(..) failed";
                return response;
            }

            foreach (EfCaOrder efcaOrder in response.Orders)
            {
                EfCaOrder orderToUpdate = null;
                OrderHeader orderHeader = CreateROOrderHeader(internalUserInfos, efcaOrder, roTaskField.StringValue);
                if (orderHeader != null)
                {
                    Order order = new Order(orderHeader, null, null, null, 0, null, efcaOrder.Ident, DateTime.Now, ActionCodeEnum.DELETE, null);
                    roOrders.Add(order);

                    if (efcaOrder.SpecialCodes != null)
                    {
                        int indexFound = efcaOrder.SpecialCodes.FindIndex(item => item.Key == "DUMMY-ORDER-IDENT");
                        if(indexFound != -1)
                        {
                            Order deleteDummyOrder = new Order(orderHeader, null, null, null, 0, null, efcaOrder.SpecialCodes[indexFound].Value, DateTime.Now, ActionCodeEnum.DELETE, null);
                            roOrders.Add(deleteDummyOrder);
                            orderToUpdate = ExtensionMethods.Clone<EfCaOrder>(efcaOrder);
                            orderToUpdate.SpecialCodes.RemoveAt(indexFound);
                        }

                        //foreach (SpecialCode specialCode in efcaOrder.SpecialCodes)
                        //{
                        //    if (specialCode.Key.Equals("DUMMY-ORDER-IDENT"))
                        //    {
                        //        Order deleteDummyOrder = new Order(orderHeader, null, null, null, 0, null, specialCode.Value, DateTime.Now, ActionCodeEnum.DELETE, null);
                        //        roOrders.Add(deleteDummyOrder);
                        //        orderToUpdate = ExtensionMethods.Clone<EfCaOrder>(efcaOrder);
                        //        orderToUpdate.SpecialCodes.Remove(specialCode);
                        //        orderToUpdate.SpecialCodes.FindIndex(item => item.Key == "DUMMY-ORDER-IDENT");
                        //    }
                        //}
                        if (orderToUpdate != null)
                        {
                            _mongoDBService.UpdateOrder(internalUserInfos, orderToUpdate);
                        }
                    }
                }
                else
                {
                    response = new EfCaOrderResp();
                    response.Error = true;
                    response.ErrorMsg = $"Delete Smartour Order({efcaOrder.Ident}) failed, nothing exported";

                    //return response;
                }
            }

            OrdersApi ordersApi = new OrdersApi(roWebAPIUrl.Url);
            ImportResults result = ordersApi.OrdersPostAsync(roOrders);
            if (result != null && result.ImportDetailedResults != null && result.ImportDetailedResults.Count > 0)
            {
                //response.Error = false;
                //foreach (EfCaOrder efCaOrder in response.Orders)
                //{
                //    ImportResult importResultSUCCESS = result.ImportDetailedResults.Find(order => order.Key == efCaOrder.Ident && order.Result == "SUCCESS");
                //    if (importResultSUCCESS != null)
                //    {
                //        efCaOrder.State = "IN_PLANNING";
                //        _mongoDBService.UpdateOrder(internalUserInfos, efCaOrder);
                //    }
                //    ImportResult importResultERROR = result.ImportDetailedResults.Find(order => order.Key == efCaOrder.Ident && order.Result == "ERROR");
                //    if (importResultERROR != null)
                //    {
                //        efCaOrder.State = "EXPORT_FAILED";
                //        _mongoDBService.UpdateOrder(internalUserInfos, efCaOrder);
                //    }
                //}
            }

            return response;
        }

        private EfCaOrderResp CreateOrder(InternalUserInfos internalUserInfos, EfCaOrder order, string orderType, string orderUnit, string state, bool mainOrder)
        {
            EfCaOrder efCaOrder = new EfCaOrder(order, orderType, orderUnit, state, mainOrder);
            EfCaOrderResp response = _mongoDBService.AddOrders(internalUserInfos, new List<EfCaOrder>() { efCaOrder });

            return response;
        }

        private EfCaOrderResp UpdateMainOrder(InternalUserInfos internalUserInfos, EfCaOrder order, string state, List<string> relatedOrderIds)
        {
            order.RelatedBoxOrderIds = new List<string>();
            order.RelatedBoxOrderIds = relatedOrderIds;
            order.State = state;

            return _mongoDBService.UpdateOrder(internalUserInfos, order);
        }
        #endregion

        #region Order images
        public EfCaOrderResp AddOrderImages(InternalUserInfos internalUserInfos, List<EfCaImageDocumentation> imageDocumentations)
        {
            EfCaOrderResp response = null;
            List<DetailInfo> detailInfos = new List<DetailInfo>();

            List<string> distinctOrderIds = imageDocumentations.Where(item => item.OrderId != null).Select(item => item.OrderId).Distinct().ToList();
            if(distinctOrderIds == null || distinctOrderIds.Count != imageDocumentations.Count)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "NULL value in OrderId not valid";

                return response;
            }

            // JSt: ToDo: Sollte für einen rollback verwendet werden können.
            List<EfCaOrder> orgOrderList = new List<EfCaOrder>();
            foreach (string orderId in distinctOrderIds)
            {
                EfCaOrder finder = new EfCaOrder();
                finder.Ident = orderId;
                EfCaOrderResp findResponse = _mongoDBService.FindOrdersByFinder(internalUserInfos, finder, null);
                if (findResponse.Orders.Count != 1)
                {
                    continue;
                }
                EfCaOrder orderFound = findResponse.Orders[0];
                if (orderFound.ImageIds == null)
                {
                    orderFound.ImageIds = new List<string>();
                }
                orgOrderList.Add(ExtensionMethods.Clone< EfCaOrder>(findResponse.Orders[0]));

                List<EfCaImageDocumentation> selectedImages = imageDocumentations.Where(item => item.OrderId.Equals(orderId)).ToList();
                foreach (EfCaImageDocumentation item in selectedImages)
                {

                    item.Ident = Guid.NewGuid().ToString();
                    orderFound.ImageIds.Add(item.Ident);
                }
                response =_mongoDBService.AddOrderImageDocumentations(internalUserInfos, selectedImages);
                if(response.Error == false)
                {
                    _mongoDBService.UpdateOrder(internalUserInfos, orderFound);
                }
            }
            //response = _mongoDBService.AddOrders(internalUserInfos, items);

            return response;
        }

        public EfCaOrderResp FindOrderImageDocumentations(InternalUserInfos internalUserInfos, EfCaModelCollector finder)
        {
            EfCaOrderResp response = null;
            response = _mongoDBService.FindOrderImageDocumentations(internalUserInfos, finder);

            return response;
        }

        /// <summary>
        /// <para>Wenn die EfCaModelCollector.Idents gesetzt ist, werden diese als EfCaOrder.Ident verwendet.</para>
        /// 
        /// <para>Wenn die Liste der EfCaImageDocumentation gesetzt ist, werden diese in der Collection gelöscht und
        /// in den entsprechenden EfCaOrder(s) die Referenzen entfernt.</para>
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="deleteBmpsFromOrders"></param>
        /// <returns></returns>
        public EfCaOrderResp DeleteOrderImages(InternalUserInfos internalUserInfos, EfCaModelCollector deleteBmpsFromOrders)
        {
            EfCaOrderResp response = null;

            if(deleteBmpsFromOrders == null)
            {
                response = new EfCaOrderResp();
                response.Error = true;
                response.ErrorMsg = "Parameter EfCaModelCollector not valid";

                return response;
            }
            if(deleteBmpsFromOrders.ImageDocumentation != null && deleteBmpsFromOrders.ImageDocumentation.Count > 0)
            {
                List<string> orderIds = deleteBmpsFromOrders.ImageDocumentation.Select(item => item.OrderId).ToList();
                EfCaOrderResp finderResponse = _mongoDBService.FindOrdersByFinder(internalUserInfos, null, orderIds);
                if (finderResponse.Error == false && finderResponse.Orders.Count > 0)
                {
                    foreach (EfCaImageDocumentation imageDocumentation in deleteBmpsFromOrders.ImageDocumentation)
                    {
                        EfCaOrder order = finderResponse.Orders.Where(item => item.Ident.Equals(imageDocumentation.OrderId)).FirstOrDefault();
                        if(order != null)
                        {
                            if (order.ImageIds.Contains(imageDocumentation.Ident))
                            {
                                order.ImageIds.Remove(imageDocumentation.Ident);
                            }

                            _mongoDBService.UpdateOrder(internalUserInfos, order);
                        }
                    }
                }

            }

            response = _mongoDBService.DeleteOrderImageDocumentations(internalUserInfos, deleteBmpsFromOrders);
            return response;
        }

        #endregion

        #region Smartour WebApi converter
        /// <summary>
        /// Konverter für (Box)Aufträge die verplant werden sollen.
        /// <para> Hat NICHTS mit Sammelauftragbildung zu tun!!!!</para>
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="order"></param>
        /// <param name="taskFields"></param>
        /// <returns></returns>
        private OrderHeader CreateROOrderHeader(InternalUserInfos internalUserInfos, EfCaOrder order, string taskFields)
        {
            OrderHeader orderHeader = new OrderHeader();
            orderHeader.Volume = 1;
            orderHeader.Taskfields = taskFields;

            #region lsogit.fzi.de/efeu/efeuportal/-/issues/98
            string planningOrderType = "1"; // PickUP and Delivery
            EfCaConfig planningOrderTypeCfg = _systemService.GetConfigOfTenant(internalUserInfos, "Planning-OrderType");
            if (planningOrderTypeCfg == null)
            {
                planningOrderType = "1";
            }
            else
            {
                planningOrderType = planningOrderTypeCfg.StringValue;
            }
            if(planningOrderType == "1")
            {
                if (order.OrderType == "DELIVERY")
                {
                    orderHeader.OrderType = OrderHeader.OrderTypeEnum.DELIVERY;
                }
                else if (order.OrderType == "PICKUP")
                {
                    orderHeader.OrderType = OrderHeader.OrderTypeEnum.PICKUP;
                }
                else if (order.OrderType == "RECHARGING")
                {
                    orderHeader.OrderType = OrderHeader.OrderTypeEnum.DELIVERY;
                }
                else if (order.OrderType == "TRANSPORT")
                {
                    orderHeader.OrderType = OrderHeader.OrderTypeEnum.TRANSPORT;
                }
            }
            else if (planningOrderType == "2")
            {
                if (order.OrderType == "DELIVERY" || order.OrderType == "PICKUP" || order.OrderType == "TRANSPORT")
                {
                    orderHeader.OrderType = OrderHeader.OrderTypeEnum.TRANSPORT;
                }
                else if (order.OrderType == "RECHARGING")
                {
                    orderHeader.OrderType = OrderHeader.OrderTypeEnum.DELIVERY;
                }
            }
            else
            {
                planningOrderType = "1";
            }

            #endregion lsogit.fzi.de/efeu/efeuportal/-/issues/98

            string pickupDeviceIds;
            string deliveryDeviceIds;
            if (order.OrderMode == "ASYNCHRON")
            {
                if (order.Pickup.StorageIds.BoxMountingDeviceIds != null && order.Pickup.StorageIds.BoxMountingDeviceIds.Count > 0)
                {
                    pickupDeviceIds = string.Join(",", order.Pickup.StorageIds.BoxMountingDeviceIds);
                }
                else
                {
                    pickupDeviceIds = "PICKUP: Missing BoxMountingDevice ids";
                }

                if (order.Delivery.StorageIds.BoxMountingDeviceIds != null && order.Delivery.StorageIds.BoxMountingDeviceIds.Count > 0)
                {
                    deliveryDeviceIds = string.Join(",", order.Delivery.StorageIds.BoxMountingDeviceIds);
                }
                else
                {
                    deliveryDeviceIds = "DELIVERY: Missing BoxMountingDevice ids";
                }
            }
            else if (order.OrderMode == "SYNCHRON")
            {
                #region https://lsogit.fzi.de/efeu/efeuportal/-/issues/70
                if (order.OrderType.Equals("DELIVERY"))
                {
                    //orderHeader.ExtId2 = order.Ident;
                    orderHeader.ExtId2 = "SYNC-" + order.Ident.Split('-')[0];
                }
                else if (order.OrderType.Equals("PICKUP"))
                {
                    //orderHeader.ExtId2 = order.LinkedBoxOrderId;
                    orderHeader.ExtId2 = "SYNC-" + order.LinkedBoxOrderId.Split('-')[0];
                }
                #endregion

                if (order.Pickup.StorageIds.SyncMeetingPointId != null)
                {
                    pickupDeviceIds = order.Pickup.StorageIds.SyncMeetingPointId;
                }
                else if (order.Pickup.StorageIds.BoxMountingDeviceIds != null)
                {
                    pickupDeviceIds = string.Join(",", order.Pickup.StorageIds.BoxMountingDeviceIds);
                }
                else
                {
                    pickupDeviceIds = "PICKUP: Missing SyncMeetingPoint id";
                }

                if (order.Delivery.StorageIds.SyncMeetingPointId != null)
                {
                    deliveryDeviceIds = order.Delivery.StorageIds.SyncMeetingPointId;
                }
                else if (order.Delivery.StorageIds.BoxMountingDeviceIds != null)
                {
                    deliveryDeviceIds = string.Join(",", order.Delivery.StorageIds.BoxMountingDeviceIds);
                }
                else
                {
                    deliveryDeviceIds = "DELIVERY: Missing SyncMeetingPoint id";
                }
            }
            else if (order.OrderType == "RECHARGING")
            {
                if (order.Pickup.StorageIds.ChargingStationId != null)
                {
                    pickupDeviceIds = order.Pickup.StorageIds.ChargingStationId;
                }
                else
                {
                    pickupDeviceIds = "PICKUP: Missing ChargingStation id";
                }

                if (order.Delivery.StorageIds.ChargingStationId != null)
                {
                    deliveryDeviceIds = order.Delivery.StorageIds.ChargingStationId;
                }
                else
                {
                    deliveryDeviceIds = "DELIVERY: Missing ChargingStation id";
                }
            }
            else
            {
                pickupDeviceIds = "PICKUP: Missing sync or async device id";
                deliveryDeviceIds = "DELIVERY: Missing sync or async device id";
            }

            orderHeader.Text1 = pickupDeviceIds;
            orderHeader.Text2 = deliveryDeviceIds;
            /*
            order.PackageMode == 1
            {
                //IN_BOUND
                orderHeader.Text1 = pickupDeviceIds; // Beladestelle (Depot)
                orderHeader.Text2 = deliveryDeviceIds; // Entladestelle
            }
            order.PackageMode == 2
            {
                //OUT_BOUND
                orderHeader.Text1 = pickupDeviceIds; // Beladestelle
                orderHeader.Text2 = deliveryDeviceIds; // Entladestelle (Depot)
            }
            order.PackageMode == 3
            {
                //INTERNAL => TRANSPORT
                orderHeader.Text1 = pickupDeviceIds; // Beladestelle
                orderHeader.Text2 = deliveryDeviceIds; // Entladestelle
            }
            */
            //orderHeader.PickupExtId1 = order.Pickup.StorageIds.AddressId;
            //orderHeader.DeliveryExtId1 = order.Delivery.StorageIds.AddressId;

            //string pickupTime = DataHelper.CreateFormattedTime(order.OrderTimeSlot.Start, 0, "yyyyMMddHHmmss");
            //string deliveryTime = DataHelper.CreateFormattedTime(order.OrderTimeSlot.End, 0, "yyyyMMddHHmmss");

            //orderHeader.PickupEarliestDateTime = order.OrderTimeSlot.Start.DateTime.ToUniversalTime();
            //orderHeader.PickupLatestDateTime = order.OrderTimeSlot.End.DateTime.ToLocalTime();
            //orderHeader.DeliveryEarliestDateTime = order.OrderTimeSlot.Start.DateTime;
            //orderHeader.DeliveryLatestDateTime = order.OrderTimeSlot.End.DateTime;

            orderHeader.Weight = order.Quantities.GetCalculatedData(UnitTypes.KG);// order.Quantities.Weight / 1000;
            orderHeader.Volume = order.Quantities.GetCalculatedData(UnitTypes.CBM);// order.Quantities.Height * order.Quantities.Width * order.Quantities.Length;

            orderHeader.PreassignedTruck = order.PreassignedVehicleId;
            orderHeader.VehicleRequirements = _vehicleService.CreateSmartourQualityReference(internalUserInfos, order.PreassignedVehicleId);

            /*
            * https://lsogit.fzi.de/efeu/efeuportal/-/issues/47
            */
            if(order.SpecialCodes == null || order.SpecialCodes.Count == 0)
            {
                orderHeader.Priority = 0;
            }
            else
            {
                SpecialCode priorityFound = order.SpecialCodes.Find(x => x.Key.Equals("PRIORITY"));
                if (priorityFound == null)
                {
                    orderHeader.Priority = 0;
                }
                else
                {
                    orderHeader.Priority = Convert.ToInt32(priorityFound.Value);
                }

                SpecialCode simulationIdFound = order.SpecialCodes.Find(x => x.Key.Equals("SIMULATION_ID"));
                if (simulationIdFound != null)
                {
                    orderHeader.Text9 = simulationIdFound.Value;
                }
            }


            return orderHeader;
        }
        
        /// <summary>
        /// JSt: ToDo: Fehlermeldung einbauen
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private List<ActionPoint> CreateROActionPoints(InternalUserInfos internalUserInfos, EfCaOrder order)
        {
            List<ActionPoint> actionPoints = new List<ActionPoint>();

            ActionPoint actionPointPickup = CreateROActionPoint(internalUserInfos, order, order.Pickup, order.PickupTimeSlots, ActionPoint.ActionEnum.PICKUP);
            actionPoints.Add(actionPointPickup);

            ActionPoint actionPointDelivery = CreateROActionPoint(internalUserInfos, order, order.Delivery, order.DeliveryTimeSlots, ActionPoint.ActionEnum.DELIVERY);
            actionPoints.Add(actionPointDelivery);

            #region lsogit.fzi.de/efeu/efeuportal/-/issues/98
            /*
             * servicePeriodExternal aus einer Konfiguration lesen.
             */
            string planningOrderType = "1"; // PickUP and Delivery
            EfCaConfig planningOrderTypeCfg = _systemService.GetConfigOfTenant(internalUserInfos, "Planning-OrderType");
            if (planningOrderTypeCfg == null)
            {
                planningOrderType = "1";
            }
            else
            {
                planningOrderType = planningOrderTypeCfg.StringValue;
            }

            if(planningOrderType == "1")
            {

            }
            else if (planningOrderType == "2")
            {
                int servicePeriodExternal = 60;
                if (order.OrderType == "DELIVERY")
                {
                    actionPointPickup.ServicePeriodExternal = servicePeriodExternal;
                }
                else if (order.OrderType == "PICKUP")
                {
                    actionPointDelivery.ServicePeriodExternal = servicePeriodExternal;
                }
            }
            #endregion lsogit.fzi.de/efeu/efeuportal/-/issues/98

            return actionPoints;
        }

        private ActionPoint CreateROActionPoint(InternalUserInfos internalUserInfos, EfCaOrder efCaOrder, EfCaStorage efCaStorage, List<EfCaDateTimeSlot> orderTimeSlots, 
            ActionPoint.ActionEnum actionPointEnum)
        {
            EfCaAddress efCaAddress = new EfCaAddress();
            efCaAddress.Ident = efCaStorage.StorageIds.AddressId;
            EfCaAddressResp efCaAddressResp = _addressService.FindAddressesByFinder(internalUserInfos, efCaAddress);
            if (efCaAddressResp.Error == true)
            {
                return null;
            }

            efCaAddress = efCaAddressResp.Addresses[0];
            ActionPoint actionPoint = new ActionPoint();
            actionPoint.ExtId1 = efCaStorage.StorageIds.AddressId;
            actionPoint.Action = actionPointEnum;
            actionPoint.ServicePeriodExternal = efCaStorage.ServiceTime;

            EfCaBuilding efCaBuilding = new EfCaBuilding();
            efCaBuilding.AddressId = efCaAddress.Ident;
            EfCaBuildingResp efCaBuildingResp = _buildingService.FindBuildingsByFinder(internalUserInfos, efCaBuilding);
            if (efCaBuildingResp.Error == true)
            {
                return null;
            }

            efCaBuilding = efCaBuildingResp.Buildings[0];
            //JSt: ToDo: Implementierung der TimeSlots
            List<ActionPointOpeningHour> timeSlots = new List<ActionPointOpeningHour>();
            if (efCaOrder.OrderTimeSlot != null && efCaOrder.OrderTimeSlot.Start != null && efCaOrder.OrderTimeSlot.End != null)
            {
                //JSt: ToDo: 23.08.2022
                actionPoint.EarliestDateTime = efCaOrder.OrderTimeSlot.Start.ToLocalTime().DateTime;
                actionPoint.LatestDateTime = efCaOrder.OrderTimeSlot.End.ToLocalTime().DateTime;
            }
            if (efCaBuilding.Type == "DEPOT")
            {
                //actionPoint.EarliestDateTime = efCaOrder.OrderTimeSlot.Start.DateTime;
                //actionPoint.LatestDateTime = efCaOrder.OrderTimeSlot.End.DateTime;
                actionPoint.IsOnetime = false;

                /*
                 * JSt: ToDo: Abstimmung wo die TimeSlots des Depots herkommen fehlt noch
                 */
                //ActionPointOpeningHour actionPointOpeningHour = new ActionPointOpeningHour();


                //timeSlots.Add(actionPointOpeningHour);
            } 
            else if (efCaOrder.OrderType.Equals("RECHARGING")) {
                actionPoint.IsOnetime = false;
                if(efCaStorage.StorageIds.BoxMountingDeviceIds != null &&
                     efCaStorage.StorageIds.BoxMountingDeviceIds.Count == 1)
                    {
                        actionPoint.ExtId1 = efCaStorage.StorageIds.BoxMountingDeviceIds[0];
                    }

            }
            else
            {
                if (efCaOrder.OrderMode.Equals("SYNCHRON"))
                {
                    #region https://lsogit.fzi.de/efeu/efeuportal/-/issues/70
                    if (efCaOrder.OrderType.Equals("DELIVERY"))
                    {
                        actionPoint.ExtId1 = efCaOrder.Ident;
                    }
                    else if (efCaOrder.OrderType.Equals("PICKUP"))
                    {
                        actionPoint.ExtId1 = efCaOrder.LinkedBoxOrderId;
                    }
                    actionPoint.IsOnetime = true;
                    EfCaSyncMeetingPointResp syncMeetingPointResp = _syncMeetingPointService.FindSyncMeetingPointsByFinder(internalUserInfos, new EfCaSyncMeetingPoint() { Ident = efCaStorage.StorageIds.SyncMeetingPointId });
                    actionPoint.CoordinateType = ActionPoint.CoordinateTypeEnum.GEODECIMAL;
                    actionPoint.Latitude = syncMeetingPointResp.SyncMeetingPoints[0].Latitude;
                    actionPoint.Longitude = syncMeetingPointResp.SyncMeetingPoints[0].Longitude;
                    #endregion
                    // /*
                    // * https://lsogit.fzi.de/efeu/efeuportal/-/issues/74
                    // */
                    //if (efCaStorage.StorageIds.SyncMeetingPointId != null)
                    //{
                    //    actionPoint.ExtId1 = efCaStorage.StorageIds.SyncMeetingPointId;
                    //}
                }
                else
                {
                     /*
                     * https://lsogit.fzi.de/efeu/efeuportal/-/issues/74
                     */
                    if(efCaStorage.StorageIds.BoxMountingDeviceIds != null &&
                     efCaStorage.StorageIds.BoxMountingDeviceIds.Count == 1)
                    {
                        actionPoint.ExtId1 = efCaStorage.StorageIds.BoxMountingDeviceIds[0];
                    }
                }

                if (orderTimeSlots != null && orderTimeSlots.Count != 0) 
                {
                    foreach (EfCaDateTimeSlot dateTimeSlot in orderTimeSlots) 
                    {
                        ActionPointOpeningHour actionPointOpeningHour = CreateActionPointOpeningHourFromDateTimeSlot(dateTimeSlot);
                        timeSlots.Add(actionPointOpeningHour);
                    }
                }
                else 
                {
                    EfCaContact efCaContact = new EfCaContact();
                    efCaContact.Ident = efCaStorage.StorageIds.ContactId;
                    EfCaContactResp efCaContactResp = _contactService.FindContactsByFinder(internalUserInfos, efCaContact);
                    if(efCaContactResp.Error == true)
                    {
                        return null;
                    }
                    
                    efCaContact = efCaContactResp.Contacts[0];

                    if(efCaOrder.OrderMode == "ASYNCHRON")
                    {
                        foreach(EfCaSlotsPerDay slot in efCaContact.AsyncAvailabilityTimeSlots)
                        {
                            ActionPointOpeningHour actionPointOpeningHour = CreateActionPointOpeningHour(slot);
                            timeSlots.Add(actionPointOpeningHour);
                        }
                    }
                    else if (efCaOrder.OrderMode == "SYNCHRON")
                    {
                        foreach (EfCaSlotsPerDay slot in efCaContact.SyncAvailabilityTimeSlots)
                        {
                            ActionPointOpeningHour actionPointOpeningHour = CreateActionPointOpeningHour(slot);
                            timeSlots.Add(actionPointOpeningHour);
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                actionPoint.OpeningHours = timeSlots;
            }

            return actionPoint;
        }

        private ActionPointOpeningHour CreateActionPointOpeningHour(EfCaSlotsPerDay slot)
        {
            ActionPointOpeningHour actionPointOpeningHour = new ActionPointOpeningHour();

            if (slot.Day == 0)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.SUN;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.SUN;
            }
            else if (slot.Day == 1)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.MON;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.MON;
            }
            else if (slot.Day == 2)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.TUE;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.TUE;
            }
            else if (slot.Day == 3)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.WED;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.WED;
            }
            else if (slot.Day == 4)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.THU;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.THU;
            }
            else if (slot.Day == 5)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.FRI;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.FRI;
            }
            else if (slot.Day == 6)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.SAT;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.SAT;
            }

            actionPointOpeningHour.From = slot.TimeSlots.Start.Trim(':');
            actionPointOpeningHour.Until = slot.TimeSlots.End.Trim(':');
            
            return actionPointOpeningHour;
        }

        private ActionPointOpeningHour CreateActionPointOpeningHourFromDateTimeSlot(EfCaDateTimeSlot slot) 
        {
            ActionPointOpeningHour actionPointOpeningHour = new ActionPointOpeningHour();

            if (slot.Start.DayOfWeek == DayOfWeek.Sunday)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.SUN;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.SUN;
            }
            else if (slot.Start.DayOfWeek == DayOfWeek.Monday)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.MON;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.MON;
            }
            else if (slot.Start.DayOfWeek == DayOfWeek.Tuesday)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.TUE;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.TUE;
            }
            else if (slot.Start.DayOfWeek == DayOfWeek.Wednesday)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.WED;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.WED;
            }
            else if (slot.Start.DayOfWeek == DayOfWeek.Thursday)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.THU;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.THU;
            }
            else if (slot.Start.DayOfWeek == DayOfWeek.Friday)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.FRI;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.FRI;
            }
            else if (slot.Start.DayOfWeek == DayOfWeek.Saturday)
            {
                actionPointOpeningHour.FromWeekday = ActionPointOpeningHour.FromWeekdayEnum.SAT;
                actionPointOpeningHour.UntilWeekday = ActionPointOpeningHour.UntilWeekdayEnum.SAT;
            }

            DateTimeOffset adjustedStart = slot.Start.ToLocalTime().DateTime;
            DateTimeOffset adjustedEnd = slot.End.ToLocalTime().DateTime;

            actionPointOpeningHour.From = adjustedStart.ToString("HHmm");
            actionPointOpeningHour.Until = adjustedEnd.ToString("HHmm");
            
            return actionPointOpeningHour;
        }

        private Order CreateDummyOrder(Order order, EfCaOrder efcaOrder)
        {
            if(efcaOrder.OrderMode != "ASYNCHRON")
            {
                return null;
            }
            if(efcaOrder.OrderType == "PICKUP")
            {
                return null;
            }

            Order dummyOrder = ExtensionMethods.Clone(order);
            dummyOrder.OrderHeader.OrderType = OrderHeader.OrderTypeEnum.PICKUP;
            
            dummyOrder.ActionPoints[0] = ExtensionMethods.Clone(order.ActionPoints[1]);
            dummyOrder.ActionPoints[0].Action = ActionPoint.ActionEnum.PICKUP;
            dummyOrder.ActionPoints[0].ServicePeriodExternal = 0;
            dummyOrder.ActionPoints[0].HandlingTimeAdditional = 0;
            dummyOrder.ActionPoints[0].HandlingTimeClass = 0;

            dummyOrder.ActionPoints[1] = ExtensionMethods.Clone(order.ActionPoints[0]);
            dummyOrder.ActionPoints[1].Action = ActionPoint.ActionEnum.DELIVERY;
            dummyOrder.ActionPoints[1].ServicePeriodExternal = 0;
            dummyOrder.ActionPoints[1].HandlingTimeAdditional = 0;
            dummyOrder.ActionPoints[1].HandlingTimeClass = 0;

            dummyOrder.OrderHeader.Text5 = dummyOrder.ExtId;
            dummyOrder.ExtId = Guid.NewGuid().ToString();

            dummyOrder.OrderHeader.Volume = 0;
            dummyOrder.OrderHeader.LoadingMeter = 0;
            dummyOrder.OrderHeader.Weight = 0;
            dummyOrder.OrderHeader.Quantity1 = 0;
            dummyOrder.OrderHeader.Quantity2 = 0;
            dummyOrder.OrderHeader.Quantity3 = 0;
            dummyOrder.OrderHeader.Quantity4 = 0;
            dummyOrder.OrderHeader.Quantity5 = 0;
            dummyOrder.OrderHeader.Quantity6 = 0;
            dummyOrder.OrderHeader.Quantity7 = 0;

            //dummyOrder.ActionPoints[1].ServiceInInterval = 0;

            return dummyOrder;
        }

        #endregion
    }
}
