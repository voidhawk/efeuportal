﻿using EfeuPortal.Models.Campus.Warehouse;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class WarehouseService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AddressService));

        private MongoDBSvc _mongoDBService;

        //private RabbitMQService _rabbitMQService;

        public WarehouseService(IConfiguration config)
        {
            _mongoDBService = new MongoDBSvc(config);
        }

        public EfCaWarehousePlaceResp AddWarehousePlaces(InternalUserInfos internalUserInfos, List<EfCaWarehousePlace> warehousePlaces)
        {
            foreach (EfCaWarehousePlace item in warehousePlaces)
            {
                if (item.Ident == null) 
                {
                    item.Ident = Guid.NewGuid().ToString();
                }
            }
            EfCaWarehousePlaceResp response = null;

            response = _mongoDBService.AddWarehousePlace(internalUserInfos, warehousePlaces);
            if (response.Error)
            {
                return response;
            }

            return response;
        }

        public EfCaWarehousePlaceResp FindWarehousePlacesByFinder(InternalUserInfos internalUserInfos, EfCaWarehousePlace finder)
        {
            EfCaWarehousePlaceResp response = null;
            response = _mongoDBService.FindWarehousePlacesByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaWarehousePlaceResp PutWarehousePlace(InternalUserInfos internalUserInfos, EfCaWarehousePlace warehousePlace)
        {
            EfCaWarehousePlaceResp response = new EfCaWarehousePlaceResp() { Error = true, ErrorMsg = "PutWarehousePlace: NOT IMPLEMENTED" };
            //response = _mongoDBService.UpdateAddress(internalUserInfos, warehousePlace);

            return response;
        }

        public EfCaWarehousePlaceResp DeleteWarehousePlace(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaWarehousePlaceResp response = _mongoDBService.DeleteWarehousePlaces(internalUserInfos, ids);

            return response;
        }
    }
}
