﻿using EfeuPortal.Models;
using EfeuPortal.Models.Campus.Apartment;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Campus.SyncMeetingPoint;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.System;
using Microsoft.Extensions.Configuration;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using System;
using System.Collections.Generic;

namespace EfeuPortal.Services.Campus
{
    public class AddressService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AddressService));

        private MongoDBSvc _mongoDBService;
        private SystemService _systemService;
        private BoxMountingDeviceService _boxMountingDeviceService;
        private BuildingService _buildingService;
        private ContactService _contactService;
        private ApartmentService _apartmentService;
        private SyncMeetingPointService _syncMeetingPointService;

        public AddressService(IConfiguration config, SystemService systemService, BoxMountingDeviceService boxMountingDeviceService, BuildingService buildingService,
            ContactService contactService, ApartmentService apartmentService, SyncMeetingPointService syncMeetingPointService)
        {
            _mongoDBService = new MongoDBSvc(config);
            _systemService = systemService;
            _boxMountingDeviceService = boxMountingDeviceService;
            _buildingService = buildingService;
            _contactService = contactService;
            _apartmentService = apartmentService;
            _syncMeetingPointService = syncMeetingPointService;
        }

        public EfCaAddressResp AddAddresses(InternalUserInfos internalUserInfos, List<EfCaAddress> addresses)
        {
            foreach (EfCaAddress item in addresses)
            {
                item.Ident = Guid.NewGuid().ToString();
            }
            EfCaAddressResp response = null;
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            if (roTaskField == null)
            {
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                return response;
            }

            response = _mongoDBService.AddAddresses(internalUserInfos, addresses);
            if (response.Error)
            {
                return response;
            }

            List<Location> locationsList = ExportROWebAPILocations(response.Addresses, "NEW", roTaskField.StringValue);
            LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
            var locationResult = locationsApi.LocationsPostAsync(locationsList);

            return response;
        }

        /// <summary>
        /// Hier werden noch viel mehr Daten angelegt
        /// JSt: ToDo: 
        /// Der Fehlerfall ist nicht implementiert. Falls etwas nicht funktioniert müssen "alle" angelegten Objekte auch wieder gelöscht werden.
        /// 
        /// Den Response sollte man auch anpassen, damit alle Ergebnisse zurück geliefert werden können.
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="efCaModelCollector"></param>
        /// <returns></returns>
        public EfCaAddressResp AddAddressData(InternalUserInfos internalUserInfos, EfCaModelCollector efCaModelCollector)
        {
            EfCaAddressResp response = null;
            if(efCaModelCollector.Addresses == null || efCaModelCollector.Addresses.Count != 1)
            {
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = "AddAddressData failed, parameter error!";
                return response;
            }
            EfCaAddress efCaAddress = efCaModelCollector.Addresses[0];
            efCaAddress.Ident = Guid.NewGuid().ToString();

            EfCaBoxMountingDeviceResp efCaBoxMountingDeviceResp = null;
            if (efCaModelCollector.BoxMountingDevices != null && efCaModelCollector.BoxMountingDevices.Count > 0)
            {
                efCaBoxMountingDeviceResp = _boxMountingDeviceService.AddBoxMountingDevices(internalUserInfos, efCaModelCollector.BoxMountingDevices);
                if (efCaBoxMountingDeviceResp.Error)
                {
                    response = new EfCaAddressResp();
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (BoxMountingDevices) " + efCaBoxMountingDeviceResp.ErrorMsg;
                    return response;
                }
                if (efCaBoxMountingDeviceResp.BoxMountingDevices != null && efCaBoxMountingDeviceResp.BoxMountingDevices.Count > 0)
                {
                    efCaAddress.BoxMountingDeviceIds = new List<string>();
                    foreach (EfCaBoxMountingDevice item in efCaBoxMountingDeviceResp.BoxMountingDevices)
                    {
                        efCaAddress.BoxMountingDeviceIds.Add(item.Ident);
                    }
                }
            }

            EfCaContactResp efCaContactResp = null;
            if (efCaModelCollector.Contacts != null && efCaModelCollector.Contacts.Count > 0)
            {
                efCaContactResp = _contactService.AddContacts(internalUserInfos, efCaModelCollector.Contacts);
                if (efCaContactResp.Error)
                {
                    response = new EfCaAddressResp();
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (Contacts): " + efCaContactResp.ErrorMsg;
                    return response;
                }
            }

            EfCaApartmentResp efCaApartmentResp = null;
            if (efCaModelCollector.Apartments != null && efCaModelCollector.Apartments.Count > 0)
            {
                efCaApartmentResp = _apartmentService.AddApartments(internalUserInfos, efCaModelCollector.Apartments);
                if (efCaContactResp.Error)
                {
                    response = new EfCaAddressResp();
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (Apartments): " + efCaApartmentResp.ErrorMsg;
                    return response;
                }
            }

            EfCaSyncMeetingPointResp efCaSyncMeetingPointResp = null;
            if (efCaModelCollector.SyncMeetingPoints != null && efCaModelCollector.SyncMeetingPoints.Count == 1) 
            {
                EfCaSyncMeetingPoint syncMeetingPoint = efCaModelCollector.SyncMeetingPoints[0];
                efCaSyncMeetingPointResp = _syncMeetingPointService.AddSyncMeetingPoints(internalUserInfos, efCaModelCollector.SyncMeetingPoints);
                if (efCaSyncMeetingPointResp.Error)
                {
                    response = new EfCaAddressResp();
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (SyncMeetingPoints): " + efCaSyncMeetingPointResp.ErrorMsg;
                    return response;
                }
            }

            EfCaBuildingResp efCaBuildingResp = null;
            if (efCaModelCollector.Buildings != null && efCaModelCollector.Buildings.Count == 1)
            {
                EfCaBuilding efCaBuilding = efCaModelCollector.Buildings[0];
                efCaBuilding.AddressId = efCaAddress.Ident;
                efCaBuilding.BoxMountingDeviceIds = efCaAddress.BoxMountingDeviceIds;
                efCaBuilding.SyncMeetingPointIds = new List<string>();
                foreach (EfCaSyncMeetingPoint item in efCaSyncMeetingPointResp.SyncMeetingPoints)
                {
                    efCaBuilding.SyncMeetingPointIds.Add(item.Ident);

                }

                if (efCaContactResp != null && efCaContactResp.Error == false)
                {
                    if(efCaBuilding.ContactIds == null)
                    {
                        efCaBuilding.ContactIds = new List<string>();
                    }
                    efCaBuilding.ContactIds = new List<string>();
                    foreach (EfCaContact item in efCaContactResp.Contacts)
                    {
                        efCaBuilding.ContactIds.Add(item.Ident);
                    }
                }

                if (efCaApartmentResp != null && efCaApartmentResp.Error == false)
                {
                    if (efCaBuilding.ApartmentIds == null)
                    {
                        efCaBuilding.ApartmentIds = new List<string>();
                    }
                    foreach (EfCaApartment item in efCaApartmentResp.Apartments)
                    {
                        efCaBuilding.ApartmentIds.Add(item.Ident);
                    }
                }

                if (efCaSyncMeetingPointResp != null && efCaSyncMeetingPointResp.Error == false) 
                {
                    if(efCaBuilding.SyncMeetingPointIds == null)
                    {
                        efCaBuilding.SyncMeetingPointIds = new List<string>();
                    }
                    foreach (EfCaSyncMeetingPoint item in efCaSyncMeetingPointResp.SyncMeetingPoints)
                    {
                        if (!efCaBuilding.SyncMeetingPointIds.Contains(item.Ident))
                        {
                            efCaBuilding.SyncMeetingPointIds.Add(item.Ident);
                        }
                    }
                }

                List<EfCaBuilding> buildings = new List<EfCaBuilding>();
                buildings.Add(efCaBuilding);
                efCaBuildingResp = _buildingService.AddBuildings(internalUserInfos, buildings);
                if (efCaBuildingResp.Error)
                {
                    response = new EfCaAddressResp();
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (Building): " + efCaBuildingResp.ErrorMsg;
                    return response;
                }
            }

            if (efCaModelCollector.SyncMeetingPoints != null && efCaModelCollector.SyncMeetingPoints.Count == 1) 
            {
                EfCaSyncMeetingPoint syncMeetingPoint = efCaModelCollector.SyncMeetingPoints[0];
            }

            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            if (roTaskField == null)
            {
                response = new EfCaAddressResp();
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                return response;
            }

            List<EfCaAddress> addresses = new List<EfCaAddress>();
            addresses.Add(efCaAddress);
            response = _mongoDBService.AddAddresses(internalUserInfos, addresses);
            if (response.Error)
            {
                return response;
            }

            //List<Location> locationsList = ExportROWebAPILocations(response.Addresses, "NEW", roTaskField.StringValue);
            //LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
            //var locationResult = locationsApi.LocationsPostAsync(locationsList);

            return response;
        }

        /// <summary>
        /// Add initial depot data
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="efCaModelCollector"></param>
        /// <returns></returns>
        public EfCaAddressResp AddAddressData_43(InternalUserInfos internalUserInfos, EfCaModelCollector efCaModelCollector)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            if (efCaModelCollector.Addresses == null || efCaModelCollector.Addresses.Count != 1)
            {
                response.Error = true;
                response.ErrorMsg = "AddAddressData failed, parameter error!";
                return response;
            }
            EfCaAddress efCaAddress = efCaModelCollector.Addresses[0];
            efCaAddress.Ident = Guid.NewGuid().ToString();

            EfCaBoxMountingDeviceResp efCaBoxMountingDeviceResp = null;
            if (efCaModelCollector.BoxMountingDevices != null && efCaModelCollector.BoxMountingDevices.Count > 0)
            {
                efCaBoxMountingDeviceResp = _boxMountingDeviceService.AddBoxMountingDevices(internalUserInfos, efCaModelCollector.BoxMountingDevices);
                if (efCaBoxMountingDeviceResp.Error)
                {
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (BoxMountingDevices) " + efCaBoxMountingDeviceResp.ErrorMsg;
                    return response;
                }
                if (efCaBoxMountingDeviceResp.BoxMountingDevices != null && efCaBoxMountingDeviceResp.BoxMountingDevices.Count > 0)
                {
                    efCaAddress.BoxMountingDeviceIds = new List<string>();
                    foreach (EfCaBoxMountingDevice item in efCaBoxMountingDeviceResp.BoxMountingDevices)
                    {
                        efCaAddress.BoxMountingDeviceIds.Add(item.Ident);
                    }
                }
            }

            EfCaContactResp efCaContactResp = null;
            if (efCaModelCollector.Contacts != null && efCaModelCollector.Contacts.Count > 0)
            {
                efCaContactResp = _contactService.AddContacts(internalUserInfos, efCaModelCollector.Contacts);
                if (efCaContactResp.Error)
                {
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (Contacts): " + efCaContactResp.ErrorMsg;
                    return response;
                }
            }

            EfCaBuildingResp efCaBuildingResp = null;
            if (efCaModelCollector.Buildings != null && efCaModelCollector.Buildings.Count == 1)
            {
                EfCaBuilding efCaBuilding = efCaModelCollector.Buildings[0];
                efCaBuilding.AddressId = efCaAddress.Ident;
                efCaBuilding.BoxMountingDeviceIds = efCaAddress.BoxMountingDeviceIds;
                efCaBuilding.SyncMeetingPointIds = new List<string>();

                if (efCaContactResp != null && efCaContactResp.Error == false)
                {
                    if (efCaBuilding.ContactIds == null)
                    {
                        efCaBuilding.ContactIds = new List<string>();
                    }
                    efCaBuilding.ContactIds = new List<string>();
                    foreach (EfCaContact item in efCaContactResp.Contacts)
                    {
                        efCaBuilding.ContactIds.Add(item.Ident);
                    }
                }

                List<EfCaBuilding> buildings = new List<EfCaBuilding>();
                buildings.Add(efCaBuilding);
                efCaBuildingResp = _buildingService.AddBuildings(internalUserInfos, buildings);
                if (efCaBuildingResp.Error)
                {
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (Building): " + efCaBuildingResp.ErrorMsg;
                    return response;
                }
            }

            if (efCaModelCollector.SyncMeetingPoints != null && efCaModelCollector.SyncMeetingPoints.Count == 1)
            {
                EfCaSyncMeetingPoint syncMeetingPoint = efCaModelCollector.SyncMeetingPoints[0];
            }

            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            if (roTaskField == null)
            {
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                return response;
            }

            List<EfCaAddress> addresses = new List<EfCaAddress>();
            addresses.Add(efCaAddress);
            response = _mongoDBService.AddAddresses(internalUserInfos, addresses);
            if (response.Error)
            {
                return response;
            }

            List<Location> locationsList = ExportROWebAPILocations(response.Addresses, "NEW", roTaskField.StringValue);
            LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
            var locationResult = locationsApi.LocationsPostAsync(locationsList);

            return response;
        }

        public EfCaAddressResp AddAddressData_44(InternalUserInfos internalUserInfos, EfCaModelCollector efCaModelCollector)
        {
            EfCaAddressResp response = new EfCaAddressResp();
            if (efCaModelCollector.Addresses == null || efCaModelCollector.Addresses.Count != 1)
            {
                response.Error = true;
                response.ErrorMsg = "AddAddressData failed, parameter error!";
                return response;
            }
            EfCaAddress efCaAddress = efCaModelCollector.Addresses[0];
            efCaAddress.Ident = Guid.NewGuid().ToString();

            EfCaContactResp efCaContactResp = null;
            if (efCaModelCollector.Contacts != null && efCaModelCollector.Contacts.Count > 0)
            {
                efCaContactResp = _contactService.AddContacts(internalUserInfos, efCaModelCollector.Contacts);
                if (efCaContactResp.Error)
                {
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (Contacts): " + efCaContactResp.ErrorMsg;
                    return response;
                }
                foreach(EfCaContact contact in efCaContactResp.Contacts)
                {
                    EfCaUser user = new EfCaUser() { ContactId = contact.Ident, Email = contact.Email, Role = "EfeuUser", Notice = "Created via Postman"};
                    EfCaUserResp userResp = _systemService.AddUser(internalUserInfos, user);
                    if(userResp.Error)
                    {
                        response.AddDetailInfos(new DetailInfo() { Ident = user.Email, Detail = $"Error: {userResp.ErrorMsg}" });
                    }
                    else
                    {
                        response.AddDetailInfos(new DetailInfo() { Ident = user.Email, Detail = userResp.FirstLoginPassword });
                    }
                }
            }

            EfCaSyncMeetingPointResp efCaSyncMeetingPointResp = null;
            if (efCaModelCollector.SyncMeetingPoints != null && efCaModelCollector.SyncMeetingPoints.Count == 1)
            {
                EfCaSyncMeetingPoint syncMeetingPoint = efCaModelCollector.SyncMeetingPoints[0];
                efCaSyncMeetingPointResp = _syncMeetingPointService.AddSyncMeetingPoints(internalUserInfos, efCaModelCollector.SyncMeetingPoints);
                if (efCaSyncMeetingPointResp.Error)
                {
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (SyncMeetingPoints): " + efCaSyncMeetingPointResp.ErrorMsg;
                    return response;
                }
            }

            EfCaBuildingResp efCaBuildingResp = null;
            if (efCaModelCollector.Buildings != null && efCaModelCollector.Buildings.Count == 1)
            {
                EfCaBuilding efCaBuilding = efCaModelCollector.Buildings[0];
                efCaBuilding.AddressId = efCaAddress.Ident;
                efCaBuilding.BoxMountingDeviceIds = efCaAddress.BoxMountingDeviceIds;
                efCaBuilding.SyncMeetingPointIds = new List<string>();
                foreach (EfCaSyncMeetingPoint item in efCaSyncMeetingPointResp.SyncMeetingPoints)
                {
                    efCaBuilding.SyncMeetingPointIds.Add(item.Ident);
                }

                if (efCaContactResp != null && efCaContactResp.Error == false)
                {
                    if (efCaBuilding.ContactIds == null)
                    {
                        efCaBuilding.ContactIds = new List<string>();
                    }
                    efCaBuilding.ContactIds = new List<string>();
                    foreach (EfCaContact item in efCaContactResp.Contacts)
                    {
                        efCaBuilding.ContactIds.Add(item.Ident);
                    }
                }

                if (efCaSyncMeetingPointResp != null && efCaSyncMeetingPointResp.Error == false)
                {
                    if (efCaBuilding.SyncMeetingPointIds == null)
                    {
                        efCaBuilding.SyncMeetingPointIds = new List<string>();
                    }
                    foreach (EfCaSyncMeetingPoint item in efCaSyncMeetingPointResp.SyncMeetingPoints)
                    {
                        if (!efCaBuilding.SyncMeetingPointIds.Contains(item.Ident))
                        {
                            efCaBuilding.SyncMeetingPointIds.Add(item.Ident);
                        }
                    }
                }

                List<EfCaBuilding> buildings = new List<EfCaBuilding>();
                buildings.Add(efCaBuilding);
                efCaBuildingResp = _buildingService.AddBuildings(internalUserInfos, buildings);
                if (efCaBuildingResp.Error)
                {
                    response.Error = true;
                    response.ErrorMsg = "AddAddressData failed (Building): " + efCaBuildingResp.ErrorMsg;
                    return response;
                }
            }

            if (efCaModelCollector.SyncMeetingPoints != null && efCaModelCollector.SyncMeetingPoints.Count == 1)
            {
                EfCaSyncMeetingPoint syncMeetingPoint = efCaModelCollector.SyncMeetingPoints[0];
            }

            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            if (roTaskField == null)
            {
                response.Error = true;
                response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                return response;
            }

            List<EfCaAddress> addresses = new List<EfCaAddress>();
            addresses.Add(efCaAddress);
            EfCaAddressResp addressResp = _mongoDBService.AddAddresses(internalUserInfos, addresses);
            if (addressResp.Error)
            {
                response.AddDetailInfos(addressResp.DetailInfos);
                return response;
            }

            response.Addresses.AddRange(addressResp.Addresses);

            List<Location> locationsList = ExportROWebAPILocations(addressResp.Addresses, "NEW", roTaskField.StringValue);
            LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
            var locationResult = locationsApi.LocationsPostAsync(locationsList);

            return response;
        }

        public EfCaAddressResp FindAddressesByFinder(InternalUserInfos internalUserInfos, EfCaAddress finder)
        {
            EfCaAddressResp response = null;
            response = _mongoDBService.FindAddressesByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaAddressResp GetAllAddresses(InternalUserInfos internalUserInfos)
        {
            EfCaAddressResp response = null;
            response = _mongoDBService.ReadAllAddresses(internalUserInfos);

            return response;
        }

        public EfCaAddressResp PutAddress(InternalUserInfos internalUserInfos, EfCaAddress address)
        {
            EfCaAddressResp response = null;
            response = _mongoDBService.UpdateAddress(internalUserInfos, address);
            if(response.Error == true)
            {
                return response;
            }

            response.AddDetailInfos(new DetailInfo() { Ident = address.Ident, Detail = "Updating EfCaAddress was succesful" });
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response.AddDetailInfos(new DetailInfo() { Ident = address.Ident, Detail = "EfCaConfig(key==PTV-RO-WebAPI) is not valid" } );
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            
            if (roTaskField == null)
            {
                response.AddDetailInfos(new DetailInfo() { Ident = address.Ident, Detail = "EfCaConfig(key==PTV-RO-TaskField) is not valid" });
                return response;
            }

            List<Location> locationsList = ExportROWebAPILocations(response.Addresses, "UPDATE", roTaskField.StringValue);
            LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
            var locationResult = locationsApi.LocationsPostAsync(locationsList);

            return response;
        }

        public EfCaAddressResp DeleteAddresses(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaAddressResp response = null;
            response = _mongoDBService.DeleteAddresses(internalUserInfos, ids);
            foreach(DetailInfo detailInfo in response.DetailInfos)
            {
                if(detailInfo.Detail.Equals("AddressId deleted"))
                {
                    // in diesem Fall muss noch das Building gelöscht werden
                    string addressIdent = detailInfo.Ident;
                    EfCaBuilding finder = new EfCaBuilding() { AddressId = addressIdent };
                    EfCaBuildingResp efCaBuildingResp = _buildingService.FindBuildingsByFinder(internalUserInfos, finder);
                    if(efCaBuildingResp.Error == false)
                    {
                        List<string> buildingIds = new List<string>();
                        buildingIds.Add(efCaBuildingResp.Buildings[0].Ident);
                        _buildingService.DeleteBuilding(internalUserInfos, buildingIds);
                    }
                }
            }
            //foreach(string id in ids)
            //{
            //    DetailInfo detailInfo = new DetailInfo();
            //    detailInfo.Ident = id;
            //    detailInfo.Detail = "Default behaviour: Deleting of addresses not allowed";
            //    response.AddDetailInfos(detailInfo);
            //}
            //if(response.Error == true)
            //{
            //    return response;
            //}

            //EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
            //if (roTaskField == null)
            //{
            //    response = new EfCaAddressResp();
            //    response.Error = true;
            //    response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
            //    return response;
            //}

            //EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            //if (roWebAPIUrl == null)
            //{
            //    response = new EfCaAddressResp();
            //    response.Error = true;
            //    response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
            //    return response;
            //}

            //List<EfCaAddress> addresses = new List<EfCaAddress>();
            //foreach(string id in ids)
            //{
            //    EfCaAddress efCaAddress = new EfCaAddress();
            //    efCaAddress.Ident = id;
            //    addresses.Add(efCaAddress);
            //}
            //List<Location> locationsList = ExportROWebAPILocations(addresses, "DELETE", roTaskField.StringValue);
            //LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
            //var locationResult = locationsApi.LocationsPostAsync(locationsList);

            return response;
        }

        /// <summary>
        /// Erzeugen von Smartour Locations umd diese über die WebApi in Smartour importieren zu können.
        /// </summary>
        /// <param name="addresses"></param>
        /// <param name="actionCode"></param>
        /// <param name="taskFields"></param>
        /// <returns></returns>
        private List<Location> ExportROWebAPILocations(List<EfCaAddress> addresses, string actionCode, string taskFields)
        {
            List<Location> locationsList = new List<Location>();

            Location.ActionCodeEnum action;
            if(actionCode == "NEW")
            {
                action = Location.ActionCodeEnum.NEW;
            }
            else if (actionCode == "UPDATE")
            {
                action = Location.ActionCodeEnum.UPDATE;
            }
            else if (actionCode == "DELETE")
            {
                action = Location.ActionCodeEnum.DELETE;
            }
            else
            {
                return null;
            }


            foreach (EfCaAddress item in addresses)
            {
                Location location = new Location(null, null, null, null, item.Ident, DateTime.Now, action, null);
                if(action == Location.ActionCodeEnum.DELETE)
                {

                }
                if (action == Location.ActionCodeEnum.NEW || action == Location.ActionCodeEnum.UPDATE)
                {
                    location.LocationHeader = new LocationHeader();
                    LocationHeader.FunctionEnum function;
                    if (item.Type == "DEPOT")
                    {
                        function = LocationHeader.FunctionEnum.DEPOT;
                        location.LocationHeader.HandlingTimeClassPickup = 1;
                        location.LocationHeader.HandlingTimeClassDelivery = 2;
                    }
                    else if (item.Type == "CUSTOMER")
                    {
                        function = LocationHeader.FunctionEnum.CUSTOMER;
                    }
                    else
                    {
                        function = LocationHeader.FunctionEnum.CUSTOMER;
                    }

                    location.LocationHeader.Function = function;
                    location.LocationHeader.Taskfields = taskFields;
                    location.LocationHeader.Country = "DEU";
                    location.LocationHeader.Postcode = item.ZipCode;
                    location.LocationHeader.City = item.City;
                    location.LocationHeader.Street = item.Street;
                    location.LocationHeader.HouseNo = item.HouseNumber;
                    location.LocationHeader.ShortDescription = item.Label;
                    location.LocationHeader.HandlingTimeClassPickup = 0;
                    location.LocationHeader.HandlingTimeClassDelivery = 0;
                    location.LocationHeader.Latitude = item.Latitude;
                    location.LocationHeader.Longitude = item.Longitude;
                    location.LocationHeader.Description = item.Label;
                    location.LocationHeader.CoordinateType = LocationHeader.CoordinateTypeEnum.GEODECIMAL;

                    //location.Contacts = new List<LocationContact>();
                    //LocationContact locationContact = new LocationContact();
                    //locationContact.ExtId1 = "Test-Contact-id19";
                    //locationContact.Default = true;
                    //locationContact.Language = "DEU";
                    //locationContact.Communications = new List<ContactCommunication>();
                    //ContactCommunication contactCommunication = new ContactCommunication();
                    //contactCommunication.Type = ContactCommunication.TypeEnum.EMAIL;
                    //contactCommunication.DestinationAddress = "Mona.Kübler@outlook.com";
                    //locationContact.Communications.Add(contactCommunication);
                    //location.Contacts.Add(locationContact);
                }
                locationsList.Add(location);
            }

            return locationsList;
        }
    }
}
