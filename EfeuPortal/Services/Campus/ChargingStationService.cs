﻿using EfeuPortal.Models.Campus.ChargingStation;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.FZI;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class ChargingStationService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ChargingStationService));

        private MongoDBSvc _mongoDBService;

        private FZIStatusService _fziService;

        //private RabbitMQService _rabbitMQService;

        public ChargingStationService(IConfiguration config, FZIStatusService fziService)
        {
            _mongoDBService = new MongoDBSvc(config);
            _fziService = fziService;
            //_rabbitMQService = new RabbitMQService(config);
        }

        public EfCaChargingStationResp AddChargingStations(InternalUserInfos internalUserInfos, List<EfCaChargingStation> charchingStations)
        {
            EfCaChargingStationResp response = null;
            foreach(EfCaChargingStation item in charchingStations)
            {
                item.Ident = Guid.NewGuid().ToString();
            }
            response = _mongoDBService.AddChargingStations(internalUserInfos, charchingStations);

            return response;
        }

        public EfCaChargingStationResp FindChargingStationsByFinder(InternalUserInfos internalUserInfos, EfCaChargingStation finder)
        {
            EfCaChargingStationResp response = null;
            response = _mongoDBService.FindChargingStationsByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaChargingStationResp PutChargingStation(InternalUserInfos internalUserInfos, EfCaChargingStation dockingStation)
        {
            EfCaChargingStationResp response = null;
            response = _mongoDBService.UpdateChargingStation(internalUserInfos, dockingStation);

            return response;
        }

        public EfCaChargingStationResp GetAllChargingStations(InternalUserInfos internalUserInfos)
        {
            EfCaChargingStationResp response = null;
            response = _mongoDBService.ReadAllChargingStations(internalUserInfos);

            return response;
        }

        public EfCaChargingStationResp DeleteChargingStation(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaChargingStationResp response = null;
            response = _mongoDBService.DeleteChargingStations(internalUserInfos, ids);

            return response;
        }
    }
}
