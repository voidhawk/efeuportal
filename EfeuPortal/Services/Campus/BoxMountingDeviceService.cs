﻿using EfeuPortal.Helpers;
using EfeuPortal.Models;
using EfeuPortal.Models.Campus;
using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Shared;
using EfeuPortal.Models.System;
using EfeuPortal.Services.DB.MongoDB;
using EfeuPortal.Services.FZI;
using EfeuPortal.Services.System;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using PTVGROUP.ROWebApi.Api;
using PTVGROUP.ROWebApi.Model;
using PTVGROUP.SlotMgmt.Api;
using PTVGROUP.SlotMgmt.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using DayOfWeek = System.DayOfWeek;

namespace EfeuPortal.Services.Campus
{
    public class BoxMountingDeviceService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(BoxMountingDeviceService));

        private MongoDBSvc _mongoDBService;
        private FZIStatusService _fziService;
        private SystemService _systemService;
        private BuildingService _buildingService;
        private Dictionary<string, object> externalApis = new Dictionary<string, object>();

        //private RabbitMQService _rabbitMQService;

        //public MountService(IConfiguration config, MongoDBSvc mongoDBService)
        public BoxMountingDeviceService(IConfiguration config, FZIStatusService fziService, SystemService systemService, BuildingService buildingService)
        {
            _mongoDBService = new MongoDBSvc(config);
            _fziService = fziService;
            _systemService = systemService;
            _buildingService = buildingService;
            //_rabbitMQService = new RabbitMQService(config);
        }

        #region BoxMountingDevice
        /// <summary>
        /// Für jede neue BoxMountingDevice wird
        /// <para>im FZI Service ein Status Objekt erzeugt</para>
        /// 
        /// <para>Im Slot Scheduler ein entsprechendes Objekt erzeugt</para>
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="boxMountingDevices"></param>
        /// <returns></returns>
        public EfCaBoxMountingDeviceResp AddBoxMountingDevices(InternalUserInfos internalUserInfos, List<EfCaBoxMountingDevice> boxMountingDevices)
        {
            EfCaBoxMountingDeviceResp response = null;
            foreach(EfCaBoxMountingDevice item in boxMountingDevices)
            {
                item.Ident = Guid.NewGuid().ToString();
            }
            response = _mongoDBService.AddBoxMountingDevices(internalUserInfos, boxMountingDevices);
            if (!response.Error)
            {
                var fziResponse = _fziService.AddBoxMountingDevicesStatus(internalUserInfos, response);
                if(fziResponse == true)
                {
                    DetailInfo detailInfo = new DetailInfo() { Ident = $"FZI.AddBoxMountingDevicesStatus(FAILED)" };
                    response.AddDetailInfos(detailInfo);
                }

                /*
                 * https://lsogit.fzi.de/efeu/efeuportal/-/issues/74
                 */
                EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
                if (roWebAPIUrl == null)
                {
                    response = new EfCaBoxMountingDeviceResp();
                    response.Error = true;
                    response.ErrorMsg = "EfCaConfig(key==PTV-RO-WebAPI) is not valid";
                    return response;
                }
                EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");
                if (roTaskField == null)
                {
                    response = new EfCaBoxMountingDeviceResp();
                    response.Error = true;
                    response.ErrorMsg = "EfCaConfig(key==PTV-RO-TaskField) is not valid";
                    return response;
                }

                List<Location> locationsList = ExportROWebAPILocations(response.BoxMountingDevices, "NEW", roTaskField.StringValue);
                LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
                var locationResult = locationsApi.LocationsPostAsync(locationsList);
            }

            // if (!response.Error) 
            // {
            //     foreach (EfCaBoxMountingDevice processDevice in response.BoxMountingDevices)
            //     {
            //         PtvSlotSchedulerResponse slotSchedulerResponse = CreateSlotSchedulers(internalUserInfos, processDevice);
            //         if(slotSchedulerResponse.ReturnInfo == ReturnValue.SUCCESS)
            //         {
            //             foreach(PtvSlotScheduler slotScheduler in slotSchedulerResponse.Result)
            //             {
            //                 if(processDevice.SlotSchedulerIdents == null)
            //                 {
            //                     processDevice.SlotSchedulerIdents = new List<string>();
            //                 }
            //                 processDevice.SlotSchedulerIdents.Add(slotScheduler.Ident);
            //             }
            //             EfCaBoxMountingDeviceResp updateBoxMountingDeviceResp = _mongoDBService.UpdateBoxMountingDevice(internalUserInfos, processDevice);
            //             if (updateBoxMountingDeviceResp.Error == false)
            //             {
            //                 foreach (EfCaBoxMountingDevice updatedDevice in updateBoxMountingDeviceResp.BoxMountingDevices)
            //                 {
            //                     var index = response.BoxMountingDevices.FindIndex(item => item.Ident == updatedDevice.Ident);
            //                     response.BoxMountingDevices[index] = updatedDevice;
            //                 }
            //             }
            //         }
            //         else
            //         {
            //             DetailInfo detailInfo = new DetailInfo() { Ident = $"CreateSlotSchedulers(BoxMountingDevice.Ident({processDevice.Ident}))" };
            //             detailInfo.Detail = slotSchedulerResponse.Message;
            //             response.AddDetailInfos(detailInfo);
            //         }
            //     }
            // }

            return response;
        }

        public EfCaBoxMountingDeviceResp FindBoxMountingDevicesByFinder(InternalUserInfos internalUserInfos, EfCaBoxMountingDevice finder)
        {
            EfCaBoxMountingDeviceResp response = null;
            response = _mongoDBService.FindBoxMountingDevicesByFinder(internalUserInfos, finder);

            return response;
        }

        public EfCaBoxMountingDeviceResp GetBoxMountingDeviceDetails(InternalUserInfos internalUserInfos, string ident)
        {
            EfCaBoxMountingDeviceResp response = null;
            response = _mongoDBService.GetBoxMountingDeviceDetails(internalUserInfos, ident);

            return response;
        }

        public EfCaBoxMountingDeviceResp PutBoxMountingDevice(InternalUserInfos internalUserInfos, EfCaBoxMountingDevice boxMountingDevice)
        {
            EfCaBoxMountingDeviceResp response = null;
            response = _mongoDBService.UpdateBoxMountingDevice(internalUserInfos, boxMountingDevice);
            if (response.Error == true)
            {
                return response;
            }

            response.AddDetailInfos(new DetailInfo() { Ident = boxMountingDevice.Ident, Detail = "Updating EfCaAddress was succesful" });
            EfCaConfig roWebAPIUrl = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-WebAPI");
            if (roWebAPIUrl == null)
            {
                response.AddDetailInfos(new DetailInfo() { Ident = boxMountingDevice.Ident, Detail = "EfCaConfig(key==PTV-RO-WebAPI) is not valid" });
                return response;
            }
            EfCaConfig roTaskField = _systemService.GetConfigOfTenant(internalUserInfos, "PTV-RO-TaskField");

            if (roTaskField == null)
            {
                response.AddDetailInfos(new DetailInfo() { Ident = boxMountingDevice.Ident, Detail = "EfCaConfig(key==PTV-RO-TaskField) is not valid" });
                return response;
            }

            List<Location> locationsList = ExportROWebAPILocations(response.BoxMountingDevices, "UPDATE", roTaskField.StringValue);
            LocationsApi locationsApi = new LocationsApi(roWebAPIUrl.Url);
            var locationResult = locationsApi.LocationsPostAsync(locationsList);

            return response;
        }

        public EfCaBoxMountingDeviceResp DeleteBoxMountingDevice(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaBoxMountingDeviceResp response = null;
            /* 
             * JSt: ToDo: Alle Referenzen auf diese IDs in ALLEN Objekten löschen
             * Contact, Apartment, Building
             * 
             * Darauf achten, dass hier nichts MongoDB spezifisches ankommt
             */
            response = _mongoDBService.DeleteBoxMountingDevice(internalUserInfos, ids);
            /*
             * efeuPortal: Abhängigkeit im Building entfernen
             */
            if(response.Error == false)
            {
                EfCaBuilding finder = new EfCaBuilding();
                finder.BoxMountingDeviceIds = ids;
                EfCaBuildingResp efCaBuildingResp = _buildingService.FindBuildingsByFinder(internalUserInfos, finder);

                /*
                 * Ein Dock darf nur einem Gebäude zugewiesen sein
                 */
                if(efCaBuildingResp.Error == false && efCaBuildingResp.Buildings.Count == 1)
                {
                    efCaBuildingResp.Buildings[0].BoxMountingDeviceIds.Remove(ids[0]);
                    _buildingService.PutBuilding(internalUserInfos, efCaBuildingResp.Buildings[0]);
                }
            }

            /*
             * FZI
             */
            if (!response.Error) 
            {
                var fziResponse = _fziService.DeleteBoxMountingDeviceStatus(internalUserInfos, ids[0]);
            }

            return response;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="slotReservationRequests"></param>
        /// <returns></returns>
        internal EfCaBoxMountingDeviceResp PostBoxMountingDeviceSlotReservations(InternalUserInfos internalUserInfos, List<EfCaSlotReservationRequest> slotReservationRequests)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug($"PostBoxMountingDeviceSlotReservations(..) called User({JsonConvert.SerializeObject(internalUserInfos, Formatting.Indented)}, " +
                    $"Request({JsonConvert.SerializeObject(slotReservationRequests, Formatting.Indented)}).");
            }
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            response.ScheduledBoxMountingSlots = new List<EfCaScheduledBoxMountingSlot>();

            SlotManagementApi api = GetSlotManagementApi(internalUserInfos);
            foreach (EfCaSlotReservationRequest efcaSlotReservationRequest in slotReservationRequests)
            {
                if (efcaSlotReservationRequest.SlotSelection == SlotSelection.SINGLE)
                {
                    response = CreateSingleSlotReservations(internalUserInfos, api, efcaSlotReservationRequest);
                    return response;
                }
                else if (efcaSlotReservationRequest.SlotSelection == SlotSelection.ALL)
                {
                    response = CreateALLSlotReservations(internalUserInfos, api, efcaSlotReservationRequest);
                }
            }

            return response;
        }

        private static EfCaBoxMountingDeviceResp CreateSingleSlotReservations(InternalUserInfos internalUserInfos, SlotManagementApi api, EfCaSlotReservationRequest efcaSlotReservationRequest)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            int reservationsCreated = 0;
            PtvUserInfo ptvUserInfo = new PtvUserInfo()
            {
                Email = internalUserInfos.Email,
                TenantId = internalUserInfos.TenantId,
                Role = internalUserInfos.Role,
                InternalUserId = internalUserInfos.InternalUserId
            };
            if (response.ScheduledBoxMountingSlots == null)
            {
                response.ScheduledBoxMountingSlots = new List<EfCaScheduledBoxMountingSlot>();
            }

            while (reservationsCreated < efcaSlotReservationRequest.ReservationsRequestCount)
            {
                foreach (string ptvSchedulerIdent in efcaSlotReservationRequest.SchedulerIds)
                {
                    PtvModelCollector slotReservationsRequest = new PtvModelCollector();
                    slotReservationsRequest.UserInfo = ptvUserInfo;

                    slotReservationsRequest.SlotReservationRequest = new PtvSlotReservationRequest();
                    slotReservationsRequest.SlotReservationRequest.Ident = ptvSchedulerIdent;
                    slotReservationsRequest.SlotReservationRequest.CustomerIdent = efcaSlotReservationRequest.CustomerIdent;
                    slotReservationsRequest.SlotReservationRequest.Start = efcaSlotReservationRequest.Start;
                    slotReservationsRequest.SlotReservationRequest.End = efcaSlotReservationRequest.End;
                    slotReservationsRequest.SlotReservationRequest.TimeSlot = efcaSlotReservationRequest.TimeSlot;

                    PtvSlotReservationResponseResponse ptvSlotSchedulerResponse = api.ReserveAvailableSlot(slotReservationsRequest);
                    if (ptvSlotSchedulerResponse != null && ptvSlotSchedulerResponse.ReturnInfo == ReturnValue.SUCCESS)
                    {
                        EfCaScheduledBoxMountingSlot scheduledBoxMountingSlot = new EfCaScheduledBoxMountingSlot(ptvSlotSchedulerResponse.Result[0]);
                        response.ScheduledBoxMountingSlots.Add(scheduledBoxMountingSlot);
                    }

                    reservationsCreated++;
                    if (reservationsCreated == efcaSlotReservationRequest.ReservationsRequestCount)
                    {
                        break;
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Reservieren der freien Slots fuer eine grosse Kiste.
        /// 
        /// Bedingungen:
        ///  - es muessen 2 SchedulerIdents angegeben sein
        ///  - es muss der gleiche Uebergabebock sein
        ///  - es darf nur eine Reservierung angefragt werden
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="api"></param>
        /// <param name="efcaSlotReservationRequest"></param>
        /// <returns></returns>
        private EfCaBoxMountingDeviceResp CreateALLSlotReservations(InternalUserInfos internalUserInfos, SlotManagementApi api, EfCaSlotReservationRequest efcaSlotReservationRequest)
        {
            #region validation
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            if (response.ScheduledBoxMountingSlots == null)
            {
                response.ScheduledBoxMountingSlots = new List<EfCaScheduledBoxMountingSlot>();
            }
            PtvUserInfo ptvUserInfo = new PtvUserInfo()
            {
                Email = internalUserInfos.Email,
                TenantId = internalUserInfos.TenantId,
                Role = internalUserInfos.Role,
                InternalUserId = internalUserInfos.InternalUserId
            };

            if (efcaSlotReservationRequest.SchedulerIds == null || efcaSlotReservationRequest.SchedulerIds.Count != 2 || efcaSlotReservationRequest.ReservationsRequestCount != 1)
            {
                log.Error($"CreateAllSlotReservations(..) SchedulerIds not valid, parameter error: User({JsonConvert.SerializeObject(internalUserInfos, Formatting.Indented)}, " +
                    $"Request({JsonConvert.SerializeObject(efcaSlotReservationRequest, Formatting.Indented)}).");
                response.Error = true;
                response.ErrorMsg = "SchedulerIds not valid, parameter error";
                return response;
            }

            PtvModelCollector slotSchedulersRequest = new PtvModelCollector();
            slotSchedulersRequest.UserInfo = ptvUserInfo;
            slotSchedulersRequest.Idents = efcaSlotReservationRequest.SchedulerIds;
            PtvSlotSchedulerResponse slotSchedulerResponse = api.FindSlotSchedulers(slotSchedulersRequest);
            if(slotSchedulerResponse.ReturnInfo == ReturnValue.SUCCESS)
            {
                if(slotSchedulerResponse.Result == null || slotSchedulerResponse.Result.Count != 2)
                {
                    response.Error = true;
                    response.ErrorMsg = "With the external SlotManagement the data did not match";
                    return response;
                }
            }
            else
            {
                response.Error = true;
                response.ErrorMsg = "Something happend at the external SlotManagement";
                return response;
            }
            #endregion

            bool validSchedulerIndexABool = false;

            bool synchonizedSlotNotFound = true;
            bool schedulerIdentIndex = false;
            //Start verschieb sich mit jedem Request
            DateTimeOffset start = efcaSlotReservationRequest.Start;
            //End bleibt immer gleich
            DateTimeOffset end = efcaSlotReservationRequest.End;
            string customerIdent = efcaSlotReservationRequest.CustomerIdent;
            int timeSlot = efcaSlotReservationRequest.TimeSlot;
            PtvSlotReservationResponse ptvSlotReservation_0 = null;
            PtvSlotReservationResponse ptvSlotReservation_1 = null;
            while (synchonizedSlotNotFound)
            {
                if (ptvSlotReservation_0 == null)
                {
                    ptvSlotReservation_0 = CreateReservation(api, ptvUserInfo, efcaSlotReservationRequest.SchedulerIds[Convert.ToInt32(schedulerIdentIndex)],
                        customerIdent, start, end, timeSlot);
                    if (ptvSlotReservation_0 == null)
                    {
                        response.Error = true;
                        response.ErrorMsg = $"No reservation at Ident({efcaSlotReservationRequest.SchedulerIds[Convert.ToInt32(schedulerIdentIndex)]}) possible";
                        return response;
                    }
                    validSchedulerIndexABool = false;
                }
                schedulerIdentIndex = !schedulerIdentIndex;

                // Die NEUE Startzeit
                start = DataHelper.GetDateTimeAtStartIndex(start, ptvSlotReservation_0.StartIndex, 5);
                if (ptvSlotReservation_1 == null)
                {
                    ptvSlotReservation_1 = CreateReservation(api, ptvUserInfo, efcaSlotReservationRequest.SchedulerIds[Convert.ToInt32(schedulerIdentIndex)],
                        customerIdent, start, end, timeSlot);
                    if (ptvSlotReservation_1 == null)
                    {
                        response.Error = true;
                        response.ErrorMsg = $"No reservation at Ident({efcaSlotReservationRequest.SchedulerIds[Convert.ToInt32(schedulerIdentIndex)]}) possible";
                        return response;
                    }
                }
                if (ptvSlotReservation_0.StartIndex == ptvSlotReservation_1.StartIndex)
                {
                    // match found
                    synchonizedSlotNotFound = false;
                    EfCaScheduledBoxMountingSlot scheduledBoxMountingSlot_0 = new EfCaScheduledBoxMountingSlot(ptvSlotReservation_0);
                    response.ScheduledBoxMountingSlots.Add(scheduledBoxMountingSlot_0);

                    EfCaScheduledBoxMountingSlot scheduledBoxMountingSlot_1 = new EfCaScheduledBoxMountingSlot(ptvSlotReservation_1);
                    response.ScheduledBoxMountingSlots.Add(scheduledBoxMountingSlot_1);

                    return response;
                }
                else
                {
                    EfCaModelCollector deleteSlotReservations = new EfCaModelCollector();
                    deleteSlotReservations.ScheduledBoxMountSlots = new List<EfCaScheduledBoxMountingSlot>();
                    if (ptvSlotReservation_0.StartIndex > ptvSlotReservation_1.StartIndex)
                    {
                        validSchedulerIndexABool = false;
                    }
                    else
                    {
                        validSchedulerIndexABool = true;
                    }

                    //validSchedulerIndexABool = !validSchedulerIndexABool;
                    EfCaScheduledBoxMountingSlot scheduledBoxMountingSlot = null;
                    int newStartIndex = -1;
                    if (validSchedulerIndexABool)
                    {
                        newStartIndex = ptvSlotReservation_1.StartIndex;
                        scheduledBoxMountingSlot = new EfCaScheduledBoxMountingSlot()
                        {
                            Ident = ptvSlotReservation_0.Ident,
                            ExternalSlotReservationInfo = ptvSlotReservation_0.ReservationKey
                        };
                        ptvSlotReservation_0 = null;
                        schedulerIdentIndex = false;
                    }
                    else
                    {
                        newStartIndex = ptvSlotReservation_0.EndIndex + 1;
                        scheduledBoxMountingSlot = new EfCaScheduledBoxMountingSlot()
                        {
                            Ident = ptvSlotReservation_1.Ident,
                            ExternalSlotReservationInfo = ptvSlotReservation_1.ReservationKey
                        };
                        ptvSlotReservation_1 = null;
                        schedulerIdentIndex = true;
                    }
                    start = DataHelper.GetDateTimeAtStartIndex(start, newStartIndex, 5);
                    deleteSlotReservations.ScheduledBoxMountSlots.Add(scheduledBoxMountingSlot);
                    DeleteBoxMountingDeviceSlotRequests(internalUserInfos, deleteSlotReservations);
                }
            }

            return response;
        }

        /// <summary>
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="slotReservations"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        internal EfCaBoxMountingDeviceResp GetBoxMountingDeviceReservations(InternalUserInfos internalUserInfos, List<EfCaSlotReservation> slotReservations, DateTimeOffset date)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();
            List<EfCaSlotReservations> slotReservationsList = new List<EfCaSlotReservations>();
            response.SlotReservations = slotReservationsList;

            PtvModelCollector slotReservationsRequest = new PtvModelCollector();
            slotReservationsRequest.SlotSchedulers = new List<PtvSlotScheduler>();
            PtvUserInfo ptvUserInfo = new PtvUserInfo()
            {
                Email = internalUserInfos.Email,
                TenantId = internalUserInfos.TenantId,
                Role = internalUserInfos.Role,
                InternalUserId = internalUserInfos.InternalUserId
            };
            foreach (EfCaSlotReservation item in slotReservations)
            {
                slotReservationsRequest.SlotSchedulers.Clear();
                slotReservationsRequest.UserInfo = ptvUserInfo;
                PtvSlotScheduler slotScheduler = new PtvSlotScheduler();
                slotScheduler.Ident = item.SchedulerIdent;
                slotReservationsRequest.SlotSchedulers.Add(slotScheduler);

                SlotManagementApi api = GetSlotManagementApi(internalUserInfos);
                PtvSlotSchedulerResponse ptvSlotSchedulerResponse =  api.FindSlotSchedulerReservations(date, slotReservationsRequest);
                if(ptvSlotSchedulerResponse.ReturnInfo == ReturnValue.SUCCESS)
                {
                    List<PtvSlotScheduler> schedulers = ptvSlotSchedulerResponse.Result;
                    foreach(PtvSlotScheduler ptvSlotScheduler in schedulers)
                    {
                        EfCaSlotReservations efCaSlotReservations = new EfCaSlotReservations(ptvSlotScheduler);
                        slotReservationsList.Add(efCaSlotReservations);
                    }
                }
            }

            return response;
        }

        internal EfCaBoxMountingDeviceResp ProcessBoxMountingDeviceSlotRequests(InternalUserInfos internalUserInfos, EfCaModelCollector slots)
        {
            EfCaBoxMountingDeviceResp response = null;

            SlotManagementApi api = GetSlotManagementApi(internalUserInfos);
            if (slots == null)
            {
                // Holen der Basisdaten aller Slots
                PtvModelCollector modelCollector = new PtvModelCollector();
                modelCollector.UserInfo = new PtvUserInfo()
                {
                    Email = internalUserInfos.Email,
                    TenantId = internalUserInfos.TenantId,
                    Role = internalUserInfos.Role,
                    InternalUserId = internalUserInfos.InternalUserId
                };

                PtvSlotSchedulerResponse ptvSlotSchedulerResponse = api.FindSlotSchedulers(modelCollector);
                response = new EfCaBoxMountingDeviceResp(ptvSlotSchedulerResponse);
            }
            else if (slots.Idents != null)
            {
                // Holen der Reservierungen für die angegebenen Idents
                // odeer doch die Basisdaten der angegebenen Ids
            }
            else if(slots.SlotReservations != null)
            {
                // Holen der Reservierungen für die angegebenen Slots
            }
            else
            {
                //Holen der Basisdaten von allen Slots
            }

            return response;
        }

        internal EfCaBoxMountingDeviceResp DeleteBoxMountingDeviceSlotRequests(InternalUserInfos internalUserInfos, EfCaModelCollector deleteSlotReservations)
        {
            EfCaBoxMountingDeviceResp response = null;

            SlotManagementApi api = GetSlotManagementApi(internalUserInfos);
            PtvModelCollector modelCollector = new PtvModelCollector();
            modelCollector.UserInfo = new PtvUserInfo()
            {
                Email = internalUserInfos.Email,
                TenantId = internalUserInfos.TenantId,
                Role = internalUserInfos.Role,
                InternalUserId = internalUserInfos.InternalUserId
            };
            modelCollector.SlotReservationResponse = new List<PtvSlotReservationResponse>();
            foreach(EfCaScheduledBoxMountingSlot item in deleteSlotReservations.ScheduledBoxMountSlots)
            {
                PtvSlotReservationResponse slotReservationResponse = new PtvSlotReservationResponse();
                slotReservationResponse.Ident = item.Ident;
                slotReservationResponse.ReservationKey = item.ExternalSlotReservationInfo;

                modelCollector.SlotReservationResponse.Add(slotReservationResponse);
            }
            PtvSlotReservationResponseResponse deleteSlotReservationResponse = api.DeleteReservedSlots(modelCollector);
            response = new EfCaBoxMountingDeviceResp(deleteSlotReservationResponse);

            return response;
        }

        /// <summary>
        /// Returns the generated SlotmanagementApi.
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <returns></returns>
        private SlotManagementApi GetSlotManagementApi(InternalUserInfos internalUserInfos)
        {
            if (!externalApis.ContainsKey("PTV-SlotManagementApi"))
            {
                EfCaConfig config = _mongoDBService.GetConfigOfTenant(internalUserInfos, "PTV-SlotManagementAPI");
                if (log.IsDebugEnabled)
                {
                    log.Debug($"GetSlotManagementApi(..) Config.Key(PTV-SlotManagementApi), Config({JsonConvert.SerializeObject(config, Formatting.Indented)}");
                }
                //var externalApiUrl = _mongoDBService.GetConfigOfTenant(internalUserInfos, "PTV-SlotManagementAPI").Url;
                var externalApiUrl = config.Url;
                externalApis.Add("PTV-SlotManagementApi", new SlotManagementApi(externalApiUrl));
            }
            return (SlotManagementApi)externalApis["PTV-SlotManagementApi"];
        }

        private PtvSlotSchedulerResponse CreateSlotSchedulers(InternalUserInfos internalUserInfos, EfCaBoxMountingDevice boxMountingDevice)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug($"CreateSlotSchedulers(..) called.");
            }
            PtvModelCollector slotSchedulers = new PtvModelCollector();
            slotSchedulers.UserInfo = new PtvUserInfo()
            {
                Email = internalUserInfos.Email,
                TenantId = internalUserInfos.TenantId,
                Role = internalUserInfos.Role,
                InternalUserId = internalUserInfos.InternalUserId
            };

            slotSchedulers.SlotSchedulers = new List<PtvSlotScheduler>();
            SlotManagementApi api = GetSlotManagementApi(internalUserInfos);
            PtvSlotScheduler ptvSlotScheduler = new PtvSlotScheduler();
            ptvSlotScheduler.Description = "efeuCampus-0";
            ptvSlotScheduler.ExternalId = boxMountingDevice.Ident;
            slotSchedulers.SlotSchedulers.Add(ptvSlotScheduler);
            if (boxMountingDevice.MountCapacity == 2)
            {
                PtvSlotScheduler ptvSlotScheduler2 = new PtvSlotScheduler();
                ptvSlotScheduler2.Description = "efeuCampus-1";
                ptvSlotScheduler2.ExternalId = boxMountingDevice.Ident;

                slotSchedulers.SlotSchedulers.Add(ptvSlotScheduler2);
            }

            PtvSlotSchedulerResponse slotSchedulerResponse = api.CreateSlotScheduler(slotSchedulers);
            if (slotSchedulerResponse == null)
            {
                log.Error($"CreateSlotSchedulers(..) FAILED");
                slotSchedulerResponse = new PtvSlotSchedulerResponse();
                slotSchedulerResponse.ReturnInfo = ReturnValue.ERROR;
                slotSchedulerResponse.Message = "Creating the Slotschedulers failed, unknown reason";
            }

            if (log.IsDebugEnabled)
            {
                if (slotSchedulerResponse != null)
                {
                    log.Debug($"CreateSlotSchedulers(..) created: {slotSchedulerResponse.ToJson()}");
                }
            }
            return slotSchedulerResponse;
        }

        /// <summary>
        /// Diese Methode wird nur dazu verwendet um "große" Transportboxen zu reservieren
        /// </summary>
        /// <param name="api"></param>
        /// <param name="ptvUserInfo"></param>
        /// <param name="ident"></param>
        /// <param name="customerIdent"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="timeSlot"></param>
        /// <returns></returns>
        private PtvSlotReservationResponse CreateReservation(SlotManagementApi api, PtvUserInfo ptvUserInfo, string ident, string customerIdent,
            DateTimeOffset start, DateTimeOffset end, int timeSlot)
        {
            PtvModelCollector slotReservationsRequest = new PtvModelCollector();
            slotReservationsRequest.UserInfo = ptvUserInfo;

            slotReservationsRequest.SlotReservationRequest = new PtvSlotReservationRequest();
            slotReservationsRequest.SlotReservationRequest.Ident = ident;
            slotReservationsRequest.SlotReservationRequest.CustomerIdent = customerIdent;
            slotReservationsRequest.SlotReservationRequest.Start = start;
            slotReservationsRequest.SlotReservationRequest.End = end;
            slotReservationsRequest.SlotReservationRequest.TimeSlot = timeSlot;
            PtvSlotReservationResponseResponse ptvSlotSchedulerResponse = api.ReserveAvailableSlot(slotReservationsRequest);
            if (ptvSlotSchedulerResponse != null && ptvSlotSchedulerResponse.ReturnInfo == ReturnValue.SUCCESS && ptvSlotSchedulerResponse.Result.Count == 1)
            {
                return ptvSlotSchedulerResponse.Result[0];
            }

            return null;
        }

        private List<Location> ExportROWebAPILocations(List<EfCaBoxMountingDevice> boxMountingDevices, string actionCode, string taskFields)
        {
            List<Location> locationsList = new List<Location>();

            Location.ActionCodeEnum action;
            if (actionCode == "NEW")
            {
                action = Location.ActionCodeEnum.NEW;
            }
            else if (actionCode == "UPDATE")
            {
                action = Location.ActionCodeEnum.UPDATE;
            }
            else if (actionCode == "DELETE")
            {
                action = Location.ActionCodeEnum.DELETE;
            }
            else
            {
                return null;
            }


            foreach (EfCaBoxMountingDevice item in boxMountingDevices)
            {
                Location location = new Location(null, null, null, null, item.Ident, DateTime.Now, action, null);
                if (action == Location.ActionCodeEnum.DELETE)
                {

                }
                if (action == Location.ActionCodeEnum.NEW || action == Location.ActionCodeEnum.UPDATE)
                {
                    location.LocationHeader = new LocationHeader();
                    LocationHeader.FunctionEnum function = LocationHeader.FunctionEnum.CUSTOMER;

                    location.LocationHeader.Function = function;
                    location.LocationHeader.Taskfields = taskFields;
                    location.LocationHeader.Country = "DEU";
                    location.LocationHeader.ShortDescription = "BoxMountingDevice, capacity=" + item.MountCapacity;
                    location.LocationHeader.HandlingTimeClassPickup = 0;
                    location.LocationHeader.HandlingTimeClassDelivery = 0;
                    location.LocationHeader.Description = item.Info;

                    location.LocationHeader.CoordinateType = LocationHeader.CoordinateTypeEnum.GEODECIMAL;
                    location.LocationHeader.Latitude = item.Latitude;
                    location.LocationHeader.Longitude = item.Longitude;
                }
                locationsList.Add(location);
            }

            return locationsList;
        }

        internal EfCaBoxMountingDeviceResp WorkInProgess(InternalUserInfos internalUserInfos, int useCase, EfCaModelCollector workInProgess)
        {
            EfCaBoxMountingDeviceResp response = new EfCaBoxMountingDeviceResp();

            if (useCase == 1)
            {
                response.Error = true;
                response.ErrorMsg = $"Usecase({useCase}) is not implemented";
                return response;
            }
            else if (useCase == 2)
            {
                response.Error = true;
                response.ErrorMsg = $"Usecase({useCase}) is not implemented";
                return response;
            }
            else if (useCase == 3)
            {
                response.Error = true;
                response.ErrorMsg = $"Usecase({useCase}) is not implemented";
                return response;
            }
            else if (useCase == 4)
            {
                /*
                 * UseCase 4 wird dazu verwendet um ALLE Scheduler für ALLE Böcke im PTV Slotmanagement anzulegen
                 */
                EfCaBoxMountingDevice finder = new EfCaBoxMountingDevice();
                EfCaBoxMountingDeviceResp boxMountingDeviceResp = null;
                if (workInProgess.Idents != null)
                {
                    boxMountingDeviceResp = _mongoDBService.FindBoxMountingDevicesByFinder(internalUserInfos, null, workInProgess.Idents);
                }
                else if (workInProgess.BoxMountingDevices != null)
                {
                    response = new EfCaBoxMountingDeviceResp();
                    response.Error = true;
                    response.ErrorMsg = "Not implemented";

                    return response;
                }
                else
                {
                    boxMountingDeviceResp = _mongoDBService.FindBoxMountingDevicesByFinder(internalUserInfos, finder);
                }

                if (boxMountingDeviceResp.Error == false)
                {
                    PtvModelCollector slotSchedulers = new PtvModelCollector();
                    slotSchedulers.UserInfo = new PtvUserInfo()
                    {
                        Email = internalUserInfos.Email,
                        TenantId = internalUserInfos.TenantId,
                        Role = internalUserInfos.Role,
                        InternalUserId = internalUserInfos.InternalUserId
                    };

                    slotSchedulers.SlotSchedulers = new List<PtvSlotScheduler>();
                    SlotManagementApi api = GetSlotManagementApi(internalUserInfos);
                    foreach (EfCaBoxMountingDevice item in boxMountingDeviceResp.BoxMountingDevices)
                    {
                        PtvSlotScheduler ptvSlotScheduler = new PtvSlotScheduler();
                        ptvSlotScheduler.Description = "efeuCampus-0";
                        ptvSlotScheduler.ExternalId = item.Ident;
                        slotSchedulers.SlotSchedulers.Add(ptvSlotScheduler);
                        if (item.MountCapacity == 2)
                        {

                            PtvSlotScheduler ptvSlotScheduler2 = new PtvSlotScheduler();
                            ptvSlotScheduler2.Description = "efeuCampus-1";
                            ptvSlotScheduler2.ExternalId = item.Ident;

                            slotSchedulers.SlotSchedulers.Add(ptvSlotScheduler2);
                        }
                    }
                    PtvSlotSchedulerResponse slotSchedulerResponse = api.CreateSlotScheduler(slotSchedulers);
                    if (slotSchedulerResponse == null)
                    {
                        response = new EfCaBoxMountingDeviceResp();
                        response.Error = true;
                        response.ErrorMsg = "creation of the scheduler failed";

                        return response;
                    }
                }
            }
            else
            {
                response.Error = true;
                response.ErrorMsg = $"Usecase({useCase}) not supported";
            }
            return response;
        }
    }
}
