﻿using EfeuPortal.Models.Campus.Apartment;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using EfeuPortal.Services.DB.MongoDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Services.Campus
{
    public class ApartmentService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ApartmentService));

        private MongoDBSvc _mongoDBService;

        //private RabbitMQService _rabbitMQService;

        public ApartmentService(IConfiguration config)
        {
            _mongoDBService = new MongoDBSvc(config);
            //_rabbitMQService = new RabbitMQService(config);
        }

        public EfCaApartmentResp AddApartments(InternalUserInfos internalUserInfo, List<EfCaApartment> apartments)
        {
            EfCaApartmentResp response = null;
            foreach (EfCaApartment item in apartments)
            {
                item.Ident = Guid.NewGuid().ToString();
            }

            response = _mongoDBService.AddApartment(internalUserInfo, apartments);

            return response;
        }

        public EfCaApartmentResp FindApartmentsByFinder(InternalUserInfos internalUserInfo, EfCaApartment finder)
        {
            EfCaApartmentResp response = null;
            response = _mongoDBService.FindApartmentsByFinder(internalUserInfo, finder);

            return response;
        }

        public EfCaApartmentResp GetAllApartments(InternalUserInfos internalUserInfos)
        {
            EfCaApartmentResp response = null;
            response = _mongoDBService.ReadAllApartments(internalUserInfos);

            return response;
        }

        public EfCaApartmentResp PutApartment(InternalUserInfos internalUserInfos, EfCaApartment apartment)
        {
            EfCaApartmentResp response = null;
            response = _mongoDBService.UpdateApartment(internalUserInfos, apartment);

            return response;
        }

        /// <summary>
        /// Löschen eines Apartments aus der Datenbank.
        /// Vorher wird überprüft ob das Apartment einem Building zugeordnet ist, falls ja wird diese Relation aufgelöst.
        /// 
        /// ToDo(s):
        /// Relation zu building auflösen
        /// 
        /// Überlegen welche Fehlerquellen auftreten können und was wir dann an Fehlernachrichten zurückliefern.
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public EfCaApartmentResp DeleteApartment(InternalUserInfos internalUserInfos, List<string> ids)
        {
            EfCaApartmentResp response = null;
            response = _mongoDBService.DeleteApartments(internalUserInfos, ids);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="internalUserInfos"></param>
        /// <param name="action">ADD or REMOVE</param>
        /// <param name="type">BoxMountingDevice, Contact</param>
        /// <param name="ident">Id of the type object</param>
        /// <param name="apartment"></param>
        /// <returns></returns>
        public EfCaApartmentResp PutModifyApartmentReferences(InternalUserInfos internalUserInfos, string action, string type, string ident, EfCaApartment apartment)
        {
            EfCaApartmentResp response = null;
            response = _mongoDBService.PutModifyApartmentReferences(internalUserInfos, action, type, ident, apartment);

            return response;
        }

        private EfCaApartmentResp ModifyBoundingBoxReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaApartment apartmentDB)
        {
            EfCaApartmentResp response = null;
            response = _mongoDBService.ModifyBoundingBoxReference(internalUserInfos, action, ident, apartmentDB);
            return response;
        }

        private EfCaApartmentResp ModifyContactReference(InternalUserInfos internalUserInfos, string action, string ident, EfCaApartment apartmentDB)
        {
            EfCaApartmentResp response = null;
            response = _mongoDBService.ModifyContactReference(internalUserInfos, action, ident, apartmentDB);

            return response;
        }
    }
}
