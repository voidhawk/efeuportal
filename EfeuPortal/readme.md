﻿<h2> Beschreibung </h2>

<h2> Documentation(s)</h2>

-  [efeucampus](./Docs/efeuCampus.md)

-  [Order](./Docs/Order/EfCaOrder.md)
-  [Address](./Docs/Address/EfCaAddress.md)
-  [xServer](./Docs/xServer/xServer.md)

<h2> History </h2>

| Date | Version | Description | Changes | Who |
| ---- | ------- | ----------- | ------- | --- |
| 28.07.2021 | 4.1.0 | xServer API updated | JSt |
| 17.07.2020 | 1.3.1 | EfCaModelCollector extended. "List<string> Idents" added | JSt |
| 28.04.2020 | --- | Version deployed | JSt |
| 28.04.2020 | 1.1.0 | Order interface(s) modified | JSt |
| 23.04.2020 | 1.1.0 | Order interface(s) modified | JSt |
| 23.04.2020 | --- | Version deployed | JSt |
| 23.04.2020 | 1.0.1 | Order interface(s) modified | JSt |
| 23.04.2020 | 1.0.1 | Version now in Open API visible | JSt |
| 21.04.2020 | 1.0.0 | Initial Azure located version| JSt |

<h3> Template </h3>
|||||

<h2>hints</h2>
https://docs.microsoft.com/de-de/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.1&tabs=visual-studio