﻿## Table of Contents

- [EfCaOrder overview](#efcaorder-overview)
- [EfCaOrder dependencies](#efcaorder-dependencies)
- [UseCase 1: Paket wird im Depot angeliefert (asynchron)](./Usecase1.md)
- [UseCase 2: Wertstoff wird beim Kunden abgeholt (asynchron](./Usecase2.md)
- [UseCase 3: Paket wird im Depot angeliefert (synchron)](./Usecase3.md)
- [UseCase 4: Paket wird beim Kunden abgeholt (synchron)](./Usecase4.md)
- [UseCase 5: Paket wird vom Kunden im Depot abgeholt](./Usecase5.md)


## EfCaOrder overview
<img src="./Images/EfCaOrder.png" style="border:1px solid black" alt="EfCaOrderDB extends the base class EfCaOrder"></img>
## EfCaOrder dependencies
<img src="./Images/EfCaOrder-Details.png" style="border:1px solid black"></img>


### Usecase(s)
#### UseCase 1: Paket wird im Depot angeliefert (asynchron) => Prio 1
#### UseCase 2: Wertstoff wird beim Kunden abgeholt (asynchron) => Prio 2
#### UseCase 3: Paket wird im Depot angeliefert (synchron) => Prio 3
#### UseCase 4: Paket wird beim Kunden abgeholt (synchron) => Prio 4
#### UseCase 5: Paket wird vom Kunden im Depot abgeholt


### Bsp.: UseCase 4: Paket soll abgeholt werden
### Beschreibung
Ein Paket soll bei einem Anwohner abgeholt und versendet werden. 
Danach sind die folgenden Schritte bis zur Planung notwendig. 
1. Auftrag (A1) anlegen
2.
3.
