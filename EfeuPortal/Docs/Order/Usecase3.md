<h2> Beschreibung des Usecase 3</h2>
<div style="padding: 15px"  >
    <span style="background-color: #F8F9F9">
        Ein externer Dienstleister liefert ein Paket am Depot (Übergabestelle) ab. Während der Übergabe werden die Daten des Pakets werden automatisch erfasst. 
        <p style="background-color: #F8F9F9">Das Paket soll zu einem (bekannten) Kunden innerhalb des efeuCampus geliefert werden.</p>
    </span>
    <p>
        Um den Usecase zu vereinfachen wird immer von einem positiven Ergebnis ausgegangen. => Suche des Empfängers usw. ist erfolgreich, die Kundenzeitfenster sind konfiguriert und werden nicht mehr geändert.
    </p>
</div>

<h3>Die einzelnen Schritte, bis der Kunden sein Paket hat</h3>
<div style="padding:30px">
    Danach sind die folgenden Schritte bis zur Planung notwendig. 
    1. Auftrag (A1) anlegen
    2. Paket einlagern A1 aktualisieren und danach das ProcessMgmt triggern, damit die Aufträge weiter verarbeitet werden 
    3. Untergeordnete (Box)Aufträge erstellen (B1 und B2)
    4. Aufträge (B1) für die Planung freigeben
    5. Aufträge werden vom Planungssytem verplant und Touren exportiert
    6. Die untergeordneten (Box)Aufträge werden mit den Planungsergebnissen aktualisiert
    7. Tourbeginn, die Box wird ausgeliefert
    8. Die Box wird entladen
    9. Der Robi fährt zurück und liefert die Box ab
    10. Der Auftrag (A1) ist beendet
</div>
---
### Auftrag (A1) anlegen
<div style="padding:30px">

Ein Provider bringt ein Paket zur Übergabestation. Dort wird es automatisch erfasst und ein Auftrag angelegt.
* Addressdaten gelesen
* Paket Daten (Maße) ermittelt
* weitere Daten die verarbeitet werden können sind, ProviderId, Gefahrgut, inbound/outbound 
* ...

__OrderType__ = DELIVERY, __OrderUnit__ = PACKAGE

#### __OrderTimeSlot__
Start: Ankunft / Vereinnahmung am Depot
End: Definierter Wert bis wann der Auftrag erledigt sein muss (Das Paket an den Kunden übergeben sein muss)
#### __DeliveryTimeSlots__
Aus den Empfängerdaten kommen die synchronen/asynchronen Zeitfenster und werden im Parameter __DeliveryTimeSlots__ hinterlegt. 
#### __PickupTimeSlots__
Diese Zeitangaben sind vor der Planung nicht (komplett) bekannt, deshalb *NULL* lassen
#### __State__
????

[other md file](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md)

[html content](./html/Order/Usecases/Usecase3_Step1-2.html)
</div>
---

### Paket einlagern A1 aktualisieren
Der Depot Mitarbeiter nimmt das Paket und legt es im Lager ab, bis es zur Auslieferung wieder benötigt wird. Danach aktualisiert er A1 damit man zur Konfektionierung das Paket wieder findet
#### __Pickup__
Setzen der Parameter, wo das Paket abgelegt wurde
#### __PickupTimeSlots__
Start: Wenn das Paket eingelagert ist.
Ende: Da das Paket noch nicht verplant ist sollte man das Ende auf das Ende des OrderTimeSlots setzen
#### __State__
State =  NEW

---
### Untergeordnete (Box)Aufträge erstellen (B1 und B2)
Nachdem das Paket komplett erfasst und abgelegt wurde müssen jetzt die Box Aufträge erzeugt werden. Es gibt 2 Stk. da das Paket in einer Box geliefert und nach Leerung wieder abgeholt werden muss.
#### B1 (Lieferung von A1)
__OrderType__ = DELIVERY, __OrderUnit__ = PACKAGE_BOX
#### __DeliveryTimeSlots__
Da der Auftrag noch nicht verplant ist, stehen hier noch die __DeliveryTimeSlots__ aus A1
#### __PickupTimeSlots__
Da der Auftrag noch nicht verplant ist, ist der Abholtermin im Depot nicht bekannt, deshalb auf *NULL* lassen.
#### __State__
__State__ = NOT_READY_FOR_PLANNING
#### B2 (Abholung der leeren Box aus Auftrag A1)
__OrderType__ = PICKUP, __OrderUnit__ = PACKAGE_BOX
#### __DeliveryTimeSlots__
Da der Auftrag (Lieferung von A1/B1) noch nicht verplant ist, ist nicht bekannt wann die Box wieder im Depot ankommt, deshalb auf *NULL* lassen.
#### __PickupTimeSlots__
Da der Auftrag (Lieferung von A1/B1) noch nicht verplant ist, ist der Abholtermin beim Kunden nicht bekannt, deshalb auf *NULL* lassen.
#### __State__
__State__ = NOT_READY_FOR_PLANNING
#### Auftrag A1 mit den Relationen von B1 und B2 aktualisieren

### Auftrag (B1) für die Planung freigeben
Auf Aufträge werden dem Planungssystem übergeben, nach einer unbestimmten Zeit kommt ein Trigger und man muss die exportierten Touren verarbeiten
#### B1
__State__ = PLANNABLE

<span style="color:blue">
	Hier muss jetzt ein Trigger erfolgen, der B1 in das Planungsformat konvertiert und diesen dann in Richtung Smartour exportiert.
	Da B1 mehrere <strong>DeliveryTimeSlots</strong> enthalten kann, sind die <strong>PickupTimeSlots</strong> von B2 noch nicht "sicher". Deshalb macht es (meiner Meinung nach) keinen Sinn, diesen jetzt schon der Planung zu übergeben.
	Der State von B1 könnte jetzt auf <strong> WAIT_FOR_PLANNING_RESULT</strong> gesetzt werden.
</span>

---

### Auftrag (B1) wurde vom Planungssytem verplant und Tour exportiert
Ermitteln der Auftrags Ids in den geplanten Touren und suchen der Bx Aufträge
#### B1
__State__ = PLANNED
##### __PickupTimeSlots__
Daten aus der Planung nehmen und aktualisieren. Abholung am Depot
##### __DeliveryTimeSlots__
Daten aus der Planung nehmen und aktualisieren. Avisieren der Lieferung beim Kunden
#### B2
##### __DeliveryTimeSlots__
Da der Auftrag noch nicht verplant ist, ist der (genaue) Abholtermin beim Kunden nicht bekannt. Frühestens wäre es __DeliveryTimeSlots__ End
##### __PickupTimeSlots__
Daten aus der Planung nehmen und aktualisieren. Abholung beim Kunden (__DeliveryTimeSlots__)
__State__ = PLANNABLE
<span style="color:blue">
	Hier muss jetzt ein Trigger erfolgen, der B2 in das Planungsformat konvertiert und diesen dann in Richtung Smartour exportiert.
</span>

------
