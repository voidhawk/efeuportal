\contentsline {section}{\numberline {1}Scope of the document}{3}{section.1}%
\contentsline {section}{\numberline {2}Allgemeine Beschreibung}{4}{section.2}%
\contentsline {section}{\numberline {3}Entwicklung}{4}{section.3}%
\contentsline {section}{\numberline {4}Erster Start des "efeuPortal"}{4}{section.4}%
\contentsline {paragraph}{Login des EfeuAdministrator's}{5}{section*.3}%
\contentsline {subparagraph}{Login}{5}{section*.4}%
\contentsline {subsection}{\numberline {4.1}Tenant}{6}{subsection.4.1}%
\contentsline {paragraph}{Neuen Tenant anlegen}{7}{figure.4}%
\contentsline {paragraph}{User für die unterschiedlichen Rollen anlegen}{10}{figure.5}%
\contentsline {paragraph}{Stammdaten erzeugen}{11}{section*.8}%
\contentsline {subparagraph}{TransportBoxTypes}{11}{section*.9}%
\contentsline {subparagraph}{TransportBoxes}{11}{section*.10}%
\contentsline {subparagraph}{und viele mehr....}{11}{section*.11}%
\contentsline {subsection}{\numberline {4.2}Rechte und Rollen}{11}{subsection.4.2}%
\contentsline {paragraph}{Rollen und ihre Aufgabe}{11}{section*.12}%
\contentsline {subparagraph}{EfeuAdministrator}{11}{section*.13}%
\contentsline {subparagraph}{EfeuSystem}{11}{section*.14}%
\contentsline {subparagraph}{EfeuOperator}{11}{section*.15}%
\contentsline {subparagraph}{EfeuUser}{11}{section*.16}%
\contentsline {subparagraph}{EfeuHardware}{11}{section*.17}%
\contentsline {section}{\numberline {5}Testplan der Einzelkomponenten (PTV Group)}{12}{section.5}%
\contentsline {subsection}{\numberline {5.1}Adress}{12}{subsection.5.1}%
\contentsline {paragraph}{Eine neu Addresse erzeugen}{12}{section*.18}%
\contentsline {paragraph}{Addresse suchen}{12}{section*.19}%
\contentsline {paragraph}{Addresse editieren}{12}{section*.20}%
\contentsline {subsection}{\numberline {5.2}Contact}{12}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Building}{12}{subsection.5.3}%
\contentsline {paragraph}{Neue Buildings erzeugen}{12}{section*.21}%
\contentsline {subsection}{\numberline {5.4}BoxMountingDevice}{12}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}ChargingStation}{12}{subsection.5.5}%
\contentsline {subsection}{\numberline {5.6}KEP}{12}{subsection.5.6}%
\contentsline {subsection}{\numberline {5.7}SyncMeetingPoint}{12}{subsection.5.7}%
\contentsline {subsection}{\numberline {5.8}TransportBox}{12}{subsection.5.8}%
\contentsline {subsection}{\numberline {5.9}Vehicle}{12}{subsection.5.9}%
\contentsline {subsection}{\numberline {5.10}Order}{12}{subsection.5.10}%
\contentsline {subsection}{\numberline {5.11}Planning}{12}{subsection.5.11}%
\contentsline {subsection}{\numberline {5.12}xServer}{12}{subsection.5.12}%
\contentsline {subsection}{\numberline {5.13}reservation}{12}{subsection.5.13}%
