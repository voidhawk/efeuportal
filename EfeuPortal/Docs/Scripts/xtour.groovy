import com.ptvgroup.xserver.xtour.*;
import com.ptvgroup.xserver.exceptions.XServerException;

public class XTourGroovy extends XTourModuleAdapter {
    public ToursResponse planTours(PlanToursRequest request) throws XServerException {
        if(request.planToursOptions.calculationMode.toString().equals("EVALUATE") || request.inputPlan == null) {
            return super.planTours(request);
        } else {
            // find orders in inputPlan
            List<String> orderIds = new ArrayList();
            request.inputPlan.tours.each{ tour -> 
                tour.trips.each{ trip ->
                    trip.stops.each{ stop ->
                        stop.tasks.each{ task ->
                            if(task.taskType.toString().equals("PICKUP")) {
                                orderIds.add(task.orderId);
                            }
                        }
                    }
                }
            }
            // delete orders from orders array
            request.orders = request.orders.findAll{ item -> !orderIds.contains(item.id) };
            // delete inputPlan
            request.inputPlan = null;
            return super.planTours(request);
        }
    }
}