﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }

        public int ExpireInMinutes { get; set; }

        public string MiddlewareVersion { get; set; }

        public string DefaultAdminPwd { get; set; }

        public int SchemaVersion { get; set; }
    }
}
