﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Helpers
{
    public static class ExtensionMethods
    {
        ////aufrufende Methode
        //public IEnumerable<User> GetAll()
        //{
        //    return _users.WithoutPasswords();
        //}

        ////Extension
        //public static IEnumerable<User> WithoutPasswords(this IEnumerable<User> users)
        //{
        //    return users.Select(x => x.WithoutPassword());
        //}

        //public static User WithoutPassword(this User user)
        //{
        //    user.Password = null;
        //    return user;
        //}

        /// <summary>
        /// Extension method to clone using the NewtonSoft serializer.
        /// 
        /// https://stackoverflow.com/questions/78536/deep-cloning-objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T Clone<T>(this T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        /// <summary>
        /// Runded im Moment immer nur auf 5 Minuten auf
        /// https://stackoverflow.com/questions/5704818/round-up-c-sharp-timespan-to-5-minutes
        /// </summary>
        public static TimeSpan RoundTo(this TimeSpan timeSpan, int n)
        {
            double value = Math.Ceiling(timeSpan.TotalMinutes / n);
            return TimeSpan.FromMinutes(n * Math.Ceiling(timeSpan.TotalMinutes / n));
        }
    }
}
