﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Helpers
{
    public class DataHelper
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(DataHelper));

        public static string CreateFormattedDateTime(string date, string time, int addDays)
        {
            DateTime useDate = new DateTime(Convert.ToInt16(date.Substring(0, 4)), Convert.ToInt16(date.Substring(5, 2)), Convert.ToInt16(date.Substring(8, 2)),
                Convert.ToInt16(time.Substring(0, 2)), Convert.ToInt16(time.Substring(3, 2)), Convert.ToInt16(time.Substring(6, 2)));
            if (addDays > 0)
            {

                useDate.AddDays(addDays);
            }

            string formattedDateTime = useDate.ToString("yyyyMMddHHmmss");
            return formattedDateTime;
        }

        public static string CreateFormattedDateTime(string date, string time, int addDays, string dateTimeFormat)
        {
            DateTime useDate = new DateTime(Convert.ToInt16(date.Substring(0, 4)), Convert.ToInt16(date.Substring(5, 2)), Convert.ToInt16(date.Substring(8, 2)),
                Convert.ToInt16(time.Substring(0, 2)), Convert.ToInt16(time.Substring(3, 2)), Convert.ToInt16(time.Substring(6, 2)));
            if (addDays > 0)
            {

                useDate.AddDays(addDays);
            }

            string formattedDateTime = useDate.ToString(dateTimeFormat);
            return formattedDateTime;
        }

        public static string CreateFormattedTime(DateTimeOffset dateTimeOffset, int addDays, string dateTimeFormat)
        {
            if (addDays > 0)
            {
                dateTimeOffset.AddDays(addDays);
            }

            string formattedDateTime = dateTimeOffset.ToString(dateTimeFormat);
            return formattedDateTime;
        }

        public static int GetWeekNumber(DateTimeOffset dateTime)
        {
            int kw = ISOWeek.GetWeekOfYear(dateTime.Date);
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dateTime.Date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);
            if (kw != weekNum)
            {
                string errorMsg = $"DataHelper.GetWeekNumber({dateTime.ToUniversalTime()}, ISOWeek.GetWeekOfYear({kw}) differs to weekNum({weekNum})!";
                log.Error(errorMsg);
            }
            return weekNum;
        }

        public static string GetYearAndWeek(DateTimeOffset dateTime)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dateTime.Date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            int kw = ISOWeek.GetWeekOfYear(dateTime.Date);
            if (kw != weekNum)
            {
                string errorMsg = $"DataHelper.GetYearAndWeek({dateTime.ToUniversalTime()}, ISOWeek.GetWeekOfYear({kw}) differs to weekNum({weekNum})!";
                log.Error(errorMsg);
            }

            return $"{dateTime.Year}-{weekNum}";
        }

        public static DateTimeOffset GetSundayOfWeek(string calendarWeek)
        {
            string[] yearAndWeek = calendarWeek.Split("-");

            DateTimeOffset startOfweek = ISOWeek.ToDateTime(Convert.ToInt16(yearAndWeek[0]), Convert.ToInt16(yearAndWeek[1]), DayOfWeek.Sunday);
            DateTimeOffset utcTime = new DateTimeOffset(startOfweek.Year, startOfweek.Month, startOfweek.Day, startOfweek.Hour, startOfweek.Minute, startOfweek.Second, TimeSpan.Zero);

            return utcTime;
        }

        public static DateTimeOffset GetDateTimeAtStartIndex(DateTimeOffset dateTime, int startIndex, int intervall = 5)
        {
            int minutes = (startIndex) * intervall;
            DateTimeOffset newStartTime = new DateTimeOffset(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0, dateTime.Offset);
            newStartTime = newStartTime.AddMinutes(minutes);

            return newStartTime;
        }
    }
}
