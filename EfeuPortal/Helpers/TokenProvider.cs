﻿using EfeuPortal.Models.Shared;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EfeuPortal.Helpers
{
    public class TokenProvider
    {
        public static string CreateToken(string secret, string tenantId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, tenantId)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            string generatedToken = tokenHandler.WriteToken(token);

            return generatedToken;
        }

        public static string CreateUserDataJWTToken(string secret, int expireInMinutes, InternalUserInfos internalUserInfos)
        {

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var userSerialized = JsonConvert.SerializeObject(internalUserInfos);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, internalUserInfos.TenantId),
                    new Claim(ClaimTypes.UserData, $"{userSerialized}")
                    //new Claim(JwtRegisteredClaimNames.Jti, internalUserInfos.TenantId)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            //var userSerialized = JsonConvert.SerializeObject(internalUserInfos);
            //var tokenHandler = new JwtSecurityTokenHandler();
            //var key = Encoding.ASCII.GetBytes(secret);
            //var tokenDescriptor = new SecurityTokenDescriptor
            //{
            //    Subject = new ClaimsIdentity(new Claim[]
            //    {
            //        new Claim(ClaimTypes.Name, internalUserInfos.InternalUserId),
            //            new Claim(ClaimTypes.Email, $"{internalUserInfos.Email}"),
            //            new Claim(ClaimTypes.UserData, $"{userSerialized}"),
            //            new Claim(JwtRegisteredClaimNames.Jti, internalUserInfos.TenantId),
            //    }),
            //    Expires = DateTime.UtcNow.AddMinutes(expireInMinutes),
            //    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            //};
            //var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
