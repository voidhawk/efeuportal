﻿using EfeuPortal.Models.Shared;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EfeuPortal.Helpers
{
    public static class IHttpContextAccessorExtension
    {
        public static InternalUserInfos CurrentUserInfos(this IHttpContextAccessor httpContextAccessor)
        {
            InternalUserInfos internalUserInfos = null;
            bool? authorization = httpContextAccessor?.HttpContext?.Request?.Headers?.ContainsKey("Authorization");

            if (authorization.GetValueOrDefault(false))
            {
                var jwt = httpContextAccessor?.HttpContext?.Request.Headers?["Authorization"];
                string jwtToken = jwt.ToString().Substring(7);
                var handler = new JwtSecurityTokenHandler();
                var token = handler.ReadJwtToken(jwtToken);
                var userData = (token.Claims.FirstOrDefault(x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/userdata")).Value;
                internalUserInfos = JsonConvert.DeserializeObject<InternalUserInfos>(userData);
            }

            return internalUserInfos;
        }
    }
}
