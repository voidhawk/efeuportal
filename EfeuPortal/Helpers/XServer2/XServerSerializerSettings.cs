﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Helpers.XServer2
{
    public class XServerSerializerSettings : JsonSerializerSettings
    {
        public XServerSerializerSettings()
        {
            ContractResolver = new XServerContractResolver();
            Converters = new List<JsonConverter> { new XServerExpandoObjectConverter() };
        }
    }

    /// <summary>
    /// maps ObjectType to $type
    /// </summary>
    public class XServerContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var x = base.CreateProperty(member, memberSerialization);
            if (x.PropertyName == "objectType")
                x.PropertyName = "$type";
            return x;
        }
    }

    /// <summary>
    /// maps $type to ObjectType + Pascal casing
    /// see https://stackoverflow.com/questions/9247478/pascal-case-dynamic-properties-with-json-net
    /// </summary>
    public class XServerExpandoObjectConverter : JsonConverter
    {
        //CHANGED
        //the ExpandoObjectConverter needs this internal method so we have to copy it
        //from JsonReader.cs
        internal static bool IsPrimitiveToken(JsonToken token)
        {
            switch (token)
            {
                case JsonToken.Integer:
                case JsonToken.Float:
                case JsonToken.String:
                case JsonToken.Boolean:
                case JsonToken.Null:
                case JsonToken.Undefined:
                case JsonToken.Date:
                case JsonToken.Bytes:
                    return true;
                default:
                    return false;
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return ReadValue(reader);
        }

        private object ReadValue(JsonReader reader)
        {
            while (reader.TokenType == JsonToken.Comment)
            {
                if (!reader.Read())
                    throw new Exception("Unexpected end.");
            }

            switch (reader.TokenType)
            {
                case JsonToken.StartObject:
                    return ReadObject(reader);
                case JsonToken.StartArray:
                    return ReadList(reader);
                default:
                    //CHANGED
                    //call to static method declared inside this class
                    if (IsPrimitiveToken(reader.TokenType))
                        return reader.Value;

                    //CHANGED
                    //Use string.format instead of some util function declared inside JSON.NET
                    throw new Exception(string.Format(CultureInfo.InvariantCulture, "Unexpected token when converting ExpandoObject: {0}", reader.TokenType));
            }
        }

        private object ReadList(JsonReader reader)
        {
            IList<object> list = new List<object>();

            while (reader.Read())
            {
                switch (reader.TokenType)
                {
                    case JsonToken.Comment:
                        break;
                    default:
                        object v = ReadValue(reader);

                        list.Add(v);
                        break;
                    case JsonToken.EndArray:
                        return list;
                }
            }

            throw new Exception("Unexpected end.");
        }

        private static string ToPascalCase(string s)
        {
            if (string.IsNullOrEmpty(s) || !char.IsLower(s[0]))
                return s;

            string str = char.ToUpper(s[0], CultureInfo.InvariantCulture).ToString((IFormatProvider)CultureInfo.InvariantCulture);

            if (s.Length > 1)
                str = str + s.Substring(1);

            return str;
        }

        private object ReadObject(JsonReader reader)
        {
            IDictionary<string, object> expandoObject = new ExpandoObject();

            while (reader.Read())
            {
                switch (reader.TokenType)
                {
                    case JsonToken.PropertyName:
                        //CHANGED
                        //mapping $type to ObjectType
                        //added call to ToPascalCase extension method       
                        string propertyName = reader.Value.ToString();
                        if (propertyName == "$type")
                            propertyName = "ObjectType";
                        else
                            propertyName = ToPascalCase(propertyName);

                        if (!reader.Read())
                            throw new Exception("Unexpected end.");

                        object v = ReadValue(reader);

                        expandoObject[propertyName] = v;
                        break;
                    case JsonToken.Comment:
                        break;
                    case JsonToken.EndObject:
                        return expandoObject;
                }
            }

            throw new Exception("Unexpected end.");
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(ExpandoObject));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
