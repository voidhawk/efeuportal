/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// ChargingStationStatus
    /// </summary>
    [DataContract]
    public partial class ChargingStationStatus :  IEquatable<ChargingStationStatus>, IValidatableObject
    {
        /// <summary>
        /// Defines ChargingStationState
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum ChargingStationStateEnum
        {
            /// <summary>
            /// Enum Free for value: Free
            /// </summary>
            [EnumMember(Value = "Free")]
            Free = 1,

            /// <summary>
            /// Enum InUse for value: InUse
            /// </summary>
            [EnumMember(Value = "InUse")]
            InUse = 2,

            /// <summary>
            /// Enum Disrupted for value: Disrupted
            /// </summary>
            [EnumMember(Value = "Disrupted")]
            Disrupted = 3

        }

        /// <summary>
        /// Gets or Sets ChargingStationState
        /// </summary>
        [DataMember(Name="chargingStationState", EmitDefaultValue=false)]
        public ChargingStationStateEnum ChargingStationState { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChargingStationStatus" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected ChargingStationStatus() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChargingStationStatus" /> class.
        /// </summary>
        /// <param name="id">id (required).</param>
        /// <param name="chargingStationState">chargingStationState (required).</param>
        /// <param name="vehicleId">vehicleId.</param>
        /// <param name="timestamp">timestamp (required).</param>
        public ChargingStationStatus(string id = default(string), ChargingStationStateEnum chargingStationState = default(ChargingStationStateEnum), string vehicleId = default(string), DateTimeOffset timestamp = default(DateTimeOffset))
        {
            // to ensure "id" is required (not null)
            this.Id = id ?? throw new ArgumentNullException("id is a required property for ChargingStationStatus and cannot be null");
            this.ChargingStationState = chargingStationState;
            this.Timestamp = timestamp;
            this.VehicleId = vehicleId;
        }
        
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id", EmitDefaultValue=false)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets VehicleId
        /// </summary>
        [DataMember(Name="vehicleId", EmitDefaultValue=false)]
        public string VehicleId { get; set; }

        /// <summary>
        /// Gets or Sets Timestamp
        /// </summary>
        [DataMember(Name="timestamp", EmitDefaultValue=false)]
        public DateTimeOffset Timestamp { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ChargingStationStatus {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  ChargingStationState: ").Append(ChargingStationState).Append("\n");
            sb.Append("  VehicleId: ").Append(VehicleId).Append("\n");
            sb.Append("  Timestamp: ").Append(Timestamp).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as ChargingStationStatus);
        }

        /// <summary>
        /// Returns true if ChargingStationStatus instances are equal
        /// </summary>
        /// <param name="input">Instance of ChargingStationStatus to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ChargingStationStatus input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Id == input.Id ||
                    (this.Id != null &&
                    this.Id.Equals(input.Id))
                ) && 
                (
                    this.ChargingStationState == input.ChargingStationState ||
                    this.ChargingStationState.Equals(input.ChargingStationState)
                ) && 
                (
                    this.VehicleId == input.VehicleId ||
                    (this.VehicleId != null &&
                    this.VehicleId.Equals(input.VehicleId))
                ) && 
                (
                    this.Timestamp == input.Timestamp ||
                    (this.Timestamp != null &&
                    this.Timestamp.Equals(input.Timestamp))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Id != null)
                    hashCode = hashCode * 59 + this.Id.GetHashCode();
                hashCode = hashCode * 59 + this.ChargingStationState.GetHashCode();
                if (this.VehicleId != null)
                    hashCode = hashCode * 59 + this.VehicleId.GetHashCode();
                if (this.Timestamp != null)
                    hashCode = hashCode * 59 + this.Timestamp.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
