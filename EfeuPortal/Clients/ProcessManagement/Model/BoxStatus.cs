/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// BoxStatus
    /// </summary>
    [DataContract]
    public partial class BoxStatus :  IEquatable<BoxStatus>, IValidatableObject
    {
        /// <summary>
        /// Defines BoxState
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum BoxStateEnum
        {
            /// <summary>
            /// Enum Stored for value: Stored
            /// </summary>
            [EnumMember(Value = "Stored")]
            Stored = 1,

            /// <summary>
            /// Enum Idle for value: Idle
            /// </summary>
            [EnumMember(Value = "Idle")]
            Idle = 2,

            /// <summary>
            /// Enum InUse for value: InUse
            /// </summary>
            [EnumMember(Value = "InUse")]
            InUse = 3,

            /// <summary>
            /// Enum Deactivated for value: Deactivated
            /// </summary>
            [EnumMember(Value = "Deactivated")]
            Deactivated = 4

        }

        /// <summary>
        /// Gets or Sets BoxState
        /// </summary>
        [DataMember(Name="boxState", EmitDefaultValue=false)]
        public BoxStateEnum BoxState { get; set; }
        /// <summary>
        /// Gets or Sets BoxLoad
        /// </summary>
        [DataMember(Name="boxLoad", EmitDefaultValue=false)]
        public BoxLoadEnum BoxLoad { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="BoxStatus" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected BoxStatus() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="BoxStatus" /> class.
        /// </summary>
        /// <param name="id">id (required).</param>
        /// <param name="boxState">boxState (required).</param>
        /// <param name="boxLoad">boxLoad (required).</param>
        /// <param name="timestamp">timestamp (required).</param>
        /// <param name="vehicleId">vehicleId.</param>
        /// <param name="mountId">mountId.</param>
        /// <param name="orderId">orderId.</param>
        /// <param name="isInbound">isInbound.</param>
        public BoxStatus(string id = default(string), BoxStateEnum boxState = default(BoxStateEnum), BoxLoadEnum boxLoad = default(BoxLoadEnum), DateTimeOffset timestamp = default(DateTimeOffset), string vehicleId = default(string), string mountId = default(string), string orderId = default(string), bool? isInbound = default(bool?))
        {
            // to ensure "id" is required (not null)
            this.Id = id ?? throw new ArgumentNullException("id is a required property for BoxStatus and cannot be null");
            this.BoxState = boxState;
            this.BoxLoad = boxLoad;
            this.Timestamp = timestamp;
            this.VehicleId = vehicleId;
            this.MountId = mountId;
            this.OrderId = orderId;
            this.IsInbound = isInbound;
        }
        
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id", EmitDefaultValue=false)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets Timestamp
        /// </summary>
        [DataMember(Name="timestamp", EmitDefaultValue=false)]
        public DateTimeOffset Timestamp { get; set; }

        /// <summary>
        /// Gets or Sets VehicleId
        /// </summary>
        [DataMember(Name="vehicleId", EmitDefaultValue=false)]
        public string VehicleId { get; set; }

        /// <summary>
        /// Gets or Sets MountId
        /// </summary>
        [DataMember(Name="mountId", EmitDefaultValue=false)]
        public string MountId { get; set; }

        /// <summary>
        /// Gets or Sets OrderId
        /// </summary>
        [DataMember(Name="orderId", EmitDefaultValue=false)]
        public string OrderId { get; set; }

        /// <summary>
        /// Gets or Sets IsInbound
        /// </summary>
        [DataMember(Name="isInbound", EmitDefaultValue=true)]
        public bool? IsInbound { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class BoxStatus {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  BoxState: ").Append(BoxState).Append("\n");
            sb.Append("  BoxLoad: ").Append(BoxLoad).Append("\n");
            sb.Append("  Timestamp: ").Append(Timestamp).Append("\n");
            sb.Append("  VehicleId: ").Append(VehicleId).Append("\n");
            sb.Append("  MountId: ").Append(MountId).Append("\n");
            sb.Append("  OrderId: ").Append(OrderId).Append("\n");
            sb.Append("  IsInbound: ").Append(IsInbound).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as BoxStatus);
        }

        /// <summary>
        /// Returns true if BoxStatus instances are equal
        /// </summary>
        /// <param name="input">Instance of BoxStatus to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(BoxStatus input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Id == input.Id ||
                    (this.Id != null &&
                    this.Id.Equals(input.Id))
                ) && 
                (
                    this.BoxState == input.BoxState ||
                    this.BoxState.Equals(input.BoxState)
                ) && 
                (
                    this.BoxLoad == input.BoxLoad ||
                    this.BoxLoad.Equals(input.BoxLoad)
                ) && 
                (
                    this.Timestamp == input.Timestamp ||
                    (this.Timestamp != null &&
                    this.Timestamp.Equals(input.Timestamp))
                ) && 
                (
                    this.VehicleId == input.VehicleId ||
                    (this.VehicleId != null &&
                    this.VehicleId.Equals(input.VehicleId))
                ) && 
                (
                    this.MountId == input.MountId ||
                    (this.MountId != null &&
                    this.MountId.Equals(input.MountId))
                ) && 
                (
                    this.OrderId == input.OrderId ||
                    (this.OrderId != null &&
                    this.OrderId.Equals(input.OrderId))
                ) && 
                (
                    this.IsInbound == input.IsInbound ||
                    (this.IsInbound != null &&
                    this.IsInbound.Equals(input.IsInbound))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Id != null)
                    hashCode = hashCode * 59 + this.Id.GetHashCode();
                hashCode = hashCode * 59 + this.BoxState.GetHashCode();
                hashCode = hashCode * 59 + this.BoxLoad.GetHashCode();
                if (this.Timestamp != null)
                    hashCode = hashCode * 59 + this.Timestamp.GetHashCode();
                if (this.VehicleId != null)
                    hashCode = hashCode * 59 + this.VehicleId.GetHashCode();
                if (this.MountId != null)
                    hashCode = hashCode * 59 + this.MountId.GetHashCode();
                if (this.OrderId != null)
                    hashCode = hashCode * 59 + this.OrderId.GetHashCode();
                if (this.IsInbound != null)
                    hashCode = hashCode * 59 + this.IsInbound.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
