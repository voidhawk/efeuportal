/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// UnpackInboundBoxEvent
    /// </summary>
    [DataContract]
    public partial class UnpackInboundBoxEvent :  IEquatable<UnpackInboundBoxEvent>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnpackInboundBoxEvent" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected UnpackInboundBoxEvent() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="UnpackInboundBoxEvent" /> class.
        /// </summary>
        /// <param name="boxIdent">boxIdent (required).</param>
        public UnpackInboundBoxEvent(string boxIdent = default(string))
        {
            // to ensure "boxIdent" is required (not null)
            this.BoxIdent = boxIdent ?? throw new ArgumentNullException("boxIdent is a required property for UnpackInboundBoxEvent and cannot be null");
        }
        
        /// <summary>
        /// Gets or Sets BoxIdent
        /// </summary>
        [DataMember(Name="boxIdent", EmitDefaultValue=false)]
        public string BoxIdent { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class UnpackInboundBoxEvent {\n");
            sb.Append("  BoxIdent: ").Append(BoxIdent).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as UnpackInboundBoxEvent);
        }

        /// <summary>
        /// Returns true if UnpackInboundBoxEvent instances are equal
        /// </summary>
        /// <param name="input">Instance of UnpackInboundBoxEvent to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(UnpackInboundBoxEvent input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.BoxIdent == input.BoxIdent ||
                    (this.BoxIdent != null &&
                    this.BoxIdent.Equals(input.BoxIdent))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.BoxIdent != null)
                    hashCode = hashCode * 59 + this.BoxIdent.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
