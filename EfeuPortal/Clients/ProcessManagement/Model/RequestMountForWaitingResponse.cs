/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// RequestMountForWaitingResponse
    /// </summary>
    [DataContract]
    public partial class RequestMountForWaitingResponse :  IEquatable<RequestMountForWaitingResponse>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestMountForWaitingResponse" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected RequestMountForWaitingResponse() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestMountForWaitingResponse" /> class.
        /// </summary>
        /// <param name="success">success (required).</param>
        /// <param name="latitude">latitude.</param>
        /// <param name="longitude">longitude.</param>
        /// <param name="mountIdent">mountIdent.</param>
        public RequestMountForWaitingResponse(bool success = default(bool), float latitude = default(float), float longitude = default(float), string mountIdent = default(string))
        {
            this.Success = success;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.MountIdent = mountIdent;
        }
        
        /// <summary>
        /// Gets or Sets Success
        /// </summary>
        [DataMember(Name="success", EmitDefaultValue=false)]
        public bool Success { get; set; }

        /// <summary>
        /// Gets or Sets Latitude
        /// </summary>
        [DataMember(Name="latitude", EmitDefaultValue=false)]
        public float Latitude { get; set; }

        /// <summary>
        /// Gets or Sets Longitude
        /// </summary>
        [DataMember(Name="longitude", EmitDefaultValue=false)]
        public float Longitude { get; set; }

        /// <summary>
        /// Gets or Sets MountIdent
        /// </summary>
        [DataMember(Name="mountIdent", EmitDefaultValue=false)]
        public string MountIdent { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class RequestMountForWaitingResponse {\n");
            sb.Append("  Success: ").Append(Success).Append("\n");
            sb.Append("  Latitude: ").Append(Latitude).Append("\n");
            sb.Append("  Longitude: ").Append(Longitude).Append("\n");
            sb.Append("  MountIdent: ").Append(MountIdent).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as RequestMountForWaitingResponse);
        }

        /// <summary>
        /// Returns true if RequestMountForWaitingResponse instances are equal
        /// </summary>
        /// <param name="input">Instance of RequestMountForWaitingResponse to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(RequestMountForWaitingResponse input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Success == input.Success ||
                    this.Success.Equals(input.Success)
                ) && 
                (
                    this.Latitude == input.Latitude ||
                    this.Latitude.Equals(input.Latitude)
                ) && 
                (
                    this.Longitude == input.Longitude ||
                    this.Longitude.Equals(input.Longitude)
                ) && 
                (
                    this.MountIdent == input.MountIdent ||
                    (this.MountIdent != null &&
                    this.MountIdent.Equals(input.MountIdent))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                hashCode = hashCode * 59 + this.Success.GetHashCode();
                hashCode = hashCode * 59 + this.Latitude.GetHashCode();
                hashCode = hashCode * 59 + this.Longitude.GetHashCode();
                if (this.MountIdent != null)
                    hashCode = hashCode * 59 + this.MountIdent.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
