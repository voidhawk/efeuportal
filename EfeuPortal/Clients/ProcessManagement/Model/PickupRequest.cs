/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// PickupRequest
    /// </summary>
    [DataContract]
    public partial class PickupRequest :  IEquatable<PickupRequest>, IValidatableObject
    {
        /// <summary>
        /// Gets or Sets BoxType
        /// </summary>
        [DataMember(Name="boxType", EmitDefaultValue=false)]
        public BoxTypeEnum BoxType { get; set; }
        /// <summary>
        /// Gets or Sets BoxSize
        /// </summary>
        [DataMember(Name="boxSize", EmitDefaultValue=false)]
        public BoxSizeEnum BoxSize { get; set; }
        /// <summary>
        /// Gets or Sets OrderMode
        /// </summary>
        [DataMember(Name="orderMode", EmitDefaultValue=false)]
        public OrderModeEnum OrderMode { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="PickupRequest" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected PickupRequest() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="PickupRequest" /> class.
        /// </summary>
        /// <param name="boxType">boxType (required).</param>
        /// <param name="boxSize">boxSize (required).</param>
        /// <param name="desiredTimeWindow">desiredTimeWindow (required).</param>
        /// <param name="customerContactIdent">customerContactIdent (required).</param>
        /// <param name="orderMode">orderMode (required).</param>
        /// <param name="mountIdent">mountIdent.</param>
        /// <param name="syncMeetingPointIdent">syncMeetingPointIdent.</param>
        public PickupRequest(BoxTypeEnum boxType = default(BoxTypeEnum), BoxSizeEnum boxSize = default(BoxSizeEnum), TimeWindow desiredTimeWindow = default(TimeWindow), string customerContactIdent = default(string), OrderModeEnum orderMode = default(OrderModeEnum), string mountIdent = default(string), string syncMeetingPointIdent = default(string))
        {
            this.BoxType = boxType;
            this.BoxSize = boxSize;
            // to ensure "desiredTimeWindow" is required (not null)
            this.DesiredTimeWindow = desiredTimeWindow ?? throw new ArgumentNullException("desiredTimeWindow is a required property for PickupRequest and cannot be null");
            // to ensure "customerContactIdent" is required (not null)
            this.CustomerContactIdent = customerContactIdent ?? throw new ArgumentNullException("customerContactIdent is a required property for PickupRequest and cannot be null");
            this.OrderMode = orderMode;
            this.MountIdent = mountIdent;
            this.SyncMeetingPointIdent = syncMeetingPointIdent;
        }
        
        /// <summary>
        /// Gets or Sets DesiredTimeWindow
        /// </summary>
        [DataMember(Name="desiredTimeWindow", EmitDefaultValue=false)]
        public TimeWindow DesiredTimeWindow { get; set; }

        /// <summary>
        /// Gets or Sets CustomerContactIdent
        /// </summary>
        [DataMember(Name="customerContactIdent", EmitDefaultValue=false)]
        public string CustomerContactIdent { get; set; }

        /// <summary>
        /// Gets or Sets MountIdent
        /// </summary>
        [DataMember(Name="mountIdent", EmitDefaultValue=false)]
        public string MountIdent { get; set; }

        /// <summary>
        /// Gets or Sets SyncMeetingPointIdent
        /// </summary>
        [DataMember(Name="syncMeetingPointIdent", EmitDefaultValue=false)]
        public string SyncMeetingPointIdent { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PickupRequest {\n");
            sb.Append("  BoxType: ").Append(BoxType).Append("\n");
            sb.Append("  BoxSize: ").Append(BoxSize).Append("\n");
            sb.Append("  DesiredTimeWindow: ").Append(DesiredTimeWindow).Append("\n");
            sb.Append("  CustomerContactIdent: ").Append(CustomerContactIdent).Append("\n");
            sb.Append("  OrderMode: ").Append(OrderMode).Append("\n");
            sb.Append("  MountIdent: ").Append(MountIdent).Append("\n");
            sb.Append("  SyncMeetingPointIdent: ").Append(SyncMeetingPointIdent).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as PickupRequest);
        }

        /// <summary>
        /// Returns true if PickupRequest instances are equal
        /// </summary>
        /// <param name="input">Instance of PickupRequest to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PickupRequest input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.BoxType == input.BoxType ||
                    this.BoxType.Equals(input.BoxType)
                ) && 
                (
                    this.BoxSize == input.BoxSize ||
                    this.BoxSize.Equals(input.BoxSize)
                ) && 
                (
                    this.DesiredTimeWindow == input.DesiredTimeWindow ||
                    (this.DesiredTimeWindow != null &&
                    this.DesiredTimeWindow.Equals(input.DesiredTimeWindow))
                ) && 
                (
                    this.CustomerContactIdent == input.CustomerContactIdent ||
                    (this.CustomerContactIdent != null &&
                    this.CustomerContactIdent.Equals(input.CustomerContactIdent))
                ) && 
                (
                    this.OrderMode == input.OrderMode ||
                    this.OrderMode.Equals(input.OrderMode)
                ) && 
                (
                    this.MountIdent == input.MountIdent ||
                    (this.MountIdent != null &&
                    this.MountIdent.Equals(input.MountIdent))
                ) && 
                (
                    this.SyncMeetingPointIdent == input.SyncMeetingPointIdent ||
                    (this.SyncMeetingPointIdent != null &&
                    this.SyncMeetingPointIdent.Equals(input.SyncMeetingPointIdent))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                hashCode = hashCode * 59 + this.BoxType.GetHashCode();
                hashCode = hashCode * 59 + this.BoxSize.GetHashCode();
                if (this.DesiredTimeWindow != null)
                    hashCode = hashCode * 59 + this.DesiredTimeWindow.GetHashCode();
                if (this.CustomerContactIdent != null)
                    hashCode = hashCode * 59 + this.CustomerContactIdent.GetHashCode();
                hashCode = hashCode * 59 + this.OrderMode.GetHashCode();
                if (this.MountIdent != null)
                    hashCode = hashCode * 59 + this.MountIdent.GetHashCode();
                if (this.SyncMeetingPointIdent != null)
                    hashCode = hashCode * 59 + this.SyncMeetingPointIdent.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
