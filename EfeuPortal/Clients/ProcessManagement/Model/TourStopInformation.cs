/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// TourStopInformation
    /// </summary>
    [DataContract]
    public partial class TourStopInformation :  IEquatable<TourStopInformation>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TourStopInformation" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected TourStopInformation() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="TourStopInformation" /> class.
        /// </summary>
        /// <param name="latitude">latitude (required).</param>
        /// <param name="longitude">longitude (required).</param>
        /// <param name="actionPointInfos">actionPointInfos.</param>
        public TourStopInformation(float latitude = default(float), float longitude = default(float), List<TourActionPointInformation> actionPointInfos = default(List<TourActionPointInformation>))
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.ActionPointInfos = actionPointInfos;
        }
        
        /// <summary>
        /// Gets or Sets Latitude
        /// </summary>
        [DataMember(Name="latitude", EmitDefaultValue=false)]
        public float Latitude { get; set; }

        /// <summary>
        /// Gets or Sets Longitude
        /// </summary>
        [DataMember(Name="longitude", EmitDefaultValue=false)]
        public float Longitude { get; set; }

        /// <summary>
        /// Gets or Sets ActionPointInfos
        /// </summary>
        [DataMember(Name="actionPointInfos", EmitDefaultValue=false)]
        public List<TourActionPointInformation> ActionPointInfos { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class TourStopInformation {\n");
            sb.Append("  Latitude: ").Append(Latitude).Append("\n");
            sb.Append("  Longitude: ").Append(Longitude).Append("\n");
            sb.Append("  ActionPointInfos: ").Append(ActionPointInfos).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as TourStopInformation);
        }

        /// <summary>
        /// Returns true if TourStopInformation instances are equal
        /// </summary>
        /// <param name="input">Instance of TourStopInformation to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(TourStopInformation input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Latitude == input.Latitude ||
                    this.Latitude.Equals(input.Latitude)
                ) && 
                (
                    this.Longitude == input.Longitude ||
                    this.Longitude.Equals(input.Longitude)
                ) && 
                (
                    this.ActionPointInfos == input.ActionPointInfos ||
                    this.ActionPointInfos != null &&
                    input.ActionPointInfos != null &&
                    this.ActionPointInfos.SequenceEqual(input.ActionPointInfos)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                hashCode = hashCode * 59 + this.Latitude.GetHashCode();
                hashCode = hashCode * 59 + this.Longitude.GetHashCode();
                if (this.ActionPointInfos != null)
                    hashCode = hashCode * 59 + this.ActionPointInfos.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
