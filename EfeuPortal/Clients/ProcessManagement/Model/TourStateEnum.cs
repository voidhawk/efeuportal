/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// Defines TourStateEnum
    /// </summary>
    
    [JsonConverter(typeof(StringEnumConverter))]
    
    public enum TourStateEnum
    {
        /// <summary>
        /// Enum Planned for value: Planned
        /// </summary>
        [EnumMember(Value = "Planned")]
        Planned = 1,

        /// <summary>
        /// Enum Fixed for value: Fixed
        /// </summary>
        [EnumMember(Value = "Fixed")]
        Fixed = 2,

        /// <summary>
        /// Enum Finished for value: Finished
        /// </summary>
        [EnumMember(Value = "Finished")]
        Finished = 3,

        /// <summary>
        /// Enum Started for value: Started
        /// </summary>
        [EnumMember(Value = "Started")]
        Started = 4

    }

}
