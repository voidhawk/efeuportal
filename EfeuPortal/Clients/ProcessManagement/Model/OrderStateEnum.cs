/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// Defines OrderStateEnum
    /// </summary>
    
    [JsonConverter(typeof(StringEnumConverter))]
    
    public enum OrderStateEnum
    {
        /// <summary>
        /// Enum CHANGEABLE for value: CHANGEABLE
        /// </summary>
        [EnumMember(Value = "CHANGEABLE")]
        CHANGEABLE = 1,

        /// <summary>
        /// Enum DELIVERYINPREPARATION for value: DELIVERY_IN_PREPARATION
        /// </summary>
        [EnumMember(Value = "DELIVERY_IN_PREPARATION")]
        DELIVERYINPREPARATION = 2,

        /// <summary>
        /// Enum DELIVERYSCHEDULED for value: DELIVERY_SCHEDULED
        /// </summary>
        [EnumMember(Value = "DELIVERY_SCHEDULED")]
        DELIVERYSCHEDULED = 3,

        /// <summary>
        /// Enum TRANSPORTTOCUSTOMER for value: TRANSPORT_TO_CUSTOMER
        /// </summary>
        [EnumMember(Value = "TRANSPORT_TO_CUSTOMER")]
        TRANSPORTTOCUSTOMER = 4,

        /// <summary>
        /// Enum DELIVEREDTOMOUNT for value: DELIVERED_TO_MOUNT
        /// </summary>
        [EnumMember(Value = "DELIVERED_TO_MOUNT")]
        DELIVEREDTOMOUNT = 5,

        /// <summary>
        /// Enum ARRIVEDATSYNCSTOP for value: ARRIVED_AT_SYNC_STOP
        /// </summary>
        [EnumMember(Value = "ARRIVED_AT_SYNC_STOP")]
        ARRIVEDATSYNCSTOP = 6,

        /// <summary>
        /// Enum PICKUPSCHEDULED for value: PICKUP_SCHEDULED
        /// </summary>
        [EnumMember(Value = "PICKUP_SCHEDULED")]
        PICKUPSCHEDULED = 7,

        /// <summary>
        /// Enum TRANSPORTTODEPOT for value: TRANSPORT_TO_DEPOT
        /// </summary>
        [EnumMember(Value = "TRANSPORT_TO_DEPOT")]
        TRANSPORTTODEPOT = 8,

        /// <summary>
        /// Enum DELIVEREDTODEPOT for value: DELIVERED_TO_DEPOT
        /// </summary>
        [EnumMember(Value = "DELIVERED_TO_DEPOT")]
        DELIVEREDTODEPOT = 9,

        /// <summary>
        /// Enum DELIVERYFAILED for value: DELIVERY_FAILED
        /// </summary>
        [EnumMember(Value = "DELIVERY_FAILED")]
        DELIVERYFAILED = 10,

        /// <summary>
        /// Enum FINISHED for value: FINISHED
        /// </summary>
        [EnumMember(Value = "FINISHED")]
        FINISHED = 11,

        /// <summary>
        /// Enum EXPIRED for value: EXPIRED
        /// </summary>
        [EnumMember(Value = "EXPIRED")]
        EXPIRED = 12

    }

}
