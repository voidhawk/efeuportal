/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// SyncStopStateResponse
    /// </summary>
    [DataContract]
    public partial class SyncStopStateResponse :  IEquatable<SyncStopStateResponse>, IValidatableObject
    {
        /// <summary>
        /// Defines SyncStopState
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum SyncStopStateEnum
        {
            /// <summary>
            /// Enum Finished for value: Finished
            /// </summary>
            [EnumMember(Value = "Finished")]
            Finished = 1,

            /// <summary>
            /// Enum NotFinished for value: NotFinished
            /// </summary>
            [EnumMember(Value = "NotFinished")]
            NotFinished = 2

        }

        /// <summary>
        /// Gets or Sets SyncStopState
        /// </summary>
        [DataMember(Name="syncStopState", EmitDefaultValue=false)]
        public SyncStopStateEnum SyncStopState { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="SyncStopStateResponse" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected SyncStopStateResponse() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="SyncStopStateResponse" /> class.
        /// </summary>
        /// <param name="syncStopState">syncStopState (required).</param>
        public SyncStopStateResponse(SyncStopStateEnum syncStopState = default(SyncStopStateEnum))
        {
            this.SyncStopState = syncStopState;
        }
        
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class SyncStopStateResponse {\n");
            sb.Append("  SyncStopState: ").Append(SyncStopState).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as SyncStopStateResponse);
        }

        /// <summary>
        /// Returns true if SyncStopStateResponse instances are equal
        /// </summary>
        /// <param name="input">Instance of SyncStopStateResponse to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(SyncStopStateResponse input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.SyncStopState == input.SyncStopState ||
                    this.SyncStopState.Equals(input.SyncStopState)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                hashCode = hashCode * 59 + this.SyncStopState.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
