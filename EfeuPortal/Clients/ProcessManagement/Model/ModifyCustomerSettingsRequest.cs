/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// ModifyCustomerSettingsRequest
    /// </summary>
    [DataContract]
    public partial class ModifyCustomerSettingsRequest :  IEquatable<ModifyCustomerSettingsRequest>, IValidatableObject
    {
        /// <summary>
        /// Gets or Sets DefaultOrderMode
        /// </summary>
        [DataMember(Name="defaultOrderMode", EmitDefaultValue=false)]
        public OrderModeEnum? DefaultOrderMode { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ModifyCustomerSettingsRequest" /> class.
        /// </summary>
        /// <param name="defaultOrderMode">defaultOrderMode.</param>
        /// <param name="syncAvailabilityTimeSlots">syncAvailabilityTimeSlots.</param>
        /// <param name="asyncAvailabilityTimeSlots">asyncAvailabilityTimeSlots.</param>
        public ModifyCustomerSettingsRequest(OrderModeEnum? defaultOrderMode = default(OrderModeEnum?), List<DayTimeSlots> syncAvailabilityTimeSlots = default(List<DayTimeSlots>), List<DayTimeSlots> asyncAvailabilityTimeSlots = default(List<DayTimeSlots>))
        {
            this.DefaultOrderMode = defaultOrderMode;
            this.SyncAvailabilityTimeSlots = syncAvailabilityTimeSlots;
            this.AsyncAvailabilityTimeSlots = asyncAvailabilityTimeSlots;
        }
        
        /// <summary>
        /// Gets or Sets SyncAvailabilityTimeSlots
        /// </summary>
        [DataMember(Name="syncAvailabilityTimeSlots", EmitDefaultValue=false)]
        public List<DayTimeSlots> SyncAvailabilityTimeSlots { get; set; }

        /// <summary>
        /// Gets or Sets AsyncAvailabilityTimeSlots
        /// </summary>
        [DataMember(Name="asyncAvailabilityTimeSlots", EmitDefaultValue=false)]
        public List<DayTimeSlots> AsyncAvailabilityTimeSlots { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ModifyCustomerSettingsRequest {\n");
            sb.Append("  DefaultOrderMode: ").Append(DefaultOrderMode).Append("\n");
            sb.Append("  SyncAvailabilityTimeSlots: ").Append(SyncAvailabilityTimeSlots).Append("\n");
            sb.Append("  AsyncAvailabilityTimeSlots: ").Append(AsyncAvailabilityTimeSlots).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as ModifyCustomerSettingsRequest);
        }

        /// <summary>
        /// Returns true if ModifyCustomerSettingsRequest instances are equal
        /// </summary>
        /// <param name="input">Instance of ModifyCustomerSettingsRequest to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ModifyCustomerSettingsRequest input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DefaultOrderMode == input.DefaultOrderMode ||
                    this.DefaultOrderMode.Equals(input.DefaultOrderMode)
                ) && 
                (
                    this.SyncAvailabilityTimeSlots == input.SyncAvailabilityTimeSlots ||
                    this.SyncAvailabilityTimeSlots != null &&
                    input.SyncAvailabilityTimeSlots != null &&
                    this.SyncAvailabilityTimeSlots.SequenceEqual(input.SyncAvailabilityTimeSlots)
                ) && 
                (
                    this.AsyncAvailabilityTimeSlots == input.AsyncAvailabilityTimeSlots ||
                    this.AsyncAvailabilityTimeSlots != null &&
                    input.AsyncAvailabilityTimeSlots != null &&
                    this.AsyncAvailabilityTimeSlots.SequenceEqual(input.AsyncAvailabilityTimeSlots)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                hashCode = hashCode * 59 + this.DefaultOrderMode.GetHashCode();
                if (this.SyncAvailabilityTimeSlots != null)
                    hashCode = hashCode * 59 + this.SyncAvailabilityTimeSlots.GetHashCode();
                if (this.AsyncAvailabilityTimeSlots != null)
                    hashCode = hashCode * 59 + this.AsyncAvailabilityTimeSlots.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
