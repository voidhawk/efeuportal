/* 
 * Process management
 *
 * Process management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = EfeuPortal.Clients.ProcessManagement.Client.OpenAPIDateConverter;

namespace EfeuPortal.Clients.ProcessManagement.Model
{
    /// <summary>
    /// SimulationConfig
    /// </summary>
    [DataContract]
    public partial class SimulationConfig :  IEquatable<SimulationConfig>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimulationConfig" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected SimulationConfig() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="SimulationConfig" /> class.
        /// </summary>
        /// <param name="planningFrequencyMinutes">planningFrequencyMinutes (required).</param>
        /// <param name="boxDecommissioningTimeMinutes">boxDecommissioningTimeMinutes (required).</param>
        /// <param name="customerActionTimeAsyncMinutes">customerActionTimeAsyncMinutes (required).</param>
        /// <param name="customerActionTimeSyncMinutes">customerActionTimeSyncMinutes (required).</param>
        /// <param name="numDays">numDays (required).</param>
        /// <param name="numPickupOrdersPerDay">numPickupOrdersPerDay (required).</param>
        /// <param name="numPackagesPerDay">numPackagesPerDay (required).</param>
        /// <param name="largePackagesShare">largePackagesShare (required).</param>
        /// <param name="orderTypes">orderTypes (required).</param>
        /// <param name="orderModes">orderModes (required).</param>
        /// <param name="kepTimes">kepTimes (required).</param>
        /// <param name="scenarioFilename">scenarioFilename (required).</param>
        public SimulationConfig(int planningFrequencyMinutes = default(int), int boxDecommissioningTimeMinutes = default(int), int customerActionTimeAsyncMinutes = default(int), int customerActionTimeSyncMinutes = default(int), int numDays = default(int), int numPickupOrdersPerDay = default(int), int numPackagesPerDay = default(int), string largePackagesShare = default(string), string orderTypes = default(string), string orderModes = default(string), string kepTimes = default(string), string scenarioFilename = default(string))
        {
            this.PlanningFrequencyMinutes = planningFrequencyMinutes;
            this.BoxDecommissioningTimeMinutes = boxDecommissioningTimeMinutes;
            this.CustomerActionTimeAsyncMinutes = customerActionTimeAsyncMinutes;
            this.CustomerActionTimeSyncMinutes = customerActionTimeSyncMinutes;
            this.NumDays = numDays;
            this.NumPickupOrdersPerDay = numPickupOrdersPerDay;
            this.NumPackagesPerDay = numPackagesPerDay;
            // to ensure "largePackagesShare" is required (not null)
            this.LargePackagesShare = largePackagesShare ?? throw new ArgumentNullException("largePackagesShare is a required property for SimulationConfig and cannot be null");
            // to ensure "orderTypes" is required (not null)
            this.OrderTypes = orderTypes ?? throw new ArgumentNullException("orderTypes is a required property for SimulationConfig and cannot be null");
            // to ensure "orderModes" is required (not null)
            this.OrderModes = orderModes ?? throw new ArgumentNullException("orderModes is a required property for SimulationConfig and cannot be null");
            // to ensure "kepTimes" is required (not null)
            this.KepTimes = kepTimes ?? throw new ArgumentNullException("kepTimes is a required property for SimulationConfig and cannot be null");
            // to ensure "scenarioFilename" is required (not null)
            this.ScenarioFilename = scenarioFilename ?? throw new ArgumentNullException("scenarioFilename is a required property for SimulationConfig and cannot be null");
        }
        
        /// <summary>
        /// Gets or Sets PlanningFrequencyMinutes
        /// </summary>
        [DataMember(Name="planningFrequencyMinutes", EmitDefaultValue=false)]
        public int PlanningFrequencyMinutes { get; set; }

        /// <summary>
        /// Gets or Sets BoxDecommissioningTimeMinutes
        /// </summary>
        [DataMember(Name="boxDecommissioningTimeMinutes", EmitDefaultValue=false)]
        public int BoxDecommissioningTimeMinutes { get; set; }

        /// <summary>
        /// Gets or Sets CustomerActionTimeAsyncMinutes
        /// </summary>
        [DataMember(Name="customerActionTimeAsyncMinutes", EmitDefaultValue=false)]
        public int CustomerActionTimeAsyncMinutes { get; set; }

        /// <summary>
        /// Gets or Sets CustomerActionTimeSyncMinutes
        /// </summary>
        [DataMember(Name="customerActionTimeSyncMinutes", EmitDefaultValue=false)]
        public int CustomerActionTimeSyncMinutes { get; set; }

        /// <summary>
        /// Gets or Sets NumDays
        /// </summary>
        [DataMember(Name="numDays", EmitDefaultValue=false)]
        public int NumDays { get; set; }

        /// <summary>
        /// Gets or Sets NumPickupOrdersPerDay
        /// </summary>
        [DataMember(Name="numPickupOrdersPerDay", EmitDefaultValue=false)]
        public int NumPickupOrdersPerDay { get; set; }

        /// <summary>
        /// Gets or Sets NumPackagesPerDay
        /// </summary>
        [DataMember(Name="numPackagesPerDay", EmitDefaultValue=false)]
        public int NumPackagesPerDay { get; set; }

        /// <summary>
        /// Gets or Sets LargePackagesShare
        /// </summary>
        [DataMember(Name="largePackagesShare", EmitDefaultValue=false)]
        public string LargePackagesShare { get; set; }

        /// <summary>
        /// Gets or Sets OrderTypes
        /// </summary>
        [DataMember(Name="orderTypes", EmitDefaultValue=false)]
        public string OrderTypes { get; set; }

        /// <summary>
        /// Gets or Sets OrderModes
        /// </summary>
        [DataMember(Name="orderModes", EmitDefaultValue=false)]
        public string OrderModes { get; set; }

        /// <summary>
        /// Gets or Sets KepTimes
        /// </summary>
        [DataMember(Name="kepTimes", EmitDefaultValue=false)]
        public string KepTimes { get; set; }

        /// <summary>
        /// Gets or Sets ScenarioFilename
        /// </summary>
        [DataMember(Name="scenarioFilename", EmitDefaultValue=false)]
        public string ScenarioFilename { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class SimulationConfig {\n");
            sb.Append("  PlanningFrequencyMinutes: ").Append(PlanningFrequencyMinutes).Append("\n");
            sb.Append("  BoxDecommissioningTimeMinutes: ").Append(BoxDecommissioningTimeMinutes).Append("\n");
            sb.Append("  CustomerActionTimeAsyncMinutes: ").Append(CustomerActionTimeAsyncMinutes).Append("\n");
            sb.Append("  CustomerActionTimeSyncMinutes: ").Append(CustomerActionTimeSyncMinutes).Append("\n");
            sb.Append("  NumDays: ").Append(NumDays).Append("\n");
            sb.Append("  NumPickupOrdersPerDay: ").Append(NumPickupOrdersPerDay).Append("\n");
            sb.Append("  NumPackagesPerDay: ").Append(NumPackagesPerDay).Append("\n");
            sb.Append("  LargePackagesShare: ").Append(LargePackagesShare).Append("\n");
            sb.Append("  OrderTypes: ").Append(OrderTypes).Append("\n");
            sb.Append("  OrderModes: ").Append(OrderModes).Append("\n");
            sb.Append("  KepTimes: ").Append(KepTimes).Append("\n");
            sb.Append("  ScenarioFilename: ").Append(ScenarioFilename).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as SimulationConfig);
        }

        /// <summary>
        /// Returns true if SimulationConfig instances are equal
        /// </summary>
        /// <param name="input">Instance of SimulationConfig to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(SimulationConfig input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.PlanningFrequencyMinutes == input.PlanningFrequencyMinutes ||
                    this.PlanningFrequencyMinutes.Equals(input.PlanningFrequencyMinutes)
                ) && 
                (
                    this.BoxDecommissioningTimeMinutes == input.BoxDecommissioningTimeMinutes ||
                    this.BoxDecommissioningTimeMinutes.Equals(input.BoxDecommissioningTimeMinutes)
                ) && 
                (
                    this.CustomerActionTimeAsyncMinutes == input.CustomerActionTimeAsyncMinutes ||
                    this.CustomerActionTimeAsyncMinutes.Equals(input.CustomerActionTimeAsyncMinutes)
                ) && 
                (
                    this.CustomerActionTimeSyncMinutes == input.CustomerActionTimeSyncMinutes ||
                    this.CustomerActionTimeSyncMinutes.Equals(input.CustomerActionTimeSyncMinutes)
                ) && 
                (
                    this.NumDays == input.NumDays ||
                    this.NumDays.Equals(input.NumDays)
                ) && 
                (
                    this.NumPickupOrdersPerDay == input.NumPickupOrdersPerDay ||
                    this.NumPickupOrdersPerDay.Equals(input.NumPickupOrdersPerDay)
                ) && 
                (
                    this.NumPackagesPerDay == input.NumPackagesPerDay ||
                    this.NumPackagesPerDay.Equals(input.NumPackagesPerDay)
                ) && 
                (
                    this.LargePackagesShare == input.LargePackagesShare ||
                    (this.LargePackagesShare != null &&
                    this.LargePackagesShare.Equals(input.LargePackagesShare))
                ) && 
                (
                    this.OrderTypes == input.OrderTypes ||
                    (this.OrderTypes != null &&
                    this.OrderTypes.Equals(input.OrderTypes))
                ) && 
                (
                    this.OrderModes == input.OrderModes ||
                    (this.OrderModes != null &&
                    this.OrderModes.Equals(input.OrderModes))
                ) && 
                (
                    this.KepTimes == input.KepTimes ||
                    (this.KepTimes != null &&
                    this.KepTimes.Equals(input.KepTimes))
                ) && 
                (
                    this.ScenarioFilename == input.ScenarioFilename ||
                    (this.ScenarioFilename != null &&
                    this.ScenarioFilename.Equals(input.ScenarioFilename))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                hashCode = hashCode * 59 + this.PlanningFrequencyMinutes.GetHashCode();
                hashCode = hashCode * 59 + this.BoxDecommissioningTimeMinutes.GetHashCode();
                hashCode = hashCode * 59 + this.CustomerActionTimeAsyncMinutes.GetHashCode();
                hashCode = hashCode * 59 + this.CustomerActionTimeSyncMinutes.GetHashCode();
                hashCode = hashCode * 59 + this.NumDays.GetHashCode();
                hashCode = hashCode * 59 + this.NumPickupOrdersPerDay.GetHashCode();
                hashCode = hashCode * 59 + this.NumPackagesPerDay.GetHashCode();
                if (this.LargePackagesShare != null)
                    hashCode = hashCode * 59 + this.LargePackagesShare.GetHashCode();
                if (this.OrderTypes != null)
                    hashCode = hashCode * 59 + this.OrderTypes.GetHashCode();
                if (this.OrderModes != null)
                    hashCode = hashCode * 59 + this.OrderModes.GetHashCode();
                if (this.KepTimes != null)
                    hashCode = hashCode * 59 + this.KepTimes.GetHashCode();
                if (this.ScenarioFilename != null)
                    hashCode = hashCode * 59 + this.ScenarioFilename.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
