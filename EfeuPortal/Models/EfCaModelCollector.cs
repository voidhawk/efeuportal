﻿using EfeuPortal.Models.Campus.Apartment;
using EfeuPortal.Models.Campus.Box;
using EfeuPortal.Models.Campus.Building;
using EfeuPortal.Models.Campus.ChargingStation;
using EfeuPortal.Models.Campus.Contact;
using EfeuPortal.Models.Campus.Kep;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Campus.Place;
using EfeuPortal.Models.Campus.SyncMeetingPoint;
using EfeuPortal.Models.Campus.Vehicle;
using EfeuPortal.Models.Campus.Warehouse;
using EfeuPortal.Models.ROPlanning;
using EfeuPortal.Models.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models
{
    public class EfCaModelCollector
    {
        /// <summary>
        /// The parameter "Idents" contains the unique id(s) of objects. Which object
        /// depends on the called method. 
        /// </summary>
        public List<string> Idents { get; set; }

        public List<EfCaAddress> Addresses { get; set; }

        public List<EfCaApartment> Apartments { get; set; }

        public List<EfCaTransportBox> TransportBoxes { get; set; }

        public List<EfCaTransportBoxType> TransportBoxTypes { get; set; }

        public List<EfCaBuilding> Buildings { get; set; }

        public List<EfCaChargingStation> ChargingStations { get; set; }

        public List<EfCaContact> Contacts { get; set; }

        public List<EfCaUser> Users { get; set; }

        public List<EfCaServiceProvider> ServiceProviders { get; set; }

        public List<EfCaBoxMountingDevice> BoxMountingDevices { get; set; }

        public List<EfCaOrder> Orders { get; set; }

        public List<EfCaImageDocumentation> ImageDocumentation { get; set; }

        public List<WebAPIOrder> WebApiOrders { get; set; }

        public List<EfCaSyncMeetingPoint> SyncMeetingPoints { get; set; }

        public List<EfCaVehicle> Vehicles { get; set; }

        public List<EfCaTourFixation> TourFixations { get; set; }

        public List<EfCaSlotReservation> SlotReservations { get; set; }

        public List<EfCaSlotReservationRequest> SlotReservationRequests { get; set; }

        public List<EfCaScheduledBoxMountingSlot> ScheduledBoxMountSlots { get; set; }

        public List<EfCaWarehousePlace> WarehousePlaces { get; set; }
    }
}
