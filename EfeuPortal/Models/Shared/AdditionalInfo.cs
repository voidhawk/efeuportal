﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Shared
{
    public class AdditionalInfo
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
