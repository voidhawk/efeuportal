﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Shared
{
    public class ExtIdDetail
    {
        public string Originator { get; set; }

        public string ExtId { get; set; }

        public bool Owner { get; set; }
    }
}
