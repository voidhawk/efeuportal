﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Shared
{
    public class InternalUserInfos
    {
        public string TenantId { get; set; }

        public string InternalUserId { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }
    }
}
