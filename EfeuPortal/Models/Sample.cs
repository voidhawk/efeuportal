﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models
{
    public class Sample
    {
        /// <summary>
        /// Das ist eine Beispiel parameter und wird verwendet für....
        /// </summary>
        public string SampleParameter { get; set; }
    }
}
