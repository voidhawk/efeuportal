﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models
{
    public class Response
    {
        public bool Error { get; set; }

        public string ErrorMsg { get; set; }

        public List<DetailInfo> DetailInfos { get; private set; }

        public void AddDetailInfos(DetailInfo detailInfo)
        {
            if(DetailInfos == null)
            {
                DetailInfos = new List<DetailInfo>();
            }
            DetailInfos.Add(detailInfo);
        }

        public void AddDetailInfos(List<DetailInfo> detailInfos)
        {
            if (detailInfos == null)
            {
                return;
            }
            
            if (DetailInfos == null)
            {
                DetailInfos = new List<DetailInfo>();
            }
            DetailInfos.AddRange(detailInfos);
        }
    }

    /*
     * JSt: ToDo: 
     * - Fehler mit aufnehmen => Error: bool oder enum
     * - KeyValue mit aufnehmen
     */
    public class DetailInfo
    {
        public string Ident { get; set; }

        public string Detail { get; set; }
    }
}
