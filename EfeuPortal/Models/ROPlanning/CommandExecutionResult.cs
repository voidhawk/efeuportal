﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.ROPlanning
{
    public class CommandExecutionResultssssss
    {
        public string[] Messages { get; set; }

        /// <summary>
        /// Gets or sets the exception that occurred during the command execution.
        /// Will be null if the execution succeeded.
        /// </summary>
        public Exception Error { get; set; }

        /// <summary>
        /// Gets or sets the trace level that will be used to write the summary to the event log.
        /// </summary>
        public string ErrorLevel { get; set; }

        /// <summary>
        /// Shutdown the Daemon
        /// </summary>
        public bool ShutDownDomain { get; set; }

        /// <summary>
        /// An entry that will be written into the job execution history or null if no entry should be written.
        /// </summary>
        public string HistoryEntry { get; set; }
    }
}
