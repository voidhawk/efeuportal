﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.ROPlanning
{
    public class EfCaTourFixation
    {
        public string PlanningId { get; set; }
        public string TourId { get; set; }
        public bool Fix { get; set; }
    }
}
