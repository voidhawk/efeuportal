﻿using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Campus.TourData;
using System.Collections.Generic;

namespace EfeuPortal.Models.ROPlanning
{
    public class EfCaPlanningResp : Response
    {
        public List<EfCaTour> PlannedTours { get; set; }
        public List<string> DeletedTours { get; set; }
        public List<EfCaOrder> Orders { get; set; }

        public EfCaPlanningResp()
        {
            Error = false;
            PlannedTours = new List<EfCaTour>();
            DeletedTours = new List<string>();
        }
    }
}
