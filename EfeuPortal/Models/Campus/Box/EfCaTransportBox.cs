﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Box
{
    public class EfCaTransportBox
    {
        public string Ident { get; set; }

        /// <summary>
        /// Wertstoff, Paket
        /// </summary>
        public string TransportBoxTypeId { get; set; }

        /// <summary>
        /// Maximum
        /// Einheit: kWh, Maximale Batteriekapazität)
        /// </summary>
        public double BatteryCapacity { get; set; }

        public string IPAddress { get; set; }

        public int? Version { get; set; }

        public string Name { get; set; }
        
        /// <summary>
        /// Pareva-Schloss LockerId
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/68
        /// </summary>
        public string LockerId { get; set; }
        
        /// <summary>
        /// Pareva-Schloss LockId
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/68
        /// </summary>
        public string LockId { get; set; }

        public EfCaTransportBox()
        {
        }

        public EfCaTransportBox(EfCaTransportBoxDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaTransportBox).GetProperties())
            {
                typeof(EfCaTransportBox).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
