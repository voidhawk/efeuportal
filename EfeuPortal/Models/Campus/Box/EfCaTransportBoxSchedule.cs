﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Box
{
    public class EfCaTransportBoxSchedule
    {
        public string Ident { get; set; }

        public int? Version { get; set; }

        public EfCaTransportBoxSchedule()
        {
        }

        public EfCaTransportBoxSchedule(EfCaTransportBoxScheduleDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaTransportBox).GetProperties())
            {
                typeof(EfCaTransportBoxSchedule).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
