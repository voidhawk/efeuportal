﻿using EfeuPortal.Models.Campus.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Box
{
    public class EfCaTransportBoxType
    {
        public string Ident { get; set; }

        /// <summary>
        /// SMALL_PACKAGE_BOX, LARGE_PACKAGE_BOX, SCRAP (WASTE), ...
        /// </summary>
        public string Type { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// In a BinType used as x
        /// Unit = mm
        /// </summary>
        public double Lenght { get; set; }

        /// <summary>
        /// In a BinType used as y
        /// Unit = mm
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// In a BinType used as z
        /// Unit = mm
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// The maximum weight of this box type
        /// Unit = gr
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// The maximum volume of this box type
        /// Unit = gr
        /// </summary>
        public double Volume { get; set; }

        public int? Version { get; set; }

        public EfCaTransportBoxType()
        {
        }

        public EfCaTransportBoxType(EfCaTransportBoxTypeDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaTransportBoxType).GetProperties())
            {
                typeof(EfCaTransportBoxType).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
