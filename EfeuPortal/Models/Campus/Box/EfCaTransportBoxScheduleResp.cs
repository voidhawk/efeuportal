﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Box
{
    public class EfCaTransportBoxScheduleResp : Response
    {
        public List<EfCaTransportBoxScheduleResp> TransportBoxeSchedules { get; set; }
    }
}
