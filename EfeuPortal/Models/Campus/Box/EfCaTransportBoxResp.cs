﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Box
{
    public class EfCaTransportBoxResp : Response
    {
        public List<EfCaTransportBox> TransportBoxes { get; set; }

        public List<EfCaTransportBoxType> TransportBoxTypes { get; set; }

        public EfCaTransportBoxResp()
        {
            Error = false;
            TransportBoxes = new List<EfCaTransportBox>();
        }
    }
}
