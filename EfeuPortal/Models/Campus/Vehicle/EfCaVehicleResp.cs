﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Vehicle
{
    public class EfCaVehicleResp: Response
    {
        public List<EfCaVehicle> Vehicles { get; set; }

        public EfCaVehicleResp()
        {
            Error = false;
            Vehicles = new List<EfCaVehicle>();
        }
    }
}
