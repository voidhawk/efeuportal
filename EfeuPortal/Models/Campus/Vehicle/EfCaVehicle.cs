﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Vehicle
{
    public class EfCaVehicle
    {
        public string Ident { get; set; }

        /// <summary>
        /// Vehicle type (BIG_VEHICLE_BOT, DEFAULT_VEHICLE_BOT, Standard)
        /// 
        /// JSt: Falls es unterschiedliche Typen gibt muss diese Info dann an der Ladestelle hinterlegt werden?
        /// </summary>
        public string VehicleType { get; set; }

        /// <summary>
        /// Geschwindigkeit in m/s
        /// </summary>
        public double MaxSpeed { get; set; }

        /// <summary>
        /// Gewicht in g
        /// </summary>
        public int MaxWeight { get; set; }

        public List<string> BoxTypes { get; set; }

        /// <summary>
        /// PTVGROUP: https://jira.dev.ptv.de/browse/ECOMP-172
        /// 
        /// Datum der Inbetriebnahme
        /// </summary>
        public DateTimeOffset Launch { get; set; }

        /// <summary>
        /// Gibt die max. Anzahl an Boxen je Bot an
        /// PTVGROUP: https://jira.dev.ptv.de/browse/ECOMP-172
        /// </summary>
        public int? VehicleCapacity { get; set; }

        /// <summary>
        /// Unit: kWh, Maximale Batteriekapazität)
        /// </summary>
        public double BatteryCapacity { get; set; }

        public string IPAddress { get; set; }

        /// <summary>
        /// Einheit in W (Watt)
        /// </summary>
        public double Power { get; set; }

        /// <summary>
        /// Software Version des Fahrzeugs
        /// </summary>
        public string UnitVersionSW { get; set; }

        /// <summary>
        /// Hardware Version des Fahrzeugs
        /// </summary>
        public string UnitVersionHW { get; set; }

        public string SmartourQualityReference { get; set; }

        /// <summary>
        /// Profile for routing and planning
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/24
        /// </summary>
        public string VehicleProfile { get; set; }

        public int? Version { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/45
        /// </summary>
        public string StartDepot { get; set; }

        /// <summary>
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/45
        /// </summary>
        public string EndDepot { get; set; }

        public EfCaVehicle()
        {
        }

        public EfCaVehicle(EfCaVehicleDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaVehicle).GetProperties())
            {
                typeof(EfCaVehicle).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
