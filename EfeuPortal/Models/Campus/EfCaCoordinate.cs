﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus
{
    public class EfCaCoordinate
    {
        public double Longitude { get; set; }
        
        public double Latitude { get; set; }

        public double Height { get; set; }
    }
}
