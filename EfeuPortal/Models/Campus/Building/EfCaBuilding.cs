﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Building
{
    public class EfCaBuilding
    {
        public string Ident { get; set; }

        /// <summary>
        /// Default, HUB, Swimmingpool, ...
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Refers to the "Ident" of an EC_Address/EC_AddressDB object
        /// </summary>
        public string AddressId { get; set; }

        /// <summary>
        /// Refers to the "Ident" of an ECaSyncMeetingPoint/EfCaSyncMeetingPointDB object
        /// </summary>
        public List<string> SyncMeetingPointIds { get; set; }

        /// <summary>
        /// Refers to the "Ident" of an EC_Apartment/EfCaApartmentDB object
        /// </summary>
        public List<string> ApartmentIds { get; set; }

        public List<string> ContactIds { get; set; }

        public List<string> BoxMountingDeviceIds { get; set; }

        public List<string> ChargingStationIds { get; set; }

        public int? Version { get; set; }

        public EfCaBuilding()
        {
        }

        public EfCaBuilding(EfCaBuildingDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaBuilding).GetProperties())
            {
                typeof(EfCaBuilding).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
