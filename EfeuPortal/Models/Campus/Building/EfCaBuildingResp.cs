﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Building
{
    public class EfCaBuildingResp : Response
    {
        public List<EfCaBuilding> Buildings { get; set; }

        public EfCaBuildingResp()
        {
            Error = false;
            Buildings = new List<EfCaBuilding>();
        }
    }
}
