﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.SyncMeetingPoint
{
    public class EfCaSyncMeetingPoint
    {
        public string Ident { get; set; }

        public string Info { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public int? Version { get; set; }

        public string Name { get; set; }

        public EfCaSyncMeetingPoint()
        {

        }

        public EfCaSyncMeetingPoint(EfCaSyncMeetingPointDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaSyncMeetingPoint).GetProperties())
            {
                typeof(EfCaSyncMeetingPoint).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
