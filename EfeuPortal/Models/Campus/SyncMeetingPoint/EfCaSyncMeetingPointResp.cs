﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.SyncMeetingPoint
{
    public class EfCaSyncMeetingPointResp : Response
    {
        public List<EfCaSyncMeetingPoint> SyncMeetingPoints { get; set; }

        public EfCaSyncMeetingPointResp()
        {
            Error = false;
            SyncMeetingPoints = new List<EfCaSyncMeetingPoint>();
        }
    }
}
