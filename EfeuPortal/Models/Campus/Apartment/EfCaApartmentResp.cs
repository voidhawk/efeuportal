﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Apartment
{
    public class EfCaApartmentResp : Response
    {
        public List<EfCaApartment> Apartments { get; set; }

        public EfCaApartmentResp()
        {
            Error = false;
            Apartments = new List<EfCaApartment>();
        }
    }
}
