﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Apartment
{
    public class EfCaApartment
    {
        public string Ident { get; set; }

        /// <summary>
        /// Refers to the "Ident" of an EC_Address/EC_AddressDB object
        /// Ein Apartment hat genau eine Addresse
        /// </summary>
        public string AddressId { get; set; }

        public string Info { get; set; }

        public List<string> ContactIds { get; set; }

        public List<string> BoxMountingDeviceIds { get; set; }

        public int? Version { get; set; }

        public EfCaApartment()
        {

        }

        public EfCaApartment(EfCaApartmentDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaApartment).GetProperties())
            {
                typeof(EfCaApartment).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
