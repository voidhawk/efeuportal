﻿using PTVGROUP.xServer2.Models;
using System;

namespace EfeuPortal.Models.Campus.XServer2
{
    public class EfCaRouteCalculationStartEnd
    {
        public double StartLat { get; set; }
        public double StartLon { get; set; }
        public double EndLat { get; set; }
        public double EndLon { get; set; }
        public String VehicleIdent { get; set; }
        public RoutingResultFields ResultFields { get; set; }
    }
}
