﻿using EfeuPortal.Models.Campus.TourData;
using PTVGROUP.xServer2.Models;
using System;
using System.Collections.Generic;

namespace EfeuPortal.Models.Campus.XServer2
{
    /// <summary>
    /// Diese Klasse wird verwendet um den xServer Request zu erzeugen
    /// </summary>
    public class EfCaRouteRequest
    {
        public RouteRequest RouteRequest { get; set; }

        public EfCaRouteRequest()
        {
        }

        public EfCaRouteRequest(EfCaRouteCalculation calculateRoute)
        {
        }

        public EfCaRouteRequest(EfCaVehicleTrip vehicleTrip, RoutingResultFields resultFields)
        {
            ProcessVehicleTrip(vehicleTrip, resultFields);
        }

        public EfCaRouteRequest(EfCaRouteCalculationStartEnd routeCalculation, String profile) {
            ProcessStartEnd(routeCalculation.StartLat, routeCalculation.StartLon, routeCalculation.EndLat, routeCalculation.EndLon, routeCalculation.ResultFields, profile);
        }

        private void ProcessVehicleTrip(EfCaVehicleTrip vehicleTrip, RoutingResultFields resultFields)
        {
            RouteRequest routeRequest = new RouteRequest();
            routeRequest.Waypoints = new List<InputWaypoint>();
            foreach (EfCaTourpointCoordinate item in vehicleTrip.TourpointCoordinates)
            {
                OffRoadWaypoint offRoadWaypoint = new OffRoadWaypoint();
                offRoadWaypoint.Location = new OffRoadRouteLocation();
                offRoadWaypoint.Location.OffRoadCoordinate = new Coordinate();
                offRoadWaypoint.Location.OffRoadCoordinate.X = item.Longitude;
                offRoadWaypoint.Location.OffRoadCoordinate.Y = item.Latitude;

                routeRequest.Waypoints.Add(offRoadWaypoint);
            }

            routeRequest.RouteOptions = new RouteOptions();
            routeRequest.RouteOptions.RoutingType = RoutingType.CONVENTIONAL;
            //RouteOptions.GeographicRestrictions = new XServer2GeograficRestrictions();

            routeRequest.RouteOptions.PolylineOptions = new PolylineOptions();
            routeRequest.RouteOptions.PolylineOptions.Elevations = false;

            routeRequest.RouteOptions.EmissionOptions = new EmissionOptions();

            if (resultFields == null)
            {
                routeRequest.ResultFields = new RoutingResultFields();

                routeRequest.ResultFields.Waypoints = false;
                routeRequest.ResultFields.Legs = new LegResultFields();
                routeRequest.ResultFields.Legs.Enabled = true;
                routeRequest.ResultFields.Legs.Polyline = true;
                routeRequest.ResultFields.Legs.TollSummary = false;
                routeRequest.ResultFields.Emissions = false;

                routeRequest.ResultFields.Segments = new SegmentResultFields();
                routeRequest.ResultFields.Segments.Enabled = true;
                routeRequest.ResultFields.Segments.Emissions = false;
                routeRequest.ResultFields.Segments.Polyline = true;
                routeRequest.ResultFields.Segments.Descriptors = false;
                routeRequest.ResultFields.Segments.RoadAttributes = true;

                routeRequest.ResultFields.Nodes = true;
                routeRequest.ResultFields.Polyline = true;

                routeRequest.ResultFields.Toll = new TollResultFields();
                routeRequest.ResultFields.Toll.Enabled = false;
                routeRequest.ResultFields.Emissions = false;
                routeRequest.ResultFields.Report = false;
                routeRequest.ResultFields.GuidedNavigationRoute = false;
            }
            else
            {
                routeRequest.ResultFields = resultFields;
            }

            //routeRequest.StoredProfile = "sew-robby";
            RouteRequest = routeRequest;
            //string routerRequestAsJSON = JsonConvert.SerializeObject(efCaRouteRequest, Formatting.Indented);
            //log.Debug($"CalculateRoute(..), EfCaRouteRequest({routerRequestAsJSON})");
        }

        private void ProcessStartEnd(double startLat, double startLon, double endLat, double endLon, RoutingResultFields resultFields, String profile) {
            RouteRequest routeRequest = new RouteRequest();
            routeRequest.Waypoints = new List<InputWaypoint>();
            routeRequest.StoredProfile = profile;
            
            OffRoadWaypoint start = new OffRoadWaypoint();
            start.Location = new OffRoadRouteLocation();
            start.Location.OffRoadCoordinate = new Coordinate();
            start.Location.OffRoadCoordinate.X = startLon;
            start.Location.OffRoadCoordinate.Y = startLat;
            routeRequest.Waypoints.Add(start);

            OffRoadWaypoint end = new OffRoadWaypoint();
            end.Location = new OffRoadRouteLocation();
            end.Location.OffRoadCoordinate = new Coordinate();
            end.Location.OffRoadCoordinate.X = endLon;
            end.Location.OffRoadCoordinate.Y = endLat;
            routeRequest.Waypoints.Add(end);

            routeRequest.RouteOptions = new RouteOptions();
            routeRequest.RouteOptions.RoutingType = RoutingType.CONVENTIONAL;
            //RouteOptions.GeographicRestrictions = new XServer2GeograficRestrictions();

            routeRequest.RouteOptions.PolylineOptions = new PolylineOptions();
            routeRequest.RouteOptions.PolylineOptions.Elevations = false;

            routeRequest.RouteOptions.EmissionOptions = new EmissionOptions();

            if (resultFields == null)
            {
                routeRequest.ResultFields = new RoutingResultFields();

                routeRequest.ResultFields.Waypoints = false;
                routeRequest.ResultFields.Legs = new LegResultFields();
                routeRequest.ResultFields.Legs.Enabled = true;
                routeRequest.ResultFields.Legs.Polyline = true;
                routeRequest.ResultFields.Legs.TollSummary = false;
                routeRequest.ResultFields.Emissions = false;

                routeRequest.ResultFields.Segments = new SegmentResultFields();
                routeRequest.ResultFields.Segments.Enabled = true;
                routeRequest.ResultFields.Segments.Emissions = false;
                routeRequest.ResultFields.Segments.Polyline = true;
                routeRequest.ResultFields.Segments.Descriptors = false;
                routeRequest.ResultFields.Segments.RoadAttributes = true;

                routeRequest.ResultFields.Nodes = true;
                routeRequest.ResultFields.Polyline = true;

                routeRequest.ResultFields.Toll = new TollResultFields();
                routeRequest.ResultFields.Toll.Enabled = false;
                routeRequest.ResultFields.Emissions = false;
                routeRequest.ResultFields.Report = false;
                routeRequest.ResultFields.GuidedNavigationRoute = false;
            }
            else
            {
                routeRequest.ResultFields = resultFields;
            }
            RouteRequest = routeRequest;
        }
    }
}