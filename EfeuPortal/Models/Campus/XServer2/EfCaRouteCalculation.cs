﻿using PTVGROUP.xServer2.Models;

namespace EfeuPortal.Models.Campus.XServer2
{
    public class EfCaRouteCalculation
    {
        public string TourIdent { get; set; }

        public string TourExtId { get; set; }

        public RoutingResultFields ResultFields { get; set; }
    }
}
