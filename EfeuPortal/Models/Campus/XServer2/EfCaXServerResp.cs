﻿using PTVGROUP.xServer2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.XServer2
{
    public class EfCaXServerResp : Response
    {
        public RouteResponse RouteResponse { get; set; }

        public LocationsResponse LocationsResponse { get; set; }

        public PackedBinsResponse PackedBinsResponse { get; set; }
    }
}
