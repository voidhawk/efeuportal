﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.XServer2
{
    public class EfCaXServerConfig
    {
        public string User { get; set; }
        
        public string Token { get; set; }

        public string Url { get; set; }

        public string Version { get; set; }
    }
}
