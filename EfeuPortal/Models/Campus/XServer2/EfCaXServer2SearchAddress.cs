﻿using EfeuPortal.Models.Campus.Place;
using PTVGROUP.xServer2.Models;

namespace EfeuPortal.Models.Campus.XServer2
{
    /// <summary>
    /// Diese Klasse wird dazu verwendet umd die Request´s für den xServer2 zu erzeugen.
    /// </summary>
    public class EfCaXServer2SearchAddress
    {
        public SearchByAddressRequest SearchByAddressRequest { get; private set; }

        public SearchByTextRequest SearchByTextRequest { get; set; }

        public SearchByPositionRequest SearchByPositionRequest { get; set; }

        public EfCaXServer2SearchAddress(EfCaAddress searchByAddress)
        {
            SearchByAddressRequest = CreateSearchByAddressRequest(searchByAddress);
        }

        public EfCaXServer2SearchAddress(string searchByText)
        {
            SearchByTextRequest = CreateSearchByTextRequest(searchByText);
        }

        public EfCaXServer2SearchAddress(EfCaCoordinate efCaCoordinate)
        {
            SearchByPositionRequest = CreateSearchByPositionRequest(efCaCoordinate);
        }

        private SearchByAddressRequest CreateSearchByAddressRequest(EfCaAddress searchByAddress)
        {
            SearchByAddressRequest searchByAddressRequest = new SearchByAddressRequest();

            searchByAddressRequest.Address = new Address();
            searchByAddressRequest.Address.Country = searchByAddress.Country;
            searchByAddressRequest.Address.State = searchByAddress.State;
            searchByAddressRequest.Address.PostalCode = searchByAddress.ZipCode;
            searchByAddressRequest.Address.City = searchByAddress.City;
            searchByAddressRequest.Address.Street = searchByAddress.Street;
            searchByAddressRequest.Address.HouseNumber = searchByAddress.HouseNumber;

            return searchByAddressRequest;
        }

        private SearchByTextRequest CreateSearchByTextRequest(string searchByText)
        {
            SearchByTextRequest searchByTextRequest = new SearchByTextRequest();
            searchByTextRequest.Text = searchByText;
            return searchByTextRequest;
        }

        private SearchByPositionRequest CreateSearchByPositionRequest(EfCaCoordinate efCaCoordinate)
        {
            SearchByPositionRequest searchByPositionRequest = new SearchByPositionRequest();
            searchByPositionRequest.Coordinate = new Coordinate(efCaCoordinate.Longitude, efCaCoordinate.Latitude);

            return searchByPositionRequest;
        }
    }
}
