﻿using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.MongoDB
{
    public class EfCaImageDocumentationDB : EfCaImageDocumentation
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("extIdDetails")]
        public List<ExtIdDetail> ExtIdDetails { get; set; }

        //[BsonRepresentation(BsonType.String)]
        [BsonElement("creationTime")]
        public DateTimeOffset CreationTime { get; set; }

        //[BsonRepresentation(BsonType.String)]
        [BsonElement("updateTime")]
        public DateTimeOffset UpdateTime { get; set; }

        [BsonElement("__v")]
        public int CurrentVersion { get; set; }

        public EfCaImageDocumentationDB(InternalUserInfos internalUserInfo, EfCaImageDocumentation item)
        {
            CreationTime = DateTimeOffset.Now;
            UpdateTime = DateTimeOffset.Now;
            CurrentVersion = item.Version.GetValueOrDefault(1);

            /*
             * Die LastUpdate soll nicht in die Datenbank falls der Wert fälschlicherweise von außen gesetzt wird.
             * https://lsogit.fzi.de/efeu/efeuportal/-/issues/54
             */
            LastUpdate = null;

            ExtIdDetails = new List<ExtIdDetail>();
            ExtIdDetail detailOwner = new ExtIdDetail();
            detailOwner.Originator = internalUserInfo.TenantId;
            detailOwner.ExtId = item.Ident;
            detailOwner.Owner = true;
            ExtIdDetails.Add(detailOwner);

            foreach (PropertyInfo property in typeof(EfCaImageDocumentation).GetProperties())
            {
                if (!property.Name.Equals("Version"))
                {
                    typeof(EfCaImageDocumentationDB).GetProperty(property.Name).SetValue(this, property.GetValue(item));
                }
            }

            Version = null;
            Ident = null;
        }

        //public void Update(EfCaOrder item)
        //{
        //    if (CurrentVersion != item.Version.GetValueOrDefault(-1))
        //    {
        //        throw new Exception($"Version outdated, Current({CurrentVersion}), UpdateVersion({item.Version})");
        //    }
        //    UpdateTime = DateTimeOffset.Now;
        //    CurrentVersion++;

        //    foreach (PropertyInfo property in typeof(EfCaOrder).GetProperties())
        //    {
        //        if (!property.Name.Equals("Version"))
        //        {
        //            typeof(EfCaOrderDB).GetProperty(property.Name).SetValue(this, property.GetValue(item));
        //        }
        //    }

        //    Version = null;
        //    Ident = null;
        //    UserCreationTime = null;
        //    UserUpdateTime = null;
        //}
    }
}
