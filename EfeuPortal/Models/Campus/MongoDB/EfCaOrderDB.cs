﻿using EfeuPortal.Models.Campus.Order;
using EfeuPortal.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace EfeuPortal.Models.Campus.MongoDB
{
    [BsonIgnoreExtraElements]
    public class EfCaOrderDB : EfCaOrder
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("extIdDetails")]
        public List<ExtIdDetail> ExtIdDetails { get; set; }

        //[BsonRepresentation(BsonType.String)]
        [BsonElement("creationTime")]
        public DateTimeOffset CreationTime { get; set; }

        //[BsonRepresentation(BsonType.String)]
        [BsonElement("updateTime")]
        public DateTimeOffset UpdateTime { get; set; }

        [BsonElement("webApiOrder")]
        public PTVGROUP.ROWebApi.Model.Order WebApiOrder { get; set; }

        [BsonElement("__v")]
        public int CurrentVersion { get; set; }

        public EfCaOrderDB(InternalUserInfos internalUserInfo, EfCaOrder item)
        {
            CreationTime = DateTimeOffset.Now;
            UpdateTime = DateTimeOffset.Now;
            CurrentVersion = item.Version.GetValueOrDefault(1);

            ExtIdDetails = new List<ExtIdDetail>();
            ExtIdDetail detailOwner = new ExtIdDetail();
            detailOwner.Originator = internalUserInfo.TenantId;
            detailOwner.ExtId = item.Ident;
            detailOwner.Owner = true;
            ExtIdDetails.Add(detailOwner);

            DeliveryAttempts = 0;
                
            foreach (PropertyInfo property in typeof(EfCaOrder).GetProperties())
            {
                if (!property.Name.Equals("Version"))
                {
                    typeof(EfCaOrderDB).GetProperty(property.Name).SetValue(this, property.GetValue(item));
                }
            }

            Version = null;
            Ident = null;
            UserCreationTime = null;
            UserUpdateTime = null;
        }

        public void Update(EfCaOrder item)
        {
            if (CurrentVersion != item.Version.GetValueOrDefault(-1))
            {
                throw new Exception($"Version outdated, Current({CurrentVersion}), UpdateVersion({item.Version})");
            }
            UpdateTime = DateTimeOffset.Now;
            CurrentVersion++;

            foreach (PropertyInfo property in typeof(EfCaOrder).GetProperties())
            {
                if (!property.Name.Equals("Version"))
                {
                    typeof(EfCaOrderDB).GetProperty(property.Name).SetValue(this, property.GetValue(item));
                }
            }

            Version = null;
            Ident = null;
            UserCreationTime = null;
            UserUpdateTime = null;
        }
    }
}
