﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Contact
{
    public class EfCaContactResp : Response
    {
        public List<EfCaContact> Contacts { get; set; }

        public EfCaContactResp()
        {
            Error = false;
            Contacts = new List<EfCaContact>();
        }
    }
}
