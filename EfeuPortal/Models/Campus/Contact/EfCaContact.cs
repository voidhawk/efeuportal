﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Contact
{
    public class EfCaContact
    {
        /// <summary>
        /// In diesem Fall ist der Ident und die Email gleich
        /// </summary>
        public string Ident { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        /// <summary>
        /// Wunsch kam vom FZI 27.10.2020
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/17
        ///  UNDEFINED = 0,
        ///  SYNCHRON = 1,
        ///  ASYNCHRON = 2,
        ///  SELFSERVICE = 3
        /// </summary>
        public DefaultOrderMode DefaultOrderMode { get; set; }

        public List<EfCaSlotsPerDay> SyncAvailabilityTimeSlots { get; set; }

        public List<EfCaSlotsPerDay> AsyncAvailabilityTimeSlots { get; set; }

        /// <summary>
        /// Used to mark for example holidays, no service needed.
        /// </summary>
        public List<EfCaDateTimeSlot> NoAvailabilities { get; set; }
        // 14.01.2020

        public DateTimeOffset Birthday { get; set; }

        /// <summary>
        /// Diese Email muss zum efeuUser passen
        /// </summary>
        public string Email { get; set; }

        public List<string> BoxMountingDeviceIds { get; set; }

        /// <summary>
        /// Mögliche Keys
        /// 
        ///  - AddressRef (single)
        ///  - CalendarRef (single) => Urlaubszeiten, sonstige Abwesenheitszeiten
        ///  - MobilePhone (multi)
        ///  - Phone (multi)
        /// </summary>
        public List<AdditionalInfo> AdditionalInfos { get; set; }

        /// <summary>
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/17
        /// EfCaContact: "OrderAggregationAllowed" Bool, Neues Feld: Einstellung für das Zusammenfassen 
        /// von Bestellungen auf Contact-Ebene. Default-Wert soll bei Allowed (1, true) sein.
        /// </summary>
        public bool OrderAggregationAllowed { get; set; }

        public int? Version { get; set; }

        public EfCaContact()
        {
        }

        public EfCaContact(EfCaContactDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaContact).GetProperties())
            {
                typeof(EfCaContact).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
    public enum DefaultOrderMode
    {
        UNDEFINED = 0,
        SYNCHRON = 1,
        ASYNCHRON = 2,
        SELFSERVICE = 3
    }


}
