﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Kep
{
    public class EfCaServiceProvider
    {
        public string Ident { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public int? Version { get; set; }

        public EfCaServiceProvider()
        {
        }

        public EfCaServiceProvider(EfCaServiceProviderDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaServiceProvider).GetProperties())
            {
                typeof(EfCaServiceProvider).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
