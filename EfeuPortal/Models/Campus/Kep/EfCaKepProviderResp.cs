﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Kep
{
    public class EfCaKepProviderResp : Response
    {
        public List<EfCaServiceProvider> KepProviders { get; set; }

        public EfCaKepProviderResp()
        {
            Error = false;
            KepProviders = new List<EfCaServiceProvider>();
        }
    }
}
