﻿using EfeuPortal.Models.Campus.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Warehouse
{
    public class EfCaWarehousePlace
    {
        public string Ident { get; set; }

        /// <summary>
        /// DHL, HERMES, ...
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// COMMISIONING, KEP, ...
        /// </summary>
        public string Type { get; set; }

        public int? Version { get; set; }

        public EfCaWarehousePlace()
        {

        }

        public EfCaWarehousePlace(EfCaWarehousePlaceDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaWarehousePlace).GetProperties())
            {
                typeof(EfCaWarehousePlace).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
