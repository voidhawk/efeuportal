﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Warehouse
{
    public class EfCaWarehousePlaceResp : Response
    {
        public List<EfCaWarehousePlace> WarehousePlaces { get; set; }

        public EfCaWarehousePlaceResp()
        {
            Error = false;
            WarehousePlaces = new List<EfCaWarehousePlace>();
        }
    }
}
