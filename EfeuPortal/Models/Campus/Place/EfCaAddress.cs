﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Place
{
    public class EfCaAddress
    {
        /*
         * remarks
         */
        public string Ident { get; set; }

        public string Label { get; set; }

        /// <summary>
        /// Values:
        ///  - DEPOT
        ///  - PLACE
        ///  - CUSTOMER
        /// </summary>
        public string Type { get; set; }

        public string ZipCode { get; set; }
        public string City { get; set; }
        public string City2 { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }

        public string Province { get; set; }
        public string Country { get; set; }
        public string State { get; set; }

        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public List<string> BoxMountingDeviceIds { get; set; }

        public int? Version { get; set; }

        public EfCaAddress()
        {

        }

        public EfCaAddress(EfCaAddressDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaAddress).GetProperties())
            {
                typeof(EfCaAddress).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
