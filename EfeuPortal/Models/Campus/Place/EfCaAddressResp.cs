﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Place
{
    public class EfCaAddressResp : Response
    {
        public List<EfCaAddress> Addresses { get; set; }

        public EfCaAddressResp()
        {
            Error = false;
            Addresses = new List<EfCaAddress>();
        }
    }
}
