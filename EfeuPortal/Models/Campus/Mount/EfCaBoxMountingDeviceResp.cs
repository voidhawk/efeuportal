﻿using PTVGROUP.SlotMgmt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    public class EfCaBoxMountingDeviceResp : Response
    {
        public List<EfCaBoxMountingDevice> BoxMountingDevices { get; set; }

        public List<EfCaBoxMountingDeviceSlot> BoxMountingDeviceSlots { get; set; }

        public List<EfCaBoxMountingDeviceScheduler> BoxMountingDeviceSchedulers { get; set; }

        public List<EfCaSlotReservations> SlotReservations { get; set; }
        
        public List<EfCaScheduledBoxMountingSlot> ScheduledBoxMountingSlots { get; set; }

        public EfCaBoxMountingDeviceResp()
        {
            Error = false;
        }

        public EfCaBoxMountingDeviceResp(PtvSlotSchedulerResponse ptvSlotSchedulerResponse)
        {
            if(ptvSlotSchedulerResponse == null)
            {
                Error = true;
                return;
            }
            Error = false;
            BoxMountingDeviceSchedulers = new List<EfCaBoxMountingDeviceScheduler>();
            foreach(PtvSlotScheduler item in ptvSlotSchedulerResponse.Result)
            {
                EfCaBoxMountingDeviceScheduler efCaBoxMountingDeviceScheduler = new EfCaBoxMountingDeviceScheduler(item);
                BoxMountingDeviceSchedulers.Add(efCaBoxMountingDeviceScheduler);
            }
        }

        public EfCaBoxMountingDeviceResp(PtvSlotReservationResponseResponse deleteSlotReservationResponse)
        {
            if (deleteSlotReservationResponse == null || deleteSlotReservationResponse.ReturnInfo == ReturnValue.ERROR)
            {
                Error = true;
                return;
            }
            Error = false;
            
            //JSt: ToDo: Rückgabe überarbeiten
            //BoxMountingDeviceSchedulers = new List<EfCaBoxMountingDeviceScheduler>();
            //foreach (PtvSlotScheduler item in ptvSlotSchedulerResponse.Result)
            //{
            //    EfCaBoxMountingDeviceScheduler efCaBoxMountingDeviceScheduler = new EfCaBoxMountingDeviceScheduler(item);
            //    BoxMountingDeviceSchedulers.Add(efCaBoxMountingDeviceScheduler);
            //}
        }
    }
}
