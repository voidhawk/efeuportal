﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    /// <summary>
    /// Diese Klasse wird dazu verwendet am Reservierungsanfragen zu übergeben
    /// </summary>
    public class EfCaSlotReservationRequest
    {
        /// <summary>
        /// Die Reservierung wird für diese Id angefragt/angelegt. Der Parameter wird von der Slot Reservierung nicht verwendet, 
        /// sondern nur in der Response zurückgegeben. Der Aufrufende kennt den Type und kann dort, nach seinen Bedürfnissen, das Ergebnis 
        /// verwenden.
        /// 
        /// <para>Für den aufrufenden sollte diese ID eindeutig sein</para>
        /// </summary>
        public string CustomerIdent { get; set; }

        /// <summary>
        /// UNDEFINED, SINGLE, ALL => ALL wird verwendet wenn an EINER Stelle ALLE Scheduler für den gleichen Zeitpunkt reserviert werden sollen.
        /// 
        /// JSt: ToDo: Abstimmen wie man das am besten formulieren kann, damit es für ein Slot Reservierungssystem passt
        /// </summary>
        public SlotSelection SlotSelection { get; set; }

        /// <summary>
        ///  Dieser Parameter gibt an wie viele Reservierungen gemacht werden sollen. 
        ///  
        /// Wird nur verwendet, wenn die Anzahl der BockIds > 1 ist. Die Reservierungen werden dann auch auf verschiedene Böcke verteilt.
        /// Kann nur kleiner oder gleich wie die Anzahl der BockIds sein
        /// 
        /// </summary>
        public int ReservationsRequestCount { get; set; }

        /// <summary>
        /// Diese Liste gibt an, an welchen Stellen (Schedulern) im efeucampus Umfeld eine Reservierung versucht werden soll.
        /// 
        /// Wie viele Reservierungen gemacht werden hängt von dem Parameter "ReservationsRequestCount"
        /// </summary>
        public List<string> SchedulerIds { get; set; }

        /// <summary>
        /// Zeitschlitz (Dauer) in Sekunden, falls leer Start - End => default
        /// </summary>
        public int TimeSlot { get; set; }

        public DateTimeOffset Start { get; set; }

        public DateTimeOffset End { get; set; }
    }

    public enum SlotSelection
    {
        UNDEFINED = 0,
        SINGLE = 1,
        ALL = 2
    }
}