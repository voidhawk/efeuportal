﻿using EfeuPortal.Models.Campus.MongoDB;
using PTVGROUP.SlotMgmt.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    public class EfCaBoxMountingDeviceScheduler
    {
        public string Ident { get; set; }

        public string BoxMountingDeviceId { get; set; }

        /// <summary>
        /// ToDo: MountingSlot muss für den PTV Service allgemein werden
        /// </summary>
        public string SlotDescription { get; set; }

        public Dictionary<string, PTVSlotReservation> PtvSlotReservations { get; set; }

        public int? Version { get; set; }

        public EfCaBoxMountingDeviceScheduler()
        {

        }

        public EfCaBoxMountingDeviceScheduler(PtvSlotScheduler item)
        {
            Ident = item.Ident;
            BoxMountingDeviceId = item.ExternalId;
            SlotDescription = item.Description;
            //PTVSlotReservation
            Version = item.Version;
        }

        /// <summary>
        /// JSt: ToDo: Überprüfen ob wir diese Methode noch brauchen
        /// </summary>
        /// <param name="itemDB"></param>
        public EfCaBoxMountingDeviceScheduler(EfCaBoxMountingDeviceSchedulerDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaBoxMountingDeviceScheduler).GetProperties())
            {
                typeof(EfCaBoxMountingDeviceScheduler).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
//https://stackoverflow.com/questions/11154673/get-the-correct-week-number-of-a-given-date