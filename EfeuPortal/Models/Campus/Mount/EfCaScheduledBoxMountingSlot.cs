﻿using PTVGROUP.SlotMgmt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    /// <summary>
    /// In dieser Klasse wird gespeichert welche Slots und Positionen für eine Transportkiste 
    /// reserviert wurden.
    /// 
    /// !!!! Nach der Planung kann es nur noch ein Eintrag sein, ausser bei großen Boxen => sind es 2!!!!
    /// oder in EfCaScheduledBoxMountTray umbenennen drawer
    /// </summary>
    public class EfCaScheduledBoxMountingSlot
    {
        /// <summary>
        /// External Id of the customer.
        /// </summary>
        public string Ident { get; set; }

        /// <summary>
        /// Id des Übergabebocks
        /// </summary>
        public string CustomerIdent { get; set; }

        public string CalendarWeek { get; set; }

        public string Day { get; set; }

        public int StartIndex { get; set; }

        public int EndIndex { get; set; }

        /// <summary>
        /// Concatenation of scheduler and customer external info information.
        /// </summary>
        public string ExternalSlotReservationInfo { get; set; }

        public EfCaScheduledBoxMountingSlot()
        {
        }

        public EfCaScheduledBoxMountingSlot(PtvSlotReservationResponse ptvSlotReservationResponse)
        {
            Ident = ptvSlotReservationResponse.Ident;
            CustomerIdent = ptvSlotReservationResponse.CustomerIdent;
            CalendarWeek = ptvSlotReservationResponse.CalendarWeek;
            Day = ptvSlotReservationResponse.Day;
            StartIndex = ptvSlotReservationResponse.StartIndex;
            EndIndex = ptvSlotReservationResponse.EndIndex;
            ExternalSlotReservationInfo = ptvSlotReservationResponse.ReservationKey;
            //ptvSlotReservationResponse.State;
        }
    }
}
