﻿using EfeuPortal.Helpers;
using PTVGROUP.SlotMgmt.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DayOfWeek = System.DayOfWeek;

namespace EfeuPortal.Models.Campus.Mount
{
    /// <summary>
    /// Diese Klasse enthält für einen Slot 
    /// 
    /// - den Scheduler
    /// - und die aufbereiteten PtvAppointmentEntries
    /// </summary>
    public class EfCaSlotReservations
    {
        public string BoxMountingDeviceId { get; set; }

        public string Description { get; set; }

        public string CalendarWeek { get; set; }

        public PTVSlotReservation Scheduler { get; set; }

        public List<PtvAppointmentEntry> AppointmentEntries { get; set; }

        public EfCaSlotReservations()
        {

        }

        public EfCaSlotReservations(PtvSlotScheduler ptvSlotScheduler)
        {
            BoxMountingDeviceId = ptvSlotScheduler.ExternalId;
            Description =ptvSlotScheduler.Description;
            AppointmentEntries = new List<PtvAppointmentEntry>();
            foreach (var item in ptvSlotScheduler.PtvSlotReservations)
            {
                //item.Value.ExternalSlotReservationInfo = new Dictionary<string, string>();
                string yearAndWeek = item.Key;
                CalendarWeek = yearAndWeek;
                Scheduler = new PTVSlotReservation() { SchedulerIdent = ptvSlotScheduler.Ident };
                //Scheduler.SlotReservations = item.Value.SlotReservations;

                DateTimeOffset start = DataHelper.GetSundayOfWeek(yearAndWeek);
                start = start.AddDays(-7);
                //Scheduler = item.Value.SlotReservations;
                var temp  = item.Value.SlotReservations;
                //Dictionary<string, List<BitArray>> test = CastSlotReservations(item.Value.SlotReservations);
                if (item.Value.ExternalSlotReservationInfo != null && item.Value.ExternalSlotReservationInfo.Count > 0)
                {
                    foreach (var val in Enum.GetValues(typeof(DayOfWeek)))
                    {
                        string[] weekdays = item.Value.ExternalSlotReservationInfo.Keys.Where(x => x.Contains(val.ToString())).ToArray<string>();
                        foreach (string key in weekdays)
                        {
                            string[] splittedKeys = key.Split("-");
                            PtvAppointmentEntry ptvAppointmentEntry = new PtvAppointmentEntry();

                            Enum.TryParse(splittedKeys[2], out DayOfWeek myStatus);
                            var weekDayAsInt = (int)myStatus;
                            DateTimeOffset usedDay = start.AddDays(weekDayAsInt);

                            ptvAppointmentEntry.Key = key;
                            ptvAppointmentEntry.CustomerIdent = item.Value.ExternalSlotReservationInfo[key];

                            ptvAppointmentEntry.Start = usedDay.AddMinutes(Convert.ToInt16(splittedKeys[3]) * 5);
                            ptvAppointmentEntry.End = usedDay.AddMinutes((Convert.ToInt16(splittedKeys[4])+1) * 5);
                            //JSt: ToDo: Was nehmen wir hier? ptvAppointmentEntry.Calendar = item.;
                            //ptvAppointmentEntry.Description = $"{splittedKeys[2]}-Description-Value";
                            //ptvAppointmentEntry.Location = $"{splittedKeys[2]}-Location-Value";
                            //JSt: ToDo: Was nehmen wir hier? ptvAppointmentEntry.Subject = $"{item.BoxMountingDeviceId.Split("-")[0]}-Subject-Value";

                            AppointmentEntries.Add(ptvAppointmentEntry);
                        }
                    }
                }
            }
        }

        private Dictionary<string, List<BitArray>>  CastSlotReservations(Dictionary<string, List<Object>> slotReservations)
        {
            Dictionary<string, List<BitArray>> castedList = new Dictionary<string, List<BitArray>>();
            foreach (var item in slotReservations)
            {
                var myObject = item.Value;
                string myKey = item.Key;
                int counter = myObject.Count;
                bool[] values = new bool[counter];
                for (int i = 0; i < counter; i++)
                {
                    values[i] = (bool)myObject[i];
                }
                BitArray schedules = new BitArray(values);
                //castedList.Add(myKey, schedules);
                //castedList.Add(myKey, schedules);
            }
            //List<object> simulatedTour_uncasted = GetAllActionResultsWhereKeyStartsWith(PtvScenarioAction.ActionIdentifierPrefix(typeof(PtvExtendedTourEvent), false));
            //if (simulatedTour_uncasted.Count != 0)
            //{
            //    List<PtvExtendedTourEvent> simulatedTour = new List<PtvExtendedTourEvent>();
            //    foreach (object o in simulatedTour_uncasted)
            //    {
            //        simulatedTour.Add((PtvExtendedTourEvent)o);
            //    }
            //    simResult.SimulatedTour = simulatedTour;
            //}

            return null;
        }
    }
}
