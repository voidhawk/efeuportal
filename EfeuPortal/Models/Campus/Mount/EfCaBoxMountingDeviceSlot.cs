﻿using EfeuPortal.Models.Campus.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    public class EfCaBoxMountingDeviceSlot
    {
        public string Ident { get; set; }

        public string BockIdent { get; set; }

        public string OrderIdent { get; set; }

        public DateTimeOffset Start { get; set; }
        
        public DateTimeOffset End { get; set; }

        /// <summary>
        /// Dauer in Sekunden
        /// </summary>
        public int StartFrame { get; set; }

        /// <summary>
        /// Dauer in Sekunden
        /// </summary>
        public int MiddleFrame { get; set; }

        /// <summary>
        /// Dauer in Sekunden
        /// </summary>
        public int EndFrame { get; set; }

        /// <summary>
        /// RESERVED
        /// FIXED
        /// </summary>
        public string State { get; set; }

        public int? Version { get; set; }

        public EfCaBoxMountingDeviceSlot()
        {

        }

        public EfCaBoxMountingDeviceSlot(EfCaBoxMountingDeviceSlotDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaBoxMountingDeviceSlot).GetProperties())
            {
                typeof(EfCaBoxMountingDeviceSlot).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }

    }
}
