﻿using EfeuPortal.Models.Campus.ChargingStation;
using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using MongoDB.Bson.Serialization.Attributes;
using PTVGROUP.SlotMgmt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    public class EfCaBoxMountingDevice
    {
        public string Ident { get; set; }

        /// <summary>
        /// PUBLIC, PRIVATE
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Default = 1
        /// </summary>
        public int? MountCapacity { get; set; }

        /// <summary>
        /// longitude => x (Germany 8.2)
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// latitude => y (Germany 52.3)
        /// </summary>
        public double Latitude { get; set; }

        public string Info { get; set; }


        /// <summary>
        /// IFL Data for planning: local x-Coordinate
        /// </summary>
        public double? LocalXCoordinate { get; set; }

        /// <summary>
        /// IFL Data for planning: local y-Coordinate
        /// </summary>
        public double? LocalYCoordinate { get; set; }

        /// <summary>
        /// IFL Data for planning: SEW Mount Ident
        /// </summary>
        public int? SewMountIdent { get; set; }



        public EfCaChargingStation ChargingStation { get; set; }

        public List<string> SlotSchedulerIdents { get; set; }
        
        public int? Version { get; set; }

        public string Name { get; set; }

        public EfCaBoxMountingDevice()
        {
        }

        public EfCaBoxMountingDevice(EfCaBoxMountingDeviceDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaBoxMountingDevice).GetProperties())
            {
                typeof(EfCaBoxMountingDevice).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
