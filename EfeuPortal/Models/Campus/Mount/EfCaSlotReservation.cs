﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    /// <summary>
    /// This clazz is used to query the reservations of a specified Scheduler of a BoxMountingDevice
    /// </summary>
    public class EfCaSlotReservation
    {
        public string SchedulerIdent { get; set; }

        public string CalendarWeek { get; set; }

        /// <summary>
        /// Wenn CalendarWeek gesetzt, wird Start ignoriert
        /// </summary>
        public DateTimeOffset Start { get; set; }
    }
}
