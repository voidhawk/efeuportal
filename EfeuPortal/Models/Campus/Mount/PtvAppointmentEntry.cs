﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    public class PtvAppointmentEntry
    {
        public string Key { get; set; }

        public string CustomerIdent { get; set; }

        public string Calendar { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset End { get; set; }
        public string Location { get; set; }
        public string Subject { get; set; }
    }
}
