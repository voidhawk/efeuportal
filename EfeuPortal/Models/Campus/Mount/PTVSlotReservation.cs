﻿using PTVGROUP.SlotMgmt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Mount
{
    public class PTVSlotReservation
    {
        /// <summary>
        /// The Unique Id if the scheduler in the slot management
        /// </summary>
        public string SchedulerIdent { get; set; }

        /// <summary>
        /// Key: Year-Calendarweek-Day-StartIndex-EndIndex e.g. 2020-42-Sunday-0-10
        /// 
        /// Value: A string used from user of the slot management service to identify depending data in his environment.
        /// </summary>
        public Dictionary<string, string> ExternalSlotReservationInfo { get; set; }

        /// <summary>
        /// Key: Year-Calendarweek e.g. 2020-42
        /// 
        /// Value: A BitArray-like containing the "blocked" timeslots information.
        /// </summary>
        public Dictionary<string, BitArrayExtended> SlotReservations { get; set; }
    }
}
