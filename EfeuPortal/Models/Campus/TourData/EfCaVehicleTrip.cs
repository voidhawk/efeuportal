﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.TourData
{
    public class EfCaVehicleTrip
    {
        public string VehicleIdent { get; set; }

        public string TourIdent { get; set; }

        public string TourExtid { get; set; }

        //TOUR
        public List<EfCaTourpointCoordinate> TourpointCoordinates { get; set; }

        //ROUTE

        public EfCaVehicleTrip()
        {

        }

        public EfCaVehicleTrip(EfCaPlannedTrip plannedTrip)
        {
            VehicleIdent = plannedTrip.PlannedTour.TourHeader.VehicleExtId1;

            TourIdent = plannedTrip.Ident;

            TourExtid = plannedTrip.PlannedTour.TourHeader.ExtId1;

            TourpointCoordinates = new List<EfCaTourpointCoordinate>();
            foreach (EfCaTourStop item in plannedTrip.PlannedTour.TourStops)
            {
                EfCaTourpointCoordinate coordinate = new EfCaTourpointCoordinate(item);
                TourpointCoordinates.Add(coordinate);
            }
        }
    }

    public class EfCaTourpointCoordinate
    {
        public int Index { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public EfCaTourpointCoordinate()
        {

        }

        public EfCaTourpointCoordinate(EfCaTourStop tourStop)
        {
            Index = tourStop.ExecutionSequence;
            Longitude = tourStop.Longitude;
            Latitude = tourStop.Latitude;
        }
    }

}
