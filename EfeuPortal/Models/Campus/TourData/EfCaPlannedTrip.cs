﻿using EfeuPortal.Models.Campus.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.TourData
{
    public class EfCaPlannedTrip
    {
        public string Ident { get; set; }

        /// <summary>
        /// PLANNED, IN_EXECUTION, DELETED
        /// </summary>
        public string State { get; set; }

        public string TourExtId { get; set; }

        public EfCaTour PlannedTour { get; set; }

        public EfCaVehicleTrip VehicleTrip { get; set; }

        public int? Version { get; set; }

        public EfCaPlannedTrip()
        {

        }

        public EfCaPlannedTrip(EfCaPlannedTripDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaPlannedTrip).GetProperties())
            {
                typeof(EfCaPlannedTrip).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
