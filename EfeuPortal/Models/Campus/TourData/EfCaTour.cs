﻿using PTVGROUP.ROWebApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.TourData
{
    public class EfCaTour
    {
        public EfCaTour(Tour item)
        {
            TourHeader = new EfCaTourHeader(item.TourHeader);
            TourStops = CreateContent(item);
        }

        public EfCaTourHeader TourHeader { get; private set; }
        public List<EfCaTourStop> TourStops { get; private set; }

        private List<EfCaTourStop> CreateContent(Tour item)
        {
            List<EfCaTourStop> tourStops = new List<EfCaTourStop>();
            foreach(TourStop tourStopItem in item.TourStops)
            {
                EfCaTourStop efCaTourStop = new EfCaTourStop(tourStopItem);
                tourStops.Add(efCaTourStop);
            }

            return tourStops;
        }
    }

    public class EfCaTourHeader
    {
        #region public parameters
        /// <summary>
        /// Gets or Sets ExtId1
        /// </summary>
        public string ExtId1 { get; set; }

        /// <summary>
        /// Gets or Sets StartDateTime
        /// </summary>
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Gets or Sets EndDateTime
        /// </summary>
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// Gets or Sets LatestTourStart
        /// </summary>
        public DateTime LatestTourStart { get; set; }

        /// <summary>
        /// Gets or Sets StartOfDepotAvailability
        /// </summary>
        public DateTime StartOfDepotAvailability { get; set; }

        /// <summary>
        /// Gets or Sets EndOfDepotAvailability
        /// </summary>
        public DateTime EndOfDepotAvailability { get; set; }

        /// <summary>
        /// Gets or Sets PrecombinedTourId
        /// </summary>
        public string PrecombinedTourId { get; set; }

        /// <summary>
        /// Gets or Sets CustomerTourExtId
        /// </summary>
        public string CustomerTourExtId { get; set; }

        /// <summary>
        /// Gets or Sets ReturnState
        /// </summary>
        public string ReturnState { get; set; }

        /// <summary>
        /// Gets or Sets OrderCount
        /// </summary>
        public int OrderCount { get; set; }

        /// <summary>
        /// Gets or Sets TourPointCount
        /// </summary>
        public int TourPointCount { get; set; }

        /// <summary>
        /// Gets or Sets Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Gets or Sets LocationFromToCity
        /// </summary>
        public string LocationFromToCity { get; set; }

        /// <summary>
        /// Gets or Sets VehicleExtId1
        /// </summary>
        public string VehicleExtId1 { get; set; }

        /// <summary>
        /// Gets or Sets VehicleLicensePlate
        /// </summary>
        public string VehicleLicensePlate { get; set; }

        /// <summary>
        /// Gets or Sets HaulierExtId1
        /// </summary>
        public string HaulierExtId1 { get; set; }

        /// <summary>
        /// Gets or Sets CodriverNeeded
        /// </summary>
        public bool CodriverNeeded { get; set; }

        /// <summary>
        /// Gets or Sets Earliness
        /// </summary>
        public int Earliness { get; set; }

        /// <summary>
        /// Gets or Sets TotalDistance
        /// </summary>
        public int TotalDistance { get; set; }

        /// <summary>
        /// Gets or Sets DriveDistance
        /// </summary>
        public int DriveDistance { get; set; }

        /// <summary>
        /// Gets or Sets EmptyDistance
        /// </summary>
        public int EmptyDistance { get; set; }

        /// <summary>
        /// Gets or Sets RouteDistance
        /// </summary>
        public int RouteDistance { get; set; }

        /// <summary>
        /// Gets or Sets ContinuationDistance
        /// </summary>
        public int ContinuationDistance { get; set; }

        /// <summary>
        /// Gets or Sets TotalDistanceNoReturnDepot
        /// </summary>
        public int TotalDistanceNoReturnDepot { get; set; }

        /// <summary>
        /// Gets or Sets TotalDuration
        /// </summary>
        public int TotalDuration { get; set; }

        /// <summary>
        /// Gets or Sets DriveDuration
        /// </summary>
        public int DriveDuration { get; set; }

        /// <summary>
        /// Gets or Sets EmptyDuration
        /// </summary>
        public int EmptyDuration { get; set; }

        /// <summary>
        /// Gets or Sets RouteDuration
        /// </summary>
        public int RouteDuration { get; set; }

        /// <summary>
        /// Gets or Sets ContinuationDuration
        /// </summary>
        public int ContinuationDuration { get; set; }

        /// <summary>
        /// Gets or Sets TotalDurationNoReturnDepot
        /// </summary>
        public int TotalDurationNoReturnDepot { get; set; }

        /// <summary>
        /// Gets or Sets TotalDrivingTime
        /// </summary>
        public int TotalDrivingTime { get; set; }

        /// <summary>
        /// Gets or Sets DrivingTimeFactor
        /// </summary>
        public double DrivingTimeFactor { get; set; }

        /// <summary>
        /// Gets or Sets TotalHandlingTime
        /// </summary>
        public int TotalHandlingTime { get; set; }

        /// <summary>
        /// Gets or Sets TotalIdleTime
        /// </summary>
        public int TotalIdleTime { get; set; }

        /// <summary>
        /// Gets or Sets TotalLoadingTime
        /// </summary>
        public int TotalLoadingTime { get; set; }

        /// <summary>
        /// Gets or Sets TotalUnloadingTime
        /// </summary>
        public int TotalUnloadingTime { get; set; }

        /// <summary>
        /// Gets or Sets TotalBreakTime
        /// </summary>
        public int TotalBreakTime { get; set; }

        /// <summary>
        /// Gets or Sets TotalRestTime
        /// </summary>
        public int TotalRestTime { get; set; }

        /// <summary>
        /// Gets or Sets TotalTurnaroundTime
        /// </summary>
        public int TotalTurnaroundTime { get; set; }

        /// <summary>
        /// Gets or Sets TourRestPeriod
        /// </summary>
        public int TourRestPeriod { get; set; }

        /// <summary>
        /// Gets or Sets WaitingPeriod
        /// </summary>
        public int WaitingPeriod { get; set; }

        /// <summary>
        /// Gets or Sets ShiftPotentialSeconds
        /// </summary>
        public int ShiftPotentialSeconds { get; set; }

        /// <summary>
        /// Gets or Sets MergedViolations
        /// </summary>
        public string MergedViolations { get; set; }

        /// <summary>
        /// Gets or Sets Weight
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// Gets or Sets Volume
        /// </summary>
        public double Volume { get; set; }

        /// <summary>
        /// Gets or Sets LoadingMeter
        /// </summary>
        public double LoadingMeter { get; set; }

        /// <summary>
        /// Gets or Sets Quantity1
        /// </summary>
        public double Quantity1 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity2
        /// </summary>
        public double Quantity2 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity3
        /// </summary>
        public double Quantity3 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity4
        /// </summary>
        public double Quantity4 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity5
        /// </summary>
        public double Quantity5 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity6
        /// </summary>
        public double Quantity6 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity7
        /// </summary>
        public double Quantity7 { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacityWeight
        /// </summary>
        public double RestCapacityWeight { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacityVolume
        /// </summary>
        public double RestCapacityVolume { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacityLoadingMeter
        /// </summary>
        public double RestCapacityLoadingMeter { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacity1
        /// </summary>
        public double RestCapacity1 { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacity2
        /// </summary>
        public double RestCapacity2 { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacity3
        /// </summary>
        public double RestCapacity3 { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacity4
        /// </summary>
        public double RestCapacity4 { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacity5
        /// </summary>
        public double RestCapacity5 { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacity6
        /// </summary>
        public double RestCapacity6 { get; set; }

        /// <summary>
        /// Gets or Sets RestCapacity7
        /// </summary>
        public double RestCapacity7 { get; set; }

        /// <summary>
        /// Gets or Sets SumWeight
        /// </summary>
        public double SumWeight { get; set; }

        /// <summary>
        /// Gets or Sets SumVolume
        /// </summary>
        public double SumVolume { get; set; }

        /// <summary>
        /// Gets or Sets SumLoadingMeter
        /// </summary>
        public double SumLoadingMeter { get; set; }

        /// <summary>
        /// Gets or Sets SumQuantity1
        /// </summary>
        public double SumQuantity1 { get; set; }

        /// <summary>
        /// Gets or Sets SumQuantity2
        /// </summary>
        public double SumQuantity2 { get; set; }

        /// <summary>
        /// Gets or Sets SumQuantity3
        /// </summary>
        public double SumQuantity3 { get; set; }

        /// <summary>
        /// Gets or Sets SumQuantity4
        /// </summary>
        public double SumQuantity4 { get; set; }

        /// <summary>
        /// Gets or Sets SumQuantity5
        /// </summary>
        public double SumQuantity5 { get; set; }

        /// <summary>
        /// Gets or Sets SumQuantity6
        /// </summary>
        public double SumQuantity6 { get; set; }

        /// <summary>
        /// Gets or Sets SumQuantity7
        /// </summary>
        public double SumQuantity7 { get; set; }

        /// <summary>
        /// Gets or Sets PickupWeight
        /// </summary>
        public double PickupWeight { get; set; }

        /// <summary>
        /// Gets or Sets PickupVolume
        /// </summary>
        public double PickupVolume { get; set; }

        /// <summary>
        /// Gets or Sets PickupLoadingMeter
        /// </summary>
        public double PickupLoadingMeter { get; set; }

        /// <summary>
        /// Gets or Sets PickupQuantity1
        /// </summary>
        public double PickupQuantity1 { get; set; }

        /// <summary>
        /// Gets or Sets PickupQuantity2
        /// </summary>
        public double PickupQuantity2 { get; set; }

        /// <summary>
        /// Gets or Sets PickupQuantity3
        /// </summary>
        public double PickupQuantity3 { get; set; }

        /// <summary>
        /// Gets or Sets PickupQuantity4
        /// </summary>
        public double PickupQuantity4 { get; set; }

        /// <summary>
        /// Gets or Sets PickupQuantity5
        /// </summary>
        public double PickupQuantity5 { get; set; }

        /// <summary>
        /// Gets or Sets PickupQuantity6
        /// </summary>
        public double PickupQuantity6 { get; set; }

        /// <summary>
        /// Gets or Sets PickupQuantity7
        /// </summary>
        public double PickupQuantity7 { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryWeight
        /// </summary>
        public double DeliveryWeight { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryVolume
        /// </summary>
        public double DeliveryVolume { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryLoadingMeter
        /// </summary>
        public double DeliveryLoadingMeter { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryQuantity1
        /// </summary>
        public double DeliveryQuantity1 { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryQuantity2
        /// </summary>
        public double DeliveryQuantity2 { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryQuantity3
        /// </summary>
        public double DeliveryQuantity3 { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryQuantity4
        /// </summary>
        public double DeliveryQuantity4 { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryQuantity5
        /// </summary>
        public double DeliveryQuantity5 { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryQuantity6
        /// </summary>
        public double DeliveryQuantity6 { get; set; }

        /// <summary>
        /// Gets or Sets DeliveryQuantity7
        /// </summary>
        public double DeliveryQuantity7 { get; set; }

        /// <summary>
        /// Gets or Sets PercentWeight
        /// </summary>
        public double PercentWeight { get; set; }

        /// <summary>
        /// Gets or Sets PercentVolume
        /// </summary>
        public double PercentVolume { get; set; }

        /// <summary>
        /// Gets or Sets PercentLoadingMeter
        /// </summary>
        public double PercentLoadingMeter { get; set; }

        /// <summary>
        /// Gets or Sets PercentQuantity1
        /// </summary>
        public double PercentQuantity1 { get; set; }

        /// <summary>
        /// Gets or Sets PercentQuantity2
        /// </summary>
        public double PercentQuantity2 { get; set; }

        /// <summary>
        /// Gets or Sets PercentQuantity3
        /// </summary>
        public double PercentQuantity3 { get; set; }

        /// <summary>
        /// Gets or Sets PercentQuantity4
        /// </summary>
        public double PercentQuantity4 { get; set; }

        /// <summary>
        /// Gets or Sets PercentQuantity5
        /// </summary>
        public double PercentQuantity5 { get; set; }

        /// <summary>
        /// Gets or Sets PercentQuantity6
        /// </summary>
        public double PercentQuantity6 { get; set; }

        /// <summary>
        /// Gets or Sets PercentQuantity7
        /// </summary>
        public double PercentQuantity7 { get; set; }

        /// <summary>
        /// Gets or Sets MinHeight
        /// </summary>
        public double MinHeight { get; set; }

        /// <summary>
        /// Gets or Sets MaxHeight
        /// </summary>
        public double MaxHeight { get; set; }

        /// <summary>
        /// Gets or Sets MinLength
        /// </summary>
        public double MinLength { get; set; }

        /// <summary>
        /// Gets or Sets MaxLength
        /// </summary>
        public double MaxLength { get; set; }

        /// <summary>
        /// Gets or Sets MinWidth
        /// </summary>
        public double MinWidth { get; set; }

        /// <summary>
        /// Gets or Sets MaxWidth
        /// </summary>
        public double MaxWidth { get; set; }

        /// <summary>
        /// Gets or Sets TaskfieldNames
        /// </summary>
        public string TaskfieldNames { get; set; }

        /// <summary>
        /// Gets or Sets TotalTourCosts
        /// </summary>
        public double TotalTourCosts { get; set; }

        /// <summary>
        /// Gets or Sets CostTotalNoReturnDepot
        /// </summary>
        public double CostTotalNoReturnDepot { get; set; }

        /// <summary>
        /// Gets or Sets TotalTollCosts
        /// </summary>
        public double TotalTollCosts { get; set; }

        /// <summary>
        /// Gets or Sets TotalTollDistance
        /// </summary>
        public int TotalTollDistance { get; set; }

        /// <summary>
        /// Gets or Sets TollCostsNoReturnDepot
        /// </summary>
        public double TollCostsNoReturnDepot { get; set; }

        /// <summary>
        /// Gets or Sets TollDistanceNoReturnDepot
        /// </summary>
        public int TollDistanceNoReturnDepot { get; set; }

        /// <summary>
        /// Gets or Sets TotalFreightCosts
        /// </summary>
        public double TotalFreightCosts { get; set; }

        /// <summary>
        /// Gets or Sets FreightCostsManual
        /// </summary>
        public double FreightCostsManual { get; set; }

        /// <summary>
        /// Gets or Sets FreightCostsCalculated
        /// </summary>
        public double FreightCostsCalculated { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionState
        /// </summary>
        public string ExecutionState { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionStateTime
        /// </summary>
        public DateTime ExecutionStateTime { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionTourStart
        /// </summary>
        public string ExecutionTourStart { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionTourEnd
        /// </summary>
        public string ExecutionTourEnd { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionTourDelay
        /// </summary>
        public int ExecutionTourDelay { get; set; }

        /// <summary>
        /// Gets or Sets TelematicsState
        /// </summary>
        public string TelematicsState { get; set; }

        /// <summary>
        /// Gets or Sets IsInExecution
        /// </summary>
        public bool IsInExecution { get; set; }

        /// <summary>
        /// Gets or Sets Num1
        /// </summary>
        public double Num1 { get; set; }

        /// <summary>
        /// Gets or Sets Num2
        /// </summary>
        public double Num2 { get; set; }

        /// <summary>
        /// Gets or Sets Num3
        /// </summary>
        public double Num3 { get; set; }

        /// <summary>
        /// Gets or Sets Num4
        /// </summary>
        public double Num4 { get; set; }

        /// <summary>
        /// Gets or Sets Num5
        /// </summary>
        public double Num5 { get; set; }

        /// <summary>
        /// Gets or Sets Num6
        /// </summary>
        public double Num6 { get; set; }

        /// <summary>
        /// Gets or Sets Num7
        /// </summary>
        public double Num7 { get; set; }

        /// <summary>
        /// Gets or Sets Num8
        /// </summary>
        public double Num8 { get; set; }

        /// <summary>
        /// Gets or Sets Num9
        /// </summary>
        public double Num9 { get; set; }

        /// <summary>
        /// Gets or Sets Num10
        /// </summary>
        public double Num10 { get; set; }

        /// <summary>
        /// Gets or Sets Text1
        /// </summary>
        public string Text1 { get; set; }

        /// <summary>
        /// Gets or Sets Text2
        /// </summary>
        public string Text2 { get; set; }

        /// <summary>
        /// Gets or Sets Text3
        /// </summary>
        public string Text3 { get; set; }

        /// <summary>
        /// Gets or Sets Text4
        /// </summary>
        public string Text4 { get; set; }

        /// <summary>
        /// Gets or Sets Text5
        /// </summary>
        public string Text5 { get; set; }

        /// <summary>
        /// Gets or Sets Text6
        /// </summary>
        public string Text6 { get; set; }

        /// <summary>
        /// Gets or Sets Text7
        /// </summary>
        public string Text7 { get; set; }

        /// <summary>
        /// Gets or Sets Text8
        /// </summary>
        public string Text8 { get; set; }

        /// <summary>
        /// Gets or Sets Text9
        /// </summary>
        public string Text9 { get; set; }

        /// <summary>
        /// Gets or Sets Text10
        /// </summary>
        public string Text10 { get; set; }

        /// <summary>
        /// Gets or Sets CreateUser
        /// </summary>
        public string CreateUser { get; set; }

        /// <summary>
        /// Gets or Sets ScemId
        /// </summary>
        public string ScemId { get; set; }

        /// <summary>
        /// Gets or Sets OnBoardUnitExtId1
        /// </summary>
        public string OnBoardUnitExtId1 { get; set; }
#endregion
        public EfCaTourHeader()
        {

        }

        public EfCaTourHeader(TourHeader tourHeader)
        {
            foreach (PropertyInfo property in typeof(TourHeader).GetProperties())
            {
                if (property.Name == "Ecotaxe" || property.Name == "EcotaxeDistance")
                {
                    //gibt es nicht, ignorieren
                }
                else
                {
                    typeof(EfCaTourHeader).GetProperty(property.Name).SetValue(this, property.GetValue(tourHeader));
                }
            }
        }
    }

    public class EfCaTourStop
    {
        #region public parameters
        /// <summary>
        /// Gets or Sets CoordinateType
        /// </summary>
        //public CoordinateTypeEnum? CoordinateType { get; set; }
        public string CoordinateType { get; set; }

        /// <summary>
        /// Gets or Sets ArrivalTime
        /// </summary>
        public DateTime ArrivalTime { get; set; }

        /// <summary>
        /// Gets or Sets DepartureTime
        /// </summary>
        public DateTime DepartureTime { get; set; }

        /// <summary>
        /// Gets or Sets StartServiceTime
        /// </summary>
        public DateTime StartServiceTime { get; set; }

        /// <summary>
        /// Gets or Sets EndServiceTime
        /// </summary>
        public DateTime EndServiceTime { get; set; }

        /// <summary>
        /// Gets or Sets Delay
        /// </summary>
        public int Delay { get; set; }

        /// <summary>
        /// Gets or Sets Earliness
        /// </summary>
        public int Earliness { get; set; }

        /// <summary>
        /// Gets or Sets BreakPeriodOnRoad
        /// </summary>
        public int BreakPeriodOnRoad { get; set; }

        /// <summary>
        /// Gets or Sets BreakPeriodAtStop
        /// </summary>
        public int BreakPeriodAtStop { get; set; }

        /// <summary>
        /// Gets or Sets RestPeriodOnRoad
        /// </summary>
        public int RestPeriodOnRoad { get; set; }

        /// <summary>
        /// Gets or Sets RestPeriodAtStop
        /// </summary>
        public int RestPeriodAtStop { get; set; }

        /// <summary>
        /// Gets or Sets DrivingPeriod
        /// </summary>
        public int DrivingPeriod { get; set; }

        /// <summary>
        /// Gets or Sets ServicePeriod
        /// </summary>
        public int ServicePeriod { get; set; }

        /// <summary>
        /// Gets or Sets WaitingPeriod
        /// </summary>
        public int WaitingPeriod { get; set; }

        /// <summary>
        /// Gets or Sets TourPointCosts
        /// </summary>
        public double TourPointCosts { get; set; }

        /// <summary>
        /// Gets or Sets TotalTollCosts
        /// </summary>
        public double TotalTollCosts { get; set; }

        /// <summary>
        /// Gets or Sets TotalTollDistance
        /// </summary>
        public int TotalTollDistance { get; set; }

        /// <summary>
        /// Gets or Sets RouteDistance
        /// </summary>
        public int RouteDistance { get; set; }

        /// <summary>
        /// Gets or Sets RouteDuration
        /// </summary>
        public int RouteDuration { get; set; }

        /// <summary>
        /// Gets or Sets StartDistance
        /// </summary>
        public int StartDistance { get; set; }

        /// <summary>
        /// Gets or Sets PredDistance
        /// </summary>
        public int PredDistance { get; set; }

        /// <summary>
        /// Gets or Sets CurrentWeight
        /// </summary>
        public double CurrentWeight { get; set; }

        /// <summary>
        /// Gets or Sets CurrentVolume
        /// </summary>
        public double CurrentVolume { get; set; }

        /// <summary>
        /// Gets or Sets CurrentLoadingMeter
        /// </summary>
        public double CurrentLoadingMeter { get; set; }

        /// <summary>
        /// Gets or Sets CurrentQuantity1
        /// </summary>
        public double CurrentQuantity1 { get; set; }

        /// <summary>
        /// Gets or Sets CurrentQuantity2
        /// </summary>
        public double CurrentQuantity2 { get; set; }

        /// <summary>
        /// Gets or Sets CurrentQuantity3
        /// </summary>
        public double CurrentQuantity3 { get; set; }

        /// <summary>
        /// Gets or Sets CurrentQuantity4
        /// </summary>
        public double CurrentQuantity4 { get; set; }

        /// <summary>
        /// Gets or Sets CurrentQuantity5
        /// </summary>
        public double CurrentQuantity5 { get; set; }

        /// <summary>
        /// Gets or Sets CurrentQuantity6
        /// </summary>
        public double CurrentQuantity6 { get; set; }

        /// <summary>
        /// Gets or Sets CurrentQuantity7
        /// </summary>
        public double CurrentQuantity7 { get; set; }

        /// <summary>
        /// Gets or Sets Num1
        /// </summary>
        public double Num1 { get; set; }

        /// <summary>
        /// Gets or Sets Num2
        /// </summary>
        public double Num2 { get; set; }

        /// <summary>
        /// Gets or Sets Num3
        /// </summary>
        public double Num3 { get; set; }

        /// <summary>
        /// Gets or Sets Num4
        /// </summary>
        public double Num4 { get; set; }

        /// <summary>
        /// Gets or Sets Num5
        /// </summary>
        public double Num5 { get; set; }

        /// <summary>
        /// Gets or Sets Num6
        /// </summary>
        public double Num6 { get; set; }

        /// <summary>
        /// Gets or Sets Num7
        /// </summary>
        public double Num7 { get; set; }

        /// <summary>
        /// Gets or Sets Num8
        /// </summary>
        public double Num8 { get; set; }

        /// <summary>
        /// Gets or Sets Num9
        /// </summary>
        public double Num9 { get; set; }

        /// <summary>
        /// Gets or Sets Num10
        /// </summary>
        public double Num10 { get; set; }

        /// <summary>
        /// Gets or Sets Text1
        /// </summary>
        public string Text1 { get; set; }

        /// <summary>
        /// Gets or Sets Text2
        /// </summary>
        public string Text2 { get; set; }

        /// <summary>
        /// Gets or Sets Text3
        /// </summary>
        public string Text3 { get; set; }

        /// <summary>
        /// Gets or Sets Text4
        /// </summary>
        public string Text4 { get; set; }

        /// <summary>
        /// Gets or Sets Text5
        /// </summary>
        public string Text5 { get; set; }

        /// <summary>
        /// Gets or Sets Text6
        /// </summary>
        public string Text6 { get; set; }

        /// <summary>
        /// Gets or Sets Text7
        /// </summary>
        public string Text7 { get; set; }

        /// <summary>
        /// Gets or Sets Text8
        /// </summary>
        public string Text8 { get; set; }

        /// <summary>
        /// Gets or Sets Text9
        /// </summary>
        public string Text9 { get; set; }

        /// <summary>
        /// Gets or Sets Text10
        /// </summary>
        public string Text10 { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or Sets State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or Sets Postcode
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// Gets or Sets City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or Sets District
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// Gets or Sets Street
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Gets or Sets HouseNo
        /// </summary>
        public string HouseNo { get; set; }

        /// <summary>
        /// Gets or Sets Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or Sets Longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionSequence
        /// </summary>
        public int ExecutionSequence { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionTourPointState
        /// </summary>
        public string ExecutionTourPointState { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionTourPointStateTime
        /// </summary>
        public DateTime ExecutionTourPointStateTime { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionArrivalTime
        /// </summary>
        public DateTime ExecutionArrivalTime { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionDepartureTime
        /// </summary>
        public DateTime ExecutionDepartureTime { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionArrivalTimeOnBoardUnit
        /// </summary>
        public DateTime ExecutionArrivalTimeOnBoardUnit { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionDepartureTimeOnBoardUnit
        /// </summary>
        public DateTime ExecutionDepartureTimeOnBoardUnit { get; set; }

        /// <summary>
        /// Gets or Sets ExecutionTourStopDelay
        /// </summary>
        public int ExecutionTourStopDelay { get; set; }

        /// <summary>
        /// Gets or Sets Province
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Gets or Sets ScemId
        /// </summary>
        public string ScemId { get; set; }

        /// <summary>
        /// Gets or Sets TourPointReference
        /// </summary>
        public string TourPointReference { get; set; }

        /// <summary>
        /// Gets or Sets TourActionPoints
        /// </summary>
        public List<EfCaTourActionPoint> TourActionPoints { get; set; }

        /// <summary>
        /// Gets or Sets TourEmissions
        /// </summary>
        public List<EfCaTourEmission> TourEmissions { get; set; }

        /// <summary>
        /// Gets or Sets TourTollCosts
        /// </summary>
        public List<EfCaTourTollCost> TourTollCosts { get; set; }
        #endregion

        public EfCaTourStop()
        {

        }

        public EfCaTourStop(TourStop tourStopItem)
        {
            foreach (PropertyInfo property in typeof(TourStop).GetProperties())
            {
                if (property.Name == "Ecotaxe" || property.Name == "EcotaxeDistance")
                {
                    //gibt es nicht, ignorieren
                }
                else if (property.Name == "CoordinateType")
                {
                    CoordinateType = property.GetValue(tourStopItem).ToString();
                }
                else if (property.Name == "TourActionPoints")
                {
                    List<TourActionPoint> orgTourActionPoints = (List<TourActionPoint>)property.GetValue(tourStopItem);
                    if (orgTourActionPoints != null && orgTourActionPoints.Count > 0)
                    {
                        TourActionPoints = new List<EfCaTourActionPoint>();
                        foreach (TourActionPoint item in orgTourActionPoints)
                        {
                            EfCaTourActionPoint efCaTourActionPoint = new EfCaTourActionPoint(item);
                            TourActionPoints.Add(efCaTourActionPoint);
                        }
                    }
                }
                else if (property.Name == "TourEmissions")
                {
                    List<TourEmission> orgTourEmissions = (List<TourEmission>)property.GetValue(tourStopItem);
                    if (orgTourEmissions != null && orgTourEmissions.Count > 0)
                    {
                        TourEmissions = new List<EfCaTourEmission>();
                        foreach (TourEmission item in orgTourEmissions)
                        {
                            EfCaTourEmission efCaTourEmission = new EfCaTourEmission(item);
                            TourEmissions.Add(efCaTourEmission);
                        }
                    }
                }
                else if (property.Name == "TourTollCosts")
                {
                    List<TourTollCost> orgTourTollCosts = (List<TourTollCost>)property.GetValue(tourStopItem);
                    if (orgTourTollCosts != null && orgTourTollCosts.Count > 0)
                    {
                        TourTollCosts = new List<EfCaTourTollCost>();
                        foreach (TourTollCost item in orgTourTollCosts)
                        {
                            EfCaTourTollCost efCaTourTollCost = new EfCaTourTollCost(item);
                            TourTollCosts.Add(efCaTourTollCost);
                        }
                    }
                }
                else
                {
                    typeof(EfCaTourStop).GetProperty(property.Name).SetValue(this, property.GetValue(tourStopItem));
                }
            }
        }
    }

    public class EfCaTourResource
    {
        #region public parameters
        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or Sets ExtId1
        /// </summary>
        public string ExtId1 { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets LicensePlate
        /// </summary>
        public string LicensePlate { get; set; }
        #endregion
        public EfCaTourResource()
        {

        }

        public EfCaTourResource(TourResource tourResource)
        {
            foreach (PropertyInfo property in typeof(TourResource).GetProperties())
            {
                typeof(EfCaTourResource).GetProperty(property.Name).SetValue(this, property.GetValue(tourResource));
            }
        }
    }

    public class EfCaTourEmission
    {
        #region public parameters
        /// <summary>
        /// Gets or Sets Ammonia
        /// </summary>
        public double Ammonia { get; set; }

        /// <summary>
        /// Gets or Sets Benzene
        /// </summary>
        public double Benzene { get; set; }

        /// <summary>
        /// Gets or Sets CarbonDioxideEquivalent
        /// </summary>
        public double CarbonDioxideEquivalent { get; set; }

        /// <summary>
        /// Gets or Sets CarbonDioxide
        /// </summary>
        public double CarbonDioxide { get; set; }

        /// <summary>
        /// Gets or Sets CarbonMonoxide
        /// </summary>
        public double CarbonMonoxide { get; set; }

        /// <summary>
        /// Gets or Sets FossilCarbonDioxide
        /// </summary>
        public double FossilCarbonDioxide { get; set; }

        /// <summary>
        /// Gets or Sets Fuel
        /// </summary>
        public double Fuel { get; set; }

        /// <summary>
        /// Gets or Sets Hydrocarbons
        /// </summary>
        public double Hydrocarbons { get; set; }

        /// <summary>
        /// Gets or Sets HydrocarbonsWithoutMethane
        /// </summary>
        public double HydrocarbonsWithoutMethane { get; set; }

        /// <summary>
        /// Gets or Sets Lead
        /// </summary>
        public double Lead { get; set; }

        /// <summary>
        /// Gets or Sets Methane
        /// </summary>
        public double Methane { get; set; }

        /// <summary>
        /// Gets or Sets NitrogenDioxide
        /// </summary>
        public double NitrogenDioxide { get; set; }

        /// <summary>
        /// Gets or Sets NitrogenOxides
        /// </summary>
        public double NitrogenOxides { get; set; }

        /// <summary>
        /// Gets or Sets NitrousOxide
        /// </summary>
        public double NitrousOxide { get; set; }

        /// <summary>
        /// Gets or Sets ParticleNumber
        /// </summary>
        public double ParticleNumber { get; set; }

        /// <summary>
        /// Gets or Sets Particles
        /// </summary>
        public double Particles { get; set; }

        /// <summary>
        /// Gets or Sets SulphurDioxide
        /// </summary>
        public double SulphurDioxide { get; set; }

        /// <summary>
        /// Gets or Sets Toluene
        /// </summary>
        public double Toluene { get; set; }

        /// <summary>
        /// Gets or Sets Xylene
        /// </summary>
        public double Xylene { get; set; }

        /// <summary>
        /// Gets or Sets CENCO2TankToWheel
        /// </summary>
        public double CENCO2TankToWheel { get; set; }

        /// <summary>
        /// Gets or Sets CENCO2WheelToTank
        /// </summary>
        public double CENCO2WheelToTank { get; set; }

        /// <summary>
        /// Gets or Sets CENCO2WheelToWheel
        /// </summary>
        public double CENCO2WheelToWheel { get; set; }

        /// <summary>
        /// Gets or Sets CENEnergyTankToWheel
        /// </summary>
        public double CENEnergyTankToWheel { get; set; }

        /// <summary>
        /// Gets or Sets CENEnergyWheelToTank
        /// </summary>
        public double CENEnergyWheelToTank { get; set; }

        /// <summary>
        /// Gets or Sets CENEnergyWheelToWheel
        /// </summary>
        public double CENEnergyWheelToWheel { get; set; }

        /// <summary>
        /// Gets or Sets FRDCREECO2TankToWheel
        /// </summary>
        public double FRDCREECO2TankToWheel { get; set; }

        /// <summary>
        /// Gets or Sets FRDCREECO2WheelToTank
        /// </summary>
        public double FRDCREECO2WheelToTank { get; set; }

        /// <summary>
        /// Gets or Sets FRDCREECO2WheelToWheel
        /// </summary>
        public double FRDCREECO2WheelToWheel { get; set; }

        /// <summary>
        /// Gets or Sets N2OTankToWheel
        /// </summary>
        public double N2OTankToWheel { get; set; }

        /// <summary>
        /// Gets or Sets CH4TankToWheel
        /// </summary>
        public double CH4TankToWheel { get; set; }
        #endregion

        public EfCaTourEmission()
        {

        }

        public EfCaTourEmission(TourEmission tourEmission)
        {
            foreach (PropertyInfo property in typeof(TourEmission).GetProperties())
            {
                typeof(EfCaTourEmission).GetProperty(property.Name).SetValue(this, property.GetValue(tourEmission));
            }
        }
    }

    public class EfCaTourTollCost
    {
        #region public parameters
        /// <summary>
        /// Gets or Sets TollCountry
        /// </summary>
        public string TollCountry { get; set; }

        /// <summary>
        /// Gets or Sets TollCostsCountry
        /// </summary>
        public double TollCostsCountry { get; set; }

        /// <summary>
        /// Gets or Sets TollDistanceCountry
        /// </summary>
        public int TollDistanceCountry { get; set; }

        /// <summary>
        /// Gets or Sets TourProviderId
        /// </summary>
        public int TourProviderId { get; set; }
        #endregion
        public EfCaTourTollCost()
        {

        }

        public EfCaTourTollCost(TourTollCost tourTollCost)
        {
            foreach (PropertyInfo property in typeof(TourTollCost).GetProperties())
            {
                typeof(EfCaTourTollCost).GetProperty(property.Name).SetValue(this, property.GetValue(tourTollCost));
            }
        }
    }

    public class EfCaTourActionPoint
    {
        #region public parameters
        public string CoordinateType { get; set; }

        /// <summary>
        /// Gets or Sets PosInTour
        /// </summary>
        public int PosInTour { get; set; }

        /// <summary>
        /// Gets or Sets OrderExtId1
        /// </summary>
        public string OrderExtId1 { get; set; }

        /// <summary>
        /// Gets or Sets ArrivalTime
        /// </summary>
        public DateTime ArrivalTime { get; set; }

        /// <summary>
        /// Gets or Sets StartServiceTime
        /// </summary>
        public DateTime StartServiceTime { get; set; }

        /// <summary>
        /// Gets or Sets EndServiceTime
        /// </summary>
        public DateTime EndServiceTime { get; set; }

        /// <summary>
        /// Gets or Sets OpeningHoursTolerance
        /// </summary>
        public int OpeningHoursTolerance { get; set; }

        /// <summary>
        /// Gets or Sets Delay
        /// </summary>
        public int Delay { get; set; }

        /// <summary>
        /// Gets or Sets DepartureTime
        /// </summary>
        public DateTime DepartureTime { get; set; }

        /// <summary>
        /// Gets or Sets Earliness
        /// </summary>
        public int Earliness { get; set; }

        /// <summary>
        /// Gets or Sets IdlePeriod
        /// </summary>
        public int IdlePeriod { get; set; }

        /// <summary>
        /// Gets or Sets ServicePeriod
        /// </summary>
        public int ServicePeriod { get; set; }

        /// <summary>
        /// Gets or Sets WaitingPeriod
        /// </summary>
        public int WaitingPeriod { get; set; }

        /// <summary>
        /// Gets or Sets Action
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Gets or Sets StartingTime
        /// </summary>
        public DateTime StartingTime { get; set; }

        /// <summary>
        /// Gets or Sets EndingTime
        /// </summary>
        public DateTime EndingTime { get; set; }

        /// <summary>
        /// Gets or Sets LocationExtId1
        /// </summary>
        public string LocationExtId1 { get; set; }

        /// <summary>
        /// Gets or Sets LocationExtId2
        /// </summary>
        public string LocationExtId2 { get; set; }

        /// <summary>
        /// Gets or Sets LocationName
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or Sets State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or Sets Postcode
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// Gets or Sets City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or Sets District
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// Gets or Sets Street
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Gets or Sets HouseNo
        /// </summary>
        public string HouseNo { get; set; }

        /// <summary>
        /// Gets or Sets Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or Sets Longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Gets or Sets GeocodingClassification
        /// </summary>
        public string GeocodingClassification { get; set; }

        /// <summary>
        /// Gets or Sets Weight
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// Gets or Sets Volume
        /// </summary>
        public double Volume { get; set; }

        /// <summary>
        /// Gets or Sets LoadingMeter
        /// </summary>
        public double LoadingMeter { get; set; }

        /// <summary>
        /// Gets or Sets Quantity1
        /// </summary>
        public double Quantity1 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity2
        /// </summary>
        public double Quantity2 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity3
        /// </summary>
        public double Quantity3 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity4
        /// </summary>
        public double Quantity4 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity5
        /// </summary>
        public double Quantity5 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity6
        /// </summary>
        public double Quantity6 { get; set; }

        /// <summary>
        /// Gets or Sets Quantity7
        /// </summary>
        public double Quantity7 { get; set; }

        /// <summary>
        /// Gets or Sets SplitAction
        /// </summary>
        public string SplitAction { get; set; }

        /// <summary>
        /// Gets or Sets SplitSequence
        /// </summary>
        public int SplitSequence { get; set; }

        /// <summary>
        /// Gets or Sets SplitAssortmentGroupExtId
        /// </summary>
        public string SplitAssortmentGroupExtId { get; set; }

        /// <summary>
        /// Gets or Sets SplitBaseOrderExtId
        /// </summary>
        public string SplitBaseOrderExtId { get; set; }

        /// <summary>
        /// Gets or Sets SplitInfo
        /// </summary>
        public string SplitInfo { get; set; }

        /// <summary>
        /// Gets or Sets SplitResult
        /// </summary>
        public string SplitResult { get; set; }

        /// <summary>
        /// Gets or Sets SplitRole
        /// </summary>
        public string SplitRole { get; set; }

        /// <summary>
        /// Gets or Sets NumberOfSplits
        /// </summary>
        public int NumberOfSplits { get; set; }

        /// <summary>
        /// Gets or Sets GroupedOrderExtId
        /// </summary>
        public string GroupedOrderExtId { get; set; }

        /// <summary>
        /// Gets or Sets GroupedOrderRole
        /// </summary>
        public string GroupedOrderRole { get; set; }

        /// <summary>
        /// Gets or Sets GroupedOrderPartCount
        /// </summary>
        public int GroupedOrderPartCount { get; set; }

        /// <summary>
        /// Gets or Sets GroupedOrderSequence
        /// </summary>
        public int GroupedOrderSequence { get; set; }

        /// <summary>
        /// Gets or Sets Num1
        /// </summary>
        public double Num1 { get; set; }

        /// <summary>
        /// Gets or Sets Num2
        /// </summary>
        public double Num2 { get; set; }

        /// <summary>
        /// Gets or Sets Num3
        /// </summary>
        public double Num3 { get; set; }

        /// <summary>
        /// Gets or Sets Num4
        /// </summary>
        public double Num4 { get; set; }

        /// <summary>
        /// Gets or Sets Num5
        /// </summary>
        public double Num5 { get; set; }

        /// <summary>
        /// Gets or Sets Num6
        /// </summary>
        public double Num6 { get; set; }

        /// <summary>
        /// Gets or Sets Num7
        /// </summary>
        public double Num7 { get; set; }

        /// <summary>
        /// Gets or Sets Num8
        /// </summary>
        public double Num8 { get; set; }

        /// <summary>
        /// Gets or Sets Num9
        /// </summary>
        public double Num9 { get; set; }

        /// <summary>
        /// Gets or Sets Num10
        /// </summary>
        public double Num10 { get; set; }

        /// <summary>
        /// Gets or Sets Text1
        /// </summary>
        public string Text1 { get; set; }

        /// <summary>
        /// Gets or Sets Text2
        /// </summary>
        public string Text2 { get; set; }

        /// <summary>
        /// Gets or Sets Text3
        /// </summary>
        public string Text3 { get; set; }

        /// <summary>
        /// Gets or Sets Text4
        /// </summary>
        public string Text4 { get; set; }

        /// <summary>
        /// Gets or Sets Text5
        /// </summary>
        public string Text5 { get; set; }

        /// <summary>
        /// Gets or Sets Text6
        /// </summary>
        public string Text6 { get; set; }

        /// <summary>
        /// Gets or Sets Text7
        /// </summary>
        public string Text7 { get; set; }

        /// <summary>
        /// Gets or Sets Text8
        /// </summary>
        public string Text8 { get; set; }

        /// <summary>
        /// Gets or Sets Text9
        /// </summary>
        public string Text9 { get; set; }

        /// <summary>
        /// Gets or Sets Text10
        /// </summary>
        public string Text10 { get; set; }

        /// <summary>
        /// Gets or Sets StableSequenceNumber
        /// </summary>
        public string StableSequenceNumber { get; set; }

        /// <summary>
        /// Gets or Sets OldStableSequenceNumber
        /// </summary>
        public string OldStableSequenceNumber { get; set; }

        /// <summary>
        /// Gets or Sets Province
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Gets or Sets LocationFunction
        /// </summary>
        public string LocationFunction { get; set; }
        #endregion

        public EfCaTourActionPoint()
        {

        }

        public EfCaTourActionPoint(TourActionPoint tourActionPoint)
        {
            foreach (PropertyInfo property in typeof(TourActionPoint).GetProperties())
            {
                if (property.Name == "CoordinateType")
                {
                    CoordinateType = property.GetValue(tourActionPoint).ToString();
                }
                else
                {
                    typeof(EfCaTourActionPoint).GetProperty(property.Name).SetValue(this, property.GetValue(tourActionPoint));
                }
            }
        }
    }
}
