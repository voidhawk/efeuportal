﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.TourData
{
    public class EfCaTourResp : Response
    {
        public List<EfCaPlannedTrip> PlannedTrips { get; set; }

        public List<EfCaVehicleTrip> VehicleTrips { get; set; }
    }
}
