﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus
{
    public class EfCaTimeSlot
    {
        /// <summary>
        /// Format 12:15
        /// </summary>
        public string Start { get; set; }

        /// <summary>
        /// Format 17:00
        /// </summary>
        public string End { get; set; }
    }

    public class EfCaDateTimeSlot
    {
        public string TimeZoneById { get; set; }

        private DateTimeOffset _Start;
        private DateTimeOffset _End;

        /// <summary>
        /// Format 
        /// </summary>
        public DateTimeOffset Start
        {
            get { return this._Start; }
            set
            {
                DateTimeOffset newValue = new DateTimeOffset(value.Year, value.Month, value.Day, value.Hour, value.Minute, 0, value.Offset);
                this._Start = newValue;
            }
        }

        /// <summary>
        /// Format 
        /// </summary>
        public DateTimeOffset End {
            get { return _End; } 
            set
            {
                DateTimeOffset newValue = new DateTimeOffset(value.Year, value.Month, value.Day, value.Hour, value.Minute, 0, value.Offset);
                this._End = newValue;
            }
        }
    }

    public class EfCaSlotsPerDay
    {
        public string TimeZoneById { get; set; }

        /// <summary>
        /// 0 = Sunday
        /// </summary>
        public int Day { get; set; }

        public EfCaTimeSlot TimeSlots { get; set; }
    }
}
