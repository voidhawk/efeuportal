﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.ChargingStation
{
    public class EfCaChargingStation
    {
        /// <summary>
        /// Unique identifier of the docking station.
        /// 
        /// Refers to "ID_Charging_Station"
        /// </summary>
        public string Ident { get; set; }

        /// <summary>
        /// No reference to the document
        /// </summary>
        public string Info { get; set; }

        /// <summary>
        /// No reference to the document
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// No reference to the document
        /// </summary>
        public double ChargingCapacity { get; set; }

        /// <summary>
        /// Supported Types BIG_VEHICLE_BOT, DEFAULT_VEHICLE_BOT, ...
        /// 
        /// No reference to the document
        /// </summary>
        public List<string> SupportedVehicleTypes { get; set; }

        /// <summary>
        /// Software Version der Dockingstation
        /// </summary>
        public string UnitVersionSW { get; set; }

        /// <summary>
        /// Hardware Version der Dockingstation
        /// </summary>
        public string UnitVersionHW { get; set; }

        /// <summary>
        /// Longitude
        /// 
        /// No reference to the document
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Latitude
        /// 
        /// No reference to the document
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// The Version ot the actually stored DockingStation. This parameter is used to secure updates triggered from different users.
        /// </summary>
        public int? Version { get; set; }

        public EfCaChargingStation()
        {
        }

        public EfCaChargingStation(EfCaChargingStationDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaChargingStation).GetProperties())
            {
                typeof(EfCaChargingStation).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
