﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.ChargingStation
{
    public class EfCaChargingStationResp : Response
    {
        public List<EfCaChargingStation> ChargingStations { get; set; }

        public EfCaChargingStationResp()
        {
            Error = false;
            ChargingStations = new List<EfCaChargingStation>();
        }
    }
}
