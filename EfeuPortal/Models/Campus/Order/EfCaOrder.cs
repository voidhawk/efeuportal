﻿using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Place;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace EfeuPortal.Models.Campus.Order
{
    public class EfCaOrder
    {
        public string Ident { get; set; }

        public DateTimeOffset? UserCreationTime { get; set; }

        public DateTimeOffset? UserUpdateTime { get; set; }
        
        /// <summary>
        /// Kennung des Paket Lieferanten (KEP) oder Entsorger
        /// </summary>
        public List<string> ProviderIds { get; set; }

        public string TrackingId { get; set; }

        /// <summary>
        /// Einen Parameter, der angibt, ob eine Order von einem Kunden erzeugt ist oder nicht. Bzw. besser ausgedrückt, 
        /// ob es sich um eine Abholung eines Pakets beim Kunden handelt oder eine Zustellung eines Pakets.
        /// </summary>
        public bool CustomerGeneratedOrder { get; set; }

        /// <summary>
        /// PACKAGE_DELIVERY, PACKAGE_PICKUP, WASTE_PICKUP
        /// Dieser Parameter gibt an zu welchem Prozesstyp der Auftrag aus Kundensicht gehört.
        /// </summary>
        public String CustomerProcess { get; set; }

        /// <summary>
        /// Dieser Parameter wird verwendet um einem Auftrag ein Fahrzeug zuzuteilen. Das wird z.B. fuer Ladeaufträge benoetigt.
        /// </summary>
        public string PreassignedVehicleId { get; set; }

        /// <summary>Flen
        /// AuftragsId z.B von Amazon
        /// </summary>
        public string ExternalOrderId { get; set; }

        /// <summary>
        /// DELIVERY, PICKUP, TRANSPORT, OTHERS (sonstige), 
        /// PERSONAL_PICKUP, PERSONAL_DELIVERY (Selbstabholung bzw. hinbringen)
        /// ELECTRICAL_CHARGE (elektrisches laden?)
        /// </summary>
        public string OrderType { get; set; }

        /// <summary>
        /// Transport (SYNCHRON oder ASYNCHRON)
        /// Personal Pick-Up(nur für
        /// Personal Delivery
        /// </summary>
        public string OrderMode { get; set; }

        /// <summary>
        /// WASTE (Wertstoff)
        /// WASTE_BOX
        /// PACKAGE (Paket)
        /// PACKAGE_BOX (kann die Sammelaufträge enthalten)
        /// OTHER (???) Dummy Auftrag
        /// </summary>
        public string OrderUnit { get; set; }

        /// <summary>
        /// SINGLE=false, MULTIPLE=true
        /// </summary>
        public bool? OrderAggregation { get; set; }

        /// <summary>
        /// Darf das Paket gemeinsam mit anderen versendet werden
        /// Anforderung von FZI 
        /// </summary>
        public bool OrderAggregationAllowed { get; set; }

        /// <summary>
        /// If OrderAggregation= true (MULTIPLE), this List contains the contained package orders
        /// 
        /// Sammelauftragsbildung
        /// </summary>
        public List<string> CollectedOrderIds { get; set; }

        /// <summary>
        /// Finden des Box Auftrages, welches Paket ist in welchem Boxauftrag?
        /// Welches Paket wird mit welchem Box Auftrag geliefert?
        /// </summary>
        public List<string> RelatedBoxOrderIds { get; set; }

        /// <summary>
        /// Nur BoxOrders können Main sein
        /// </summary>
        public bool? MainOrder { get; set; }

        /// <summary>
        /// Vorgänger oder Nachfolger Box Auftrag, immer gesetzt???
        /// </summary>
        public string LinkedBoxOrderId { get; set; }
        //public List<EfCaOrder> LinkedBoxOrderIds { get; set; }

        /// <summary>
        /// Keine Bewegungsdaten
        /// 
        ///  NEW: Auftrag wurde von der "Automatischen Übergabe Stelle angelegt"
        ///  READY: Depotmitarbeiter hat eingelagert
        ///  IN_PROCESS: ProcessManagement arbeitet daran
        ///  FINISHED, 
        ///  
        /// eventuell einen INVALID (ev. noch unvollständig) , DISRUPTION
        /// JSt: ToDo: 28.05.2020 In Abstimmung mit Anna fällt dieser Status später weg. Es wir beim
        /// anlegen ein Aufruf in die "StatusDB" gemacht und dort dann der Status verwaltet
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Die Bilder sind in eine eigene Klasse ausgelagert, damit man noch eine Beschreibung zum Bild hinzufügen kann.
        /// 
        /// z.B. 
        /// "Schaden an der linken Ecke"
        /// "Empfänger nicht lesbar"
        /// <para>https://lsogit.fzi.de/efeu/efeuportal/-/issues/43</para>
        /// </summary>
        public List<string> ImageIds { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// <para>undefind=0</para>
        /// <para>InBound=1</para>
        /// <para>Outbound=2</para>
        /// <para>Internal=3</para>
        /// </summary>
        public int PackageMode { get; set; } 

        /// <summary>
        /// GLASS, GROUP
        /// </summary>
        public List<SpecialCode> SpecialCodes { get; set; }

        public EfCaStorage Pickup { get; set; }

        public EfCaStorage Delivery { get; set; }

        public EfCaQuantities Quantities { get; set; }

        public EfCaDateTimeSlot OrderTimeSlot { get; set; }

        public List<EfCaDateTimeSlot> PickupTimeSlots { get; set; }

        public List<EfCaDateTimeSlot> DeliveryTimeSlots { get; set; }

        /// <summary>
        /// Externe Adresse des Empfängers im Outbound Fall
        /// </summary>
        public EfCaAddress ExternalRecipientAddress { get; set; }

        public List<EfCaScheduledBoxMountingSlot> MountDeviceSlots { get; set; }

        /// <summary>
        /// https://lsogit.fzi.de/efeu/efeuportal/-/issues/17
        /// </summary>
        public int DeliveryAttempts { get; set; }

        /// <summary>
        /// Pakete, die zu groß sind, können nur selbst abgeholt werden.
        /// <para> https://lsogit.fzi.de/efeu/efeuportal/-/issues/54 </para>
        /// </summary>
        public bool? OnlySelfService {get; set; }


        public EfCaOrderEvaluation OrderEvaluation {get; set; }

        public int? Version { get; set; }

        /// <summary>
        /// Default Constructor, es wird nur ein leeres Objekt angelegt.
        /// </summary>
        public EfCaOrder()
        {
            ///*
            //* https://lsogit.fzi.de/efeu/efeuportal/-/issues/47
            //*/
            //SpecialCodes = new List<SpecialCode>();
            //SpecialCodes.Add(new SpecialCode() { Key = "PRIORITY", Value = "0" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="orderType"></param>
        /// <param name="orderUnit"></param>
        /// <param name="state"></param>
        /// <param name="mainOrder"></param>
        public EfCaOrder(EfCaOrder item, string orderType, string orderUnit, string state, bool mainOrder)
        {
            foreach (PropertyInfo property in typeof(EfCaOrder).GetProperties())
            {
                if(orderType == "PICKUP")
                {
                    if (property.Name == "Pickup")
                    {
                        typeof(EfCaOrder).GetProperty("Delivery").SetValue(this, property.GetValue(item));
                    }
                    else if (property.Name == "Delivery")
                    {
                        typeof(EfCaOrder).GetProperty("Pickup").SetValue(this, property.GetValue(item));
                    }
                }
                typeof(EfCaOrder).GetProperty(property.Name).SetValue(this, property.GetValue(item));
            }

            Ident = Guid.NewGuid().ToString();
            OrderType = orderType;
            OrderUnit = orderUnit;
            State = state;
            MainOrder = mainOrder;

            ///*
            // * https://lsogit.fzi.de/efeu/efeuportal/-/issues/47
            // */
            //if (SpecialCodes == null)
            //{
            //    SpecialCodes = new List<SpecialCode>();
            //}
            //SpecialCode priorityFound = SpecialCodes.Find(x => x.Key.Equals("PRIORITY"));
            //if(priorityFound == null)
            //{
            //    SpecialCodes.Add(new SpecialCode() { Key = "PRIORITY", Value = "0" });
            //}
        }

        public EfCaOrder(EfCaOrderDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaOrder).GetProperties())
            {
                typeof(EfCaOrder).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
            UserCreationTime = itemDB.CreationTime;
            UserUpdateTime = itemDB.UpdateTime;
        }
    }

    public class EfCaStorage
    {
        /// <summary>
        /// z.B. Lagerplatz im Depot, Regal
        /// </summary>
        public string StorageInfo { get; set; }

        /// <summary>
        /// Zeit in Sekunden
        /// </summary>
        public int ServiceTime { get; set; }

        public EfCaStorageType StorageType { get; set; }

        /// <summary>
        /// DEPRICATED use List of EfCaOrderDependentIds OrderDependentIds
        /// </summary>

        public EfCaConnectionIds StorageIds { get; set; }

        public List<EfCaOrderDependentIds> OrderDependentIds { get; set; }
    }

    /// <summary>
    /// Contains building, address or contact id(s)
    /// </summary>
    public class EfCaConnectionIds
    {
        public string AddressId { get; set; }

        public string ApartmentId { get; set; }

        public string BuildingId { get; set; }

        public string ContactId { get; set; }

        public string SyncMeetingPointId { get; set; }

        public List<string> BoxMountingDeviceIds { get; set; }

        public string ChargingStationId { get; set; }
    }

    public class EfCaOrderDependentIds
    {
        /// <summary>
        ///  AddressId, ApartmentId, BuildingId, 
        ///  ContactId, SyncMeetingPointId, BoxMountingDeviceIds, 
        ///  ChargingStationId, TransportBoxId
        ///  
        /// JSt: ToDo: Durch einen Type ersetzen
        /// </summary>
        public string Key { get; set; }
        public string Ident { get; set; }

    }

    public class EfCaQuantities
    {
        /// <summary>
        /// Unit cm
        /// </summary>
        public double Length { get; set; }

        /// <summary>
        /// Unit cm
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// Unit cm
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// Unit gr
        /// </summary>
        public double Weight { get; set; }

        public double GetCalculatedData(UnitTypes unit)
        {
            if (unit.Equals(UnitTypes.CBM))
            {
                return (Width * Height * Length) / (Math.Pow(1000, 3)) ;
            }
            else if (unit.Equals(UnitTypes.KG))
            {
                return Weight / 1000;
            }

            return -1;
        }
    }

    /// <summary>
    /// DEPOT, BOCK
    /// 
    ///  Wo befindet sich das Paket / Auftrag? 
    ///   Übergabebock
    ///   Regalplatz im Lager
    /// </summary>
    public class EfCaStorageType
    {
        /// <summary>
        /// DEPOT, TransportBoxType, 
        /// </summary>
        public string Storage { get; set; }

        public string StorageId { get; set; }
    }

    /// <summary>
    /// <para>Key: GROUPING (Sammelauftrag) used PTV, PRIORITY, HAZARDOROUS_GOODS</para>
    /// <para>Value: GROUPING (UUID), Battery</para>
    /// 
    /// </summary>
    public class SpecialCode
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }

    public enum ImportActionType
    {
        UNDEFINED,
        NEW,
        UPDATE,
        DELETE
    }

    public enum ObjectTypes
    {
        UNDEFINED,
        ADDRESS,
        APARTMENT,
        BUILDING,
        CONTACT,
        SYNCMEETINGPOINT,
        BOXMOUNTINGDEVICE,
        CHARGINGSTATION,
        TRANSPORTBOX
    }

    public enum UnitTypes
    {
        UNDEFINED = 0,

        /// <summary>
        /// Millimeter
        /// </summary>
        MM = 1,
        CM = 2,
        DM = 3,

        /// <summary>
        /// Kilometer
        /// </summary>
        KM = 4,

        /// <summary>
        /// Kubikmeter
        /// </summary>
        CBM = 20,

        /// <summary>
        /// Kilogramm
        /// </summary>
        KG = 30
    }
}
