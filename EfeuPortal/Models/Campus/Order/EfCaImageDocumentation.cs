﻿using EfeuPortal.Models.Campus.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Order
{
    public class EfCaImageDocumentation
    {
        public string Ident { get; set; }

        public string OrderId { get; set; }

        public string Description { get; set; }

        public byte[] img_byte_arr { get; set; }

        public int? Version { get; set; }

        /// <summary>
        /// This parameter is mapped to the UpdateTime in the database.
        /// <para>https://lsogit.fzi.de/efeu/efeuportal/-/issues/54</para>
        /// </summary>
        public DateTimeOffset? LastUpdate { get; set; }

        public EfCaImageDocumentation()
        {

        }

        public EfCaImageDocumentation(EfCaImageDocumentationDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaImageDocumentation).GetProperties())
            {
                typeof(EfCaImageDocumentation).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;

            LastUpdate = itemDB.UpdateTime;
        }
    }
}
