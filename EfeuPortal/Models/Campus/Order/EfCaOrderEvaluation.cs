using EfeuPortal.Models.Campus.MongoDB;
using EfeuPortal.Models.Campus.Mount;
using EfeuPortal.Models.Campus.Place;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace EfeuPortal.Models.Campus.Order
{
    public class EfCaOrderEvaluation
    {
        public string Ident { get; set; }

        public string OrderIdent { get; set; }

        // Kann gesetzt werden für Box-Aufträge
        public List<EfCaDateTimeSlot> PlannedTimeSlots { get; set; }

        // Kann gesetzt werden für Box-Aufträge
        public List<EfCaDateTimeSlot> ShiftedTimeSlots { get; set; }
        
        // Kann gesetzt werden für Box-Aufträge
        public List<String> ReasonForUnplanning { get; set; }

        // Kann gesetzt werden für Box-Aufträge
        public DateTimeOffset? DeliveryTimeActual { get; set; }

        // Kann gesetzt werden für Package-Aufträge und für Box-Aufträge
        public DateTimeOffset? StartOfTourForOrder { get; set; }

        // Kann gesetzt werden für Package-Aufträge und für Box-Aufträge
        public DateTimeOffset? CustomerActionFinished { get; set; }

        // Kann gesetzt werden für Box-Aufträge
        public bool? OrderOnTimeWithinFirstDeliverySlot { get; set; }


    }
}