﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.Campus.Order
{
    public class EfCaOrderResp : Response
    {
        public List<EfCaOrder> Orders { get; set; }

        /// <summary>
        /// Zum hinzufügen von Inhalten ausschlieslich "AddImageDocumentation" verwenden!
        /// </summary>
        public List<EfCaImageDocumentation> ImageDocumentations { get { return Images; } }

        private List<EfCaImageDocumentation> Images { get; set; }

        public EfCaOrderResp()
        {
            Error = false;
            Orders = new List<EfCaOrder>();
        }

        public void AddImageDocumentation(List<EfCaImageDocumentation> images)
        {
            if(images == null)
            {
                return;
            }
            if (Images == null)
            {
                Images = new List<EfCaImageDocumentation>();
            }
            Images.AddRange(images);
        }

        public void AddImageDocumentation(EfCaImageDocumentation image)
        {
            if (image == null)
            {
                return;
            }
            if (Images == null)
            {
                Images = new List<EfCaImageDocumentation>();
            }
            Images.Add(image);
        }
    }
}
