﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaRight
    {
        /// <summary>
        /// As example ADDRESS
        /// </summary>
        public string Section { get; set; }

        /// <summary>
        /// Methoden
        /// - PostAddAddresses
        /// - FindAddressesByFinder
        /// - PutAddress
        /// - DeleteAddress
        /// </summary>
        public List<string> Functions { get; set; }
    }
}
