using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaResetPasswordRequest
    {
        public string Email { get; set; }

        public long DateTime { get; set; }

        public int? Version { get; set; }
    }
}
