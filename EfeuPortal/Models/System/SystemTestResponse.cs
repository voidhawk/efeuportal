﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class SystemTestResponse : Response
    {
        public int ExecutedTests { get; set; }
    }
}
