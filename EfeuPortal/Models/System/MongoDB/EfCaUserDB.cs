﻿using EfeuPortal.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System.MongoDB
{
    public class EfCaUserDB : EfCaUser
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        //[BsonRepresentation(BsonType.String)]
        [BsonElement("creationTime")]
        public DateTimeOffset CreationTime { get; set; }

        //[BsonRepresentation(BsonType.String)]
        [BsonElement("updateTime")]
        public DateTimeOffset UpdateTime { get; set; }

        [BsonElement("userId")]
        public string UserId { get; set; }

        [BsonElement("tenantId")]
        public string TenantId { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("__v")]
        public int CurrentVersion { get; set; }

        public EfCaUserDB(InternalUserInfos internalUserInfo, EfCaUser item)
        {
            CreationTime = DateTimeOffset.Now;
            UpdateTime = DateTimeOffset.Now;
            CurrentVersion = item.Version.GetValueOrDefault(1);
            UserId = item.Ident;
            //TenantId = "0e8110b9-f451-4d93-ba8c-de87a52e900c";
            TenantId = internalUserInfo.TenantId;

            foreach (PropertyInfo property in typeof(EfCaUser).GetProperties())
            {
                if (!property.Name.Equals("Version"))
                {
                    typeof(EfCaUserDB).GetProperty(property.Name).SetValue(this, property.GetValue(item));
                }
            }

            Version = null;
            Ident = null;
        }

        public void Update(EfCaUser item)
        {
            if (CurrentVersion != item.Version.GetValueOrDefault(-1))
            {
                throw new Exception($"Version outdated, Current({CurrentVersion}), UpdateVersion({item.Version})");
            }
            UpdateTime = DateTimeOffset.Now;
            CurrentVersion++;

            foreach (PropertyInfo property in typeof(EfCaUser).GetProperties())
            {
                if (!property.Name.Equals("Version"))
                {
                    typeof(EfCaUserDB).GetProperty(property.Name).SetValue(this, property.GetValue(item));
                }
            }

            Version = null;
            Ident = null;
        }

        public void Update(EfCaUser item, string password)
        {
            if (CurrentVersion != item.Version.GetValueOrDefault(-1))
            {
                throw new Exception($"Version outdated, Current({CurrentVersion}), UpdateVersion({item.Version})");
            }
            UpdateTime = DateTimeOffset.Now;
            CurrentVersion++;

            Password = password;

            Version = null;
            Ident = null;
        }
    }
}
