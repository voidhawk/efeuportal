﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System.MongoDB
{
    public class EfCaSystemDB : EfCaSystem
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        //[BsonRepresentation(BsonType.String)]
        [BsonElement("creationTime")]
        public DateTimeOffset CreationTime { get; set; }

        //[BsonRepresentation(BsonType.String)]
        [BsonElement("updateTime")]
        public DateTimeOffset UpdateTime { get; set; }

        [BsonElement("__v")]
        public int CurrentVersion { get; set; }

        public EfCaSystemDB(EfCaSystem item)
        {
            CreationTime = DateTimeOffset.Now;
            UpdateTime = DateTimeOffset.Now;
            CurrentVersion = item.Version.GetValueOrDefault(1);

            foreach (PropertyInfo property in typeof(EfCaSystem).GetProperties())
            {
                if (!property.Name.Equals("Version"))
                {
                    typeof(EfCaSystemDB).GetProperty(property.Name).SetValue(this, property.GetValue(item));
                }
            }

            Version = null;
        }

        public void Update(EfCaSystem item)
        {
            if (CurrentVersion != item.Version.GetValueOrDefault(-1))
            {
                throw new Exception($"Version outdated, Current({CurrentVersion}), UpdateVersion({item.Version})");
            }
            UpdateTime = DateTimeOffset.Now;
            CurrentVersion++;

            foreach (PropertyInfo property in typeof(EfCaSystem).GetProperties())
            {
                if (!property.Name.Equals("Version"))
                {
                    typeof(EfCaSystemDB).GetProperty(property.Name).SetValue(this, property.GetValue(item));
                }
            }

            Version = null;
        }
    }
}
