﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class LoginResp : Response
    {
        public EfCaUser User { get; set; }

        public string TenantId { get; set; }
    }
}
