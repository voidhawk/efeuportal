﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaRegisterResp : Response
    {
        public EfCaUser User { get; set; }

        public string FirstLoginPassword { get; set; }
    }
}
