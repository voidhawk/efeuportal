﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaUserResp : Response
    {
        public List<EfCaUser> Users { get; set; }

        public string FirstLoginPassword { get; set; }

        public EfCaUserResp()
        {
            Error = false;
            Users = new List<EfCaUser>();
        }
    }
}
