﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaLoginResp : Response
    {
        public EfCaUser User { get; set; }

        public string Token { get; set; }
    }
}
