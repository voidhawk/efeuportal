﻿using EfeuPortal.Models.System.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    /// <summary>
    /// Der efeuCampus User (EfCaUser) ist der Anwender des Systems, also derjenige der die Software bedienen kann und darf.
    /// 
    /// Jedem EfCaUser ist eine Rolle und ein Contact (EfCaContact => Dieser Contact ist der Vertragsinhaber) zugeordnet.
    /// </summary>
    public class EfCaUser
    {
        public string Ident { get; set; }

        public string Email { get; set; }

        public string ContactId { get; set; }

        public string Role { get; set; }

        public string Notice { get; set; }

        public List<EfCaConfig> Configs { get; set; }
        
        public int? Version { get; set; }

        public EfCaUser()
        {

        }

        public EfCaUser(EfCaUserDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaUser).GetProperties())
            {
                typeof(EfCaUser).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.UserId;
        }
    }
}
