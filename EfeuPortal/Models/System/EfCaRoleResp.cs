﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    /// <summary>
    /// The default constructor creates the <![CDATA[List<EfCaRole> ]]> Roles
    /// </summary>
    public class EfCaRoleResp : Response
    {
        public List<EfCaRole> Roles { get; set; }

        public EfCaRoleResp()
        {
            Error = false;
            Roles = new List<EfCaRole>();
        }
    }
}
