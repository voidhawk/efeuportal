﻿using EfeuPortal.Models.Campus.XServer2;
using EfeuPortal.Models.System.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaConfig
    {
        public string Ident { get; set; }

        public string Key { get; set; }

        /// <summary>
        /// URL, STRING_VALUE, TABLE_CONFIGURATION, XSERVER2_CONFIG
        /// </summary>
        public string Type { get; set; }

        public string StringValue { get; set; }

        public string Url { get; set; }

        public EfCaXServerConfig XServerConfig { get; set; }

        public int? Version { get; set; }

        public EfCaConfig()
        {

        }

        public EfCaConfig(EfCaConfigDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaConfig).GetProperties())
            {
                typeof(EfCaConfig).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
