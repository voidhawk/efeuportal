﻿using EfeuPortal.Models.System.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaSystem
    {
        public int SchemaVersion { get; set; }

        public List<ProviderUnspecificData> TenantUnspecificData { get; set; }
    
        public int? Version { get; set; }

        public EfCaSystem()
        {

        }

        public EfCaSystem(EfCaSystemDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaSystem).GetProperties())
            {
                typeof(EfCaSystem).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
        }
    }

    public class ProviderUnspecificData
    {
        public string Key { get; set; }

        public object Data { get; set; }

        public List<SmartourQuality> SmartourQualities { get; set; }
    }

    public class SmartourQuality
    {
        public string Name { get; set; }

        public int Number { get; set; }
    }
}
