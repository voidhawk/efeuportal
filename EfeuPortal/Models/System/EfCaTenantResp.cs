﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaTenantResp : Response
    {
        public List<EfCaTenant> Tenants { get; set; }

        public EfCaUser TenantAdminUser { get; set; }

        public string FirstLoginPassword { get; set; }

        public EfCaTenantResp()
        {
            Error = false;
            Tenants = new List<EfCaTenant>();
        }
    }
}

