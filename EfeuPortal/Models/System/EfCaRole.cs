﻿using EfeuPortal.Models.System.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    /// <summary>
    /// Anwender Funktionalität wird als Rolle bezeichnet. Es wird z.B. die Rolle
    ///  - SystemManager
    ///  - User
    ///  - Dispatching
    ///  - ...
    /// geben.
    /// 
    /// Jede Rolle beeinhaltet Rechte (Gruppen der API z.B. Address) und jedes Recht hat 
    /// für jede Methode eine Funktion
    /// </summary>
    public class EfCaRole
    {
        public string Ident { get; set; }

        public string TenantId { get; set; }

        public string Description { get; set; }

        public List<EfCaRight> Rights { get; set; }

        public List<EfCaConfig> Configs { get; set; }

        public int? Version { get; set; }

        public EfCaRole()
        {

        }

        public EfCaRole(EfCaRoleDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaRole).GetProperties())
            {
                typeof(EfCaRole).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            TenantId = itemDB.ExtIdDetails[0].Originator;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
