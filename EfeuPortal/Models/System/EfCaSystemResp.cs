﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaSystemResp : Response
    {
        public List<EfCaConfig> Configs { get; set; }

        public EfCaSystemResp()
        {
            Error = false;
            //Configs = new List<EfCaConfig>();
        }
    }
}
