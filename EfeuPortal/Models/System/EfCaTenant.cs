﻿using EfeuPortal.Models.System.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EfeuPortal.Models.System
{
    public class EfCaTenant
    {
        public string Ident { get; set; }

        public string EMail { get; set; }

        public string Description { get; set; }

        public List<EfCaConfig> Configs { get; set; }

        public int? Version { get; set; }

        public EfCaTenant()
        {

        }

        public EfCaTenant(EfCaTenantDB itemDB)
        {
            foreach (PropertyInfo property in typeof(EfCaTenant).GetProperties())
            {
                typeof(EfCaTenant).GetProperty(property.Name).SetValue(this, property.GetValue(itemDB));
            }
            Version = itemDB.CurrentVersion;
            Ident = itemDB.ExtIdDetails[0].ExtId;
        }
    }
}
