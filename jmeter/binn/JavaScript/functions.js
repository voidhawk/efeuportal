function generateRandomIdent(length) {

	var text = "";
    //var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for( var i=0; i < length; i++ ) {
		
        text += possible.charAt(Math.floor(Math.random() * possible.length));
	}

    return text;
}

function selectRandomElement(array) {

	var arrayLength = array.length;
	var randomElement = Math.floor(Math.random() * array.length);
	//log.info("selectRandomElement(..).array.length=" + array.length);
	//log.info("selectRandomElement(..).index=" + randomElement);
	
    return array[randomElement];
}

function findApartmentOfContact(apartments, contactId) {
	
	var arrayLength = apartments.length;
	for (var apartmentCounter = 0; apartmentCounter < apartments.length -1; apartmentCounter++) {
		
		if(apartments[apartmentCounter].contactIds != null && apartments[apartmentCounter].contactIds.length > 0) {
			
			for (var contactCounter = 0; contactCounter < apartments[apartmentCounter].contactIds.length; contactCounter++) {
				
				if(apartments[apartmentCounter].contactIds[contactCounter] === contactId) {
					return apartments[apartmentCounter];
				}
			}		
		}		
	}
	return null;	
}

function findBuildingOfAddress(buildings, addressId) {
	
	var arrayLength = buildings.length;
	for (var buildingsCounter = 0; buildingsCounter < buildings.length -1; buildingsCounter++) {
		
		if(buildings[buildingsCounter].addressId === addressId) {
			return buildings[buildingsCounter];
		}
	}
	
	return null;
}

function generateXIMRequest() {

	var request = {
		"routingOptions": {
			"wrappedExcludedOperatorCodes": [],
			"wrappedExcludedTerminalCodes": [],
			"wrappedExcludedTransportModeCodes": [],
			"accompanied": false,
			"maxCosts": 30000,
			"maxDuration": 0,
			"maxTerminalDistance": 0,
			"maxTransfers": 0,
			"numberOfAlternatives": 5,
			"planningHorizon": 0,
			"startTime": "",
			"startTimeSpecified": true,
			"timeCostWeight": 75,
			"withWayList": false
		},
		"wrappedStopOffs": [
			{
				"point": { 
					"x": 0,
					"y": 0,
					"z": 0.0,
					"zSpecified": true
				},
				"city": "",
				"code": "",
				"country": "",
				"houseNumber": "",
				"name": "",
				"postalCode": "",
				"street": ""
			},
			{
				"point": {
					"x": 0,
					"y": 0,
					"z": 0.0,
					"zSpecified": true
				},
				"city": "",
				"code": "",
				"country": "",
				"houseNumber": "",
				"name": "",
				"postalCode": "",
				"street": ""
			}
		]	
	}

	return request;
}

function generateXIMRequest() {

	var request = {
		
	}
}
