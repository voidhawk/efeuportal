FROM mcr.microsoft.com/dotnet/core/aspnet:3.1

RUN mkdir -p /usr/src/app
RUN apt-get update
RUN apt-get install tzdata

COPY ./EfeuPortal/bin/Release/netcoreapp3.1/publish/ /usr/src/app/

WORKDIR /usr/src/app

ENV ASPNETCORE_URLS http://+:80;https://+:443
ENV TZ=Europe/Berlin

ENTRYPOINT ["dotnet", "EfeuPortal.dll"]